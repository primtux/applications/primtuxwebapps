gdjs.infosCode = {};
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDinfo_95951Objects1= [];
gdjs.infosCode.GDinfo_95951Objects2= [];
gdjs.infosCode.GDinfo_95952Objects1= [];
gdjs.infosCode.GDinfo_95952Objects2= [];
gdjs.infosCode.GDinfo_95953Objects1= [];
gdjs.infosCode.GDinfo_95953Objects2= [];
gdjs.infosCode.GDinfo_95954Objects1= [];
gdjs.infosCode.GDinfo_95954Objects2= [];
gdjs.infosCode.GDinfo_95955Objects1= [];
gdjs.infosCode.GDinfo_95955Objects2= [];
gdjs.infosCode.GDimage_9595divisioneuclidienneObjects1= [];
gdjs.infosCode.GDimage_9595divisioneuclidienneObjects2= [];
gdjs.infosCode.GDimage_9595jeulibreObjects1= [];
gdjs.infosCode.GDimage_9595jeulibreObjects2= [];
gdjs.infosCode.GDimage_9595jeu1Objects1= [];
gdjs.infosCode.GDimage_9595jeu1Objects2= [];
gdjs.infosCode.GDimage_9595jeu2Objects1= [];
gdjs.infosCode.GDimage_9595jeu2Objects2= [];
gdjs.infosCode.GDimage_9595scoreObjects1= [];
gdjs.infosCode.GDimage_9595scoreObjects2= [];
gdjs.infosCode.GDinfo_95956Objects1= [];
gdjs.infosCode.GDinfo_95956Objects2= [];
gdjs.infosCode.GDimage_9595aideObjects1= [];
gdjs.infosCode.GDimage_9595aideObjects2= [];
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDavatar_9595ver1Objects1= [];
gdjs.infosCode.GDavatar_9595ver1Objects2= [];
gdjs.infosCode.GDavatar_9595ver2Objects1= [];
gdjs.infosCode.GDavatar_9595ver2Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].getBehavior("Opacity").setOpacity(100);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDinfo_95951Objects1.length = 0;
gdjs.infosCode.GDinfo_95951Objects2.length = 0;
gdjs.infosCode.GDinfo_95952Objects1.length = 0;
gdjs.infosCode.GDinfo_95952Objects2.length = 0;
gdjs.infosCode.GDinfo_95953Objects1.length = 0;
gdjs.infosCode.GDinfo_95953Objects2.length = 0;
gdjs.infosCode.GDinfo_95954Objects1.length = 0;
gdjs.infosCode.GDinfo_95954Objects2.length = 0;
gdjs.infosCode.GDinfo_95955Objects1.length = 0;
gdjs.infosCode.GDinfo_95955Objects2.length = 0;
gdjs.infosCode.GDimage_9595divisioneuclidienneObjects1.length = 0;
gdjs.infosCode.GDimage_9595divisioneuclidienneObjects2.length = 0;
gdjs.infosCode.GDimage_9595jeulibreObjects1.length = 0;
gdjs.infosCode.GDimage_9595jeulibreObjects2.length = 0;
gdjs.infosCode.GDimage_9595jeu1Objects1.length = 0;
gdjs.infosCode.GDimage_9595jeu1Objects2.length = 0;
gdjs.infosCode.GDimage_9595jeu2Objects1.length = 0;
gdjs.infosCode.GDimage_9595jeu2Objects2.length = 0;
gdjs.infosCode.GDimage_9595scoreObjects1.length = 0;
gdjs.infosCode.GDimage_9595scoreObjects2.length = 0;
gdjs.infosCode.GDinfo_95956Objects1.length = 0;
gdjs.infosCode.GDinfo_95956Objects2.length = 0;
gdjs.infosCode.GDimage_9595aideObjects1.length = 0;
gdjs.infosCode.GDimage_9595aideObjects2.length = 0;
gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDavatar_9595ver1Objects1.length = 0;
gdjs.infosCode.GDavatar_9595ver1Objects2.length = 0;
gdjs.infosCode.GDavatar_9595ver2Objects1.length = 0;
gdjs.infosCode.GDavatar_9595ver2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
