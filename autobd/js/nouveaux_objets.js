// Création d'une nouvelle onomatopée
function onomatopee(mode){
    if (clicked){declique('clic',clicked);}
    let scrollBD = bd.scrollTop;
    let nouvelle_onomatopee = document.createElement('div');
    nouvelle_onomatopee.classList.add('draggable','ancrable','onomatopee');
    nouvelle_onomatopee.style.left = bouton_onomatopee.offsetLeft - decalage_bd + 'px';
    nouvelle_onomatopee.style.top = scrollBD+35+'px';
    nouvelle_onomatopee.style.zIndex=hauteur;
    hauteur+=1;
    createHandles(nouvelle_onomatopee);
    bd.appendChild(nouvelle_onomatopee);  
  
    let nouveau_paragraphe = document.createElement('p');
    nouveau_paragraphe.contentEditable=true;
    nouveau_paragraphe.innerHTML='PAF';
    nouveau_paragraphe.style.zIndex=hauteur;
    nouveau_paragraphe.style.color='rgb(0,0,0)';
    nouveau_paragraphe.style.WebkitTextStrokeColor='rgb(255,255,255)';
    hauteur+=1;
    surveillerFocus(nouveau_paragraphe);
    nouvelle_onomatopee.appendChild(nouveau_paragraphe);
  
    var texteSelectionne;
  
    if (window.getSelection) {
        // Pour les navigateurs modernes
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(nouveau_paragraphe);
        selection.removeAllRanges();
        selection.addRange(range);
        texteSelectionne = selection.toString();
    } else if (document.body.createTextRange) {
        // Pour les anciennes versions d'Internet Explorer
        var range = document.body.createTextRange();
        range.moveToElementText(nouveau_paragraphe);
        range.select();
        texteSelectionne = range.text;
    }
  
    clicked=nouvelle_onomatopee;
    nouvelle_onomatopee.classList.add('clicked');
    nouvelle_onomatopee.id=0;
    bouton_ancrer.disabled=false;
    maj_controles(nouvelle_onomatopee);
    maj_credits_police();
}



// Création d'un cartouche
function cartouche(vignette){  
    if (!vignette.classList.contains('contient_cartouche')){
    let nouveau_cartouche=document.createElement('div');
    nouveau_cartouche.classList.add('cartouche','draggable','cartouche_top','catouche_left');
    nouveau_cartouche.style.backgroundColor = '#FFFF00';
    vignette.appendChild(nouveau_cartouche);
    vignette.classList.add('contient_cartouche');  
  
    let nouveau_paragraphe = document.createElement('p');
    nouveau_paragraphe.contentEditable=true;
    nouveau_paragraphe.style.fontFamily=police_family;
    nouveau_paragraphe.style.fontSize=police_size+'px';
    nouveau_paragraphe.style.zIndex=hauteur;
    hauteur+=1;
    surveillerFocus(nouveau_paragraphe);
    nouveau_cartouche.appendChild(nouveau_paragraphe);
  
    declique('');
    clicked=nouveau_paragraphe;
    maj_controles(nouveau_cartouche);
    nouveau_paragraphe.focus();
  
    } else {
      let cartouche = vignette.querySelector(".cartouche");
      cartouche.remove();
      vignette.classList.remove('contient_cartouche');
    }
  
    maj_credits_police();   
  }

// Création d'une nouvelle bulle
function bubble(mode){
    if (clicked){declique('clic')}
    reclic=false;
  
    let scrollBD = bd.scrollTop;
    let nouvelle_bulle = document.createElement('div');
    nouvelle_bulle.style.zIndex=hauteur;
    hauteur+=1;
    nouvelle_bulle.classList.add('bubble');
    if (mode==='pensee' || mode==='ordi' || mode==='colere' || mode==='telephone' ){nouvelle_bulle.classList.add('pensee');}
    if (mode==='ordi'){nouvelle_bulle.classList.add('ordi');}
    if (mode==='telephone'){nouvelle_bulle.classList.add('telephone');}
    if (mode==='colere'){nouvelle_bulle.classList.add('colere');}
    nouvelle_bulle.classList.add('draggable','ancrable');
    nouvelle_bulle.style.top = scrollBD+50+'px';
    nouvelle_bulle.style.left = bulles.offsetLeft-decalage_bd+100+'px';
  
    nouvelle_bulle.style.setProperty('--margin-after', '-14px');
    nouvelle_bulle.style.setProperty('--margin-before', '-17px');
  
    let nouvelle_image=null;
    if (mode==='colere'){
      nouvelle_image=modele_colere.cloneNode(true);
      nouvelle_image.id=null;
      nouvelle_image.classList.add('img_bulle');
      nouvelle_image.style.zIndex=hauteur-1;
      nouvelle_bulle.appendChild(nouvelle_image);
    }
  
    createHandles(nouvelle_bulle);
    bd.appendChild(nouvelle_bulle);
    
  
    let nouveau_paragraphe = document.createElement('p');
    nouveau_paragraphe.contentEditable=true;
    nouveau_paragraphe.innerHTML='Texte';
    if (mode==='ordi'){
      nouveau_paragraphe.style.fontFamily='Segment';
      nouveau_paragraphe.style.fontSize='28px';    
    } else if (mode==='telephone'){
      nouveau_paragraphe.style.fontFamily='Teleindicadore';
      nouveau_paragraphe.style.fontSize='24px'; 
      nouveau_paragraphe.style.fontWeight='bold';   
    }
    
    else {
      nouveau_paragraphe.style.fontFamily=police_family;
      nouveau_paragraphe.style.fontSize=police_size+'px';
    }
  
    
  
    surveillerFocus(nouveau_paragraphe);
    if (mode==='colere') {
      nouveau_paragraphe.addEventListener('input', function() {
      maj_img_bulle(nouvelle_bulle,nouvelle_image);
      });
    }
    nouvelle_bulle.appendChild(nouveau_paragraphe);
  
    nouveau_paragraphe.focus();
  
    var texteSelectionne;
  
  if (window.getSelection) {
      // Pour les navigateurs modernes
      var selection = window.getSelection();
      var range = document.createRange();
      range.selectNodeContents(nouveau_paragraphe);
      selection.removeAllRanges();
      selection.addRange(range);
      texteSelectionne = selection.toString();
  } else if (document.body.createTextRange) {
      // Pour les anciennes versions d'Internet Explorer
      var range = document.body.createTextRange();
      range.moveToElementText(nouveau_paragraphe);
      range.select();
      texteSelectionne = range.text;
  }
  
    let nouvelle_pointe = document.createElement('div');
    nouvelle_pointe.classList.add('pointe');
    nouvelle_pointe.id=1;
    nouvelle_bulle.appendChild(nouvelle_pointe);
    clicked=nouvelle_bulle;
  
    bouton_ancrer.disabled=false;
  
    let nouvelle_image_pointe;
    if (mode==='pensee'){
      nouvelle_image_pointe=modele_pensee.cloneNode(true);
    } else if (mode==='ordi' || mode==='colere' || mode==='telephone') {
      nouvelle_image_pointe=modele_ordi.cloneNode(true);
    }
    else {
      nouvelle_image_pointe=modele_pointe.cloneNode(true);
    }  
  
    nouvelle_image_pointe.style.position='absolute';
    nouvelle_image_pointe.style.bottom='0px';
  
    nouvelle_image_pointe.style.left='0px';
    nouvelle_image_pointe.id=null;  
    nouvelle_pointe.appendChild(nouvelle_image_pointe);
    if (mode==='ordi'){
      change_fond_couleur('rgb(0,0,0)');
      change_police_couleur('rgb(0,255,76)');
    } else if (mode==='telephone'){
      change_fond_couleur('rgb(255,255,255)');
      change_police_couleur('rgb(0,0,0)');
    }
    else if (mode!='colere') {
      change_fond_couleur('rgb(255,255,255)');
      change_police_couleur('rgb(0,0,0)');
    } else {
      change_fond_couleur(fond_couleur.value);
      change_police_couleur(police_couleur.value);
    }
    if (nouvelle_image){
      maj_img_bulle(nouvelle_bulle,nouvelle_image);
    }
    maj_credits_police();
  
  }

  function maj_img_bulle(bulle,image){
    let hauteur = bulle.offsetHeight;
    image.style.transform='scaleY('+(hauteur/100)+')';
  }

  // Création d'une vignette et de tous les boutons associés
function cree_vignette(mode,bande,cote_vignette,index_bande,index_vignette,reference) {
    let vignette = document.createElement('div');
    vignette.classList.add('vignette');  
    vignette.style.width=cote_vignette+'px';
    vignette.style.backgroundColor='rgb(255,255,255';
    if (mode==='bande'){bande.appendChild(vignette);}
    else {
      if (mode===1){reference.insertAdjacentElement('afterend', vignette)}
      else {reference.insertAdjacentElement('beforebegin', vignette)}
    }
  
    let nouvelle_div_fond = document.createElement('div');
    nouvelle_div_fond.classList.add('div_fond');
    vignette.appendChild(nouvelle_div_fond);
  
    let div_haut=document.createElement('div');
    div_haut.style.position='absolute';
    div_haut.style.top='0px';
    div_haut.style.width='100%';
    div_haut.style.textAlign='center';
    div_haut.classList.add('zone_outil','zone_haut');
    vignette.appendChild(div_haut);  
  
    let nouvelle_div=document.createElement('div');
    nouvelle_div.classList.add('outils_vignette');
    nouvelle_div.classList.add('haut');
    div_haut.appendChild(nouvelle_div);
  
    let nouveau_bouton=document.createElement('button');
    nouveau_bouton.style.backgroundImage='url(images/boutons/zoom_in.svg)';
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.title="Zoomer l'image de fond";
    nouveau_bouton.addEventListener('click', function() {
      zoom(vignette,1);
    });
    nouvelle_div.appendChild(nouveau_bouton); 
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.style.backgroundImage='url(images/boutons/zoom_out.svg)';
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.title="Dé-zéoomer l'image de fond";
    nouveau_bouton.addEventListener('click', function() {
      zoom(vignette,-1);
    });
    nouvelle_div.appendChild(nouveau_bouton); 
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.style.backgroundImage='url(images/boutons/supprimer_background.png)';
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.title="Supprimer l'image de fond";
    nouveau_bouton.addEventListener('click', function() {
      supprime_fond(vignette);
    });
    nouvelle_div.appendChild(nouveau_bouton);
  
    let nouveau_input=document.createElement('input');
    nouveau_input.style.display='none';
    nouveau_input.type='color';
    nouveau_input.addEventListener('input', function() {
      change_couleur(vignette,this.value);
    });
    nouvelle_div.appendChild(nouveau_input);
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.classList.add('bouton_couleur');
    nouveau_bouton.style.backgroundImage='url(images/boutons/couleurs.svg)';
    nouveau_bouton.title="Changer la couleur de fond";
    nouveau_bouton.addEventListener('click', function() {
      nouveau_input.click();
    });
    nouvelle_div.appendChild(nouveau_bouton);
  
    let div_bas=document.createElement('div');
    div_bas.style.position='absolute';
    div_bas.style.bottom='57px';
    div_bas.style.width='100%';
    div_bas.style.textAlign='center';
    div_bas.classList.add('zone_outil','zone_bas');
    vignette.appendChild(div_bas);
    
    let nouvelle_div2=document.createElement('div');
    nouvelle_div2.classList.add('outils_vignette');
    nouvelle_div2.classList.add('bas');
    div_bas.appendChild(nouvelle_div2);
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.style.backgroundImage='url(images/boutons/plus_gauche.svg)';
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.title="Ajouter une vignette à gauche";
    nouveau_bouton.addEventListener('click', function() {
      ajoute_vignette(vignette,0);
    });
    nouvelle_div2.appendChild(nouveau_bouton);
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.style.backgroundImage='url(images/boutons/supprimer.svg)';
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.title="Supprimer cette vignette";
    nouveau_bouton.addEventListener('click', function() {
      supprime_vignette(vignette);
    });
    nouvelle_div2.appendChild(nouveau_bouton);
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.style.backgroundImage='url(images/boutons/plus.svg)';
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.title="Ajouter une vignette à droite";
    nouveau_bouton.addEventListener('click', function() {
      ajoute_vignette(vignette,1);
    });
    nouvelle_div2.appendChild(nouveau_bouton);
  
    let div_gauche=document.createElement('div');
    div_gauche.style.position='absolute';
    div_gauche.style.top='0px';
    div_gauche.style.height='100%';
    div_gauche.style.textAlign='center';
    div_gauche.style.alignItems='center';
    div_gauche.classList.add('zone_outil','zone_gauche');
    vignette.appendChild(div_gauche);
  
    let nouvelle_div3=document.createElement('div');
    nouvelle_div3.classList.add('outils_vignette');
    nouvelle_div3.classList.add('gauche');
    div_gauche.appendChild(nouvelle_div3);
  
    nouveau_bouton=document.createElement('button');
    nouveau_bouton.classList.add('bouton_vignette');
    nouveau_bouton.classList.add('bouton_cartouche');
    nouveau_bouton.style.backgroundImage='url(images/boutons/cartouche.svg)';
    nouveau_bouton.title="Créer un cartouche";
    nouveau_bouton.addEventListener('click', function() {
      cartouche(vignette);
    });
    nouvelle_div3.appendChild(nouveau_bouton);
  
  
  
    liste_vignettes[index_bande].splice(index_vignette, 0, vignette);
    vignette.id='vignette_'+index_bande+'_'+index_vignette;
  
  }
  
  // Création d'une nouvelle bande
  function cree_bande(nb_vignettes,debut){
      
    // Création d'une bande
    liste_vignettes.push([]);
    let bande = document.createElement('div');    
    bande.classList.add('bande');
    bd.insertBefore(bande, bouton_moins);
    liste_bandes.push(bande);
    let largeur_bande = largeur_vignette_droite=parseFloat(document.defaultView.getComputedStyle(bande).width, 10);
    let cote_vignette=largeur_bande/nb_vignettes-16;
    bande.style.height=cote_vignette+16+'px';
    let index_vignette = 0;
  
    // Création des vignettes
    for (let i = 0; i < nb_vignettes; i++) {
      cree_vignette('bande',bande,cote_vignette,liste_vignettes.length-1,index_vignette);        
      index_vignette+=1;
    }
  
    if(liste_bandes.length>1){
      bouton_moins.style.display='inline';
    }  
    if (!debut){sauvegarde();}
    
  }
  
  // Ajout manuel d'une nouvelle vignette
  function ajoute_vignette(vignette,decalage){
  
    let largeur_vignette_a_decouper = parseFloat(document.defaultView.getComputedStyle(vignette).width, 10);
    let vignette_a_decouper=null;
  
    if (largeur_vignette_a_decouper>=76){
      vignette_a_decouper=vignette;
    }
  
    if (vignette_a_decouper) {
      vignette_a_decouper.style.width=largeur_vignette_a_decouper-76+'px';
      let index_bande=cherche_vignette(vignette)[0];
      let index_vignette=cherche_vignette(vignette)[1];
      cree_vignette(decalage,vignette.parentNode,60,index_bande,index_vignette+decalage,vignette);
    }
  
    sauvegarde();
  
  }

  function createHandles(element) {

    let tour=1;
    let conteneur;
    const handles = [
      'top-left', 'top-right', 'bottom-left', 'bottom-right', 'top', 'bottom', 'left', 'right','fleche','rotate'
    ];
    
    handles.forEach(handleClass => {
      const handle = document.createElement('div');
      handle.classList.add('handle', handleClass);
   
      if (tour<5){
        handle.classList.add('coin');
        conteneur = element;
      }
      else {
        handle.classList.add('pas_coin');
        if (element.classList.contains('objet')){
          conteneur = element.querySelector('.cadre');
          handle.classList.add('rogne');
          
          if (tour>=9){
            handle.classList.remove('rogne');
            conteneur = element;
          } //hack temporaire
  
  
        } else {
          conteneur = element;
        }
      }    
    
      conteneur.appendChild(handle);
      tour+=1;
  
    });
  
  
  }


  // Duplication d'un objet
function duplique(){
    if (clicked && (clicked.classList.contains('objet') || clicked.classList.contains('bubble') || clicked.classList.contains('onomatopee'))){    
      const objet=clicked;    
      const divDupliquee = objet.cloneNode(true);
      declique('clic',divDupliquee);
      clicked=divDupliquee;
      const styles = window.getComputedStyle(objet);
      const posXobjet = parseFloat(styles.left);
      const posYobjet = parseFloat(styles.top); 
      divDupliquee.style.left=posXobjet+30+'px';
      divDupliquee.style.top=posYobjet+30+'px';
      bd.appendChild(divDupliquee);
      sauvegarde();
    }
  }