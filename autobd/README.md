# Autobd

## Présentation

AutoBD est une application libre permettant de réaliser une bande dessinée, et de l’exporter sous forme d’image.

Bien qu'elle soit en ligne, tout est écécuté localement et rien n'est stocké sur le serveur.

## Démonstration

Une instance en fonctionnement peut être utilisée sur https://achampollion.forge.aeif.fr/autobd .

## Remerciements

Merci aux personnes qui ont testé l'application et fourni leur précieux conseils sur Tchap, canaux Forge des Communs Numériques & AutoBD.

# Licence

Cette application est sous licence libree GNU/GPL.