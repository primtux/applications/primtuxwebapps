"use strict"

const messages = {
  erreur1: "Malheureusement, toutes les images ne sont pas dans l'ordre !",
  erreur2: "Les images ne sont toujours pas dans l'ordre !<p></p>L'image opaque est la première image mal placée.",
  felicitations: "Félicitations ! <br/>Tu as réussi à classer correctement les 14 histoires.",
}
const nbListes = Object.keys(items).length;
var histoireId;
var solution = [];
var listesFaites = [];
var titreHistoire;
var dernierSurvol;
var ctErreurs = 0;
// Espace minimal laissé en fin de ligne pour permettre le drop
var espaceDrop = 50;
const receptacleImg = document.getElementById("receptacle");

function getIntValeur(nbPixels) {
  let valeur;
  valeur = parseInt(nbPixels.substr(0, nbPixels.length - 2));
  return valeur;
}

function getImgErreur(proposition) {
  let i;
  let tab1 = proposition.split(',');
  let tab2 = solution.split(',');
  for (i = 0; i < tab2.length; i++) {
    if (tab1[i] !== tab2[i]) {
      return tab1[i];
    }
  }
}

function creeListeItems() {
  let i;
  let listeItems = items[histoireId];
  solution = items[histoireId].join();
  listeItems = melange(listeItems);
  for (i = 0; i < listeItems.length; i++) {
    let newImg = new Image();
    newImg.id = listeItems[i];
    newImg.src = "img/" + listeItems[i];
    newImg.setAttribute('draggable', false);
    let container = document.createElement('span');
    container.className = "collection";
    container.id = "item" + i;
    container.setAttribute('draggable', true);
    container.addEventListener("dragstart", drag);
    container.addEventListener("dragend", finSurvol);
    container.appendChild(newImg);
    document.getElementById("reserve").appendChild(container);
  }
  receptacleImg.style.minHeight = hauteurImages[histoireId];
}

function changeDisableBtListe(value) {
    let _list = document.getElementsByClassName("btliste");
    for (var i = 0, len = _list.length; i < len; i++) {
        if (listesFaites.indexOf(_list[i].id) !== -1)
            _list[i].disabled = true
        else
            _list[i].disabled = value;
    }
}

function defListe(element) {
  reinitialise();
  histoireId = element.id;
  titreHistoire = element.innerHTML;
  changeDisableBtListe(true);
  document.getElementById("imprimer").disabled = true;
  commencer();
}

function getIdObjet(infosObjet) {
  if (!infosObjet)
    return null;
  let tableau = infosObjet.split("-");
  return tableau[0];
}

function getIdParent(infosObjet) {
  let tableau = infosObjet.split("-");
  return tableau[1];
}

function  reinitialise() {
  while (receptacleImg.firstChild) {
    receptacleImg.removeChild(receptacleImg.firstChild);
  }
}

function commencer() {
  creeListeItems();
  document.getElementById("verifier").disabled = false;
  document.getElementById('consignes').classList.add('hidden');
}


function afficheBravo() {
  document.getElementById("ecran-noir").className = "visible";
  document.getElementById("bravo").className = "visible";
}

function effaceBravo() {
  document.getElementById("ecran-noir").className = "invisible";
  document.getElementById("bravo").className = "invisible";
}

function verification() {
  let i;
  let proposition = "";
  let position;
  let imgErreur = "";
  let nodeErreur;
  for (i = 0; i < receptacleImg.childNodes.length; i++) {
    receptacleImg.childNodes[i].classList.remove('erreur-image');
    position = receptacleImg.childNodes[i].childNodes[0].src.lastIndexOf("/") + 1;
    if (i + 1 < receptacleImg.childNodes.length) {
      proposition = proposition + receptacleImg.childNodes[i].childNodes[0].src.substr(position) + ",";
      }
      else {
        proposition = proposition + receptacleImg.childNodes[i].childNodes[0].src.substr(position);
      }
  }
  if (proposition !== solution) {
    ctErreurs += 1;
    if (ctErreurs < 2) {
      showDialog(messages.erreur1,0.5,'img/tux-triste.png', 115, 101, 'left');
      return;
    }
    else {
      imgErreur = getImgErreur(proposition);
      nodeErreur = document.getElementById(imgErreur);
      nodeErreur.parentNode.classList.add('erreur-image');
      showDialog(messages.erreur2,0.5,'img/tux-triste.png', 115, 101, 'left');
      return;
    }
  }
  document.getElementById('consignes').classList.remove('hidden');
  ctErreurs = 0;
  listesFaites.push(histoireId);
  if (listesFaites.length < nbListes) {
    afficheBravo();
  }
  else {
    showDialog(messages.felicitations,0.5,'img/trophee.png', 128, 128, 'left')
  }
  document.getElementById("verifier").disabled = true;
  document.getElementById("imprimer").disabled = false;
  changeDisableBtListe(false);
}

function resize() {
  // Si on dispose d'un espace suffisant pour le drop en fin de ligne, on supprime la ligne supplémentaire si elle existe
  if ((receptacleImg.lastChild) && (receptacleImg.lastChild.x + espaceDrop < receptacleImg.offsetWidth)) {
    if (getIntValeur(receptacleImg.style.minHeight) - getIntValeur(hauteurImages[numeroListe - 1]) > 0 ) {
      receptacleImg.style.minHeight = getIntValeur(receptacleImg.style.minHeight) - getIntValeur(hauteurImages[numeroListe - 1]) + "px";
    }
  }
  // Si l'espace en fin de ligne est insuffisant pour le drop, on ajoute une ligne
  if ((receptacleImg.lastChild) && (receptacleImg.lastChild.x + receptacleImg.lastChild.clientWidth + espaceDrop > receptacleImg.offsetWidth)) {
    receptacleImg.style.minHeight = getIntValeur(receptacleImg.style.minHeight) + getIntValeur(hauteurImages[numeroListe - 1]) + "px";
  }
}

function allowDrop(event) {
    event.preventDefault();
    if (stockage.indexOf('receptacle') !== -1) {
        let dragElement = document.getElementById(stockage.replace('-receptacle', ''));
        if (dragElement.parentNode.id === 'receptacle' && !dragElement.nextSibling)
            return;
    }
    let first_id = 'item1';
    let fake_id = 'fake-' + first_id;
    if (document.getElementsByClassName('fake-element').length)
      return;
    let fake_node = document.createElement('span');
    fake_node.appendChild(
        document.getElementById(first_id).childNodes[0].cloneNode()
    );
    fake_node.id = fake_id;
    fake_node.classList.add('collection');
    fake_node.classList.add('fake-element');
    fake_node.childNodes[0].classList.add('hidden');
    receptacleImg.appendChild(fake_node);
}

function deleteLastFakeElement(ev) {
    ev.preventDefault();
    remove_drap_spaces();
}

let stockage = null;
function drag(ev) {
	var infosObjet;
	infosObjet = ev.target.id + "-" + ev.target.parentNode.id;
  ev.dataTransfer.setData("text", infosObjet);
  stockage = infosObjet;
}

function auSurvol(ev) {
  ev.preventDefault();
  const idObjet  = getIdObjet(stockage);
  dernierSurvol = ev.target;
  if (!idObjet || ev.target.id === idObjet) {
    return;
  }
  let fake_id = 'fake-' + dernierSurvol.id;
  if (document.getElementsByClassName('fake-element').length)
    return;
  let fake_node = dernierSurvol.cloneNode(true);
  fake_node.id = fake_id;
  fake_node.classList.add('fake-element');
  dernierSurvol.after(fake_node);
  dernierSurvol.childNodes[0].classList.add('hidden');
}

function remove_drap_spaces() {
    let _list = document.getElementsByClassName("fake-element");
    for (var i = 0, len = _list.length; i < len; i++) {
        _list[i].parentNode.removeChild(_list[i]);
    }
    _list = receptacleImg.getElementsByClassName("hidden");
    for (var i = 0, len = _list.length; i < len; i++) {
        _list[i].classList.remove('hidden');
    }
}

function finSurvol(ev) {
    remove_drap_spaces();
}

function drop(ev) {
  const idObjet  = getIdObjet(ev.dataTransfer.getData('Text'));
  const idParent = getIdParent(ev.dataTransfer.getData('Text'));
  const Objet = document.getElementById(idObjet);
  ev.preventDefault();
  if (idParent === "reserve") {
    Objet.addEventListener("dragover", auSurvol);
    Objet.addEventListener("dragleave", finSurvol);
  }
  if (ev.target.id === "receptacle") {
    ev.target.appendChild(Objet);
    }
    else {
      // if (Objet.className === "collection") {
      if (Objet.classList.contains("collection")) {
        ev.target.parentNode.insertBefore(Objet, ev.target);
      }
  }
  // Si l'espace en fin de ligne est insuffisant pour le drop, on ajoute une ligne
  if ((receptacleImg.lastChild) && (receptacleImg.lastChild.x + Objet.clientWidth + espaceDrop > receptacleImg.offsetWidth)) {
    receptacleImg.style.minHeight = getIntValeur(receptacleImg.style.minHeight) + getIntValeur(hauteurImages[numeroListe - 1]) + "px";
  }
  if (espaceDrop < parseInt(Objet.clientWidth / 2)) {
    espaceDrop = parseInt(Objet.clientWidth / 2);
  }
}

function pageImpression() {
  sessionStorage.setItem("titre",titreHistoire);
  // sessionStorage.setItem("serie",listesFaites[listesFaites.length-1]-1);
  sessionStorage.setItem("serie",histoireId);
  window.open('impression.html');
}

function load() {
    let hash = document.location.hash.substr(1);
    if (hash in items)
        defListe(document.getElementById(hash));
}

window.onresize = resize;
