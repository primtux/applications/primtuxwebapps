gdjs.infoCode = {};
gdjs.infoCode.GDscoreObjects1= [];
gdjs.infoCode.GDscoreObjects2= [];
gdjs.infoCode.GDscore_953Objects1= [];
gdjs.infoCode.GDscore_953Objects2= [];
gdjs.infoCode.GDfondObjects1= [];
gdjs.infoCode.GDfondObjects2= [];
gdjs.infoCode.GDbouton_95retourObjects1= [];
gdjs.infoCode.GDbouton_95retourObjects2= [];
gdjs.infoCode.GDfond_95blancObjects1= [];
gdjs.infoCode.GDfond_95blancObjects2= [];
gdjs.infoCode.GDinfoObjects1= [];
gdjs.infoCode.GDinfoObjects2= [];

gdjs.infoCode.conditionTrue_0 = {val:false};
gdjs.infoCode.condition0IsTrue_0 = {val:false};
gdjs.infoCode.condition1IsTrue_0 = {val:false};
gdjs.infoCode.condition2IsTrue_0 = {val:false};
gdjs.infoCode.condition3IsTrue_0 = {val:false};
gdjs.infoCode.conditionTrue_1 = {val:false};
gdjs.infoCode.condition0IsTrue_1 = {val:false};
gdjs.infoCode.condition1IsTrue_1 = {val:false};
gdjs.infoCode.condition2IsTrue_1 = {val:false};
gdjs.infoCode.condition3IsTrue_1 = {val:false};


gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infoCode.GDbouton_95retourObjects1});
gdjs.infoCode.eventsList0 = function(runtimeScene) {

{


gdjs.infoCode.condition0IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infoCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infoCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infoCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infoCode.GDfond_95blancObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infoCode.GDbouton_95retourObjects1);

gdjs.infoCode.condition0IsTrue_0.val = false;
gdjs.infoCode.condition1IsTrue_0.val = false;
gdjs.infoCode.condition2IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infoCode.condition0IsTrue_0.val ) {
{
gdjs.infoCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.infoCode.condition1IsTrue_0.val ) {
{
{gdjs.infoCode.conditionTrue_1 = gdjs.infoCode.condition2IsTrue_0;
gdjs.infoCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14691132);
}
}}
}
if (gdjs.infoCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


{


{
}

}


};

gdjs.infoCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infoCode.GDscoreObjects1.length = 0;
gdjs.infoCode.GDscoreObjects2.length = 0;
gdjs.infoCode.GDscore_953Objects1.length = 0;
gdjs.infoCode.GDscore_953Objects2.length = 0;
gdjs.infoCode.GDfondObjects1.length = 0;
gdjs.infoCode.GDfondObjects2.length = 0;
gdjs.infoCode.GDbouton_95retourObjects1.length = 0;
gdjs.infoCode.GDbouton_95retourObjects2.length = 0;
gdjs.infoCode.GDfond_95blancObjects1.length = 0;
gdjs.infoCode.GDfond_95blancObjects2.length = 0;
gdjs.infoCode.GDinfoObjects1.length = 0;
gdjs.infoCode.GDinfoObjects2.length = 0;

gdjs.infoCode.eventsList0(runtimeScene);
return;

}

gdjs['infoCode'] = gdjs.infoCode;
