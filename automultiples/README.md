# Auto multiples

Automultiples est une application qui affiche les multiples d'un nombre.

Elle peut être notamment utile en technique opératoire de la division, lorsque l'objectif de la séance n'est pas justement de chercher ces multiples.

## Version Openboard

Télécharger et dézipper le fichier https://forge.aeif.fr/achampollion/automultiples/-/blob/main/automultiples.wgt.zip?ref_type=heads .

Puis copier le dossier obtenu dans le dossier des applications d'Openboard.

## Licence

Cette application est écrite par Arnaud Champollion et partagée sous licence libre GNU/GPL

Sources et téléchargement de versions hors-ligne / Openboard sur la Forge.