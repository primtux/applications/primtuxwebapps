gdjs.infosCode = {};
gdjs.infosCode.GDtxt_9595info_95950Objects1= [];
gdjs.infosCode.GDtxt_9595info_95950Objects2= [];
gdjs.infosCode.GDtxt_9595info_95951Objects1= [];
gdjs.infosCode.GDtxt_9595info_95951Objects2= [];
gdjs.infosCode.GDinfo_95951Objects1= [];
gdjs.infosCode.GDinfo_95951Objects2= [];
gdjs.infosCode.GDinfo_95952Objects1= [];
gdjs.infosCode.GDinfo_95952Objects2= [];
gdjs.infosCode.GDinfo_95953Objects1= [];
gdjs.infosCode.GDinfo_95953Objects2= [];
gdjs.infosCode.GDinfo_95954Objects1= [];
gdjs.infosCode.GDinfo_95954Objects2= [];
gdjs.infosCode.GDtxt_9595info_95952Objects1= [];
gdjs.infosCode.GDtxt_9595info_95952Objects2= [];
gdjs.infosCode.GDtxt_9595info_95953Objects1= [];
gdjs.infosCode.GDtxt_9595info_95953Objects2= [];
gdjs.infosCode.GDtxt_9595info_95954Objects1= [];
gdjs.infosCode.GDtxt_9595info_95954Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDtxt_9595info_95955Objects1= [];
gdjs.infosCode.GDtxt_9595info_95955Objects2= [];
gdjs.infosCode.GDserge_9595lamaObjects1= [];
gdjs.infosCode.GDserge_9595lamaObjects2= [];
gdjs.infosCode.GDtxt_9595info_95956Objects1= [];
gdjs.infosCode.GDtxt_9595info_95956Objects2= [];
gdjs.infosCode.GDinfo_95955Objects1= [];
gdjs.infosCode.GDinfo_95955Objects2= [];
gdjs.infosCode.GDtxt_9595info_95957Objects1= [];
gdjs.infosCode.GDtxt_9595info_95957Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDMetalRedBarObjects1= [];
gdjs.infosCode.GDMetalRedBarObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDtxt_9595info_95950Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95950Objects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95951Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95951Objects2.length = 0;
gdjs.infosCode.GDinfo_95951Objects1.length = 0;
gdjs.infosCode.GDinfo_95951Objects2.length = 0;
gdjs.infosCode.GDinfo_95952Objects1.length = 0;
gdjs.infosCode.GDinfo_95952Objects2.length = 0;
gdjs.infosCode.GDinfo_95953Objects1.length = 0;
gdjs.infosCode.GDinfo_95953Objects2.length = 0;
gdjs.infosCode.GDinfo_95954Objects1.length = 0;
gdjs.infosCode.GDinfo_95954Objects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95952Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95952Objects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95953Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95953Objects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95954Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95954Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95955Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95955Objects2.length = 0;
gdjs.infosCode.GDserge_9595lamaObjects1.length = 0;
gdjs.infosCode.GDserge_9595lamaObjects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95956Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95956Objects2.length = 0;
gdjs.infosCode.GDinfo_95955Objects1.length = 0;
gdjs.infosCode.GDinfo_95955Objects2.length = 0;
gdjs.infosCode.GDtxt_9595info_95957Objects1.length = 0;
gdjs.infosCode.GDtxt_9595info_95957Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDMetalRedBarObjects1.length = 0;
gdjs.infosCode.GDMetalRedBarObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
