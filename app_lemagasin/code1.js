gdjs.menu2Code = {};
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1= [];
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2= [];
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects3= [];
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1= [];
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2= [];
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects3= [];
gdjs.menu2Code.GDswitch_9595decimauxObjects1= [];
gdjs.menu2Code.GDswitch_9595decimauxObjects2= [];
gdjs.menu2Code.GDswitch_9595decimauxObjects3= [];
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595decimauxObjects1= [];
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595decimauxObjects2= [];
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595decimauxObjects3= [];
gdjs.menu2Code.GDtxt_9595reglages_9595prix_9595maxiObjects1= [];
gdjs.menu2Code.GDtxt_9595reglages_9595prix_9595maxiObjects2= [];
gdjs.menu2Code.GDtxt_9595reglages_9595prix_9595maxiObjects3= [];
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1= [];
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2= [];
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects3= [];
gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects1= [];
gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2= [];
gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects3= [];
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595articlesObjects1= [];
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595articlesObjects2= [];
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595articlesObjects3= [];
gdjs.menu2Code.GDversionObjects1= [];
gdjs.menu2Code.GDversionObjects2= [];
gdjs.menu2Code.GDversionObjects3= [];
gdjs.menu2Code.GDSquareWhiteSliderffObjects1= [];
gdjs.menu2Code.GDSquareWhiteSliderffObjects2= [];
gdjs.menu2Code.GDSquareWhiteSliderffObjects3= [];
gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects1= [];
gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects2= [];
gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects3= [];
gdjs.menu2Code.GDperso_9595sergeObjects1= [];
gdjs.menu2Code.GDperso_9595sergeObjects2= [];
gdjs.menu2Code.GDperso_9595sergeObjects3= [];
gdjs.menu2Code.GDperso_9595emmaObjects1= [];
gdjs.menu2Code.GDperso_9595emmaObjects2= [];
gdjs.menu2Code.GDperso_9595emmaObjects3= [];
gdjs.menu2Code.GDperso_9595vendeur3Objects1= [];
gdjs.menu2Code.GDperso_9595vendeur3Objects2= [];
gdjs.menu2Code.GDperso_9595vendeur3Objects3= [];
gdjs.menu2Code.GDNewSpriteObjects1= [];
gdjs.menu2Code.GDNewSpriteObjects2= [];
gdjs.menu2Code.GDNewSpriteObjects3= [];
gdjs.menu2Code.GDperso_9595alexObjects1= [];
gdjs.menu2Code.GDperso_9595alexObjects2= [];
gdjs.menu2Code.GDperso_9595alexObjects3= [];
gdjs.menu2Code.GDperso_9595vendeur1Objects1= [];
gdjs.menu2Code.GDperso_9595vendeur1Objects2= [];
gdjs.menu2Code.GDperso_9595vendeur1Objects3= [];
gdjs.menu2Code.GDperso_9595vendeur2Objects1= [];
gdjs.menu2Code.GDperso_9595vendeur2Objects2= [];
gdjs.menu2Code.GDperso_9595vendeur2Objects3= [];
gdjs.menu2Code.GDfond_9595menuObjects1= [];
gdjs.menu2Code.GDfond_9595menuObjects2= [];
gdjs.menu2Code.GDfond_9595menuObjects3= [];
gdjs.menu2Code.GDbouton_9595optionsObjects1= [];
gdjs.menu2Code.GDbouton_9595optionsObjects2= [];
gdjs.menu2Code.GDbouton_9595optionsObjects3= [];
gdjs.menu2Code.GDserge_9595lamaObjects1= [];
gdjs.menu2Code.GDserge_9595lamaObjects2= [];
gdjs.menu2Code.GDserge_9595lamaObjects3= [];
gdjs.menu2Code.GDauteurObjects1= [];
gdjs.menu2Code.GDauteurObjects2= [];
gdjs.menu2Code.GDauteurObjects3= [];
gdjs.menu2Code.GDimagesObjects1= [];
gdjs.menu2Code.GDimagesObjects2= [];
gdjs.menu2Code.GDimagesObjects3= [];
gdjs.menu2Code.GDbouton_9595infoObjects1= [];
gdjs.menu2Code.GDbouton_9595infoObjects2= [];
gdjs.menu2Code.GDbouton_9595infoObjects3= [];
gdjs.menu2Code.GDbouton_9595retourObjects1= [];
gdjs.menu2Code.GDbouton_9595retourObjects2= [];
gdjs.menu2Code.GDbouton_9595retourObjects3= [];
gdjs.menu2Code.GDMetalRedBarObjects1= [];
gdjs.menu2Code.GDMetalRedBarObjects2= [];
gdjs.menu2Code.GDMetalRedBarObjects3= [];


gdjs.menu2Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_lemagasin", "reglages");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_lemagasin", "reglages", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(0));
}}

}


};gdjs.menu2Code.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SquareWhiteSliderff"), gdjs.menu2Code.GDSquareWhiteSliderffObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDSquareWhiteSliderffObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDSquareWhiteSliderffObjects2[i].SetValue(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("intervalle").getChild("maxi")), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("SquareWhiteSliderff"), gdjs.menu2Code.GDSquareWhiteSliderffObjects2);
gdjs.copyArray(runtimeScene.getObjects("txt_prix_maxi"), gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects2[i].getBehavior("Text").setText(gdjs.evtTools.common.toString((( gdjs.menu2Code.GDSquareWhiteSliderffObjects2.length === 0 ) ? 0 :gdjs.menu2Code.GDSquareWhiteSliderffObjects2[0].Value((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)))));
}
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("switch_decimaux"), gdjs.menu2Code.GDswitch_9595decimauxObjects2);
gdjs.copyArray(runtimeScene.getObjects("switch_nb_articles"), gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDswitch_9595decimauxObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDswitch_9595decimauxObjects2[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2[i].getBehavior("Animation").pauseAnimation();
}
}}

}


{


gdjs.menu2Code.eventsList0(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("MetalRedBar"), gdjs.menu2Code.GDMetalRedBarObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDMetalRedBarObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDMetalRedBarObjects2[i].getVariableNumber(gdjs.menu2Code.GDMetalRedBarObjects2[i].getVariables().getFromIndex(0)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDMetalRedBarObjects2[k] = gdjs.menu2Code.GDMetalRedBarObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDMetalRedBarObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menu2Code.GDMetalRedBarObjects2 */
{for(var i = 0, len = gdjs.menu2Code.GDMetalRedBarObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDMetalRedBarObjects2[i].SetValue(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("scores").getChild("mode_2")), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("MetalRedBar"), gdjs.menu2Code.GDMetalRedBarObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDMetalRedBarObjects1.length;i<l;++i) {
    if ( gdjs.menu2Code.GDMetalRedBarObjects1[i].getVariableNumber(gdjs.menu2Code.GDMetalRedBarObjects1[i].getVariables().getFromIndex(0)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDMetalRedBarObjects1[k] = gdjs.menu2Code.GDMetalRedBarObjects1[i];
        ++k;
    }
}
gdjs.menu2Code.GDMetalRedBarObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menu2Code.GDMetalRedBarObjects1 */
{for(var i = 0, len = gdjs.menu2Code.GDMetalRedBarObjects1.length ;i < len;++i) {
    gdjs.menu2Code.GDMetalRedBarObjects1[i].SetValue(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("scores").getChild("mode_3")), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


};gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDperso_95959595sergeObjects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595emmaObjects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595alexObjects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595vendeur3Objects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595vendeur1Objects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595vendeur2Objects1ObjectsGDgdjs_9546menu2Code_9546GDtxt_95959595jeu_95959595libreObjects1ObjectsGDgdjs_9546menu2Code_9546GDtxt_95959595jeu_95959595jepayeObjects1ObjectsGDgdjs_9546menu2Code_9546GDtxt_95959595jeu_95959595jerendsObjects1Objects = Hashtable.newFrom({"perso_serge": gdjs.menu2Code.GDperso_9595sergeObjects1, "perso_emma": gdjs.menu2Code.GDperso_9595emmaObjects1, "perso_alex": gdjs.menu2Code.GDperso_9595alexObjects1, "perso_vendeur3": gdjs.menu2Code.GDperso_9595vendeur3Objects1, "perso_vendeur1": gdjs.menu2Code.GDperso_9595vendeur1Objects1, "perso_vendeur2": gdjs.menu2Code.GDperso_9595vendeur2Objects1, "txt_jeu_libre": gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1, "txt_jeu_jepaye": gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1, "txt_jeu_jerends": gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1});
gdjs.menu2Code.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.menu2Code.GDperso_9595alexObjects1, gdjs.menu2Code.GDperso_9595alexObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595emmaObjects1, gdjs.menu2Code.GDperso_9595emmaObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595sergeObjects1, gdjs.menu2Code.GDperso_9595sergeObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur1Objects1, gdjs.menu2Code.GDperso_9595vendeur1Objects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur2Objects1, gdjs.menu2Code.GDperso_9595vendeur2Objects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur3Objects1, gdjs.menu2Code.GDperso_9595vendeur3Objects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595sergeObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595sergeObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595sergeObjects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595sergeObjects2[k] = gdjs.menu2Code.GDperso_9595sergeObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595sergeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595emmaObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595emmaObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595emmaObjects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595emmaObjects2[k] = gdjs.menu2Code.GDperso_9595emmaObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595emmaObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595alexObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595alexObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595alexObjects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595alexObjects2[k] = gdjs.menu2Code.GDperso_9595alexObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595alexObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur3Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur3Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur3Objects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur3Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur3Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur3Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur1Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur1Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur1Objects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur1Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur1Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur1Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur2Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur2Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur2Objects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur2Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur2Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur2Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i].getVariables().get("valeur")) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}}

}


{

gdjs.copyArray(gdjs.menu2Code.GDperso_9595alexObjects1, gdjs.menu2Code.GDperso_9595alexObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595emmaObjects1, gdjs.menu2Code.GDperso_9595emmaObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595sergeObjects1, gdjs.menu2Code.GDperso_9595sergeObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur1Objects1, gdjs.menu2Code.GDperso_9595vendeur1Objects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur2Objects1, gdjs.menu2Code.GDperso_9595vendeur2Objects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur3Objects1, gdjs.menu2Code.GDperso_9595vendeur3Objects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595sergeObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595sergeObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595sergeObjects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595sergeObjects2[k] = gdjs.menu2Code.GDperso_9595sergeObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595sergeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595emmaObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595emmaObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595emmaObjects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595emmaObjects2[k] = gdjs.menu2Code.GDperso_9595emmaObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595emmaObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595alexObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595alexObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595alexObjects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595alexObjects2[k] = gdjs.menu2Code.GDperso_9595alexObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595alexObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur3Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur3Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur3Objects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur3Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur3Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur3Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur1Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur1Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur1Objects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur1Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur1Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur1Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur2Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur2Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur2Objects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur2Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur2Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur2Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i].getVariables().get("valeur")) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(2);
}}

}


{

gdjs.copyArray(gdjs.menu2Code.GDperso_9595alexObjects1, gdjs.menu2Code.GDperso_9595alexObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595emmaObjects1, gdjs.menu2Code.GDperso_9595emmaObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595sergeObjects1, gdjs.menu2Code.GDperso_9595sergeObjects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur1Objects1, gdjs.menu2Code.GDperso_9595vendeur1Objects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur2Objects1, gdjs.menu2Code.GDperso_9595vendeur2Objects2);

gdjs.copyArray(gdjs.menu2Code.GDperso_9595vendeur3Objects1, gdjs.menu2Code.GDperso_9595vendeur3Objects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2);

gdjs.copyArray(gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1, gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595sergeObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595sergeObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595sergeObjects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595sergeObjects2[k] = gdjs.menu2Code.GDperso_9595sergeObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595sergeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595emmaObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595emmaObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595emmaObjects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595emmaObjects2[k] = gdjs.menu2Code.GDperso_9595emmaObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595emmaObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595alexObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595alexObjects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595alexObjects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595alexObjects2[k] = gdjs.menu2Code.GDperso_9595alexObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595alexObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur3Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur3Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur3Objects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur3Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur3Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur3Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur1Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur1Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur1Objects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur1Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur1Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur1Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDperso_9595vendeur2Objects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDperso_9595vendeur2Objects2[i].getVariableNumber(gdjs.menu2Code.GDperso_9595vendeur2Objects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDperso_9595vendeur2Objects2[k] = gdjs.menu2Code.GDperso_9595vendeur2Objects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDperso_9595vendeur2Objects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length;i<l;++i) {
    if ( gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i].getVariableNumber(gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i].getVariables().get("valeur")) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[k] = gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2[i];
        ++k;
    }
}
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length = k;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(3);
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


};gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDswitch_95959595decimauxObjects1Objects = Hashtable.newFrom({"switch_decimaux": gdjs.menu2Code.GDswitch_9595decimauxObjects1});
gdjs.menu2Code.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nombres_decimaux")) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16682908);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("switch_decimaux"), gdjs.menu2Code.GDswitch_9595decimauxObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDswitch_9595decimauxObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDswitch_9595decimauxObjects2[i].setAnimationFrame(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nombres_decimaux")) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16683796);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("switch_decimaux"), gdjs.menu2Code.GDswitch_9595decimauxObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDswitch_9595decimauxObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDswitch_9595decimauxObjects2[i].setAnimationFrame(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nombres_decimaux")) > 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16684820);
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nombres_decimaux").setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("switch_decimaux"), gdjs.menu2Code.GDswitch_9595decimauxObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDswitch_95959595decimauxObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16684988);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nombres_decimaux").add(1);
}}

}


};gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDswitch_95959595nb_95959595articlesObjects1Objects = Hashtable.newFrom({"switch_nb_articles": gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects1});
gdjs.menu2Code.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("articles_nombre")) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16686548);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("switch_nb_articles"), gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2[i].setAnimationFrame(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("articles_nombre")) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16687060);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("switch_nb_articles"), gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2);
{for(var i = 0, len = gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2.length ;i < len;++i) {
    gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2[i].setAnimationFrame(1);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("articles_nombre")) > 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16688564);
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("articles_nombre").setNumber(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("switch_nb_articles"), gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDswitch_95959595nb_95959595articlesObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16689180);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("articles_nombre").add(1);
}}

}


};gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDbouton_95959595optionsObjects1Objects = Hashtable.newFrom({"bouton_options": gdjs.menu2Code.GDbouton_9595optionsObjects1});
gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDbouton_95959595infoObjects1Objects = Hashtable.newFrom({"bouton_info": gdjs.menu2Code.GDbouton_9595infoObjects1});
gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDserge_95959595lamaObjects1Objects = Hashtable.newFrom({"serge_lama": gdjs.menu2Code.GDserge_9595lamaObjects1});
gdjs.menu2Code.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lemagasin");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lemagasin", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(0)));
}}

}


};gdjs.menu2Code.eventsList6 = function(runtimeScene) {

{


gdjs.menu2Code.eventsList5(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.menu2Code.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_menu"), gdjs.menu2Code.GDfond_9595menuObjects1);
{for(var i = 0, len = gdjs.menu2Code.GDfond_9595menuObjects1.length ;i < len;++i) {
    gdjs.menu2Code.GDfond_9595menuObjects1[i].getBehavior("Opacity").setOpacity(150);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}
{ //Subevents
gdjs.menu2Code.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("perso_alex"), gdjs.menu2Code.GDperso_9595alexObjects1);
gdjs.copyArray(runtimeScene.getObjects("perso_emma"), gdjs.menu2Code.GDperso_9595emmaObjects1);
gdjs.copyArray(runtimeScene.getObjects("perso_serge"), gdjs.menu2Code.GDperso_9595sergeObjects1);
gdjs.copyArray(runtimeScene.getObjects("perso_vendeur1"), gdjs.menu2Code.GDperso_9595vendeur1Objects1);
gdjs.copyArray(runtimeScene.getObjects("perso_vendeur2"), gdjs.menu2Code.GDperso_9595vendeur2Objects1);
gdjs.copyArray(runtimeScene.getObjects("perso_vendeur3"), gdjs.menu2Code.GDperso_9595vendeur3Objects1);
gdjs.copyArray(runtimeScene.getObjects("txt_jeu_jepaye"), gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1);
gdjs.copyArray(runtimeScene.getObjects("txt_jeu_jerends"), gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1);
gdjs.copyArray(runtimeScene.getObjects("txt_jeu_libre"), gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDperso_95959595sergeObjects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595emmaObjects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595alexObjects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595vendeur3Objects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595vendeur1Objects1ObjectsGDgdjs_9546menu2Code_9546GDperso_95959595vendeur2Objects1ObjectsGDgdjs_9546menu2Code_9546GDtxt_95959595jeu_95959595libreObjects1ObjectsGDgdjs_9546menu2Code_9546GDtxt_95959595jeu_95959595jepayeObjects1ObjectsGDgdjs_9546menu2Code_9546GDtxt_95959595jeu_95959595jerendsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.menu2Code.eventsList2(runtimeScene);} //End of subevents
}

}


{



}


{



}


{



}


{


gdjs.menu2Code.eventsList3(runtimeScene);
}


{


gdjs.menu2Code.eventsList4(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("SquareWhiteSliderff"), gdjs.menu2Code.GDSquareWhiteSliderffObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menu2Code.GDSquareWhiteSliderffObjects1.length;i<l;++i) {
    if ( gdjs.menu2Code.GDSquareWhiteSliderffObjects1[i].IsBeingDragged((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.menu2Code.GDSquareWhiteSliderffObjects1[k] = gdjs.menu2Code.GDSquareWhiteSliderffObjects1[i];
        ++k;
    }
}
gdjs.menu2Code.GDSquareWhiteSliderffObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menu2Code.GDSquareWhiteSliderffObjects1 */
gdjs.copyArray(runtimeScene.getObjects("txt_prix_maxi"), gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects1);
{for(var i = 0, len = gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects1.length ;i < len;++i) {
    gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects1[i].getBehavior("Text").setText(gdjs.evtTools.common.toString((( gdjs.menu2Code.GDSquareWhiteSliderffObjects1.length === 0 ) ? 0 :gdjs.menu2Code.GDSquareWhiteSliderffObjects1[0].Value((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)))));
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("intervalle").getChild("maxi").setNumber((( gdjs.menu2Code.GDSquareWhiteSliderffObjects1.length === 0 ) ? 0 :gdjs.menu2Code.GDSquareWhiteSliderffObjects1[0].Value((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined))));
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_options"), gdjs.menu2Code.GDbouton_9595optionsObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDbouton_95959595optionsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_info"), gdjs.menu2Code.GDbouton_9595infoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDbouton_95959595infoObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge_lama"), gdjs.menu2Code.GDserge_9595lamaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menu2Code.mapOfGDgdjs_9546menu2Code_9546GDserge_95959595lamaObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(16693684);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) >= 5;
if (isConditionTrue_0) {
{gdjs.evtTools.variable.variableClearChildren(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("scores"));
}
{ //Subevents
gdjs.menu2Code.eventsList6(runtimeScene);} //End of subevents
}

}


};

gdjs.menu2Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595jepayeObjects3.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595libreObjects3.length = 0;
gdjs.menu2Code.GDswitch_9595decimauxObjects1.length = 0;
gdjs.menu2Code.GDswitch_9595decimauxObjects2.length = 0;
gdjs.menu2Code.GDswitch_9595decimauxObjects3.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595decimauxObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595decimauxObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595decimauxObjects3.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595prix_9595maxiObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595prix_9595maxiObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595prix_9595maxiObjects3.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595jeu_9595jerendsObjects3.length = 0;
gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects1.length = 0;
gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects2.length = 0;
gdjs.menu2Code.GDswitch_9595nb_9595articlesObjects3.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595articlesObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595articlesObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595reglages_9595nb_9595articlesObjects3.length = 0;
gdjs.menu2Code.GDversionObjects1.length = 0;
gdjs.menu2Code.GDversionObjects2.length = 0;
gdjs.menu2Code.GDversionObjects3.length = 0;
gdjs.menu2Code.GDSquareWhiteSliderffObjects1.length = 0;
gdjs.menu2Code.GDSquareWhiteSliderffObjects2.length = 0;
gdjs.menu2Code.GDSquareWhiteSliderffObjects3.length = 0;
gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects1.length = 0;
gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects2.length = 0;
gdjs.menu2Code.GDtxt_9595prix_9595maxiObjects3.length = 0;
gdjs.menu2Code.GDperso_9595sergeObjects1.length = 0;
gdjs.menu2Code.GDperso_9595sergeObjects2.length = 0;
gdjs.menu2Code.GDperso_9595sergeObjects3.length = 0;
gdjs.menu2Code.GDperso_9595emmaObjects1.length = 0;
gdjs.menu2Code.GDperso_9595emmaObjects2.length = 0;
gdjs.menu2Code.GDperso_9595emmaObjects3.length = 0;
gdjs.menu2Code.GDperso_9595vendeur3Objects1.length = 0;
gdjs.menu2Code.GDperso_9595vendeur3Objects2.length = 0;
gdjs.menu2Code.GDperso_9595vendeur3Objects3.length = 0;
gdjs.menu2Code.GDNewSpriteObjects1.length = 0;
gdjs.menu2Code.GDNewSpriteObjects2.length = 0;
gdjs.menu2Code.GDNewSpriteObjects3.length = 0;
gdjs.menu2Code.GDperso_9595alexObjects1.length = 0;
gdjs.menu2Code.GDperso_9595alexObjects2.length = 0;
gdjs.menu2Code.GDperso_9595alexObjects3.length = 0;
gdjs.menu2Code.GDperso_9595vendeur1Objects1.length = 0;
gdjs.menu2Code.GDperso_9595vendeur1Objects2.length = 0;
gdjs.menu2Code.GDperso_9595vendeur1Objects3.length = 0;
gdjs.menu2Code.GDperso_9595vendeur2Objects1.length = 0;
gdjs.menu2Code.GDperso_9595vendeur2Objects2.length = 0;
gdjs.menu2Code.GDperso_9595vendeur2Objects3.length = 0;
gdjs.menu2Code.GDfond_9595menuObjects1.length = 0;
gdjs.menu2Code.GDfond_9595menuObjects2.length = 0;
gdjs.menu2Code.GDfond_9595menuObjects3.length = 0;
gdjs.menu2Code.GDbouton_9595optionsObjects1.length = 0;
gdjs.menu2Code.GDbouton_9595optionsObjects2.length = 0;
gdjs.menu2Code.GDbouton_9595optionsObjects3.length = 0;
gdjs.menu2Code.GDserge_9595lamaObjects1.length = 0;
gdjs.menu2Code.GDserge_9595lamaObjects2.length = 0;
gdjs.menu2Code.GDserge_9595lamaObjects3.length = 0;
gdjs.menu2Code.GDauteurObjects1.length = 0;
gdjs.menu2Code.GDauteurObjects2.length = 0;
gdjs.menu2Code.GDauteurObjects3.length = 0;
gdjs.menu2Code.GDimagesObjects1.length = 0;
gdjs.menu2Code.GDimagesObjects2.length = 0;
gdjs.menu2Code.GDimagesObjects3.length = 0;
gdjs.menu2Code.GDbouton_9595infoObjects1.length = 0;
gdjs.menu2Code.GDbouton_9595infoObjects2.length = 0;
gdjs.menu2Code.GDbouton_9595infoObjects3.length = 0;
gdjs.menu2Code.GDbouton_9595retourObjects1.length = 0;
gdjs.menu2Code.GDbouton_9595retourObjects2.length = 0;
gdjs.menu2Code.GDbouton_9595retourObjects3.length = 0;
gdjs.menu2Code.GDMetalRedBarObjects1.length = 0;
gdjs.menu2Code.GDMetalRedBarObjects2.length = 0;
gdjs.menu2Code.GDMetalRedBarObjects3.length = 0;

gdjs.menu2Code.eventsList7(runtimeScene);

return;

}

gdjs['menu2Code'] = gdjs.menu2Code;
