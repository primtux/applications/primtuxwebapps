gdjs.infosCode = {};
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscoreObjects1= [];
gdjs.infosCode.GDscoreObjects2= [];
gdjs.infosCode.GDaide_95sergeObjects1= [];
gdjs.infosCode.GDaide_95sergeObjects2= [];
gdjs.infosCode.GDdominoObjects1= [];
gdjs.infosCode.GDdominoObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDsouris_95Objects1= [];
gdjs.infosCode.GDsouris_95Objects2= [];
gdjs.infosCode.GDbouton_95aideObjects1= [];
gdjs.infosCode.GDbouton_95aideObjects2= [];
gdjs.infosCode.GDsourisb_95Objects1= [];
gdjs.infosCode.GDsourisb_95Objects2= [];
gdjs.infosCode.GDlamaObjects1= [];
gdjs.infosCode.GDlamaObjects2= [];
gdjs.infosCode.GDdrapeau_951Objects1= [];
gdjs.infosCode.GDdrapeau_951Objects2= [];
gdjs.infosCode.GDdrapeau_952Objects1= [];
gdjs.infosCode.GDdrapeau_952Objects2= [];
gdjs.infosCode.GDpoint_95passageObjects1= [];
gdjs.infosCode.GDpoint_95passageObjects2= [];
gdjs.infosCode.GDfond_95infoObjects1= [];
gdjs.infosCode.GDfond_95infoObjects2= [];
gdjs.infosCode.GDinfos_952Objects1= [];
gdjs.infosCode.GDinfos_952Objects2= [];
gdjs.infosCode.GDinfos_953Objects1= [];
gdjs.infosCode.GDinfos_953Objects2= [];
gdjs.infosCode.GDinfos_954Objects1= [];
gdjs.infosCode.GDinfos_954Objects2= [];
gdjs.infosCode.GDinfos_955Objects1= [];
gdjs.infosCode.GDinfos_955Objects2= [];
gdjs.infosCode.GDinfos_9562Objects1= [];
gdjs.infosCode.GDinfos_9562Objects2= [];
gdjs.infosCode.GDinfos_956Objects1= [];
gdjs.infosCode.GDinfos_956Objects2= [];
gdjs.infosCode.GDinfos_95Objects1= [];
gdjs.infosCode.GDinfos_95Objects2= [];
gdjs.infosCode.GDinfos_951Objects1= [];
gdjs.infosCode.GDinfos_951Objects2= [];
gdjs.infosCode.GDNewObjectObjects1= [];
gdjs.infosCode.GDNewObjectObjects2= [];
gdjs.infosCode.GDconsigne2Objects1= [];
gdjs.infosCode.GDconsigne2Objects2= [];
gdjs.infosCode.GDconsigne3Objects1= [];
gdjs.infosCode.GDconsigne3Objects2= [];
gdjs.infosCode.GDconsigne4Objects1= [];
gdjs.infosCode.GDconsigne4Objects2= [];
gdjs.infosCode.GDconsigne5Objects1= [];
gdjs.infosCode.GDconsigne5Objects2= [];
gdjs.infosCode.GDconsigneObjects1= [];
gdjs.infosCode.GDconsigneObjects2= [];
gdjs.infosCode.GDRightArrowButtonObjects1= [];
gdjs.infosCode.GDRightArrowButtonObjects2= [];
gdjs.infosCode.GDnuageObjects1= [];
gdjs.infosCode.GDnuageObjects2= [];
gdjs.infosCode.GDconsigne_95intervalleObjects1= [];
gdjs.infosCode.GDconsigne_95intervalleObjects2= [];
gdjs.infosCode.GDsergeObjects1= [];
gdjs.infosCode.GDsergeObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score"), gdjs.infosCode.GDscoreObjects1);
gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.infosCode.GDsergeObjects1);
{for(var i = 0, len = gdjs.infosCode.GDscoreObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDscoreObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.infosCode.GDsergeObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDsergeObjects1[i].flipX(true);
}
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscoreObjects1.length = 0;
gdjs.infosCode.GDscoreObjects2.length = 0;
gdjs.infosCode.GDaide_95sergeObjects1.length = 0;
gdjs.infosCode.GDaide_95sergeObjects2.length = 0;
gdjs.infosCode.GDdominoObjects1.length = 0;
gdjs.infosCode.GDdominoObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDsouris_95Objects1.length = 0;
gdjs.infosCode.GDsouris_95Objects2.length = 0;
gdjs.infosCode.GDbouton_95aideObjects1.length = 0;
gdjs.infosCode.GDbouton_95aideObjects2.length = 0;
gdjs.infosCode.GDsourisb_95Objects1.length = 0;
gdjs.infosCode.GDsourisb_95Objects2.length = 0;
gdjs.infosCode.GDlamaObjects1.length = 0;
gdjs.infosCode.GDlamaObjects2.length = 0;
gdjs.infosCode.GDdrapeau_951Objects1.length = 0;
gdjs.infosCode.GDdrapeau_951Objects2.length = 0;
gdjs.infosCode.GDdrapeau_952Objects1.length = 0;
gdjs.infosCode.GDdrapeau_952Objects2.length = 0;
gdjs.infosCode.GDpoint_95passageObjects1.length = 0;
gdjs.infosCode.GDpoint_95passageObjects2.length = 0;
gdjs.infosCode.GDfond_95infoObjects1.length = 0;
gdjs.infosCode.GDfond_95infoObjects2.length = 0;
gdjs.infosCode.GDinfos_952Objects1.length = 0;
gdjs.infosCode.GDinfos_952Objects2.length = 0;
gdjs.infosCode.GDinfos_953Objects1.length = 0;
gdjs.infosCode.GDinfos_953Objects2.length = 0;
gdjs.infosCode.GDinfos_954Objects1.length = 0;
gdjs.infosCode.GDinfos_954Objects2.length = 0;
gdjs.infosCode.GDinfos_955Objects1.length = 0;
gdjs.infosCode.GDinfos_955Objects2.length = 0;
gdjs.infosCode.GDinfos_9562Objects1.length = 0;
gdjs.infosCode.GDinfos_9562Objects2.length = 0;
gdjs.infosCode.GDinfos_956Objects1.length = 0;
gdjs.infosCode.GDinfos_956Objects2.length = 0;
gdjs.infosCode.GDinfos_95Objects1.length = 0;
gdjs.infosCode.GDinfos_95Objects2.length = 0;
gdjs.infosCode.GDinfos_951Objects1.length = 0;
gdjs.infosCode.GDinfos_951Objects2.length = 0;
gdjs.infosCode.GDNewObjectObjects1.length = 0;
gdjs.infosCode.GDNewObjectObjects2.length = 0;
gdjs.infosCode.GDconsigne2Objects1.length = 0;
gdjs.infosCode.GDconsigne2Objects2.length = 0;
gdjs.infosCode.GDconsigne3Objects1.length = 0;
gdjs.infosCode.GDconsigne3Objects2.length = 0;
gdjs.infosCode.GDconsigne4Objects1.length = 0;
gdjs.infosCode.GDconsigne4Objects2.length = 0;
gdjs.infosCode.GDconsigne5Objects1.length = 0;
gdjs.infosCode.GDconsigne5Objects2.length = 0;
gdjs.infosCode.GDconsigneObjects1.length = 0;
gdjs.infosCode.GDconsigneObjects2.length = 0;
gdjs.infosCode.GDRightArrowButtonObjects1.length = 0;
gdjs.infosCode.GDRightArrowButtonObjects2.length = 0;
gdjs.infosCode.GDnuageObjects1.length = 0;
gdjs.infosCode.GDnuageObjects2.length = 0;
gdjs.infosCode.GDconsigne_95intervalleObjects1.length = 0;
gdjs.infosCode.GDconsigne_95intervalleObjects2.length = 0;
gdjs.infosCode.GDsergeObjects1.length = 0;
gdjs.infosCode.GDsergeObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
