// Temps d'affichage du mot en millisecondes
var tempsAffichage = 2000;
// Fonte par défaut, "script" ou "cursive"
var Fonte = "script";
// Nombre de mots à proposer par exercice
var nbPhrasesBase = 5;
// Nombre de clics autorisés sur le bouton [Revoir]
var nbClicsRevoir = 2;
