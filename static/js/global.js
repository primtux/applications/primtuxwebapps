const PRIMTUXMENU_ORIGIN_URL = 'http://127.0.0.1:5500';

function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

function go_back_primtuxmenu() {
    referrer = getCookie('primtuxmenu_url');
    if (!referrer)
        referrer = PRIMTUXMENU_ORIGIN_URL;
    location.href = referrer;
}
