gdjs.menuCode = {};
gdjs.menuCode.GDchienObjects1_1final = [];

gdjs.menuCode.GDlama_95courseObjects1_1final = [];

gdjs.menuCode.GDniv1Objects1_1final = [];

gdjs.menuCode.GDniv2Objects1_1final = [];

gdjs.menuCode.GDniv3Objects1_1final = [];

gdjs.menuCode.GDscore1Objects1_1final = [];

gdjs.menuCode.GDscore2Objects1_1final = [];

gdjs.menuCode.GDscore3Objects1_1final = [];

gdjs.menuCode.GDsourisObjects1_1final = [];

gdjs.menuCode.GDlama_95courseObjects1= [];
gdjs.menuCode.GDlama_95courseObjects2= [];
gdjs.menuCode.GDlama_95courseObjects3= [];
gdjs.menuCode.GDfond_95courseObjects1= [];
gdjs.menuCode.GDfond_95courseObjects2= [];
gdjs.menuCode.GDfond_95courseObjects3= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDscore1Objects3= [];
gdjs.menuCode.GDscore3Objects1= [];
gdjs.menuCode.GDscore3Objects2= [];
gdjs.menuCode.GDscore3Objects3= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore2Objects3= [];
gdjs.menuCode.GDfond_95titreObjects1= [];
gdjs.menuCode.GDfond_95titreObjects2= [];
gdjs.menuCode.GDfond_95titreObjects3= [];
gdjs.menuCode.GDbouton_95retourObjects1= [];
gdjs.menuCode.GDbouton_95retourObjects2= [];
gdjs.menuCode.GDbouton_95retourObjects3= [];
gdjs.menuCode.GDniv1Objects1= [];
gdjs.menuCode.GDniv1Objects2= [];
gdjs.menuCode.GDniv1Objects3= [];
gdjs.menuCode.GDniv3Objects1= [];
gdjs.menuCode.GDniv3Objects2= [];
gdjs.menuCode.GDniv3Objects3= [];
gdjs.menuCode.GDniv2Objects1= [];
gdjs.menuCode.GDniv2Objects2= [];
gdjs.menuCode.GDniv2Objects3= [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDtitreObjects3= [];
gdjs.menuCode.GDsourisObjects1= [];
gdjs.menuCode.GDsourisObjects2= [];
gdjs.menuCode.GDsourisObjects3= [];
gdjs.menuCode.GDchienObjects1= [];
gdjs.menuCode.GDchienObjects2= [];
gdjs.menuCode.GDchienObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDNewObjectObjects1= [];
gdjs.menuCode.GDNewObjectObjects2= [];
gdjs.menuCode.GDNewObjectObjects3= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDcompetencesObjects3= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDsoustitreObjects3= [];
gdjs.menuCode.GDforme1Objects1= [];
gdjs.menuCode.GDforme1Objects2= [];
gdjs.menuCode.GDforme1Objects3= [];
gdjs.menuCode.GDforme2Objects1= [];
gdjs.menuCode.GDforme2Objects2= [];
gdjs.menuCode.GDforme2Objects3= [];
gdjs.menuCode.GDforme3Objects1= [];
gdjs.menuCode.GDforme3Objects2= [];
gdjs.menuCode.GDforme3Objects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDcreditsObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDdrapeauxObjects1= [];
gdjs.menuCode.GDdrapeauxObjects2= [];
gdjs.menuCode.GDdrapeauxObjects3= [];
gdjs.menuCode.GDbouton_95infosObjects1= [];
gdjs.menuCode.GDbouton_95infosObjects2= [];
gdjs.menuCode.GDbouton_95infosObjects3= [];

gdjs.menuCode.conditionTrue_0 = {val:false};
gdjs.menuCode.condition0IsTrue_0 = {val:false};
gdjs.menuCode.condition1IsTrue_0 = {val:false};
gdjs.menuCode.condition2IsTrue_0 = {val:false};
gdjs.menuCode.condition3IsTrue_0 = {val:false};
gdjs.menuCode.conditionTrue_1 = {val:false};
gdjs.menuCode.condition0IsTrue_1 = {val:false};
gdjs.menuCode.condition1IsTrue_1 = {val:false};
gdjs.menuCode.condition2IsTrue_1 = {val:false};
gdjs.menuCode.condition3IsTrue_1 = {val:false};


gdjs.menuCode.eventsList0 = function(runtimeScene) {

};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}}

}


{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne_carre.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne_cercle.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne_rectangle.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne_triangle.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/vrai.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/faux.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/applaudissements.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/bulle_1.mp3");
}}

}


};gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv3Objects2Objects = Hashtable.newFrom({"niv3": gdjs.menuCode.GDniv3Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore3Objects2Objects = Hashtable.newFrom({"score3": gdjs.menuCode.GDscore3Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDlama_9595courseObjects2Objects = Hashtable.newFrom({"lama_course": gdjs.menuCode.GDlama_95courseObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv2Objects2Objects = Hashtable.newFrom({"niv2": gdjs.menuCode.GDniv2Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDchienObjects2Objects = Hashtable.newFrom({"chien": gdjs.menuCode.GDchienObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore2Objects2Objects = Hashtable.newFrom({"score2": gdjs.menuCode.GDscore2Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv1Objects2Objects = Hashtable.newFrom({"niv1": gdjs.menuCode.GDniv1Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsourisObjects2Objects = Hashtable.newFrom({"souris": gdjs.menuCode.GDsourisObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore1Objects2Objects = Hashtable.newFrom({"score1": gdjs.menuCode.GDscore1Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infosObjects1Objects = Hashtable.newFrom({"bouton_infos": gdjs.menuCode.GDbouton_95infosObjects1});
gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
gdjs.copyArray(runtimeScene.getObjects("score3"), gdjs.menuCode.GDscore3Objects1);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 2, false, 100, 1);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu2")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore3Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore3Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu3")));
}
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.menuCode.GDlama_95courseObjects1.length = 0;

gdjs.menuCode.GDniv3Objects1.length = 0;

gdjs.menuCode.GDscore3Objects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDlama_95courseObjects1_1final.length = 0;gdjs.menuCode.GDniv3Objects1_1final.length = 0;gdjs.menuCode.GDscore3Objects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
gdjs.menuCode.condition1IsTrue_1.val = false;
gdjs.menuCode.condition2IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("niv3"), gdjs.menuCode.GDniv3Objects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv3Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDniv3Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDniv3Objects1_1final.indexOf(gdjs.menuCode.GDniv3Objects2[j]) === -1 )
            gdjs.menuCode.GDniv3Objects1_1final.push(gdjs.menuCode.GDniv3Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score3"), gdjs.menuCode.GDscore3Objects2);
gdjs.menuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore3Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition1IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDscore3Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDscore3Objects1_1final.indexOf(gdjs.menuCode.GDscore3Objects2[j]) === -1 )
            gdjs.menuCode.GDscore3Objects1_1final.push(gdjs.menuCode.GDscore3Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("lama_course"), gdjs.menuCode.GDlama_95courseObjects2);
gdjs.menuCode.condition2IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDlama_9595courseObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition2IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDlama_95courseObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDlama_95courseObjects1_1final.indexOf(gdjs.menuCode.GDlama_95courseObjects2[j]) === -1 )
            gdjs.menuCode.GDlama_95courseObjects1_1final.push(gdjs.menuCode.GDlama_95courseObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDlama_95courseObjects1_1final, gdjs.menuCode.GDlama_95courseObjects1);
gdjs.copyArray(gdjs.menuCode.GDniv3Objects1_1final, gdjs.menuCode.GDniv3Objects1);
gdjs.copyArray(gdjs.menuCode.GDscore3Objects1_1final, gdjs.menuCode.GDscore3Objects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(3);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu3").setNumber(0);
}}

}


{

gdjs.menuCode.GDchienObjects1.length = 0;

gdjs.menuCode.GDniv2Objects1.length = 0;

gdjs.menuCode.GDscore2Objects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDchienObjects1_1final.length = 0;gdjs.menuCode.GDniv2Objects1_1final.length = 0;gdjs.menuCode.GDscore2Objects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
gdjs.menuCode.condition1IsTrue_1.val = false;
gdjs.menuCode.condition2IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("niv2"), gdjs.menuCode.GDniv2Objects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv2Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDniv2Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDniv2Objects1_1final.indexOf(gdjs.menuCode.GDniv2Objects2[j]) === -1 )
            gdjs.menuCode.GDniv2Objects1_1final.push(gdjs.menuCode.GDniv2Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("chien"), gdjs.menuCode.GDchienObjects2);
gdjs.menuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDchienObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition1IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDchienObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDchienObjects1_1final.indexOf(gdjs.menuCode.GDchienObjects2[j]) === -1 )
            gdjs.menuCode.GDchienObjects1_1final.push(gdjs.menuCode.GDchienObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
gdjs.menuCode.condition2IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore2Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition2IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDscore2Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDscore2Objects1_1final.indexOf(gdjs.menuCode.GDscore2Objects2[j]) === -1 )
            gdjs.menuCode.GDscore2Objects1_1final.push(gdjs.menuCode.GDscore2Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDchienObjects1_1final, gdjs.menuCode.GDchienObjects1);
gdjs.copyArray(gdjs.menuCode.GDniv2Objects1_1final, gdjs.menuCode.GDniv2Objects1);
gdjs.copyArray(gdjs.menuCode.GDscore2Objects1_1final, gdjs.menuCode.GDscore2Objects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu2").setNumber(0);
}}

}


{

gdjs.menuCode.GDniv1Objects1.length = 0;

gdjs.menuCode.GDscore1Objects1.length = 0;

gdjs.menuCode.GDsourisObjects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDniv1Objects1_1final.length = 0;gdjs.menuCode.GDscore1Objects1_1final.length = 0;gdjs.menuCode.GDsourisObjects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
gdjs.menuCode.condition1IsTrue_1.val = false;
gdjs.menuCode.condition2IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("niv1"), gdjs.menuCode.GDniv1Objects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDniv1Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDniv1Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDniv1Objects1_1final.indexOf(gdjs.menuCode.GDniv1Objects2[j]) === -1 )
            gdjs.menuCode.GDniv1Objects1_1final.push(gdjs.menuCode.GDniv1Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("souris"), gdjs.menuCode.GDsourisObjects2);
gdjs.menuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsourisObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition1IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDsourisObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDsourisObjects1_1final.indexOf(gdjs.menuCode.GDsourisObjects2[j]) === -1 )
            gdjs.menuCode.GDsourisObjects1_1final.push(gdjs.menuCode.GDsourisObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects2);
gdjs.menuCode.condition2IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore1Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition2IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDscore1Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDscore1Objects1_1final.indexOf(gdjs.menuCode.GDscore1Objects2[j]) === -1 )
            gdjs.menuCode.GDscore1Objects1_1final.push(gdjs.menuCode.GDscore1Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDniv1Objects1_1final, gdjs.menuCode.GDniv1Objects1);
gdjs.copyArray(gdjs.menuCode.GDscore1Objects1_1final, gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(gdjs.menuCode.GDsourisObjects1_1final, gdjs.menuCode.GDsourisObjects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu1").setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition2IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9777556);
}
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) >= 5;
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
gdjs.copyArray(runtimeScene.getObjects("score3"), gdjs.menuCode.GDscore3Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu1").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu2").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu3").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu2")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore3Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore3Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("jeu3")));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_infos"), gdjs.menuCode.GDbouton_95infosObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infosObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
gdjs.menuCode.condition2IsTrue_0.val = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") > 0.5;
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{


{
}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDlama_95courseObjects1.length = 0;
gdjs.menuCode.GDlama_95courseObjects2.length = 0;
gdjs.menuCode.GDlama_95courseObjects3.length = 0;
gdjs.menuCode.GDfond_95courseObjects1.length = 0;
gdjs.menuCode.GDfond_95courseObjects2.length = 0;
gdjs.menuCode.GDfond_95courseObjects3.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDscore1Objects3.length = 0;
gdjs.menuCode.GDscore3Objects1.length = 0;
gdjs.menuCode.GDscore3Objects2.length = 0;
gdjs.menuCode.GDscore3Objects3.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore2Objects3.length = 0;
gdjs.menuCode.GDfond_95titreObjects1.length = 0;
gdjs.menuCode.GDfond_95titreObjects2.length = 0;
gdjs.menuCode.GDfond_95titreObjects3.length = 0;
gdjs.menuCode.GDbouton_95retourObjects1.length = 0;
gdjs.menuCode.GDbouton_95retourObjects2.length = 0;
gdjs.menuCode.GDbouton_95retourObjects3.length = 0;
gdjs.menuCode.GDniv1Objects1.length = 0;
gdjs.menuCode.GDniv1Objects2.length = 0;
gdjs.menuCode.GDniv1Objects3.length = 0;
gdjs.menuCode.GDniv3Objects1.length = 0;
gdjs.menuCode.GDniv3Objects2.length = 0;
gdjs.menuCode.GDniv3Objects3.length = 0;
gdjs.menuCode.GDniv2Objects1.length = 0;
gdjs.menuCode.GDniv2Objects2.length = 0;
gdjs.menuCode.GDniv2Objects3.length = 0;
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDsourisObjects1.length = 0;
gdjs.menuCode.GDsourisObjects2.length = 0;
gdjs.menuCode.GDsourisObjects3.length = 0;
gdjs.menuCode.GDchienObjects1.length = 0;
gdjs.menuCode.GDchienObjects2.length = 0;
gdjs.menuCode.GDchienObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDNewObjectObjects1.length = 0;
gdjs.menuCode.GDNewObjectObjects2.length = 0;
gdjs.menuCode.GDNewObjectObjects3.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects3.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects3.length = 0;
gdjs.menuCode.GDforme1Objects1.length = 0;
gdjs.menuCode.GDforme1Objects2.length = 0;
gdjs.menuCode.GDforme1Objects3.length = 0;
gdjs.menuCode.GDforme2Objects1.length = 0;
gdjs.menuCode.GDforme2Objects2.length = 0;
gdjs.menuCode.GDforme2Objects3.length = 0;
gdjs.menuCode.GDforme3Objects1.length = 0;
gdjs.menuCode.GDforme3Objects2.length = 0;
gdjs.menuCode.GDforme3Objects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDdrapeauxObjects1.length = 0;
gdjs.menuCode.GDdrapeauxObjects2.length = 0;
gdjs.menuCode.GDdrapeauxObjects3.length = 0;
gdjs.menuCode.GDbouton_95infosObjects1.length = 0;
gdjs.menuCode.GDbouton_95infosObjects2.length = 0;
gdjs.menuCode.GDbouton_95infosObjects3.length = 0;

gdjs.menuCode.eventsList2(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
