gdjs.infosCode = {};
gdjs.infosCode.GDlama_95courseObjects1= [];
gdjs.infosCode.GDlama_95courseObjects2= [];
gdjs.infosCode.GDlama_95courseObjects3= [];
gdjs.infosCode.GDfond_95courseObjects1= [];
gdjs.infosCode.GDfond_95courseObjects2= [];
gdjs.infosCode.GDfond_95courseObjects3= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDscore1Objects3= [];
gdjs.infosCode.GDscore3Objects1= [];
gdjs.infosCode.GDscore3Objects2= [];
gdjs.infosCode.GDscore3Objects3= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscore2Objects3= [];
gdjs.infosCode.GDfond_95titreObjects1= [];
gdjs.infosCode.GDfond_95titreObjects2= [];
gdjs.infosCode.GDfond_95titreObjects3= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects3= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDfond_95blancObjects3= [];
gdjs.infosCode.GDinfos_95Objects1= [];
gdjs.infosCode.GDinfos_95Objects2= [];
gdjs.infosCode.GDinfos_95Objects3= [];
gdjs.infosCode.GDinfos_955Objects1= [];
gdjs.infosCode.GDinfos_955Objects2= [];
gdjs.infosCode.GDinfos_955Objects3= [];
gdjs.infosCode.GDinfos_952Objects1= [];
gdjs.infosCode.GDinfos_952Objects2= [];
gdjs.infosCode.GDinfos_952Objects3= [];
gdjs.infosCode.GDinfos_953Objects1= [];
gdjs.infosCode.GDinfos_953Objects2= [];
gdjs.infosCode.GDinfos_953Objects3= [];
gdjs.infosCode.GDinfos_954Objects1= [];
gdjs.infosCode.GDinfos_954Objects2= [];
gdjs.infosCode.GDinfos_954Objects3= [];
gdjs.infosCode.GDcapture_95niv1Objects1= [];
gdjs.infosCode.GDcapture_95niv1Objects2= [];
gdjs.infosCode.GDcapture_95niv1Objects3= [];
gdjs.infosCode.GDcapture_95niv2Objects1= [];
gdjs.infosCode.GDcapture_95niv2Objects2= [];
gdjs.infosCode.GDcapture_95niv2Objects3= [];
gdjs.infosCode.GDcapture_95niv3Objects1= [];
gdjs.infosCode.GDcapture_95niv3Objects2= [];
gdjs.infosCode.GDcapture_95niv3Objects3= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};
gdjs.infosCode.condition3IsTrue_0 = {val:false};


gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


{
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(100);
}
}}

}


};gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList1 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.infosCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
gdjs.infosCode.condition2IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.infosCode.condition1IsTrue_0.val ) {
{
gdjs.infosCode.condition2IsTrue_0.val = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") > 0.5;
}}
}
if (gdjs.infosCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDlama_95courseObjects1.length = 0;
gdjs.infosCode.GDlama_95courseObjects2.length = 0;
gdjs.infosCode.GDlama_95courseObjects3.length = 0;
gdjs.infosCode.GDfond_95courseObjects1.length = 0;
gdjs.infosCode.GDfond_95courseObjects2.length = 0;
gdjs.infosCode.GDfond_95courseObjects3.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDscore1Objects3.length = 0;
gdjs.infosCode.GDscore3Objects1.length = 0;
gdjs.infosCode.GDscore3Objects2.length = 0;
gdjs.infosCode.GDscore3Objects3.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscore2Objects3.length = 0;
gdjs.infosCode.GDfond_95titreObjects1.length = 0;
gdjs.infosCode.GDfond_95titreObjects2.length = 0;
gdjs.infosCode.GDfond_95titreObjects3.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects3.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects3.length = 0;
gdjs.infosCode.GDinfos_95Objects1.length = 0;
gdjs.infosCode.GDinfos_95Objects2.length = 0;
gdjs.infosCode.GDinfos_95Objects3.length = 0;
gdjs.infosCode.GDinfos_955Objects1.length = 0;
gdjs.infosCode.GDinfos_955Objects2.length = 0;
gdjs.infosCode.GDinfos_955Objects3.length = 0;
gdjs.infosCode.GDinfos_952Objects1.length = 0;
gdjs.infosCode.GDinfos_952Objects2.length = 0;
gdjs.infosCode.GDinfos_952Objects3.length = 0;
gdjs.infosCode.GDinfos_953Objects1.length = 0;
gdjs.infosCode.GDinfos_953Objects2.length = 0;
gdjs.infosCode.GDinfos_953Objects3.length = 0;
gdjs.infosCode.GDinfos_954Objects1.length = 0;
gdjs.infosCode.GDinfos_954Objects2.length = 0;
gdjs.infosCode.GDinfos_954Objects3.length = 0;
gdjs.infosCode.GDcapture_95niv1Objects1.length = 0;
gdjs.infosCode.GDcapture_95niv1Objects2.length = 0;
gdjs.infosCode.GDcapture_95niv1Objects3.length = 0;
gdjs.infosCode.GDcapture_95niv2Objects1.length = 0;
gdjs.infosCode.GDcapture_95niv2Objects2.length = 0;
gdjs.infosCode.GDcapture_95niv2Objects3.length = 0;
gdjs.infosCode.GDcapture_95niv3Objects1.length = 0;
gdjs.infosCode.GDcapture_95niv3Objects2.length = 0;
gdjs.infosCode.GDcapture_95niv3Objects3.length = 0;

gdjs.infosCode.eventsList1(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
