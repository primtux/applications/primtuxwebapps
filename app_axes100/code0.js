gdjs.menuCode = {};
gdjs.menuCode.GDjeu_950_9510aObjects1_1final = [];

gdjs.menuCode.GDjeu_950_9510bObjects1_1final = [];

gdjs.menuCode.GDjeu_950_9520aObjects1_1final = [];

gdjs.menuCode.GDscore1Objects1_1final = [];

gdjs.menuCode.GDscore2Objects1_1final = [];

gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDdoigtObjects1= [];
gdjs.menuCode.GDdoigtObjects2= [];
gdjs.menuCode.GDdoigt2Objects1= [];
gdjs.menuCode.GDdoigt2Objects2= [];
gdjs.menuCode.GDquestion_95nbObjects1= [];
gdjs.menuCode.GDquestion_95nbObjects2= [];
gdjs.menuCode.GDtrainObjects1= [];
gdjs.menuCode.GDtrainObjects2= [];
gdjs.menuCode.GDtrain2Objects1= [];
gdjs.menuCode.GDtrain2Objects2= [];
gdjs.menuCode.GDajout_95centaineObjects1= [];
gdjs.menuCode.GDajout_95centaineObjects2= [];
gdjs.menuCode.GDajout_95dizaineObjects1= [];
gdjs.menuCode.GDajout_95dizaineObjects2= [];
gdjs.menuCode.GDajout_95uniteObjects1= [];
gdjs.menuCode.GDajout_95uniteObjects2= [];
gdjs.menuCode.GDretrait_95centaineObjects1= [];
gdjs.menuCode.GDretrait_95centaineObjects2= [];
gdjs.menuCode.GDretrait_95dizaineObjects1= [];
gdjs.menuCode.GDretrait_95dizaineObjects2= [];
gdjs.menuCode.GDretrait_95uniteObjects1= [];
gdjs.menuCode.GDretrait_95uniteObjects2= [];
gdjs.menuCode.GDnb_95unitesObjects1= [];
gdjs.menuCode.GDnb_95unitesObjects2= [];
gdjs.menuCode.GDnb_95centainesObjects1= [];
gdjs.menuCode.GDnb_95centainesObjects2= [];
gdjs.menuCode.GDnb_95dizainesObjects1= [];
gdjs.menuCode.GDnb_95dizainesObjects2= [];
gdjs.menuCode.GDaxe_95fleche_95reponseObjects1= [];
gdjs.menuCode.GDaxe_95fleche_95reponseObjects2= [];
gdjs.menuCode.GDbouton_95retourObjects1= [];
gdjs.menuCode.GDbouton_95retourObjects2= [];
gdjs.menuCode.GDmaison2Objects1= [];
gdjs.menuCode.GDmaison2Objects2= [];
gdjs.menuCode.GDmaison3Objects1= [];
gdjs.menuCode.GDmaison3Objects2= [];
gdjs.menuCode.GDmaison4Objects1= [];
gdjs.menuCode.GDmaison4Objects2= [];
gdjs.menuCode.GDmaison5Objects1= [];
gdjs.menuCode.GDmaison5Objects2= [];
gdjs.menuCode.GDmaison1Objects1= [];
gdjs.menuCode.GDmaison1Objects2= [];
gdjs.menuCode.GDrouteObjects1= [];
gdjs.menuCode.GDrouteObjects2= [];
gdjs.menuCode.GDville1Objects1= [];
gdjs.menuCode.GDville1Objects2= [];
gdjs.menuCode.GDfond_95blancObjects1= [];
gdjs.menuCode.GDfond_95blancObjects2= [];
gdjs.menuCode.GDjeu_950_9510bObjects1= [];
gdjs.menuCode.GDjeu_950_9510bObjects2= [];
gdjs.menuCode.GDjeu_950_9520aObjects1= [];
gdjs.menuCode.GDjeu_950_9520aObjects2= [];
gdjs.menuCode.GDjeu_950_9510aObjects1= [];
gdjs.menuCode.GDjeu_950_9510aObjects2= [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDtrain2Objects1= [];
gdjs.menuCode.GDtrain2Objects2= [];
gdjs.menuCode.GDoptionsObjects1= [];
gdjs.menuCode.GDoptionsObjects2= [];
gdjs.menuCode.GDnombre100Objects1= [];
gdjs.menuCode.GDnombre100Objects2= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDplein_95ecranObjects1= [];
gdjs.menuCode.GDplein_95ecranObjects2= [];
gdjs.menuCode.GDbouton_95infoObjects1= [];
gdjs.menuCode.GDbouton_95infoObjects2= [];

gdjs.menuCode.conditionTrue_0 = {val:false};
gdjs.menuCode.condition0IsTrue_0 = {val:false};
gdjs.menuCode.condition1IsTrue_0 = {val:false};
gdjs.menuCode.condition2IsTrue_0 = {val:false};
gdjs.menuCode.condition3IsTrue_0 = {val:false};
gdjs.menuCode.condition4IsTrue_0 = {val:false};
gdjs.menuCode.conditionTrue_1 = {val:false};
gdjs.menuCode.condition0IsTrue_1 = {val:false};
gdjs.menuCode.condition1IsTrue_1 = {val:false};
gdjs.menuCode.condition2IsTrue_1 = {val:false};
gdjs.menuCode.condition3IsTrue_1 = {val:false};
gdjs.menuCode.condition4IsTrue_1 = {val:false};


gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infoObjects1Objects = Hashtable.newFrom({"bouton_info": gdjs.menuCode.GDbouton_95infoObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu_95950_959510aObjects2Objects = Hashtable.newFrom({"jeu_0_10a": gdjs.menuCode.GDjeu_950_9510aObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore1Objects2Objects = Hashtable.newFrom({"score1": gdjs.menuCode.GDscore1Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu_95950_959510bObjects2Objects = Hashtable.newFrom({"jeu_0_10b": gdjs.menuCode.GDjeu_950_9510bObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore2Objects2Objects = Hashtable.newFrom({"score2": gdjs.menuCode.GDscore2Objects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu_95950_959520aObjects2Objects = Hashtable.newFrom({"jeu_0_20a": gdjs.menuCode.GDjeu_950_9520aObjects2});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDoptionsObjects1Objects = Hashtable.newFrom({"options": gdjs.menuCode.GDoptionsObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDplein_9595ecranObjects1Objects = Hashtable.newFrom({"plein_ecran": gdjs.menuCode.GDplein_95ecranObjects1});
gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDplein_9595ecranObjects1Objects = Hashtable.newFrom({"plein_ecran": gdjs.menuCode.GDplein_95ecranObjects1});
gdjs.menuCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_info"), gdjs.menuCode.GDbouton_95infoObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595infoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{runtimeScene.getVariables().get("score_zero").setNumber(0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.menuCode.GDjeu_950_9510aObjects1.length = 0;

gdjs.menuCode.GDscore1Objects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDjeu_950_9510aObjects1_1final.length = 0;gdjs.menuCode.GDscore1Objects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
gdjs.menuCode.condition1IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu_0_10a"), gdjs.menuCode.GDjeu_950_9510aObjects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu_95950_959510aObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDjeu_950_9510aObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDjeu_950_9510aObjects1_1final.indexOf(gdjs.menuCode.GDjeu_950_9510aObjects2[j]) === -1 )
            gdjs.menuCode.GDjeu_950_9510aObjects1_1final.push(gdjs.menuCode.GDjeu_950_9510aObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects2);
gdjs.menuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore1Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition1IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDscore1Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDscore1Objects1_1final.indexOf(gdjs.menuCode.GDscore1Objects2[j]) === -1 )
            gdjs.menuCode.GDscore1Objects1_1final.push(gdjs.menuCode.GDscore1Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu_950_9510aObjects1_1final, gdjs.menuCode.GDjeu_950_9510aObjects1);
gdjs.copyArray(gdjs.menuCode.GDscore1Objects1_1final, gdjs.menuCode.GDscore1Objects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.menuCode.GDjeu_950_9510bObjects1.length = 0;

gdjs.menuCode.GDscore2Objects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDjeu_950_9510bObjects1_1final.length = 0;gdjs.menuCode.GDscore2Objects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
gdjs.menuCode.condition1IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu_0_10b"), gdjs.menuCode.GDjeu_950_9510bObjects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu_95950_959510bObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDjeu_950_9510bObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDjeu_950_9510bObjects1_1final.indexOf(gdjs.menuCode.GDjeu_950_9510bObjects2[j]) === -1 )
            gdjs.menuCode.GDjeu_950_9510bObjects1_1final.push(gdjs.menuCode.GDjeu_950_9510bObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
gdjs.menuCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDscore2Objects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition1IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDscore2Objects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDscore2Objects1_1final.indexOf(gdjs.menuCode.GDscore2Objects2[j]) === -1 )
            gdjs.menuCode.GDscore2Objects1_1final.push(gdjs.menuCode.GDscore2Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu_950_9510bObjects1_1final, gdjs.menuCode.GDjeu_950_9510bObjects1);
gdjs.copyArray(gdjs.menuCode.GDscore2Objects1_1final, gdjs.menuCode.GDscore2Objects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.menuCode.GDjeu_950_9520aObjects1.length = 0;


gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition1IsTrue_0;
gdjs.menuCode.GDjeu_950_9520aObjects1_1final.length = 0;gdjs.menuCode.condition0IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu_0_20a"), gdjs.menuCode.GDjeu_950_9520aObjects2);
gdjs.menuCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu_95950_959520aObjects2Objects, runtimeScene, true, false);
if( gdjs.menuCode.condition0IsTrue_1.val ) {
    gdjs.menuCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.menuCode.GDjeu_950_9520aObjects2.length;j<jLen;++j) {
        if ( gdjs.menuCode.GDjeu_950_9520aObjects1_1final.indexOf(gdjs.menuCode.GDjeu_950_9520aObjects2[j]) === -1 )
            gdjs.menuCode.GDjeu_950_9520aObjects1_1final.push(gdjs.menuCode.GDjeu_950_9520aObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu_950_9520aObjects1_1final, gdjs.menuCode.GDjeu_950_9520aObjects1);
}
}
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("mini").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("maxi").setNumber(20);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(3);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition2IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(11434924);
}
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{runtimeScene.getVariables().get("score_zero").add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().get("score_zero")) == 5;
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{runtimeScene.getVariables().get("score_zero").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("train2"), gdjs.menuCode.GDtrain2Objects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.menuCode.GDtrain2Objects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDtrain2Objects1[i].getX() < 400 ) {
        gdjs.menuCode.condition0IsTrue_0.val = true;
        gdjs.menuCode.GDtrain2Objects1[k] = gdjs.menuCode.GDtrain2Objects1[i];
        ++k;
    }
}
gdjs.menuCode.GDtrain2Objects1.length = k;}if (gdjs.menuCode.condition0IsTrue_0.val) {
/* Reuse gdjs.menuCode.GDtrain2Objects1 */
{for(var i = 0, len = gdjs.menuCode.GDtrain2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDtrain2Objects1[i].setX(gdjs.menuCode.GDtrain2Objects1[i].getX() + (4));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("options"), gdjs.menuCode.GDoptionsObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDoptionsObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("plein_ecran"), gdjs.menuCode.GDplein_95ecranObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
gdjs.menuCode.condition3IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDplein_9595ecranObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
gdjs.menuCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 0;
}if ( gdjs.menuCode.condition2IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition3IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(11302292);
}
}}
}
}
if (gdjs.menuCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(9).setNumber(1);
}{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("plein_ecran"), gdjs.menuCode.GDplein_95ecranObjects1);

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
gdjs.menuCode.condition3IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDplein_9595ecranObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
gdjs.menuCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 1;
}if ( gdjs.menuCode.condition2IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition3IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(11304228);
}
}}
}
}
if (gdjs.menuCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(9).setNumber(0);
}{gdjs.evtTools.window.setFullScreen(runtimeScene, false, true);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDdoigtObjects1.length = 0;
gdjs.menuCode.GDdoigtObjects2.length = 0;
gdjs.menuCode.GDdoigt2Objects1.length = 0;
gdjs.menuCode.GDdoigt2Objects2.length = 0;
gdjs.menuCode.GDquestion_95nbObjects1.length = 0;
gdjs.menuCode.GDquestion_95nbObjects2.length = 0;
gdjs.menuCode.GDtrainObjects1.length = 0;
gdjs.menuCode.GDtrainObjects2.length = 0;
gdjs.menuCode.GDtrain2Objects1.length = 0;
gdjs.menuCode.GDtrain2Objects2.length = 0;
gdjs.menuCode.GDajout_95centaineObjects1.length = 0;
gdjs.menuCode.GDajout_95centaineObjects2.length = 0;
gdjs.menuCode.GDajout_95dizaineObjects1.length = 0;
gdjs.menuCode.GDajout_95dizaineObjects2.length = 0;
gdjs.menuCode.GDajout_95uniteObjects1.length = 0;
gdjs.menuCode.GDajout_95uniteObjects2.length = 0;
gdjs.menuCode.GDretrait_95centaineObjects1.length = 0;
gdjs.menuCode.GDretrait_95centaineObjects2.length = 0;
gdjs.menuCode.GDretrait_95dizaineObjects1.length = 0;
gdjs.menuCode.GDretrait_95dizaineObjects2.length = 0;
gdjs.menuCode.GDretrait_95uniteObjects1.length = 0;
gdjs.menuCode.GDretrait_95uniteObjects2.length = 0;
gdjs.menuCode.GDnb_95unitesObjects1.length = 0;
gdjs.menuCode.GDnb_95unitesObjects2.length = 0;
gdjs.menuCode.GDnb_95centainesObjects1.length = 0;
gdjs.menuCode.GDnb_95centainesObjects2.length = 0;
gdjs.menuCode.GDnb_95dizainesObjects1.length = 0;
gdjs.menuCode.GDnb_95dizainesObjects2.length = 0;
gdjs.menuCode.GDaxe_95fleche_95reponseObjects1.length = 0;
gdjs.menuCode.GDaxe_95fleche_95reponseObjects2.length = 0;
gdjs.menuCode.GDbouton_95retourObjects1.length = 0;
gdjs.menuCode.GDbouton_95retourObjects2.length = 0;
gdjs.menuCode.GDmaison2Objects1.length = 0;
gdjs.menuCode.GDmaison2Objects2.length = 0;
gdjs.menuCode.GDmaison3Objects1.length = 0;
gdjs.menuCode.GDmaison3Objects2.length = 0;
gdjs.menuCode.GDmaison4Objects1.length = 0;
gdjs.menuCode.GDmaison4Objects2.length = 0;
gdjs.menuCode.GDmaison5Objects1.length = 0;
gdjs.menuCode.GDmaison5Objects2.length = 0;
gdjs.menuCode.GDmaison1Objects1.length = 0;
gdjs.menuCode.GDmaison1Objects2.length = 0;
gdjs.menuCode.GDrouteObjects1.length = 0;
gdjs.menuCode.GDrouteObjects2.length = 0;
gdjs.menuCode.GDville1Objects1.length = 0;
gdjs.menuCode.GDville1Objects2.length = 0;
gdjs.menuCode.GDfond_95blancObjects1.length = 0;
gdjs.menuCode.GDfond_95blancObjects2.length = 0;
gdjs.menuCode.GDjeu_950_9510bObjects1.length = 0;
gdjs.menuCode.GDjeu_950_9510bObjects2.length = 0;
gdjs.menuCode.GDjeu_950_9520aObjects1.length = 0;
gdjs.menuCode.GDjeu_950_9520aObjects2.length = 0;
gdjs.menuCode.GDjeu_950_9510aObjects1.length = 0;
gdjs.menuCode.GDjeu_950_9510aObjects2.length = 0;
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDtrain2Objects1.length = 0;
gdjs.menuCode.GDtrain2Objects2.length = 0;
gdjs.menuCode.GDoptionsObjects1.length = 0;
gdjs.menuCode.GDoptionsObjects2.length = 0;
gdjs.menuCode.GDnombre100Objects1.length = 0;
gdjs.menuCode.GDnombre100Objects2.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDplein_95ecranObjects1.length = 0;
gdjs.menuCode.GDplein_95ecranObjects2.length = 0;
gdjs.menuCode.GDbouton_95infoObjects1.length = 0;
gdjs.menuCode.GDbouton_95infoObjects2.length = 0;

gdjs.menuCode.eventsList0(runtimeScene);
return;

}

gdjs['menuCode'] = gdjs.menuCode;
