gdjs.menuCode = {};
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDtitreObjects3= [];
gdjs.menuCode.GDjeu2Objects1= [];
gdjs.menuCode.GDjeu2Objects2= [];
gdjs.menuCode.GDjeu2Objects3= [];
gdjs.menuCode.GDjeu1Objects1= [];
gdjs.menuCode.GDjeu1Objects2= [];
gdjs.menuCode.GDjeu1Objects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDbouton_9595jeu1Objects1= [];
gdjs.menuCode.GDbouton_9595jeu1Objects2= [];
gdjs.menuCode.GDbouton_9595jeu1Objects3= [];
gdjs.menuCode.GDfond_9595blancObjects1= [];
gdjs.menuCode.GDfond_9595blancObjects2= [];
gdjs.menuCode.GDfond_9595blancObjects3= [];
gdjs.menuCode.GDtabletteObjects1= [];
gdjs.menuCode.GDtabletteObjects2= [];
gdjs.menuCode.GDtabletteObjects3= [];
gdjs.menuCode.GDplein_9595ecranObjects1= [];
gdjs.menuCode.GDplein_9595ecranObjects2= [];
gdjs.menuCode.GDplein_9595ecranObjects3= [];
gdjs.menuCode.GDimagesObjects1= [];
gdjs.menuCode.GDimagesObjects2= [];
gdjs.menuCode.GDimagesObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDsoustitreObjects3= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDcompetencesObjects3= [];
gdjs.menuCode.GDbouton_9595infosObjects1= [];
gdjs.menuCode.GDbouton_9595infosObjects2= [];
gdjs.menuCode.GDbouton_9595infosObjects3= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore2Objects3= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDscore1Objects3= [];
gdjs.menuCode.GDbouton_9595jeu1Objects1= [];
gdjs.menuCode.GDbouton_9595jeu1Objects2= [];
gdjs.menuCode.GDbouton_9595jeu1Objects3= [];
gdjs.menuCode.GDlamaObjects1= [];
gdjs.menuCode.GDlamaObjects2= [];
gdjs.menuCode.GDlamaObjects3= [];
gdjs.menuCode.GDbouton_9595jeu2Objects1= [];
gdjs.menuCode.GDbouton_9595jeu2Objects2= [];
gdjs.menuCode.GDbouton_9595jeu2Objects3= [];
gdjs.menuCode.GDfond_9595menuObjects1= [];
gdjs.menuCode.GDfond_9595menuObjects2= [];
gdjs.menuCode.GDfond_9595menuObjects3= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_phonolama", "score");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_phonolama", "score", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde_temporaire"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde_temporaire")), runtimeScene.getGame().getVariables().getFromIndex(4));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/4.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/5.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/6.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/7.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/8.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/9.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/10.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/101.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/102.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/103.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/104.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne1.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne2.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/91.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/92.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/93.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/94.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/95.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/96.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/97.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/98.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/99.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/100.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/81.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/82.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/83.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/84.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/85.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/86.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/87.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/88.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/89.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/90.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/71.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/72.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/73.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/74.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/75.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/76.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/77.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/78.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/79.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/80.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/61.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/62.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/63.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/64.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/65.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/66.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/67.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/68.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/69.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/70.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/51.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/52.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/53.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/54.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/55.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/56.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/57.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/58.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/59.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/60.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/41.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/42.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/43.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/44.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/45.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/46.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/47.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/48.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/49.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/50.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/31.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/32.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/33.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/34.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/35.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/36.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/37.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/38.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/39.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/40.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/21.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/22.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/23.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/24.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/25.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/26.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/27.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/28.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/29.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/30.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/11.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/12.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/13.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/14.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/15.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/16.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/17.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/18.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/19.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/20.mp3");
}}

}


};gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects2);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
{for(var i = 0, len = gdjs.menuCode.GDscore1Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("score1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("score2")));
}
}}

}


{


gdjs.menuCode.eventsList1(runtimeScene);
}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595jeu1Objects1Objects = Hashtable.newFrom({"bouton_jeu1": gdjs.menuCode.GDbouton_9595jeu1Objects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595jeu2Objects1Objects = Hashtable.newFrom({"bouton_jeu2": gdjs.menuCode.GDbouton_9595jeu2Objects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(14007468);
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_2.mp3", 1, false, 100, 1);
}}

}


};gdjs.menuCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_phonolama");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_phonolama", "score", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(4)));
}}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects = Hashtable.newFrom({"bouton_infos": gdjs.menuCode.GDbouton_9595infosObjects1});
gdjs.menuCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.menuCode.GDfond_9595blancObjects1);
gdjs.copyArray(runtimeScene.getObjects("lama"), gdjs.menuCode.GDlamaObjects1);
{for(var i = 0, len = gdjs.menuCode.GDlamaObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlamaObjects1[i].flipX(true);
}
}{for(var i = 0, len = gdjs.menuCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDfond_9595blancObjects1[i].setOpacity(150);
}
}
{ //Subevents
gdjs.menuCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_jeu1"), gdjs.menuCode.GDbouton_9595jeu1Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595jeu1Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_jeu2"), gdjs.menuCode.GDbouton_9595jeu2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595jeu2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage2", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("lama"), gdjs.menuCode.GDlamaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDlamaObjects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDlamaObjects1[i].getX() < 224 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDlamaObjects1[k] = gdjs.menuCode.GDlamaObjects1[i];
        ++k;
    }
}
gdjs.menuCode.GDlamaObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDlamaObjects1 */
{for(var i = 0, len = gdjs.menuCode.GDlamaObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlamaObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.menuCode.GDlamaObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlamaObjects1[i].setX(gdjs.menuCode.GDlamaObjects1[i].getX() + (4));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("lama"), gdjs.menuCode.GDlamaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDlamaObjects1.length;i<l;++i) {
    if ( gdjs.menuCode.GDlamaObjects1[i].getX() >= 224 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDlamaObjects1[k] = gdjs.menuCode.GDlamaObjects1[i];
        ++k;
    }
}
gdjs.menuCode.GDlamaObjects1.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDlamaObjects1 */
{for(var i = 0, len = gdjs.menuCode.GDlamaObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDlamaObjects1[i].setAnimation(0);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}
{ //Subevents
gdjs.menuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("score1").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("score1")));
}
}{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("score2").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4).getChild("score2")));
}
}
{ //Subevents
gdjs.menuCode.eventsList4(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_infos"), gdjs.menuCode.GDbouton_9595infosObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDjeu2Objects1.length = 0;
gdjs.menuCode.GDjeu2Objects2.length = 0;
gdjs.menuCode.GDjeu2Objects3.length = 0;
gdjs.menuCode.GDjeu1Objects1.length = 0;
gdjs.menuCode.GDjeu1Objects2.length = 0;
gdjs.menuCode.GDjeu1Objects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDbouton_9595jeu1Objects1.length = 0;
gdjs.menuCode.GDbouton_9595jeu1Objects2.length = 0;
gdjs.menuCode.GDbouton_9595jeu1Objects3.length = 0;
gdjs.menuCode.GDfond_9595blancObjects1.length = 0;
gdjs.menuCode.GDfond_9595blancObjects2.length = 0;
gdjs.menuCode.GDfond_9595blancObjects3.length = 0;
gdjs.menuCode.GDtabletteObjects1.length = 0;
gdjs.menuCode.GDtabletteObjects2.length = 0;
gdjs.menuCode.GDtabletteObjects3.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects1.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects2.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects3.length = 0;
gdjs.menuCode.GDimagesObjects1.length = 0;
gdjs.menuCode.GDimagesObjects2.length = 0;
gdjs.menuCode.GDimagesObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects3.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects3.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects1.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects2.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects3.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore2Objects3.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDscore1Objects3.length = 0;
gdjs.menuCode.GDbouton_9595jeu1Objects1.length = 0;
gdjs.menuCode.GDbouton_9595jeu1Objects2.length = 0;
gdjs.menuCode.GDbouton_9595jeu1Objects3.length = 0;
gdjs.menuCode.GDlamaObjects1.length = 0;
gdjs.menuCode.GDlamaObjects2.length = 0;
gdjs.menuCode.GDlamaObjects3.length = 0;
gdjs.menuCode.GDbouton_9595jeu2Objects1.length = 0;
gdjs.menuCode.GDbouton_9595jeu2Objects2.length = 0;
gdjs.menuCode.GDbouton_9595jeu2Objects3.length = 0;
gdjs.menuCode.GDfond_9595menuObjects1.length = 0;
gdjs.menuCode.GDfond_9595menuObjects2.length = 0;
gdjs.menuCode.GDfond_9595menuObjects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;

gdjs.menuCode.eventsList5(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
