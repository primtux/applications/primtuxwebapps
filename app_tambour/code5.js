gdjs.infosCode = {};
gdjs.infosCode.GDtexte_9595info1Objects1= [];
gdjs.infosCode.GDtexte_9595info1Objects2= [];
gdjs.infosCode.GDtambour_9595mini2Objects1= [];
gdjs.infosCode.GDtambour_9595mini2Objects2= [];
gdjs.infosCode.GDtambour_9595miniObjects1= [];
gdjs.infosCode.GDtambour_9595miniObjects2= [];
gdjs.infosCode.GDtambourObjects1= [];
gdjs.infosCode.GDtambourObjects2= [];
gdjs.infosCode.GDecouterObjects1= [];
gdjs.infosCode.GDecouterObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDtxt_9595versionObjects1= [];
gdjs.infosCode.GDtxt_9595versionObjects2= [];
gdjs.infosCode.GDfond2Objects1= [];
gdjs.infosCode.GDfond2Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDfleche_9595clignotanteObjects1= [];
gdjs.infosCode.GDfleche_9595clignotanteObjects2= [];
gdjs.infosCode.GDbulle_9595penseeObjects1= [];
gdjs.infosCode.GDbulle_9595penseeObjects2= [];
gdjs.infosCode.GDbulle_9595chiffreObjects1= [];
gdjs.infosCode.GDbulle_9595chiffreObjects2= [];
gdjs.infosCode.GDfond_9595musiqueObjects1= [];
gdjs.infosCode.GDfond_9595musiqueObjects2= [];
gdjs.infosCode.GDaideObjects1= [];
gdjs.infosCode.GDaideObjects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDbouton_9595optionsObjects1= [];
gdjs.infosCode.GDbouton_9595optionsObjects2= [];
gdjs.infosCode.GDlamaObjects1= [];
gdjs.infosCode.GDlamaObjects2= [];
gdjs.infosCode.GDpointsObjects1= [];
gdjs.infosCode.GDpointsObjects2= [];
gdjs.infosCode.GDdrapeau1Objects1= [];
gdjs.infosCode.GDdrapeau1Objects2= [];
gdjs.infosCode.GDdrapeau2Objects1= [];
gdjs.infosCode.GDdrapeau2Objects2= [];
gdjs.infosCode.GDsergeObjects1= [];
gdjs.infosCode.GDsergeObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond2"), gdjs.infosCode.GDfond2Objects1);
{for(var i = 0, len = gdjs.infosCode.GDfond2Objects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond2Objects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDtexte_9595info1Objects1.length = 0;
gdjs.infosCode.GDtexte_9595info1Objects2.length = 0;
gdjs.infosCode.GDtambour_9595mini2Objects1.length = 0;
gdjs.infosCode.GDtambour_9595mini2Objects2.length = 0;
gdjs.infosCode.GDtambour_9595miniObjects1.length = 0;
gdjs.infosCode.GDtambour_9595miniObjects2.length = 0;
gdjs.infosCode.GDtambourObjects1.length = 0;
gdjs.infosCode.GDtambourObjects2.length = 0;
gdjs.infosCode.GDecouterObjects1.length = 0;
gdjs.infosCode.GDecouterObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDtxt_9595versionObjects1.length = 0;
gdjs.infosCode.GDtxt_9595versionObjects2.length = 0;
gdjs.infosCode.GDfond2Objects1.length = 0;
gdjs.infosCode.GDfond2Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDfleche_9595clignotanteObjects1.length = 0;
gdjs.infosCode.GDfleche_9595clignotanteObjects2.length = 0;
gdjs.infosCode.GDbulle_9595penseeObjects1.length = 0;
gdjs.infosCode.GDbulle_9595penseeObjects2.length = 0;
gdjs.infosCode.GDbulle_9595chiffreObjects1.length = 0;
gdjs.infosCode.GDbulle_9595chiffreObjects2.length = 0;
gdjs.infosCode.GDfond_9595musiqueObjects1.length = 0;
gdjs.infosCode.GDfond_9595musiqueObjects2.length = 0;
gdjs.infosCode.GDaideObjects1.length = 0;
gdjs.infosCode.GDaideObjects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.infosCode.GDbouton_9595optionsObjects2.length = 0;
gdjs.infosCode.GDlamaObjects1.length = 0;
gdjs.infosCode.GDlamaObjects2.length = 0;
gdjs.infosCode.GDpointsObjects1.length = 0;
gdjs.infosCode.GDpointsObjects2.length = 0;
gdjs.infosCode.GDdrapeau1Objects1.length = 0;
gdjs.infosCode.GDdrapeau1Objects2.length = 0;
gdjs.infosCode.GDdrapeau2Objects1.length = 0;
gdjs.infosCode.GDdrapeau2Objects2.length = 0;
gdjs.infosCode.GDsergeObjects1.length = 0;
gdjs.infosCode.GDsergeObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
