gdjs.menuCode = {};
gdjs.menuCode.GDniveau1Objects1_1final = [];

gdjs.menuCode.GDniveau2Objects1_1final = [];

gdjs.menuCode.GDscore1Objects1_1final = [];

gdjs.menuCode.GDscore2Objects1_1final = [];

gdjs.menuCode.GDtambour_9595mini2Objects1_1final = [];

gdjs.menuCode.GDtambour_9595miniObjects1_1final = [];

gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDtitreObjects3= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDsoustitreObjects3= [];
gdjs.menuCode.GDniveau2Objects1= [];
gdjs.menuCode.GDniveau2Objects2= [];
gdjs.menuCode.GDniveau2Objects3= [];
gdjs.menuCode.GDniveau1Objects1= [];
gdjs.menuCode.GDniveau1Objects2= [];
gdjs.menuCode.GDniveau1Objects3= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDcompetencesObjects3= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDcreditsObjects3= [];
gdjs.menuCode.GDNewObject2Objects1= [];
gdjs.menuCode.GDNewObject2Objects2= [];
gdjs.menuCode.GDNewObject2Objects3= [];
gdjs.menuCode.GDCreditsObjects1= [];
gdjs.menuCode.GDCreditsObjects2= [];
gdjs.menuCode.GDCreditsObjects3= [];
gdjs.menuCode.GDAuteurObjects1= [];
gdjs.menuCode.GDAuteurObjects2= [];
gdjs.menuCode.GDAuteurObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDNewObjectObjects1= [];
gdjs.menuCode.GDNewObjectObjects2= [];
gdjs.menuCode.GDNewObjectObjects3= [];
gdjs.menuCode.GDbouton_9595infosObjects1= [];
gdjs.menuCode.GDbouton_9595infosObjects2= [];
gdjs.menuCode.GDbouton_9595infosObjects3= [];
gdjs.menuCode.GDtambour_9595mini2Objects1= [];
gdjs.menuCode.GDtambour_9595mini2Objects2= [];
gdjs.menuCode.GDtambour_9595mini2Objects3= [];
gdjs.menuCode.GDtambour_9595miniObjects1= [];
gdjs.menuCode.GDtambour_9595miniObjects2= [];
gdjs.menuCode.GDtambour_9595miniObjects3= [];
gdjs.menuCode.GDtambourObjects1= [];
gdjs.menuCode.GDtambourObjects2= [];
gdjs.menuCode.GDtambourObjects3= [];
gdjs.menuCode.GDecouterObjects1= [];
gdjs.menuCode.GDecouterObjects2= [];
gdjs.menuCode.GDecouterObjects3= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects3= [];
gdjs.menuCode.GDtxt_9595versionObjects1= [];
gdjs.menuCode.GDtxt_9595versionObjects2= [];
gdjs.menuCode.GDtxt_9595versionObjects3= [];
gdjs.menuCode.GDfond2Objects1= [];
gdjs.menuCode.GDfond2Objects2= [];
gdjs.menuCode.GDfond2Objects3= [];
gdjs.menuCode.GDfondObjects1= [];
gdjs.menuCode.GDfondObjects2= [];
gdjs.menuCode.GDfondObjects3= [];
gdjs.menuCode.GDfleche_9595clignotanteObjects1= [];
gdjs.menuCode.GDfleche_9595clignotanteObjects2= [];
gdjs.menuCode.GDfleche_9595clignotanteObjects3= [];
gdjs.menuCode.GDbulle_9595penseeObjects1= [];
gdjs.menuCode.GDbulle_9595penseeObjects2= [];
gdjs.menuCode.GDbulle_9595penseeObjects3= [];
gdjs.menuCode.GDbulle_9595chiffreObjects1= [];
gdjs.menuCode.GDbulle_9595chiffreObjects2= [];
gdjs.menuCode.GDbulle_9595chiffreObjects3= [];
gdjs.menuCode.GDfond_9595musiqueObjects1= [];
gdjs.menuCode.GDfond_9595musiqueObjects2= [];
gdjs.menuCode.GDfond_9595musiqueObjects3= [];
gdjs.menuCode.GDaideObjects1= [];
gdjs.menuCode.GDaideObjects2= [];
gdjs.menuCode.GDaideObjects3= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDscore1Objects3= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore2Objects3= [];
gdjs.menuCode.GDbouton_9595optionsObjects1= [];
gdjs.menuCode.GDbouton_9595optionsObjects2= [];
gdjs.menuCode.GDbouton_9595optionsObjects3= [];
gdjs.menuCode.GDlamaObjects1= [];
gdjs.menuCode.GDlamaObjects2= [];
gdjs.menuCode.GDlamaObjects3= [];
gdjs.menuCode.GDpointsObjects1= [];
gdjs.menuCode.GDpointsObjects2= [];
gdjs.menuCode.GDpointsObjects3= [];
gdjs.menuCode.GDdrapeau1Objects1= [];
gdjs.menuCode.GDdrapeau1Objects2= [];
gdjs.menuCode.GDdrapeau1Objects3= [];
gdjs.menuCode.GDdrapeau2Objects1= [];
gdjs.menuCode.GDdrapeau2Objects2= [];
gdjs.menuCode.GDdrapeau2Objects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_tambourbattant", "reglages");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_tambourbattant", "reglages", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(2));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score2")));
}
}}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDtambour_95959595miniObjects2Objects = Hashtable.newFrom({"tambour_mini": gdjs.menuCode.GDtambour_9595miniObjects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDniveau1Objects2Objects = Hashtable.newFrom({"niveau1": gdjs.menuCode.GDniveau1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore1Objects2Objects = Hashtable.newFrom({"score1": gdjs.menuCode.GDscore1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDniveau2Objects2Objects = Hashtable.newFrom({"niveau2": gdjs.menuCode.GDniveau2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDtambour_95959595mini2Objects2Objects = Hashtable.newFrom({"tambour_mini2": gdjs.menuCode.GDtambour_9595mini2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore2Objects2Objects = Hashtable.newFrom({"score2": gdjs.menuCode.GDscore2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_tambourbattant", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(2)));
}}

}


};gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


gdjs.menuCode.eventsList2(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects = Hashtable.newFrom({"bouton_infos": gdjs.menuCode.GDbouton_9595infosObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595optionsObjects1Objects = Hashtable.newFrom({"bouton_options": gdjs.menuCode.GDbouton_9595optionsObjects1});
gdjs.menuCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("tambour"), gdjs.menuCode.GDtambourObjects1);
{for(var i = 0, len = gdjs.menuCode.GDtambourObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDtambourObjects1[i].setAnimation(3);
}
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.menuCode.GDniveau1Objects1.length = 0;

gdjs.menuCode.GDscore1Objects1.length = 0;

gdjs.menuCode.GDtambour_9595miniObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.menuCode.GDniveau1Objects1_1final.length = 0;
gdjs.menuCode.GDscore1Objects1_1final.length = 0;
gdjs.menuCode.GDtambour_9595miniObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("tambour_mini"), gdjs.menuCode.GDtambour_9595miniObjects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDtambour_95959595miniObjects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDtambour_9595miniObjects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDtambour_9595miniObjects1_1final.indexOf(gdjs.menuCode.GDtambour_9595miniObjects2[j]) === -1 )
            gdjs.menuCode.GDtambour_9595miniObjects1_1final.push(gdjs.menuCode.GDtambour_9595miniObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("niveau1"), gdjs.menuCode.GDniveau1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDniveau1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDniveau1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDniveau1Objects1_1final.indexOf(gdjs.menuCode.GDniveau1Objects2[j]) === -1 )
            gdjs.menuCode.GDniveau1Objects1_1final.push(gdjs.menuCode.GDniveau1Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDscore1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDscore1Objects1_1final.indexOf(gdjs.menuCode.GDscore1Objects2[j]) === -1 )
            gdjs.menuCode.GDscore1Objects1_1final.push(gdjs.menuCode.GDscore1Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDniveau1Objects1_1final, gdjs.menuCode.GDniveau1Objects1);
gdjs.copyArray(gdjs.menuCode.GDscore1Objects1_1final, gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(gdjs.menuCode.GDtambour_9595miniObjects1_1final, gdjs.menuCode.GDtambour_9595miniObjects1);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(96);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.menuCode.GDniveau2Objects1.length = 0;

gdjs.menuCode.GDscore2Objects1.length = 0;

gdjs.menuCode.GDtambour_9595mini2Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
{gdjs.menuCode.GDniveau2Objects1_1final.length = 0;
gdjs.menuCode.GDscore2Objects1_1final.length = 0;
gdjs.menuCode.GDtambour_9595mini2Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("niveau2"), gdjs.menuCode.GDniveau2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDniveau2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDniveau2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDniveau2Objects1_1final.indexOf(gdjs.menuCode.GDniveau2Objects2[j]) === -1 )
            gdjs.menuCode.GDniveau2Objects1_1final.push(gdjs.menuCode.GDniveau2Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("tambour_mini2"), gdjs.menuCode.GDtambour_9595mini2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDtambour_95959595mini2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDtambour_9595mini2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDtambour_9595mini2Objects1_1final.indexOf(gdjs.menuCode.GDtambour_9595mini2Objects2[j]) === -1 )
            gdjs.menuCode.GDtambour_9595mini2Objects1_1final.push(gdjs.menuCode.GDtambour_9595mini2Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDscore2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDscore2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDscore2Objects1_1final.indexOf(gdjs.menuCode.GDscore2Objects2[j]) === -1 )
            gdjs.menuCode.GDscore2Objects1_1final.push(gdjs.menuCode.GDscore2Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDniveau2Objects1_1final, gdjs.menuCode.GDniveau2Objects1);
gdjs.copyArray(gdjs.menuCode.GDscore2Objects1_1final, gdjs.menuCode.GDscore2Objects1);
gdjs.copyArray(gdjs.menuCode.GDtambour_9595mini2Objects1_1final, gdjs.menuCode.GDtambour_9595mini2Objects1);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(2);
}{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(96);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(11851716);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score1").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score2").setNumber(0);
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].pauseAnimation();
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimationFrame(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("score2")));
}
}
{ //Subevents
gdjs.menuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_infos"), gdjs.menuCode.GDbouton_9595infosObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_options"), gdjs.menuCode.GDbouton_9595optionsObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595optionsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "code", false);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects3.length = 0;
gdjs.menuCode.GDniveau2Objects1.length = 0;
gdjs.menuCode.GDniveau2Objects2.length = 0;
gdjs.menuCode.GDniveau2Objects3.length = 0;
gdjs.menuCode.GDniveau1Objects1.length = 0;
gdjs.menuCode.GDniveau1Objects2.length = 0;
gdjs.menuCode.GDniveau1Objects3.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects3.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects3.length = 0;
gdjs.menuCode.GDNewObject2Objects1.length = 0;
gdjs.menuCode.GDNewObject2Objects2.length = 0;
gdjs.menuCode.GDNewObject2Objects3.length = 0;
gdjs.menuCode.GDCreditsObjects1.length = 0;
gdjs.menuCode.GDCreditsObjects2.length = 0;
gdjs.menuCode.GDCreditsObjects3.length = 0;
gdjs.menuCode.GDAuteurObjects1.length = 0;
gdjs.menuCode.GDAuteurObjects2.length = 0;
gdjs.menuCode.GDAuteurObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDNewObjectObjects1.length = 0;
gdjs.menuCode.GDNewObjectObjects2.length = 0;
gdjs.menuCode.GDNewObjectObjects3.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects1.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects2.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects3.length = 0;
gdjs.menuCode.GDtambour_9595mini2Objects1.length = 0;
gdjs.menuCode.GDtambour_9595mini2Objects2.length = 0;
gdjs.menuCode.GDtambour_9595mini2Objects3.length = 0;
gdjs.menuCode.GDtambour_9595miniObjects1.length = 0;
gdjs.menuCode.GDtambour_9595miniObjects2.length = 0;
gdjs.menuCode.GDtambour_9595miniObjects3.length = 0;
gdjs.menuCode.GDtambourObjects1.length = 0;
gdjs.menuCode.GDtambourObjects2.length = 0;
gdjs.menuCode.GDtambourObjects3.length = 0;
gdjs.menuCode.GDecouterObjects1.length = 0;
gdjs.menuCode.GDecouterObjects2.length = 0;
gdjs.menuCode.GDecouterObjects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.menuCode.GDtxt_9595versionObjects1.length = 0;
gdjs.menuCode.GDtxt_9595versionObjects2.length = 0;
gdjs.menuCode.GDtxt_9595versionObjects3.length = 0;
gdjs.menuCode.GDfond2Objects1.length = 0;
gdjs.menuCode.GDfond2Objects2.length = 0;
gdjs.menuCode.GDfond2Objects3.length = 0;
gdjs.menuCode.GDfondObjects1.length = 0;
gdjs.menuCode.GDfondObjects2.length = 0;
gdjs.menuCode.GDfondObjects3.length = 0;
gdjs.menuCode.GDfleche_9595clignotanteObjects1.length = 0;
gdjs.menuCode.GDfleche_9595clignotanteObjects2.length = 0;
gdjs.menuCode.GDfleche_9595clignotanteObjects3.length = 0;
gdjs.menuCode.GDbulle_9595penseeObjects1.length = 0;
gdjs.menuCode.GDbulle_9595penseeObjects2.length = 0;
gdjs.menuCode.GDbulle_9595penseeObjects3.length = 0;
gdjs.menuCode.GDbulle_9595chiffreObjects1.length = 0;
gdjs.menuCode.GDbulle_9595chiffreObjects2.length = 0;
gdjs.menuCode.GDbulle_9595chiffreObjects3.length = 0;
gdjs.menuCode.GDfond_9595musiqueObjects1.length = 0;
gdjs.menuCode.GDfond_9595musiqueObjects2.length = 0;
gdjs.menuCode.GDfond_9595musiqueObjects3.length = 0;
gdjs.menuCode.GDaideObjects1.length = 0;
gdjs.menuCode.GDaideObjects2.length = 0;
gdjs.menuCode.GDaideObjects3.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDscore1Objects3.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore2Objects3.length = 0;
gdjs.menuCode.GDbouton_9595optionsObjects1.length = 0;
gdjs.menuCode.GDbouton_9595optionsObjects2.length = 0;
gdjs.menuCode.GDbouton_9595optionsObjects3.length = 0;
gdjs.menuCode.GDlamaObjects1.length = 0;
gdjs.menuCode.GDlamaObjects2.length = 0;
gdjs.menuCode.GDlamaObjects3.length = 0;
gdjs.menuCode.GDpointsObjects1.length = 0;
gdjs.menuCode.GDpointsObjects2.length = 0;
gdjs.menuCode.GDpointsObjects3.length = 0;
gdjs.menuCode.GDdrapeau1Objects1.length = 0;
gdjs.menuCode.GDdrapeau1Objects2.length = 0;
gdjs.menuCode.GDdrapeau1Objects3.length = 0;
gdjs.menuCode.GDdrapeau2Objects1.length = 0;
gdjs.menuCode.GDdrapeau2Objects2.length = 0;
gdjs.menuCode.GDdrapeau2Objects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;

gdjs.menuCode.eventsList4(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
