var CtAnimaux = [0,0,0,0];
var AnimalparEnclos = [];
var imgAnimalparEnclos = [];
var imgEnclos = ['url("img/enclos-lapins.svg")','url("img/enclos-moutons.svg")','url("img/enclos-vaches.svg")','url("img/enclos-poules.svg")'];
var classAnimalparEnclos = [];

var solution = [];
var proposition = [];
for (var i=0; i<4; i++) {
  solution[i] = [0,0,0,0];
  proposition[i] = [0,0,0,0];
}
var difficulte = "";
var informationsObjet = document.getElementById("informations");
var Poubelle = document.getElementById("poubelle");
var zoomAnimal1 = document.createElement("img");
zoomAnimal1.src = "img/lapin-zoom.png";
var zoomAnimal2 = document.createElement("img");
zoomAnimal2.src = "img/mouton-zoom.png";
var zoomAnimal3 = document.createElement("img");
zoomAnimal3.src = "img/vache-zoom.png";
var zoomAnimal4 = document.createElement("img");
zoomAnimal4.src = "img/poule-zoom.png";

var btNiveau1 = document.getElementById("Niveau1");
var btNiveau2 = document.getElementById("Niveau2");
var btVerif = document.getElementById("verifier");
btVerif.addEventListener("click", verification);

var Modele1 = document.getElementById("modele1");
var Modele2 = document.getElementById("modele2");
var Modele3 = document.getElementById("modele3");
var Modele4 = document.getElementById("modele4");

var Pres = [];
Pres[0] = document.getElementById("pre1");
Pres[1] = document.getElementById("pre2");
Pres[2] = document.getElementById("pre3");
Pres[3] = document.getElementById("pre4");

var Pre1 = document.getElementById("pre1");
var Pre2 = document.getElementById("pre2");
var Pre3 = document.getElementById("pre3");
var Pre4 = document.getElementById("pre4");

function alea(min,max) {
  var nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function estPresent(nombre, tableau) {
  for (var i = 0; i < tableau.length; i++) {
    if ( tableau[i] === nombre ) {
      return true;
    }
  }
  return false;
}

function aleaDifferent(min,max,quantite) {
  var tableau = [];
  var nombre;
  for (var i = 0; i < quantite; i++) {
    do {
      nombre = alea(min, max);
    } while (estPresent(nombre, tableau));
    tableau[i] = nombre;
  }
  return tableau;
}

function melange(tableau) {
  var tirage;
  var tab_alea = [];

  for (var i = 0; i < tableau.length; i++) {
    do {
      tirage = alea(0, tableau.length -1);
    } while (tableau[tirage] === 0);
    tab_alea[i] = tableau[tirage];
    tableau[tirage] = 0;
  }
  return tab_alea;
}

function activeBtNiveau() {
  document.getElementById("Niveau1").disabled = false;
  document.getElementById("Niveau2").disabled = false;
}

function desactiveBtNiveau() {
  document.getElementById("Niveau1").disabled = true;
  document.getElementById("Niveau2").disabled = true;
}

function defNiveau1() {
  difficulte = btNiveau1.value;
  desactiveBtNiveau();
  nouvelExercice();
}

function defNiveau2() {
  difficulte = btNiveau2.value;
  desactiveBtNiveau();
  nouvelExercice();
}

function tirageAnimaux() {
  var EnclosAleatoire = [1,2,3,4];
  EnclosAleatoire = melange(EnclosAleatoire);
  var valeurs = [];

  switch(difficulte) {
    case "Niveau1":
      valeurs = aleaDifferent(1,10,4);
      break;
    case "Niveau2":
      valeurs = aleaDifferent(10,20,4);
      break;
  }

  solution[0][EnclosAleatoire[0]-1] = valeurs[0];
  solution[1][EnclosAleatoire[1]-1] = valeurs[1];
  solution[2][EnclosAleatoire[2]-1] = valeurs[2];
  solution[3][EnclosAleatoire[3]-1] = valeurs[3];
  for (var i=0; i<4; i++) {
    AnimalparEnclos[EnclosAleatoire[i]-1] = i;
  }
  dessineEnclos();
}

function dessineEnclos() {
  for (var i=0; i<4; i++) {
    switch(AnimalparEnclos[i]) {
      case 0:
        Pres[i].style.backgroundImage = imgEnclos[0];
        break;
      case 1:
        Pres[i].style.backgroundImage = imgEnclos[1];
        break;
      case 2:
        Pres[i].style.backgroundImage = imgEnclos[2];
        break;
      case 3:
        Pres[i].style.backgroundImage = imgEnclos[3];
        break;
    }
  }
}

function nouvelExercice() {
  tirageAnimaux();
  afficheExercice();
  document.getElementById("verifier").disabled = false;
}

function verification() {
  var IdErreur;
  var NoErreur;
  var erreur = false;
  var SolutionParEnclos = [];
  var PropositionParEnclos = [];
  for (var i = 0; i < 4; i++) {
    SolutionParEnclos[i] = [0,0,0,0];
    PropositionParEnclos[i] = [0,0,0,0];
  }
  for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 4; j++) {
      SolutionParEnclos[j][i] = solution[i][j];
      PropositionParEnclos[j][i] = proposition[i][j];
    }
  }
  for (var i = 0; i < 4; i++) {
    NoErreur = i + 1;
    IdErreur = "erreur" + NoErreur;
    if (PropositionParEnclos[i].join() === SolutionParEnclos[i].join()) {
      document.getElementById(IdErreur).innerHTML = "";
      }
      else {
        document.getElementById(IdErreur).innerHTML = "X";
        erreur = true;
      }
  }
  if (! erreur) {
    showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left');
    document.getElementById("verifier").disabled = true;
    reinitialise();
  }
}

// Elimine les animaux des enclos
function effaceAnimaux() {
  var tab = document.getElementsByClassName("masque");
  for (var i = 0; i < tab.length; i++) {
    while (tab[i].firstChild) {
      tab[i].removeChild(tab[i].firstChild);
    }
  }
}

function reinitialise() {
  var IdErreur;
  var NoErreur;
  for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 4; j++) {
      solution[i][j] = 0;
      proposition[i][j] = 0;
    }
    NoErreur = i + 1;
    IdErreur = "erreur" + NoErreur;
    document.getElementById(IdErreur).innerHTML = "";
  }
  activeBtNiveau();
  Modele1.style.backgroundImage = "";
  Modele2.style.backgroundImage = "";
  Modele3.style.backgroundImage = "";
  Modele4.style.backgroundImage = "";
  Modele1.innerHTML = "";
  Modele2.innerHTML = "";
  Modele3.innerHTML = "";
  Modele4.innerHTML = "";
  effaceAnimaux();
}

function getNomAnimal(objet) {
  var Nom = "";
  var tableau = objet.split("-");
  Nom = tableau[0];
  return Nom;
}

function getIdAnimal(objet) {
  var IdAnimal = "";
  var tableau = objet.split("-");
  IdAnimal = tableau[1];
  return IdAnimal;
}

function getIdAccueil(objet) {
  var IdAccueil = "";
  var tableau = objet.split("-");
  IdAccueil = tableau[2];
  if (tableau[3]) {
      IdAccueil += '-' + tableau[3];
  }
  return IdAccueil;
}

function getIndiceAnimal(animal) {
  return parseInt(animal.charAt(6) - 1);
}

function getIndiceEnclos(enclos) {
  return parseInt(enclos.charAt(13) - 1);
}

function getImgAnimalparEnclos() {
  for (var i=0; i<4; i++) {
    switch(AnimalparEnclos[i]) {
      case 0:
        imgAnimalparEnclos[i] = "img/lapin.png";
        classAnimalparEnclos[i] = "lapin-mini";
        break;
      case 1:
        imgAnimalparEnclos[i] = "img/mouton.png";
        classAnimalparEnclos[i] = "mouton-mini";
        break;
      case 2:
        imgAnimalparEnclos[i] = "img/vache.png";
        classAnimalparEnclos[i] = "vache-mini";
        break;
      case 3:
        imgAnimalparEnclos[i] = "img/poule.png";
        classAnimalparEnclos[i] = "poule-mini";
        break;
   }
  }
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  var infosObjet;
  // Grossit l'image de l'animal lors du drag
  function afficheImgZoom(animal) {
    switch(animal) {
      case 'animal1':
        var offsetX = zoomAnimal1.width / 2;
        var offsetY = zoomAnimal1.height /2;
        ev.dataTransfer.setDragImage(zoomAnimal1, offsetX, offsetY);
        break;
      case 'animal2':
        var offsetX = zoomAnimal2.width / 2;
        var offsetY = zoomAnimal2.height /2;
        ev.dataTransfer.setDragImage(zoomAnimal2, offsetX, offsetY);
        break;
      case 'animal3':
        var offsetX = zoomAnimal3.width / 2;
        var offsetY = zoomAnimal3.height /2;
        ev.dataTransfer.setDragImage(zoomAnimal3, offsetX, offsetY);
        break;
      case 'animal4':
        var offsetX = zoomAnimal4.width / 2;
        var offsetY = zoomAnimal4.height /2;
        ev.dataTransfer.setDragImage(zoomAnimal4, offsetX, offsetY);
        break;
    }
  }
  infosObjet = ev.target.alt + "-" + ev.target.id + "-" + ev.target.parentNode.id;
  afficheImgZoom(ev.target.alt);
  ev.dataTransfer.setData("text", infosObjet);
}

// Elimine les animaux des enclos quand on les fait glisser dans la zone de suppression
function detruit(ev) {
  ev.preventDefault();
  var IdAnimal = getIdAnimal(ev.dataTransfer.getData("text"));
  var Animal = document.getElementById(IdAnimal);
  var NomAnimal = getNomAnimal(ev.dataTransfer.getData("Text"));
  var NoAnimal = getIndiceAnimal(NomAnimal);
  var IdDepart = getIdAccueil(ev.dataTransfer.getData("text"));
  var NoEnclos = getIndiceEnclos(IdDepart);
  if (IdDepart != "reserve") {
      Animal.parentNode.removeChild(Animal);
      proposition[NoAnimal][NoEnclos] -= 1;
  }
}

function drop(ev) {
  ev.preventDefault();
  var Animal = getNomAnimal(ev.dataTransfer.getData('Text'));
  var NoAnimal = getIndiceAnimal(Animal);
  var IdAnimal = getIdAnimal(ev.dataTransfer.getData("text"));
  var IdDepart = getIdAccueil(ev.dataTransfer.getData("text"));
  var NoEnclos = getIndiceEnclos(ev.target.id);
  if ((IdDepart === "reserve") && ((ev.target.id.startsWith("masque-enclos")) || (ev.target.parentNode.id.startsWith("masque-enclos")))) {
    var nodeCopy = document.getElementById(IdAnimal).cloneNode(true);
    nodeCopy.id = Animal + "_" + CtAnimaux[NoAnimal];
    if (ev.target.id.startsWith("masque-enclos")) {
        document.getElementById(ev.target.id).appendChild(nodeCopy);
      }
      else {
        document.getElementById(ev.target.parentNode.id).appendChild(nodeCopy);
        NoEnclos = getIndiceEnclos(ev.target.parentNode.id);
    }
    CtAnimaux[NoAnimal] += 1;
    proposition[NoAnimal][NoEnclos] += 1;
    }
    else if ((IdDepart.startsWith("masque-enclos"))  && ((ev.target.id.startsWith("masque-enclos")) || (ev.target.parentNode.id.startsWith("masque-enclos")))) {
      if (ev.target.id.startsWith("masque-enclos")) {
        document.getElementById(ev.target.id).appendChild(document.getElementById(IdAnimal));
        }
        else {
          document.getElementById(ev.target.parentNode.id).appendChild(document.getElementById(IdAnimal));
          NoEnclos = getIndiceEnclos(ev.target.parentNode.id);
      }
      proposition[NoAnimal][getIndiceEnclos(IdDepart)] -= 1;
      proposition[NoAnimal][NoEnclos] += 1;
    }
}

// Affiche le nombre d'animaux à placer dans les enclos
function afficheExercice() {
  getImgAnimalparEnclos();
  Modele1.style.backgroundImage = "url('img/bulle.png')";
  Modele2.style.backgroundImage = "url('img/bulle.png')";
  Modele3.style.backgroundImage = "url('img/bulle.png')";
  Modele4.style.backgroundImage = "url('img/bulle.png')";
  Modele1.parentNode.classList.add(classAnimalparEnclos[0].replace('-mini', ''));
  Modele2.parentNode.classList.add(classAnimalparEnclos[1].replace('-mini', ''));
  Modele3.parentNode.classList.add(classAnimalparEnclos[2].replace('-mini', ''));
  Modele4.parentNode.classList.add(classAnimalparEnclos[3].replace('-mini', ''));
  Modele1.innerHTML = '<img src="' + imgAnimalparEnclos[0] + '" class="' + classAnimalparEnclos[0] + '"> ' + solution[AnimalparEnclos[0]][0];
  Modele2.innerHTML = '<img src="' + imgAnimalparEnclos[1] + '" class="' + classAnimalparEnclos[1] + '"> ' + solution[AnimalparEnclos[1]][1];
  Modele3.innerHTML = '<img src="' + imgAnimalparEnclos[2] + '" class="' + classAnimalparEnclos[2] + '"> ' + solution[AnimalparEnclos[2]][2];
  Modele4.innerHTML = '<img src="' + imgAnimalparEnclos[3] + '" class="' + classAnimalparEnclos[3] + '"> ' + solution[AnimalparEnclos[3]][3];
}
