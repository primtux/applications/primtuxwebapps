gdjs.infosCode = {};
gdjs.infosCode.localVariables = [];
gdjs.infosCode.GDjeu2_9595imagesObjects1= [];
gdjs.infosCode.GDjeu2_9595imagesObjects2= [];
gdjs.infosCode.GDbbtxt_9595infos_9595jeu2Objects1= [];
gdjs.infosCode.GDbbtxt_9595infos_9595jeu2Objects2= [];
gdjs.infosCode.GDjeu1_9595imagesObjects1= [];
gdjs.infosCode.GDjeu1_9595imagesObjects2= [];
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1Objects1= [];
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1Objects2= [];
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1bObjects1= [];
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1bObjects2= [];
gdjs.infosCode.GDjeu1b_9595imagesObjects1= [];
gdjs.infosCode.GDjeu1b_9595imagesObjects2= [];
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv1Objects1= [];
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv1Objects2= [];
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv2Objects1= [];
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv2Objects2= [];
gdjs.infosCode.GDrepresentation_9595chiffreeObjects1= [];
gdjs.infosCode.GDrepresentation_9595chiffreeObjects2= [];
gdjs.infosCode.GDsp_9595demo_9595bouton_9595switchObjects1= [];
gdjs.infosCode.GDsp_9595demo_9595bouton_9595switchObjects2= [];
gdjs.infosCode.GDsp_9595demo_9595quantiteObjects1= [];
gdjs.infosCode.GDsp_9595demo_9595quantiteObjects2= [];
gdjs.infosCode.GDbbtxt_9595bouton_9595switchObjects1= [];
gdjs.infosCode.GDbbtxt_9595bouton_9595switchObjects2= [];
gdjs.infosCode.GDsp_9595sergeObjects1= [];
gdjs.infosCode.GDsp_9595sergeObjects2= [];
gdjs.infosCode.GDbbtxt_9595scores_9595razObjects1= [];
gdjs.infosCode.GDbbtxt_9595scores_9595razObjects2= [];
gdjs.infosCode.GDbbtxt_9595info_9595devObjects1= [];
gdjs.infosCode.GDbbtxt_9595info_9595devObjects2= [];
gdjs.infosCode.GDbbtxt_9595info_9595appObjects1= [];
gdjs.infosCode.GDbbtxt_9595info_9595appObjects2= [];
gdjs.infosCode.GDsp_9595guideObjects1= [];
gdjs.infosCode.GDsp_9595guideObjects2= [];
gdjs.infosCode.GDsp_9595licenceObjects1= [];
gdjs.infosCode.GDsp_9595licenceObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595validerObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595validerObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595retourObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595retourObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595chiffre_9595deObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595chiffre_9595deObjects2= [];
gdjs.infosCode.GDtxt_9595representation_9595quantiteObjects1= [];
gdjs.infosCode.GDtxt_9595representation_9595quantiteObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595rejouerObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595rejouerObjects2= [];
gdjs.infosCode.GDsp_9595scoreObjects1= [];
gdjs.infosCode.GDsp_9595scoreObjects2= [];
gdjs.infosCode.GDsp_9595faux_9595vraiObjects1= [];
gdjs.infosCode.GDsp_9595faux_9595vraiObjects2= [];
gdjs.infosCode.GDsp_9595bouton_9595aideObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595aideObjects2= [];
gdjs.infosCode.GDsp_9595maisonObjects1= [];
gdjs.infosCode.GDsp_9595maisonObjects2= [];
gdjs.infosCode.GDsp_9595fond_9595maison_95951Objects1= [];
gdjs.infosCode.GDsp_9595fond_9595maison_95951Objects2= [];
gdjs.infosCode.GDsp_9595fond_9595maison_95952Objects1= [];
gdjs.infosCode.GDsp_9595fond_9595maison_95952Objects2= [];
gdjs.infosCode.GDsp_9595bouton_9595quantiteObjects1= [];
gdjs.infosCode.GDsp_9595bouton_9595quantiteObjects2= [];
gdjs.infosCode.GDsp_9595etiquette_9595etObjects1= [];
gdjs.infosCode.GDsp_9595etiquette_9595etObjects2= [];
gdjs.infosCode.GDsp_9595ourson_95951Objects1= [];
gdjs.infosCode.GDsp_9595ourson_95951Objects2= [];
gdjs.infosCode.GDsp_9595decompositionObjects1= [];
gdjs.infosCode.GDsp_9595decompositionObjects2= [];
gdjs.infosCode.GDGreenFlatBarObjects1= [];
gdjs.infosCode.GDGreenFlatBarObjects2= [];
gdjs.infosCode.GDtsp_9595Objects1= [];
gdjs.infosCode.GDtsp_9595Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDsp_95959595bouton_95959595retourObjects1Objects = Hashtable.newFrom({"sp_bouton_retour": gdjs.infosCode.GDsp_9595bouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.infosCode.GDfondObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfondObjects1[i].getBehavior("Animation").setAnimationIndex(1);
}
}{for(var i = 0, len = gdjs.infosCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfondObjects1[i].getBehavior("Opacity").setOpacity(150);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_retour"), gdjs.infosCode.GDsp_9595bouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDsp_95959595bouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.3;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDjeu2_9595imagesObjects1.length = 0;
gdjs.infosCode.GDjeu2_9595imagesObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595infos_9595jeu2Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595infos_9595jeu2Objects2.length = 0;
gdjs.infosCode.GDjeu1_9595imagesObjects1.length = 0;
gdjs.infosCode.GDjeu1_9595imagesObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1bObjects1.length = 0;
gdjs.infosCode.GDbbtxt_9595infos_9595jeu1bObjects2.length = 0;
gdjs.infosCode.GDjeu1b_9595imagesObjects1.length = 0;
gdjs.infosCode.GDjeu1b_9595imagesObjects2.length = 0;
gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv1Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv1Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv2Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595jeu1_9595niv2Objects2.length = 0;
gdjs.infosCode.GDrepresentation_9595chiffreeObjects1.length = 0;
gdjs.infosCode.GDrepresentation_9595chiffreeObjects2.length = 0;
gdjs.infosCode.GDsp_9595demo_9595bouton_9595switchObjects1.length = 0;
gdjs.infosCode.GDsp_9595demo_9595bouton_9595switchObjects2.length = 0;
gdjs.infosCode.GDsp_9595demo_9595quantiteObjects1.length = 0;
gdjs.infosCode.GDsp_9595demo_9595quantiteObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595bouton_9595switchObjects1.length = 0;
gdjs.infosCode.GDbbtxt_9595bouton_9595switchObjects2.length = 0;
gdjs.infosCode.GDsp_9595sergeObjects1.length = 0;
gdjs.infosCode.GDsp_9595sergeObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595scores_9595razObjects1.length = 0;
gdjs.infosCode.GDbbtxt_9595scores_9595razObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info_9595devObjects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info_9595devObjects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info_9595appObjects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info_9595appObjects2.length = 0;
gdjs.infosCode.GDsp_9595guideObjects1.length = 0;
gdjs.infosCode.GDsp_9595guideObjects2.length = 0;
gdjs.infosCode.GDsp_9595licenceObjects1.length = 0;
gdjs.infosCode.GDsp_9595licenceObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595validerObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595validerObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595chiffre_9595deObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595chiffre_9595deObjects2.length = 0;
gdjs.infosCode.GDtxt_9595representation_9595quantiteObjects1.length = 0;
gdjs.infosCode.GDtxt_9595representation_9595quantiteObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595rejouerObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595rejouerObjects2.length = 0;
gdjs.infosCode.GDsp_9595scoreObjects1.length = 0;
gdjs.infosCode.GDsp_9595scoreObjects2.length = 0;
gdjs.infosCode.GDsp_9595faux_9595vraiObjects1.length = 0;
gdjs.infosCode.GDsp_9595faux_9595vraiObjects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595aideObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595aideObjects2.length = 0;
gdjs.infosCode.GDsp_9595maisonObjects1.length = 0;
gdjs.infosCode.GDsp_9595maisonObjects2.length = 0;
gdjs.infosCode.GDsp_9595fond_9595maison_95951Objects1.length = 0;
gdjs.infosCode.GDsp_9595fond_9595maison_95951Objects2.length = 0;
gdjs.infosCode.GDsp_9595fond_9595maison_95952Objects1.length = 0;
gdjs.infosCode.GDsp_9595fond_9595maison_95952Objects2.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595quantiteObjects1.length = 0;
gdjs.infosCode.GDsp_9595bouton_9595quantiteObjects2.length = 0;
gdjs.infosCode.GDsp_9595etiquette_9595etObjects1.length = 0;
gdjs.infosCode.GDsp_9595etiquette_9595etObjects2.length = 0;
gdjs.infosCode.GDsp_9595ourson_95951Objects1.length = 0;
gdjs.infosCode.GDsp_9595ourson_95951Objects2.length = 0;
gdjs.infosCode.GDsp_9595decompositionObjects1.length = 0;
gdjs.infosCode.GDsp_9595decompositionObjects2.length = 0;
gdjs.infosCode.GDGreenFlatBarObjects1.length = 0;
gdjs.infosCode.GDGreenFlatBarObjects2.length = 0;
gdjs.infosCode.GDtsp_9595Objects1.length = 0;
gdjs.infosCode.GDtsp_9595Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
