gdjs.jeu_951Code = {};
gdjs.jeu_951Code.GDfilleObjects2_1final = [];

gdjs.jeu_951Code.GDmainsObjects2_1final = [];

gdjs.jeu_951Code.GDsyllabesObjects2_1final = [];

gdjs.jeu_951Code.GDvrai_9595fauxObjects1= [];
gdjs.jeu_951Code.GDvrai_9595fauxObjects2= [];
gdjs.jeu_951Code.GDvrai_9595fauxObjects3= [];
gdjs.jeu_951Code.GDvrai_9595fauxObjects4= [];
gdjs.jeu_951Code.GDbouton_9595validerObjects1= [];
gdjs.jeu_951Code.GDbouton_9595validerObjects2= [];
gdjs.jeu_951Code.GDbouton_9595validerObjects3= [];
gdjs.jeu_951Code.GDbouton_9595validerObjects4= [];
gdjs.jeu_951Code.GDbouton_9595recommencerObjects1= [];
gdjs.jeu_951Code.GDbouton_9595recommencerObjects2= [];
gdjs.jeu_951Code.GDbouton_9595recommencerObjects3= [];
gdjs.jeu_951Code.GDbouton_9595recommencerObjects4= [];
gdjs.jeu_951Code.GDNewObjectObjects1= [];
gdjs.jeu_951Code.GDNewObjectObjects2= [];
gdjs.jeu_951Code.GDNewObjectObjects3= [];
gdjs.jeu_951Code.GDNewObjectObjects4= [];
gdjs.jeu_951Code.GDsyllabesObjects1= [];
gdjs.jeu_951Code.GDsyllabesObjects2= [];
gdjs.jeu_951Code.GDsyllabesObjects3= [];
gdjs.jeu_951Code.GDsyllabesObjects4= [];
gdjs.jeu_951Code.GDscore2Objects1= [];
gdjs.jeu_951Code.GDscore2Objects2= [];
gdjs.jeu_951Code.GDscore2Objects3= [];
gdjs.jeu_951Code.GDscore2Objects4= [];
gdjs.jeu_951Code.GDscore1Objects1= [];
gdjs.jeu_951Code.GDscore1Objects2= [];
gdjs.jeu_951Code.GDscore1Objects3= [];
gdjs.jeu_951Code.GDscore1Objects4= [];
gdjs.jeu_951Code.GDmains3Objects1= [];
gdjs.jeu_951Code.GDmains3Objects2= [];
gdjs.jeu_951Code.GDmains3Objects3= [];
gdjs.jeu_951Code.GDmains3Objects4= [];
gdjs.jeu_951Code.GDmains2Objects1= [];
gdjs.jeu_951Code.GDmains2Objects2= [];
gdjs.jeu_951Code.GDmains2Objects3= [];
gdjs.jeu_951Code.GDmains2Objects4= [];
gdjs.jeu_951Code.GDmains1Objects1= [];
gdjs.jeu_951Code.GDmains1Objects2= [];
gdjs.jeu_951Code.GDmains1Objects3= [];
gdjs.jeu_951Code.GDmains1Objects4= [];
gdjs.jeu_951Code.GDmainsObjects1= [];
gdjs.jeu_951Code.GDmainsObjects2= [];
gdjs.jeu_951Code.GDmainsObjects3= [];
gdjs.jeu_951Code.GDmainsObjects4= [];
gdjs.jeu_951Code.GDtheatreObjects1= [];
gdjs.jeu_951Code.GDtheatreObjects2= [];
gdjs.jeu_951Code.GDtheatreObjects3= [];
gdjs.jeu_951Code.GDtheatreObjects4= [];
gdjs.jeu_951Code.GDfilleObjects1= [];
gdjs.jeu_951Code.GDfilleObjects2= [];
gdjs.jeu_951Code.GDfilleObjects3= [];
gdjs.jeu_951Code.GDfilleObjects4= [];
gdjs.jeu_951Code.GDgarconObjects1= [];
gdjs.jeu_951Code.GDgarconObjects2= [];
gdjs.jeu_951Code.GDgarconObjects3= [];
gdjs.jeu_951Code.GDgarconObjects4= [];
gdjs.jeu_951Code.GDmot_95951syllabeObjects1= [];
gdjs.jeu_951Code.GDmot_95951syllabeObjects2= [];
gdjs.jeu_951Code.GDmot_95951syllabeObjects3= [];
gdjs.jeu_951Code.GDmot_95951syllabeObjects4= [];
gdjs.jeu_951Code.GDmot_95952syllabesObjects1= [];
gdjs.jeu_951Code.GDmot_95952syllabesObjects2= [];
gdjs.jeu_951Code.GDmot_95952syllabesObjects3= [];
gdjs.jeu_951Code.GDmot_95952syllabesObjects4= [];
gdjs.jeu_951Code.GDmot_95953syllabesObjects1= [];
gdjs.jeu_951Code.GDmot_95953syllabesObjects2= [];
gdjs.jeu_951Code.GDmot_95953syllabesObjects3= [];
gdjs.jeu_951Code.GDmot_95953syllabesObjects4= [];
gdjs.jeu_951Code.GDfond_9595vertObjects1= [];
gdjs.jeu_951Code.GDfond_9595vertObjects2= [];
gdjs.jeu_951Code.GDfond_9595vertObjects3= [];
gdjs.jeu_951Code.GDfond_9595vertObjects4= [];
gdjs.jeu_951Code.GDbouton_9595retourObjects1= [];
gdjs.jeu_951Code.GDbouton_9595retourObjects2= [];
gdjs.jeu_951Code.GDbouton_9595retourObjects3= [];
gdjs.jeu_951Code.GDbouton_9595retourObjects4= [];


gdjs.jeu_951Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0")) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0")) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0")) == 3;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0")) == 4;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0")) == 5;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0")) == 6;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].setX(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3")));
}
}}

}


};gdjs.jeu_951Code.eventsList1 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("0").setNumber(gdjs.randomInRange(1, 6));
}
{ //Subevents
gdjs.jeu_951Code.eventsList0(runtimeScene);} //End of subevents
}

}


};gdjs.jeu_951Code.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("2")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 3;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("3")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("3")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 4;
if (isConditionTrue_0) {
gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects1, gdjs.jeu_951Code.GDmot_95951syllabeObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects1, gdjs.jeu_951Code.GDmot_95952syllabesObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects1, gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("4")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("4")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 5;
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_951Code.GDmot_95951syllabeObjects1 */
/* Reuse gdjs.jeu_951Code.GDmot_95952syllabesObjects1 */
/* Reuse gdjs.jeu_951Code.GDmot_95953syllabesObjects1 */
{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("5")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("5")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("5")));
}
}}

}


};gdjs.jeu_951Code.eventsList3 = function(runtimeScene) {

{


gdjs.jeu_951Code.eventsList1(runtimeScene);
}


{


gdjs.jeu_951Code.eventsList2(runtimeScene);
}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmainsObjects3Objects = Hashtable.newFrom({"mains": gdjs.jeu_951Code.GDmainsObjects3});
gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDsyllabesObjects3Objects = Hashtable.newFrom({"syllabes": gdjs.jeu_951Code.GDsyllabesObjects3});
gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDfilleObjects3Objects = Hashtable.newFrom({"fille": gdjs.jeu_951Code.GDfilleObjects3});
gdjs.jeu_951Code.eventsList4 = function(runtimeScene) {

{

gdjs.jeu_951Code.GDfilleObjects2.length = 0;

gdjs.jeu_951Code.GDmainsObjects2.length = 0;

gdjs.jeu_951Code.GDsyllabesObjects2.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.jeu_951Code.GDfilleObjects2_1final.length = 0;
gdjs.jeu_951Code.GDmainsObjects2_1final.length = 0;
gdjs.jeu_951Code.GDsyllabesObjects2_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects3);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmainsObjects3Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeu_951Code.GDmainsObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeu_951Code.GDmainsObjects2_1final.indexOf(gdjs.jeu_951Code.GDmainsObjects3[j]) === -1 )
            gdjs.jeu_951Code.GDmainsObjects2_1final.push(gdjs.jeu_951Code.GDmainsObjects3[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects3);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDsyllabesObjects3Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeu_951Code.GDsyllabesObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeu_951Code.GDsyllabesObjects2_1final.indexOf(gdjs.jeu_951Code.GDsyllabesObjects3[j]) === -1 )
            gdjs.jeu_951Code.GDsyllabesObjects2_1final.push(gdjs.jeu_951Code.GDsyllabesObjects3[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("fille"), gdjs.jeu_951Code.GDfilleObjects3);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDfilleObjects3Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.jeu_951Code.GDfilleObjects3.length; j < jLen ; ++j) {
        if ( gdjs.jeu_951Code.GDfilleObjects2_1final.indexOf(gdjs.jeu_951Code.GDfilleObjects3[j]) === -1 )
            gdjs.jeu_951Code.GDfilleObjects2_1final.push(gdjs.jeu_951Code.GDfilleObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.jeu_951Code.GDfilleObjects2_1final, gdjs.jeu_951Code.GDfilleObjects2);
gdjs.copyArray(gdjs.jeu_951Code.GDmainsObjects2_1final, gdjs.jeu_951Code.GDmainsObjects2);
gdjs.copyArray(gdjs.jeu_951Code.GDsyllabesObjects2_1final, gdjs.jeu_951Code.GDsyllabesObjects2);
}
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(5), false);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].hide(false);
}
for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].hide(false);
}
for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].hide(false);
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects2[i].setAnimation(0);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects2);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/clap.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setAnimation(1);
}
}{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) > 1;
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects2);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/clap 2.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setAnimation(1);
}
}{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(3);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) > 2;
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects1);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/clap 3.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects1[i].setAnimation(1);
}
}{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmot_959595951syllabeObjects2Objects = Hashtable.newFrom({"mot_1syllabe": gdjs.jeu_951Code.GDmot_95951syllabeObjects2});
gdjs.jeu_951Code.eventsList5 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bain.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/balle.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bois.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 4 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bras.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 5 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chat.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 6 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chien.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 7 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ciel.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 8 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/clé.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 9 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/dent.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 10 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/doigt.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 11 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/douche.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 12 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/eau.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 13 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/feu.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 14 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/fleur.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 15 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/fruit.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 16 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/lait.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 17 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/loup.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 18 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/mer.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 19 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/nez.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 20 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/oeil.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 21 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/oeuf.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 22 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pain.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95951syllabeObjects2, gdjs.jeu_951Code.GDmot_95951syllabeObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i].getAnimation() == 23 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects3[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/poule.mp3", 1, false, 100, 1);
}}

}


{

/* Reuse gdjs.jeu_951Code.GDmot_95951syllabeObjects2 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].getAnimation() == 24 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects2[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/roi.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmot_959595952syllabesObjects2Objects = Hashtable.newFrom({"mot_2syllabes": gdjs.jeu_951Code.GDmot_95952syllabesObjects2});
gdjs.jeu_951Code.eventsList6 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/abeille.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/antenne.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/balai.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 4 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/balance.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 5 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ballon.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 6 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bisou.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 7 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bonnet.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 8 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bouteille.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 9 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/cahier.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 10 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/carton.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 11 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/cerceau.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 12 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chenille.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 13 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/corbeau.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 14 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/coussin.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 15 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/dauphin.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 16 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/épée.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 17 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/lama.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 18 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/mamie.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 19 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/moto.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 20 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/panier.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 21 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/photo.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 22 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/poussin.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95952syllabesObjects2, gdjs.jeu_951Code.GDmot_95952syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i].getAnimation() == 23 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/robot.mp3", 1, false, 100, 1);
}}

}


{

/* Reuse gdjs.jeu_951Code.GDmot_95952syllabesObjects2 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].getAnimation() == 24 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/souris.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmot_959595953syllabesObjects2Objects = Hashtable.newFrom({"mot_3syllabes": gdjs.jeu_951Code.GDmot_95953syllabesObjects2});
gdjs.jeu_951Code.eventsList7 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 1 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ambulance.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 2 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/ananas.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 3 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/arrosage.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 4 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/balançoire.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 5 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/champignon.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 6 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/chocolat.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 7 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/coccinelle.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 8 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/confiture.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 9 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/coquillage.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 10 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/couverture.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 11 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/crocodille.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 12 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/écureuil.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 13 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/éléphant.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 14 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/escargot.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 15 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/herrisson.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 16 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/magasin.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 17 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pantalon.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 18 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/papillon.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 19 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/perroquet.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 20 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/poulailler.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 21 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/pyjama.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 22 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/robinet.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(gdjs.jeu_951Code.GDmot_95953syllabesObjects2, gdjs.jeu_951Code.GDmot_95953syllabesObjects3);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i].getAnimation() == 23 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects3[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/tablier.mp3", 1, false, 100, 1);
}}

}


{

/* Reuse gdjs.jeu_951Code.GDmot_95953syllabesObjects2 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].getAnimation() == 24 ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/telephone.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_951Code.eventsList8 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects3);
gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmainsObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmainsObjects3[i].getX() < (( gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length === 0 ) ? 0 :gdjs.jeu_951Code.GDmot_95951syllabeObjects3[0].getPointX("")) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmainsObjects3[k] = gdjs.jeu_951Code.GDmainsObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmainsObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects3);
/* Reuse gdjs.jeu_951Code.GDmainsObjects3 */
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects3);
{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects3[i].setX(gdjs.jeu_951Code.GDmainsObjects3[i].getX() + (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects3[i].setX(gdjs.jeu_951Code.GDsyllabesObjects3[i].getX() + (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects3[i].setX(gdjs.jeu_951Code.GDfond_9595vertObjects3[i].getX() + (32));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmainsObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmainsObjects2[i].getX() > (( gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length === 0 ) ? 0 :gdjs.jeu_951Code.GDmot_95951syllabeObjects2[0].getPointX("")) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmainsObjects2[k] = gdjs.jeu_951Code.GDmainsObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmainsObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects2);
/* Reuse gdjs.jeu_951Code.GDmainsObjects2 */
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects2);
{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setX(gdjs.jeu_951Code.GDmainsObjects2[i].getX() - (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects2[i].setX(gdjs.jeu_951Code.GDsyllabesObjects2[i].getX() - (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects2[i].setX(gdjs.jeu_951Code.GDfond_9595vertObjects2[i].getX() - (32));
}
}}

}


};gdjs.jeu_951Code.eventsList9 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects3);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects3);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmainsObjects3.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmainsObjects3[i].getX() < (( gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length === 0 ) ? 0 :gdjs.jeu_951Code.GDmot_95952syllabesObjects3[0].getPointX("")) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmainsObjects3[k] = gdjs.jeu_951Code.GDmainsObjects3[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmainsObjects3.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects3);
/* Reuse gdjs.jeu_951Code.GDmainsObjects3 */
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects3);
{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects3[i].setX(gdjs.jeu_951Code.GDmainsObjects3[i].getX() + (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects3[i].setX(gdjs.jeu_951Code.GDsyllabesObjects3[i].getX() + (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects3[i].setX(gdjs.jeu_951Code.GDfond_9595vertObjects3[i].getX() + (32));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmainsObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmainsObjects2[i].getX() > (( gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length === 0 ) ? 0 :gdjs.jeu_951Code.GDmot_95952syllabesObjects2[0].getPointX("")) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmainsObjects2[k] = gdjs.jeu_951Code.GDmainsObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmainsObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects2);
/* Reuse gdjs.jeu_951Code.GDmainsObjects2 */
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects2);
{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setX(gdjs.jeu_951Code.GDmainsObjects2[i].getX() - (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects2[i].setX(gdjs.jeu_951Code.GDsyllabesObjects2[i].getX() - (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects2[i].setX(gdjs.jeu_951Code.GDfond_9595vertObjects2[i].getX() - (32));
}
}}

}


};gdjs.jeu_951Code.eventsList10 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmainsObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmainsObjects2[i].getX() < (( gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length === 0 ) ? 0 :gdjs.jeu_951Code.GDmot_95953syllabesObjects2[0].getPointX("")) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmainsObjects2[k] = gdjs.jeu_951Code.GDmainsObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmainsObjects2.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects2);
/* Reuse gdjs.jeu_951Code.GDmainsObjects2 */
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects2);
{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects2[i].setX(gdjs.jeu_951Code.GDmainsObjects2[i].getX() + (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects2[i].setX(gdjs.jeu_951Code.GDsyllabesObjects2[i].getX() + (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects2[i].setX(gdjs.jeu_951Code.GDfond_9595vertObjects2[i].getX() + (32));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.jeu_951Code.GDmainsObjects1);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmainsObjects1.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmainsObjects1[i].getX() > (( gdjs.jeu_951Code.GDmot_95953syllabesObjects1.length === 0 ) ? 0 :gdjs.jeu_951Code.GDmot_95953syllabesObjects1[0].getPointX("")) ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmainsObjects1[k] = gdjs.jeu_951Code.GDmainsObjects1[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmainsObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects1);
/* Reuse gdjs.jeu_951Code.GDmainsObjects1 */
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects1);
{for(var i = 0, len = gdjs.jeu_951Code.GDmainsObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmainsObjects1[i].setX(gdjs.jeu_951Code.GDmainsObjects1[i].getX() - (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects1[i].setX(gdjs.jeu_951Code.GDsyllabesObjects1[i].getX() - (32));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects1[i].setX(gdjs.jeu_951Code.GDfond_9595vertObjects1[i].getX() - (32));
}
}}

}


};gdjs.jeu_951Code.eventsList11 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmot_959595951syllabeObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects2[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30960596);
}
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(5), false);
}
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.jeu_951Code.GDbouton_9595validerObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595validerObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.jeu_951Code.eventsList5(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmot_959595952syllabesObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects2[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(5), false);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.jeu_951Code.GDbouton_9595validerObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(2);
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595validerObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.jeu_951Code.eventsList6(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects2);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDmot_959595953syllabesObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95951syllabeObjects2[k] = gdjs.jeu_951Code.GDmot_95951syllabeObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95952syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95952syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length = k;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDmot_95953syllabesObjects2[k] = gdjs.jeu_951Code.GDmot_95953syllabesObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(5), false);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.jeu_951Code.GDbouton_9595validerObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(3).setNumber(3);
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595validerObjects2[i].hide(false);
}
}
{ //Subevents
gdjs.jeu_951Code.eventsList7(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_951Code.eventsList8(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_951Code.eventsList9(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == 3;
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_951Code.eventsList10(runtimeScene);} //End of subevents
}

}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDbouton_95959595validerObjects1Objects = Hashtable.newFrom({"bouton_valider": gdjs.jeu_951Code.GDbouton_9595validerObjects1});
gdjs.jeu_951Code.eventsList12 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")) < 20;
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.jeu_951Code.GDscore1Objects3);
{runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1").add(2);
}{for(var i = 0, len = gdjs.jeu_951Code.GDscore1Objects3.length ;i < len;++i) {
    gdjs.jeu_951Code.GDscore1Objects3[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(4)) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")) < 20;
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.jeu_951Code.GDscore1Objects2);
{runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1").add(1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDscore1Objects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDscore1Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")));
}
}}

}


};gdjs.jeu_951Code.eventsList13 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13597676);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_recommencer"), gdjs.jeu_951Code.GDbouton_9595recommencerObjects2);
gdjs.copyArray(gdjs.jeu_951Code.GDbouton_9595validerObjects1, gdjs.jeu_951Code.GDbouton_9595validerObjects2);

gdjs.copyArray(gdjs.jeu_951Code.GDvrai_9595fauxObjects1, gdjs.jeu_951Code.GDvrai_9595fauxObjects2);

{for(var i = 0, len = gdjs.jeu_951Code.GDvrai_9595fauxObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDvrai_9595fauxObjects2[i].setAnimation(0);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/vrai.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595validerObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595validerObjects2[i].hide();
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595recommencerObjects2.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595recommencerObjects2[i].hide(false);
}
}{gdjs.evtTools.variable.setVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(5), true);
}
{ //Subevents
gdjs.jeu_951Code.eventsList12(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(3)) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0));
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13602580);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.jeu_951Code.GDvrai_9595fauxObjects1 */
{for(var i = 0, len = gdjs.jeu_951Code.GDvrai_9595fauxObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDvrai_9595fauxObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/faux.mp3", 1, false, 100, 1);
}}

}


};gdjs.jeu_951Code.eventsList14 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.jeu_951Code.GDbouton_9595validerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDbouton_95959595validerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDbouton_9595validerObjects1.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDbouton_9595validerObjects1[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDbouton_9595validerObjects1[k] = gdjs.jeu_951Code.GDbouton_9595validerObjects1[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDbouton_9595validerObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("vrai_faux"), gdjs.jeu_951Code.GDvrai_9595fauxObjects1);
{for(var i = 0, len = gdjs.jeu_951Code.GDvrai_9595fauxObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDvrai_9595fauxObjects1[i].hide(false);
}
}{runtimeScene.getScene().getVariables().getFromIndex(4).add(1);
}
{ //Subevents
gdjs.jeu_951Code.eventsList13(runtimeScene);} //End of subevents
}

}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDbouton_95959595recommencerObjects2Objects = Hashtable.newFrom({"bouton_recommencer": gdjs.jeu_951Code.GDbouton_9595recommencerObjects2});
gdjs.jeu_951Code.eventsList15 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) < 5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_1", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 5;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


};gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.jeu_951Code.GDbouton_9595retourObjects1});
gdjs.jeu_951Code.eventsList16 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_recommencer"), gdjs.jeu_951Code.GDbouton_9595recommencerObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDbouton_95959595recommencerObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.jeu_951Code.GDbouton_9595recommencerObjects2.length;i<l;++i) {
    if ( gdjs.jeu_951Code.GDbouton_9595recommencerObjects2[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.jeu_951Code.GDbouton_9595recommencerObjects2[k] = gdjs.jeu_951Code.GDbouton_9595recommencerObjects2[i];
        ++k;
    }
}
gdjs.jeu_951Code.GDbouton_9595recommencerObjects2.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13604876);
}
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.jeu_951Code.eventsList15(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.jeu_951Code.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.jeu_951Code.mapOfGDgdjs_9546jeu_9595951Code_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13606740);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.jeu_951Code.eventsList17 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_recommencer"), gdjs.jeu_951Code.GDbouton_9595recommencerObjects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.jeu_951Code.GDbouton_9595validerObjects1);
gdjs.copyArray(runtimeScene.getObjects("fond_vert"), gdjs.jeu_951Code.GDfond_9595vertObjects1);
gdjs.copyArray(runtimeScene.getObjects("mot_1syllabe"), gdjs.jeu_951Code.GDmot_95951syllabeObjects1);
gdjs.copyArray(runtimeScene.getObjects("mot_2syllabes"), gdjs.jeu_951Code.GDmot_95952syllabesObjects1);
gdjs.copyArray(runtimeScene.getObjects("mot_3syllabes"), gdjs.jeu_951Code.GDmot_95953syllabesObjects1);
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.jeu_951Code.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("syllabes"), gdjs.jeu_951Code.GDsyllabesObjects1);
gdjs.copyArray(runtimeScene.getObjects("vrai_faux"), gdjs.jeu_951Code.GDvrai_9595fauxObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(gdjs.randomInRange(1, 3));
}{for(var i = 0, len = gdjs.jeu_951Code.GDsyllabesObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDsyllabesObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)));
}
}{runtimeScene.getGame().getVariables().getFromIndex(4).add(1);
}{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("1").setNumber(256);
}{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("2").setNumber(576);
}{runtimeScene.getScene().getVariables().getFromIndex(2).getChild("3").setNumber(896);
}{for(var i = 0, len = gdjs.jeu_951Code.GDmot_95951syllabeObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95951syllabeObjects1[i].hide();
}
for(var i = 0, len = gdjs.jeu_951Code.GDmot_95952syllabesObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95952syllabesObjects1[i].hide();
}
for(var i = 0, len = gdjs.jeu_951Code.GDmot_95953syllabesObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDmot_95953syllabesObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDvrai_9595fauxObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDvrai_9595fauxObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDscore1Objects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")));
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595validerObjects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.jeu_951Code.GDbouton_9595recommencerObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDbouton_9595recommencerObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.jeu_951Code.GDfond_9595vertObjects1.length ;i < len;++i) {
    gdjs.jeu_951Code.GDfond_9595vertObjects1[i].setAnimation(1);
}
}{gdjs.evtTools.variable.setVariableBoolean(runtimeScene.getScene().getVariables().getFromIndex(5), false);
}
{ //Subevents
gdjs.jeu_951Code.eventsList3(runtimeScene);} //End of subevents
}

}


{


gdjs.jeu_951Code.eventsList4(runtimeScene);
}


{



}


{



}


{


gdjs.jeu_951Code.eventsList11(runtimeScene);
}


{


gdjs.jeu_951Code.eventsList14(runtimeScene);
}


{


gdjs.jeu_951Code.eventsList16(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.jeu_951Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.jeu_951Code.GDvrai_9595fauxObjects1.length = 0;
gdjs.jeu_951Code.GDvrai_9595fauxObjects2.length = 0;
gdjs.jeu_951Code.GDvrai_9595fauxObjects3.length = 0;
gdjs.jeu_951Code.GDvrai_9595fauxObjects4.length = 0;
gdjs.jeu_951Code.GDbouton_9595validerObjects1.length = 0;
gdjs.jeu_951Code.GDbouton_9595validerObjects2.length = 0;
gdjs.jeu_951Code.GDbouton_9595validerObjects3.length = 0;
gdjs.jeu_951Code.GDbouton_9595validerObjects4.length = 0;
gdjs.jeu_951Code.GDbouton_9595recommencerObjects1.length = 0;
gdjs.jeu_951Code.GDbouton_9595recommencerObjects2.length = 0;
gdjs.jeu_951Code.GDbouton_9595recommencerObjects3.length = 0;
gdjs.jeu_951Code.GDbouton_9595recommencerObjects4.length = 0;
gdjs.jeu_951Code.GDNewObjectObjects1.length = 0;
gdjs.jeu_951Code.GDNewObjectObjects2.length = 0;
gdjs.jeu_951Code.GDNewObjectObjects3.length = 0;
gdjs.jeu_951Code.GDNewObjectObjects4.length = 0;
gdjs.jeu_951Code.GDsyllabesObjects1.length = 0;
gdjs.jeu_951Code.GDsyllabesObjects2.length = 0;
gdjs.jeu_951Code.GDsyllabesObjects3.length = 0;
gdjs.jeu_951Code.GDsyllabesObjects4.length = 0;
gdjs.jeu_951Code.GDscore2Objects1.length = 0;
gdjs.jeu_951Code.GDscore2Objects2.length = 0;
gdjs.jeu_951Code.GDscore2Objects3.length = 0;
gdjs.jeu_951Code.GDscore2Objects4.length = 0;
gdjs.jeu_951Code.GDscore1Objects1.length = 0;
gdjs.jeu_951Code.GDscore1Objects2.length = 0;
gdjs.jeu_951Code.GDscore1Objects3.length = 0;
gdjs.jeu_951Code.GDscore1Objects4.length = 0;
gdjs.jeu_951Code.GDmains3Objects1.length = 0;
gdjs.jeu_951Code.GDmains3Objects2.length = 0;
gdjs.jeu_951Code.GDmains3Objects3.length = 0;
gdjs.jeu_951Code.GDmains3Objects4.length = 0;
gdjs.jeu_951Code.GDmains2Objects1.length = 0;
gdjs.jeu_951Code.GDmains2Objects2.length = 0;
gdjs.jeu_951Code.GDmains2Objects3.length = 0;
gdjs.jeu_951Code.GDmains2Objects4.length = 0;
gdjs.jeu_951Code.GDmains1Objects1.length = 0;
gdjs.jeu_951Code.GDmains1Objects2.length = 0;
gdjs.jeu_951Code.GDmains1Objects3.length = 0;
gdjs.jeu_951Code.GDmains1Objects4.length = 0;
gdjs.jeu_951Code.GDmainsObjects1.length = 0;
gdjs.jeu_951Code.GDmainsObjects2.length = 0;
gdjs.jeu_951Code.GDmainsObjects3.length = 0;
gdjs.jeu_951Code.GDmainsObjects4.length = 0;
gdjs.jeu_951Code.GDtheatreObjects1.length = 0;
gdjs.jeu_951Code.GDtheatreObjects2.length = 0;
gdjs.jeu_951Code.GDtheatreObjects3.length = 0;
gdjs.jeu_951Code.GDtheatreObjects4.length = 0;
gdjs.jeu_951Code.GDfilleObjects1.length = 0;
gdjs.jeu_951Code.GDfilleObjects2.length = 0;
gdjs.jeu_951Code.GDfilleObjects3.length = 0;
gdjs.jeu_951Code.GDfilleObjects4.length = 0;
gdjs.jeu_951Code.GDgarconObjects1.length = 0;
gdjs.jeu_951Code.GDgarconObjects2.length = 0;
gdjs.jeu_951Code.GDgarconObjects3.length = 0;
gdjs.jeu_951Code.GDgarconObjects4.length = 0;
gdjs.jeu_951Code.GDmot_95951syllabeObjects1.length = 0;
gdjs.jeu_951Code.GDmot_95951syllabeObjects2.length = 0;
gdjs.jeu_951Code.GDmot_95951syllabeObjects3.length = 0;
gdjs.jeu_951Code.GDmot_95951syllabeObjects4.length = 0;
gdjs.jeu_951Code.GDmot_95952syllabesObjects1.length = 0;
gdjs.jeu_951Code.GDmot_95952syllabesObjects2.length = 0;
gdjs.jeu_951Code.GDmot_95952syllabesObjects3.length = 0;
gdjs.jeu_951Code.GDmot_95952syllabesObjects4.length = 0;
gdjs.jeu_951Code.GDmot_95953syllabesObjects1.length = 0;
gdjs.jeu_951Code.GDmot_95953syllabesObjects2.length = 0;
gdjs.jeu_951Code.GDmot_95953syllabesObjects3.length = 0;
gdjs.jeu_951Code.GDmot_95953syllabesObjects4.length = 0;
gdjs.jeu_951Code.GDfond_9595vertObjects1.length = 0;
gdjs.jeu_951Code.GDfond_9595vertObjects2.length = 0;
gdjs.jeu_951Code.GDfond_9595vertObjects3.length = 0;
gdjs.jeu_951Code.GDfond_9595vertObjects4.length = 0;
gdjs.jeu_951Code.GDbouton_9595retourObjects1.length = 0;
gdjs.jeu_951Code.GDbouton_9595retourObjects2.length = 0;
gdjs.jeu_951Code.GDbouton_9595retourObjects3.length = 0;
gdjs.jeu_951Code.GDbouton_9595retourObjects4.length = 0;

gdjs.jeu_951Code.eventsList17(runtimeScene);

return;

}

gdjs['jeu_951Code'] = gdjs.jeu_951Code;
