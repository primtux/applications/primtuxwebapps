gdjs.menuCode = {};
gdjs.menuCode.GDjeu2Objects1= [];
gdjs.menuCode.GDjeu2Objects2= [];
gdjs.menuCode.GDjeu1Objects1= [];
gdjs.menuCode.GDjeu1Objects2= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDplein_9595ecranObjects1= [];
gdjs.menuCode.GDplein_9595ecranObjects2= [];
gdjs.menuCode.GDbouton_9595infosObjects1= [];
gdjs.menuCode.GDbouton_9595infosObjects2= [];
gdjs.menuCode.GDscore2Objects1= [];
gdjs.menuCode.GDscore2Objects2= [];
gdjs.menuCode.GDscore1Objects1= [];
gdjs.menuCode.GDscore1Objects2= [];
gdjs.menuCode.GDmains3Objects1= [];
gdjs.menuCode.GDmains3Objects2= [];
gdjs.menuCode.GDmains2Objects1= [];
gdjs.menuCode.GDmains2Objects2= [];
gdjs.menuCode.GDmains1Objects1= [];
gdjs.menuCode.GDmains1Objects2= [];
gdjs.menuCode.GDmainsObjects1= [];
gdjs.menuCode.GDmainsObjects2= [];
gdjs.menuCode.GDtheatreObjects1= [];
gdjs.menuCode.GDtheatreObjects2= [];
gdjs.menuCode.GDfilleObjects1= [];
gdjs.menuCode.GDfilleObjects2= [];
gdjs.menuCode.GDgarconObjects1= [];
gdjs.menuCode.GDgarconObjects2= [];
gdjs.menuCode.GDmot_95951syllabeObjects1= [];
gdjs.menuCode.GDmot_95951syllabeObjects2= [];
gdjs.menuCode.GDmot_95952syllabesObjects1= [];
gdjs.menuCode.GDmot_95952syllabesObjects2= [];
gdjs.menuCode.GDmot_95953syllabesObjects1= [];
gdjs.menuCode.GDmot_95953syllabesObjects2= [];
gdjs.menuCode.GDfond_9595vertObjects1= [];
gdjs.menuCode.GDfond_9595vertObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];


gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu1Objects1ObjectsGDgdjs_9546menuCode_9546GDscore1Objects1ObjectsGDgdjs_9546menuCode_9546GDfilleObjects1Objects = Hashtable.newFrom({"jeu1": gdjs.menuCode.GDjeu1Objects1, "score1": gdjs.menuCode.GDscore1Objects1, "fille": gdjs.menuCode.GDfilleObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu2Objects1ObjectsGDgdjs_9546menuCode_9546GDscore2Objects1ObjectsGDgdjs_9546menuCode_9546GDgarconObjects1Objects = Hashtable.newFrom({"jeu2": gdjs.menuCode.GDjeu2Objects1, "score2": gdjs.menuCode.GDscore2Objects1, "garcon": gdjs.menuCode.GDgarconObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects = Hashtable.newFrom({"bouton_infos": gdjs.menuCode.GDbouton_9595infosObjects1});
gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("mains"), gdjs.menuCode.GDmainsObjects1);
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("2")));
}
}{for(var i = 0, len = gdjs.menuCode.GDmainsObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDmainsObjects1[i].setAnimation(2);
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("fille"), gdjs.menuCode.GDfilleObjects1);
gdjs.copyArray(runtimeScene.getObjects("jeu1"), gdjs.menuCode.GDjeu1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu1Objects1ObjectsGDgdjs_9546menuCode_9546GDscore1Objects1ObjectsGDgdjs_9546menuCode_9546GDfilleObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("garcon"), gdjs.menuCode.GDgarconObjects1);
gdjs.copyArray(runtimeScene.getObjects("jeu2"), gdjs.menuCode.GDjeu2Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu2Objects1ObjectsGDgdjs_9546menuCode_9546GDscore2Objects1ObjectsGDgdjs_9546menuCode_9546GDgarconObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30082852);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30083796);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score1"), gdjs.menuCode.GDscore1Objects1);
gdjs.copyArray(runtimeScene.getObjects("score2"), gdjs.menuCode.GDscore2Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(5).getChild("2").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore1Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore2Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5).getChild("2")));
}
}}

}


{



}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_infos"), gdjs.menuCode.GDbouton_9595infosObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDjeu2Objects1.length = 0;
gdjs.menuCode.GDjeu2Objects2.length = 0;
gdjs.menuCode.GDjeu1Objects1.length = 0;
gdjs.menuCode.GDjeu1Objects2.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects1.length = 0;
gdjs.menuCode.GDplein_9595ecranObjects2.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects1.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects2.length = 0;
gdjs.menuCode.GDscore2Objects1.length = 0;
gdjs.menuCode.GDscore2Objects2.length = 0;
gdjs.menuCode.GDscore1Objects1.length = 0;
gdjs.menuCode.GDscore1Objects2.length = 0;
gdjs.menuCode.GDmains3Objects1.length = 0;
gdjs.menuCode.GDmains3Objects2.length = 0;
gdjs.menuCode.GDmains2Objects1.length = 0;
gdjs.menuCode.GDmains2Objects2.length = 0;
gdjs.menuCode.GDmains1Objects1.length = 0;
gdjs.menuCode.GDmains1Objects2.length = 0;
gdjs.menuCode.GDmainsObjects1.length = 0;
gdjs.menuCode.GDmainsObjects2.length = 0;
gdjs.menuCode.GDtheatreObjects1.length = 0;
gdjs.menuCode.GDtheatreObjects2.length = 0;
gdjs.menuCode.GDfilleObjects1.length = 0;
gdjs.menuCode.GDfilleObjects2.length = 0;
gdjs.menuCode.GDgarconObjects1.length = 0;
gdjs.menuCode.GDgarconObjects2.length = 0;
gdjs.menuCode.GDmot_95951syllabeObjects1.length = 0;
gdjs.menuCode.GDmot_95951syllabeObjects2.length = 0;
gdjs.menuCode.GDmot_95952syllabesObjects1.length = 0;
gdjs.menuCode.GDmot_95952syllabesObjects2.length = 0;
gdjs.menuCode.GDmot_95953syllabesObjects1.length = 0;
gdjs.menuCode.GDmot_95953syllabesObjects2.length = 0;
gdjs.menuCode.GDfond_9595vertObjects1.length = 0;
gdjs.menuCode.GDfond_9595vertObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;

gdjs.menuCode.eventsList0(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
