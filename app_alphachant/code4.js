gdjs.infosCode = {};
gdjs.infosCode.GDNewSpriteObjects1= [];
gdjs.infosCode.GDNewSpriteObjects2= [];
gdjs.infosCode.GDfond_9595blancObjects1= [];
gdjs.infosCode.GDfond_9595blancObjects2= [];
gdjs.infosCode.GDtxt_9595infosObjects1= [];
gdjs.infosCode.GDtxt_9595infosObjects2= [];
gdjs.infosCode.GDpiano_9595joueObjects1= [];
gdjs.infosCode.GDpiano_9595joueObjects2= [];
gdjs.infosCode.GDzone_9595depot_9595Objects1= [];
gdjs.infosCode.GDzone_9595depot_9595Objects2= [];
gdjs.infosCode.GDzone_9595depot_95952Objects1= [];
gdjs.infosCode.GDzone_9595depot_95952Objects2= [];
gdjs.infosCode.GDzone_9595depot_95953Objects1= [];
gdjs.infosCode.GDzone_9595depot_95953Objects2= [];
gdjs.infosCode.GDzone_9595depot_95954Objects1= [];
gdjs.infosCode.GDzone_9595depot_95954Objects2= [];
gdjs.infosCode.GDzone_9595depot_95955Objects1= [];
gdjs.infosCode.GDzone_9595depot_95955Objects2= [];
gdjs.infosCode.GDlettre_9595choix_9595Objects1= [];
gdjs.infosCode.GDlettre_9595choix_9595Objects2= [];
gdjs.infosCode.GDlettre_9595choix_95952Objects1= [];
gdjs.infosCode.GDlettre_9595choix_95952Objects2= [];
gdjs.infosCode.GDlettre_9595choix_95953Objects1= [];
gdjs.infosCode.GDlettre_9595choix_95953Objects2= [];
gdjs.infosCode.GDlettre_9595choix_95954Objects1= [];
gdjs.infosCode.GDlettre_9595choix_95954Objects2= [];
gdjs.infosCode.GDlettre_9595choix_95955Objects1= [];
gdjs.infosCode.GDlettre_9595choix_95955Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDbouton_9595playObjects1= [];
gdjs.infosCode.GDbouton_9595playObjects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDsergeObjects1= [];
gdjs.infosCode.GDsergeObjects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDbouton_9595pauseObjects1= [];
gdjs.infosCode.GDbouton_9595pauseObjects2= [];
gdjs.infosCode.GDbouton_9595razObjects1= [];
gdjs.infosCode.GDbouton_9595razObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_9595blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_9595blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_9595blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDNewSpriteObjects1.length = 0;
gdjs.infosCode.GDNewSpriteObjects2.length = 0;
gdjs.infosCode.GDfond_9595blancObjects1.length = 0;
gdjs.infosCode.GDfond_9595blancObjects2.length = 0;
gdjs.infosCode.GDtxt_9595infosObjects1.length = 0;
gdjs.infosCode.GDtxt_9595infosObjects2.length = 0;
gdjs.infosCode.GDpiano_9595joueObjects1.length = 0;
gdjs.infosCode.GDpiano_9595joueObjects2.length = 0;
gdjs.infosCode.GDzone_9595depot_9595Objects1.length = 0;
gdjs.infosCode.GDzone_9595depot_9595Objects2.length = 0;
gdjs.infosCode.GDzone_9595depot_95952Objects1.length = 0;
gdjs.infosCode.GDzone_9595depot_95952Objects2.length = 0;
gdjs.infosCode.GDzone_9595depot_95953Objects1.length = 0;
gdjs.infosCode.GDzone_9595depot_95953Objects2.length = 0;
gdjs.infosCode.GDzone_9595depot_95954Objects1.length = 0;
gdjs.infosCode.GDzone_9595depot_95954Objects2.length = 0;
gdjs.infosCode.GDzone_9595depot_95955Objects1.length = 0;
gdjs.infosCode.GDzone_9595depot_95955Objects2.length = 0;
gdjs.infosCode.GDlettre_9595choix_9595Objects1.length = 0;
gdjs.infosCode.GDlettre_9595choix_9595Objects2.length = 0;
gdjs.infosCode.GDlettre_9595choix_95952Objects1.length = 0;
gdjs.infosCode.GDlettre_9595choix_95952Objects2.length = 0;
gdjs.infosCode.GDlettre_9595choix_95953Objects1.length = 0;
gdjs.infosCode.GDlettre_9595choix_95953Objects2.length = 0;
gdjs.infosCode.GDlettre_9595choix_95954Objects1.length = 0;
gdjs.infosCode.GDlettre_9595choix_95954Objects2.length = 0;
gdjs.infosCode.GDlettre_9595choix_95955Objects1.length = 0;
gdjs.infosCode.GDlettre_9595choix_95955Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDbouton_9595playObjects1.length = 0;
gdjs.infosCode.GDbouton_9595playObjects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDsergeObjects1.length = 0;
gdjs.infosCode.GDsergeObjects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDbouton_9595pauseObjects1.length = 0;
gdjs.infosCode.GDbouton_9595pauseObjects2.length = 0;
gdjs.infosCode.GDbouton_9595razObjects1.length = 0;
gdjs.infosCode.GDbouton_9595razObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
