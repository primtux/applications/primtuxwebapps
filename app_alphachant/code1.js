gdjs.tirageCode = {};
gdjs.tirageCode.GDpianoObjects1= [];
gdjs.tirageCode.GDpianoObjects2= [];
gdjs.tirageCode.GDpianoObjects3= [];
gdjs.tirageCode.GDpiano_9595joueObjects1= [];
gdjs.tirageCode.GDpiano_9595joueObjects2= [];
gdjs.tirageCode.GDpiano_9595joueObjects3= [];
gdjs.tirageCode.GDzone_9595depot_9595Objects1= [];
gdjs.tirageCode.GDzone_9595depot_9595Objects2= [];
gdjs.tirageCode.GDzone_9595depot_9595Objects3= [];
gdjs.tirageCode.GDzone_9595depot_95952Objects1= [];
gdjs.tirageCode.GDzone_9595depot_95952Objects2= [];
gdjs.tirageCode.GDzone_9595depot_95952Objects3= [];
gdjs.tirageCode.GDzone_9595depot_95953Objects1= [];
gdjs.tirageCode.GDzone_9595depot_95953Objects2= [];
gdjs.tirageCode.GDzone_9595depot_95953Objects3= [];
gdjs.tirageCode.GDzone_9595depot_95954Objects1= [];
gdjs.tirageCode.GDzone_9595depot_95954Objects2= [];
gdjs.tirageCode.GDzone_9595depot_95954Objects3= [];
gdjs.tirageCode.GDzone_9595depot_95955Objects1= [];
gdjs.tirageCode.GDzone_9595depot_95955Objects2= [];
gdjs.tirageCode.GDzone_9595depot_95955Objects3= [];
gdjs.tirageCode.GDlettre_9595choix_9595Objects1= [];
gdjs.tirageCode.GDlettre_9595choix_9595Objects2= [];
gdjs.tirageCode.GDlettre_9595choix_9595Objects3= [];
gdjs.tirageCode.GDlettre_9595choix_95952Objects1= [];
gdjs.tirageCode.GDlettre_9595choix_95952Objects2= [];
gdjs.tirageCode.GDlettre_9595choix_95952Objects3= [];
gdjs.tirageCode.GDlettre_9595choix_95953Objects1= [];
gdjs.tirageCode.GDlettre_9595choix_95953Objects2= [];
gdjs.tirageCode.GDlettre_9595choix_95953Objects3= [];
gdjs.tirageCode.GDlettre_9595choix_95954Objects1= [];
gdjs.tirageCode.GDlettre_9595choix_95954Objects2= [];
gdjs.tirageCode.GDlettre_9595choix_95954Objects3= [];
gdjs.tirageCode.GDlettre_9595choix_95955Objects1= [];
gdjs.tirageCode.GDlettre_9595choix_95955Objects2= [];
gdjs.tirageCode.GDlettre_9595choix_95955Objects3= [];
gdjs.tirageCode.GDfondObjects1= [];
gdjs.tirageCode.GDfondObjects2= [];
gdjs.tirageCode.GDfondObjects3= [];
gdjs.tirageCode.GDbouton_9595playObjects1= [];
gdjs.tirageCode.GDbouton_9595playObjects2= [];
gdjs.tirageCode.GDbouton_9595playObjects3= [];
gdjs.tirageCode.GDbouton_9595retourObjects1= [];
gdjs.tirageCode.GDbouton_9595retourObjects2= [];
gdjs.tirageCode.GDbouton_9595retourObjects3= [];
gdjs.tirageCode.GDsergeObjects1= [];
gdjs.tirageCode.GDsergeObjects2= [];
gdjs.tirageCode.GDsergeObjects3= [];
gdjs.tirageCode.GDscore1Objects1= [];
gdjs.tirageCode.GDscore1Objects2= [];
gdjs.tirageCode.GDscore1Objects3= [];
gdjs.tirageCode.GDscore2Objects1= [];
gdjs.tirageCode.GDscore2Objects2= [];
gdjs.tirageCode.GDscore2Objects3= [];
gdjs.tirageCode.GDbouton_9595pauseObjects1= [];
gdjs.tirageCode.GDbouton_9595pauseObjects2= [];
gdjs.tirageCode.GDbouton_9595pauseObjects3= [];
gdjs.tirageCode.GDbouton_9595razObjects1= [];
gdjs.tirageCode.GDbouton_9595razObjects2= [];
gdjs.tirageCode.GDbouton_9595razObjects3= [];


gdjs.tirageCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_2", false);
}}

}


};gdjs.tirageCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1").setNumber(gdjs.randomInRange(1, 4));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(2);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2").setNumber(gdjs.randomInRange(1, 3));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(3);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3").setNumber(gdjs.randomInRange(1, 4));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(4);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4").setNumber(gdjs.randomInRange(1, 5));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(5);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("5").setNumber(gdjs.randomInRange(1, 3));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(6);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("6").setNumber(gdjs.randomInRange(1, 3));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(7);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("7").setNumber(gdjs.randomInRange(1, 4));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(8);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 8;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.tirageCode.eventsList0(runtimeScene);} //End of subevents
}

}


};gdjs.tirageCode.eventsList2 = function(runtimeScene) {

};gdjs.tirageCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("piano_joue"), gdjs.tirageCode.GDpiano_9595joueObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/intro.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirageCode.GDpiano_9595joueObjects1.length ;i < len;++i) {
    gdjs.tirageCode.GDpiano_9595joueObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirageCode.eventsList1(runtimeScene);
}


{


gdjs.tirageCode.eventsList2(runtimeScene);
}


};

gdjs.tirageCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirageCode.GDpianoObjects1.length = 0;
gdjs.tirageCode.GDpianoObjects2.length = 0;
gdjs.tirageCode.GDpianoObjects3.length = 0;
gdjs.tirageCode.GDpiano_9595joueObjects1.length = 0;
gdjs.tirageCode.GDpiano_9595joueObjects2.length = 0;
gdjs.tirageCode.GDpiano_9595joueObjects3.length = 0;
gdjs.tirageCode.GDzone_9595depot_9595Objects1.length = 0;
gdjs.tirageCode.GDzone_9595depot_9595Objects2.length = 0;
gdjs.tirageCode.GDzone_9595depot_9595Objects3.length = 0;
gdjs.tirageCode.GDzone_9595depot_95952Objects1.length = 0;
gdjs.tirageCode.GDzone_9595depot_95952Objects2.length = 0;
gdjs.tirageCode.GDzone_9595depot_95952Objects3.length = 0;
gdjs.tirageCode.GDzone_9595depot_95953Objects1.length = 0;
gdjs.tirageCode.GDzone_9595depot_95953Objects2.length = 0;
gdjs.tirageCode.GDzone_9595depot_95953Objects3.length = 0;
gdjs.tirageCode.GDzone_9595depot_95954Objects1.length = 0;
gdjs.tirageCode.GDzone_9595depot_95954Objects2.length = 0;
gdjs.tirageCode.GDzone_9595depot_95954Objects3.length = 0;
gdjs.tirageCode.GDzone_9595depot_95955Objects1.length = 0;
gdjs.tirageCode.GDzone_9595depot_95955Objects2.length = 0;
gdjs.tirageCode.GDzone_9595depot_95955Objects3.length = 0;
gdjs.tirageCode.GDlettre_9595choix_9595Objects1.length = 0;
gdjs.tirageCode.GDlettre_9595choix_9595Objects2.length = 0;
gdjs.tirageCode.GDlettre_9595choix_9595Objects3.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95952Objects1.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95952Objects2.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95952Objects3.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95953Objects1.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95953Objects2.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95953Objects3.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95954Objects1.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95954Objects2.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95954Objects3.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95955Objects1.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95955Objects2.length = 0;
gdjs.tirageCode.GDlettre_9595choix_95955Objects3.length = 0;
gdjs.tirageCode.GDfondObjects1.length = 0;
gdjs.tirageCode.GDfondObjects2.length = 0;
gdjs.tirageCode.GDfondObjects3.length = 0;
gdjs.tirageCode.GDbouton_9595playObjects1.length = 0;
gdjs.tirageCode.GDbouton_9595playObjects2.length = 0;
gdjs.tirageCode.GDbouton_9595playObjects3.length = 0;
gdjs.tirageCode.GDbouton_9595retourObjects1.length = 0;
gdjs.tirageCode.GDbouton_9595retourObjects2.length = 0;
gdjs.tirageCode.GDbouton_9595retourObjects3.length = 0;
gdjs.tirageCode.GDsergeObjects1.length = 0;
gdjs.tirageCode.GDsergeObjects2.length = 0;
gdjs.tirageCode.GDsergeObjects3.length = 0;
gdjs.tirageCode.GDscore1Objects1.length = 0;
gdjs.tirageCode.GDscore1Objects2.length = 0;
gdjs.tirageCode.GDscore1Objects3.length = 0;
gdjs.tirageCode.GDscore2Objects1.length = 0;
gdjs.tirageCode.GDscore2Objects2.length = 0;
gdjs.tirageCode.GDscore2Objects3.length = 0;
gdjs.tirageCode.GDbouton_9595pauseObjects1.length = 0;
gdjs.tirageCode.GDbouton_9595pauseObjects2.length = 0;
gdjs.tirageCode.GDbouton_9595pauseObjects3.length = 0;
gdjs.tirageCode.GDbouton_9595razObjects1.length = 0;
gdjs.tirageCode.GDbouton_9595razObjects2.length = 0;
gdjs.tirageCode.GDbouton_9595razObjects3.length = 0;

gdjs.tirageCode.eventsList3(runtimeScene);

return;

}

gdjs['tirageCode'] = gdjs.tirageCode;
