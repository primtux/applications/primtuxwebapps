gdjs.codeCode = {};
gdjs.codeCode.GDtexte_9595codeObjects1= [];
gdjs.codeCode.GDtexte_9595codeObjects2= [];
gdjs.codeCode.GDtexte_9595codeObjects3= [];
gdjs.codeCode.GDchoix_9595nombre2Objects1= [];
gdjs.codeCode.GDchoix_9595nombre2Objects2= [];
gdjs.codeCode.GDchoix_9595nombre2Objects3= [];
gdjs.codeCode.GDchoix_9595nombre3Objects1= [];
gdjs.codeCode.GDchoix_9595nombre3Objects2= [];
gdjs.codeCode.GDchoix_9595nombre3Objects3= [];
gdjs.codeCode.GDchoix_9595nombre4Objects1= [];
gdjs.codeCode.GDchoix_9595nombre4Objects2= [];
gdjs.codeCode.GDchoix_9595nombre4Objects3= [];
gdjs.codeCode.GDchoix_9595nombre5Objects1= [];
gdjs.codeCode.GDchoix_9595nombre5Objects2= [];
gdjs.codeCode.GDchoix_9595nombre5Objects3= [];
gdjs.codeCode.GDchoix_9595nombre1Objects1= [];
gdjs.codeCode.GDchoix_9595nombre1Objects2= [];
gdjs.codeCode.GDchoix_9595nombre1Objects3= [];
gdjs.codeCode.GDbouton_9595recommencerObjects1= [];
gdjs.codeCode.GDbouton_9595recommencerObjects2= [];
gdjs.codeCode.GDbouton_9595recommencerObjects3= [];
gdjs.codeCode.GDbouton_9595suivantObjects1= [];
gdjs.codeCode.GDbouton_9595suivantObjects2= [];
gdjs.codeCode.GDbouton_9595suivantObjects3= [];
gdjs.codeCode.GDfantome1Objects1= [];
gdjs.codeCode.GDfantome1Objects2= [];
gdjs.codeCode.GDfantome1Objects3= [];
gdjs.codeCode.GDscore4Objects1= [];
gdjs.codeCode.GDscore4Objects2= [];
gdjs.codeCode.GDscore4Objects3= [];
gdjs.codeCode.GDscore3Objects1= [];
gdjs.codeCode.GDscore3Objects2= [];
gdjs.codeCode.GDscore3Objects3= [];
gdjs.codeCode.GDscore2Objects1= [];
gdjs.codeCode.GDscore2Objects2= [];
gdjs.codeCode.GDscore2Objects3= [];
gdjs.codeCode.GDscore1Objects1= [];
gdjs.codeCode.GDscore1Objects2= [];
gdjs.codeCode.GDscore1Objects3= [];
gdjs.codeCode.GDbouton_9595retourObjects1= [];
gdjs.codeCode.GDbouton_9595retourObjects2= [];
gdjs.codeCode.GDbouton_9595retourObjects3= [];
gdjs.codeCode.GDfantome_9595bleuObjects1= [];
gdjs.codeCode.GDfantome_9595bleuObjects2= [];
gdjs.codeCode.GDfantome_9595bleuObjects3= [];
gdjs.codeCode.GDfantome_9595rougeObjects1= [];
gdjs.codeCode.GDfantome_9595rougeObjects2= [];
gdjs.codeCode.GDfantome_9595rougeObjects3= [];
gdjs.codeCode.GDfantome_9595jauneObjects1= [];
gdjs.codeCode.GDfantome_9595jauneObjects2= [];
gdjs.codeCode.GDfantome_9595jauneObjects3= [];
gdjs.codeCode.GDfantome_9595vertObjects1= [];
gdjs.codeCode.GDfantome_9595vertObjects2= [];
gdjs.codeCode.GDfantome_9595vertObjects3= [];
gdjs.codeCode.GDfantome5Objects1= [];
gdjs.codeCode.GDfantome5Objects2= [];
gdjs.codeCode.GDfantome5Objects3= [];
gdjs.codeCode.GDfond_9595titreObjects1= [];
gdjs.codeCode.GDfond_9595titreObjects2= [];
gdjs.codeCode.GDfond_9595titreObjects3= [];
gdjs.codeCode.GDserge_9595caracteresObjects1= [];
gdjs.codeCode.GDserge_9595caracteresObjects2= [];
gdjs.codeCode.GDserge_9595caracteresObjects3= [];
gdjs.codeCode.GDserge_9595lamaObjects1= [];
gdjs.codeCode.GDserge_9595lamaObjects2= [];
gdjs.codeCode.GDserge_9595lamaObjects3= [];


gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre3Objects1Objects = Hashtable.newFrom({"choix_nombre3": gdjs.codeCode.GDchoix_9595nombre3Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30382372);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30383524);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre5Objects1Objects = Hashtable.newFrom({"choix_nombre5": gdjs.codeCode.GDchoix_9595nombre5Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre2Objects1Objects = Hashtable.newFrom({"choix_nombre2": gdjs.codeCode.GDchoix_9595nombre2Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 2;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre1Objects1ObjectsGDgdjs_9546codeCode_9546GDchoix_95959595nombre4Objects1Objects = Hashtable.newFrom({"choix_nombre1": gdjs.codeCode.GDchoix_9595nombre1Objects1, "choix_nombre4": gdjs.codeCode.GDchoix_9595nombre4Objects1});
gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_9595retourObjects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre3"), gdjs.codeCode.GDchoix_9595nombre3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30381644);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre5"), gdjs.codeCode.GDchoix_9595nombre5Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre5Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30384804);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre2"), gdjs.codeCode.GDchoix_9595nombre2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30387228);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre1"), gdjs.codeCode.GDchoix_9595nombre1Objects1);
gdjs.copyArray(runtimeScene.getObjects("choix_nombre4"), gdjs.codeCode.GDchoix_9595nombre4Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDchoix_95959595nombre1Objects1ObjectsGDgdjs_9546codeCode_9546GDchoix_95959595nombre4Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(30389812);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDtexte_9595codeObjects1.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects2.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre2Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre2Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre2Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre3Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre3Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre3Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre4Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre4Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre4Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre5Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre5Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre5Objects3.length = 0;
gdjs.codeCode.GDchoix_9595nombre1Objects1.length = 0;
gdjs.codeCode.GDchoix_9595nombre1Objects2.length = 0;
gdjs.codeCode.GDchoix_9595nombre1Objects3.length = 0;
gdjs.codeCode.GDbouton_9595recommencerObjects1.length = 0;
gdjs.codeCode.GDbouton_9595recommencerObjects2.length = 0;
gdjs.codeCode.GDbouton_9595recommencerObjects3.length = 0;
gdjs.codeCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.codeCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.codeCode.GDbouton_9595suivantObjects3.length = 0;
gdjs.codeCode.GDfantome1Objects1.length = 0;
gdjs.codeCode.GDfantome1Objects2.length = 0;
gdjs.codeCode.GDfantome1Objects3.length = 0;
gdjs.codeCode.GDscore4Objects1.length = 0;
gdjs.codeCode.GDscore4Objects2.length = 0;
gdjs.codeCode.GDscore4Objects3.length = 0;
gdjs.codeCode.GDscore3Objects1.length = 0;
gdjs.codeCode.GDscore3Objects2.length = 0;
gdjs.codeCode.GDscore3Objects3.length = 0;
gdjs.codeCode.GDscore2Objects1.length = 0;
gdjs.codeCode.GDscore2Objects2.length = 0;
gdjs.codeCode.GDscore2Objects3.length = 0;
gdjs.codeCode.GDscore1Objects1.length = 0;
gdjs.codeCode.GDscore1Objects2.length = 0;
gdjs.codeCode.GDscore1Objects3.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects1.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects2.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects3.length = 0;
gdjs.codeCode.GDfantome_9595bleuObjects1.length = 0;
gdjs.codeCode.GDfantome_9595bleuObjects2.length = 0;
gdjs.codeCode.GDfantome_9595bleuObjects3.length = 0;
gdjs.codeCode.GDfantome_9595rougeObjects1.length = 0;
gdjs.codeCode.GDfantome_9595rougeObjects2.length = 0;
gdjs.codeCode.GDfantome_9595rougeObjects3.length = 0;
gdjs.codeCode.GDfantome_9595jauneObjects1.length = 0;
gdjs.codeCode.GDfantome_9595jauneObjects2.length = 0;
gdjs.codeCode.GDfantome_9595jauneObjects3.length = 0;
gdjs.codeCode.GDfantome_9595vertObjects1.length = 0;
gdjs.codeCode.GDfantome_9595vertObjects2.length = 0;
gdjs.codeCode.GDfantome_9595vertObjects3.length = 0;
gdjs.codeCode.GDfantome5Objects1.length = 0;
gdjs.codeCode.GDfantome5Objects2.length = 0;
gdjs.codeCode.GDfantome5Objects3.length = 0;
gdjs.codeCode.GDfond_9595titreObjects1.length = 0;
gdjs.codeCode.GDfond_9595titreObjects2.length = 0;
gdjs.codeCode.GDfond_9595titreObjects3.length = 0;
gdjs.codeCode.GDserge_9595caracteresObjects1.length = 0;
gdjs.codeCode.GDserge_9595caracteresObjects2.length = 0;
gdjs.codeCode.GDserge_9595caracteresObjects3.length = 0;
gdjs.codeCode.GDserge_9595lamaObjects1.length = 0;
gdjs.codeCode.GDserge_9595lamaObjects2.length = 0;
gdjs.codeCode.GDserge_9595lamaObjects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);

return;

}

gdjs['codeCode'] = gdjs.codeCode;
