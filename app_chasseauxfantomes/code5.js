gdjs.infosCode = {};
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDbouton_9595recommencerObjects1= [];
gdjs.infosCode.GDbouton_9595recommencerObjects2= [];
gdjs.infosCode.GDbouton_9595suivantObjects1= [];
gdjs.infosCode.GDbouton_9595suivantObjects2= [];
gdjs.infosCode.GDfantome1Objects1= [];
gdjs.infosCode.GDfantome1Objects2= [];
gdjs.infosCode.GDscore4Objects1= [];
gdjs.infosCode.GDscore4Objects2= [];
gdjs.infosCode.GDscore3Objects1= [];
gdjs.infosCode.GDscore3Objects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDfantome_9595bleuObjects1= [];
gdjs.infosCode.GDfantome_9595bleuObjects2= [];
gdjs.infosCode.GDfantome_9595rougeObjects1= [];
gdjs.infosCode.GDfantome_9595rougeObjects2= [];
gdjs.infosCode.GDfantome_9595jauneObjects1= [];
gdjs.infosCode.GDfantome_9595jauneObjects2= [];
gdjs.infosCode.GDfantome_9595vertObjects1= [];
gdjs.infosCode.GDfantome_9595vertObjects2= [];
gdjs.infosCode.GDfantome5Objects1= [];
gdjs.infosCode.GDfantome5Objects2= [];
gdjs.infosCode.GDfond_9595titreObjects1= [];
gdjs.infosCode.GDfond_9595titreObjects2= [];
gdjs.infosCode.GDserge_9595caracteresObjects1= [];
gdjs.infosCode.GDserge_9595caracteresObjects2= [];
gdjs.infosCode.GDserge_9595lamaObjects1= [];
gdjs.infosCode.GDserge_9595lamaObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDbouton_9595recommencerObjects1.length = 0;
gdjs.infosCode.GDbouton_9595recommencerObjects2.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.infosCode.GDfantome1Objects1.length = 0;
gdjs.infosCode.GDfantome1Objects2.length = 0;
gdjs.infosCode.GDscore4Objects1.length = 0;
gdjs.infosCode.GDscore4Objects2.length = 0;
gdjs.infosCode.GDscore3Objects1.length = 0;
gdjs.infosCode.GDscore3Objects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDfantome_9595bleuObjects1.length = 0;
gdjs.infosCode.GDfantome_9595bleuObjects2.length = 0;
gdjs.infosCode.GDfantome_9595rougeObjects1.length = 0;
gdjs.infosCode.GDfantome_9595rougeObjects2.length = 0;
gdjs.infosCode.GDfantome_9595jauneObjects1.length = 0;
gdjs.infosCode.GDfantome_9595jauneObjects2.length = 0;
gdjs.infosCode.GDfantome_9595vertObjects1.length = 0;
gdjs.infosCode.GDfantome_9595vertObjects2.length = 0;
gdjs.infosCode.GDfantome5Objects1.length = 0;
gdjs.infosCode.GDfantome5Objects2.length = 0;
gdjs.infosCode.GDfond_9595titreObjects1.length = 0;
gdjs.infosCode.GDfond_9595titreObjects2.length = 0;
gdjs.infosCode.GDserge_9595caracteresObjects1.length = 0;
gdjs.infosCode.GDserge_9595caracteresObjects2.length = 0;
gdjs.infosCode.GDserge_9595lamaObjects1.length = 0;
gdjs.infosCode.GDserge_9595lamaObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
