# PrimtuxWebApps

## Introduction

L'ensemble des applications web créées, suppportées et maintenues par l'équipe **Primtux** !

## Tester en local

1. Récupérer les sources git :

```sh
git clone git@framagit.org:primtux/primtuxwebapps.git && cd primtuxwebapps
```

2. Activer les hooks git :

```sh
git config core.hooksPath=.githooks
```

3. Avoir un environnement virtuel

Il faut au préalable installer venv et pip :

```sh
sudo apt install python3.10-venv python3-pip
```

Puis :

```sh
python3 -m venv .
```

4. Installer les dépendances dans l'environnement virtuel

```sh
pip install -r requirements.txt
```

5. Lancer ce script :

```sh
make init
```

Il va dans l'ordre :

- Créer un espace virtuelle pour les dépendances python (évite d'installer des dépendances sur votre distribution).
- Installer les dépendances python pour un environnement de test et de dev.
- générer les parties communes aux différents projets

## Compiler les changements opérés dans les fichiers de template

```sh
make gen
```

# Intégrer un nouveau soft

1. créer le dossier contenant les sources
2. créer un dossier `templates` à l'intérieur
3. déplacer tous les fichiers html à l'intérieur du dossier `templates`
4. editer le fichier apps.toml avec ceci pour chaque fichier html :

```ini
[app.nom_app]
path = "dossier_application"
template = "nom_fichier.html"
```

Si le dossier a le même nom que l'applicatif, il n'y a pas besoin de le préciser.
Si le nom du template est "index.html", il n'y a pas besoin de l'écrire, c'est le comportement par défaut.

Ex : 

```ini
[app.nom_app]
```

5. Editer debian/primtuxwebapps.install
6. Editer debian/primtuxwebapps.links
7. rajouter tous les fichiers html générés dans le .gitignore

8. Lancer `make check`. Si il n'y a pas d'erreur, alors lancer `make gen`.
Ca va générer les tags nécessaires à l'intégration de l'applicatif.

## Compilation paquet Debian

Compilation :

```sh
make build.debian
```

Installation : 

```sh
sudo dpkg -i ../primtuxwebapps_0.1_all.deb
```
