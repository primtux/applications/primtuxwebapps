(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var themes = /** @class */ (function () {
        function themes() {
        }
        themes.Liste = [
    "L’ÉCOLE",
    "LE RUGBY",
    "LES MÉTIERS",
    "LES LIVRES",
    "LE HANDICAP",
    "LE TEMPS",
    "LES MOTS BIZARRES",
    "LES FRUITS",
    "LES FORMES",
    "LA SANTE",
    "L’ASTRONOMIE",
    "LE PETIT DEJEUNER",
    "FANTASTIQUE",
    "MYTHOLOGIES",
    "MOTS INVARIABLES",
    "MATHÉMATIQUES",
    "LES MÉTIERS",
    "LES SAISONS",
    "LES TRANSPORTS",
    "LES VÊTEMENTS",
    "PARTIES DU CORPS",
    "OBJETS DE LA MAISON",
    "VERBES D’ACTION",
    "ÉLEMENTS NATURELS",
    "LES JOUETS",
    "ANIMAUX MARINS",
    "LES INSCECTES",
    "LA COMMUNICATION",
    "LES LÉGUMES",
    "LA MONNAIE",
    "LES FLEURS",
    "LA PLAGE",
    "LES USTENSILES DE CUISINE",
    "ANIMAUX DE LA FERME",
    "INTERNET",
    "ÉCOLOGIE",
    "L’ESPACE",
    "LA MUSIQUE",
    "LES VOYAGES",
    "NATIONALITÉS",
    "ENTRETIEN DE LA MAISON"       
        ];
        return themes;
    }());
    exports.default = themes;
});