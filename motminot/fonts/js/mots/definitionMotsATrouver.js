(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DefinitionMotsATrouver = /** @class */ (function () {
        function DefinitionMotsATrouver() {
        }
        DefinitionMotsATrouver.Liste = ["école : Établissement où \l'on donne un enseignement collectif général.", "élève : Celui, celle qui reçoit un enseignement dans un établissement scolaire "
        ];
        return DefinitionMotsATrouver;
    }());
    exports.default = DefinitionMotsATrouver;
});
/* Si vous trouvez ce fichier, et c'est très facile, ne le dites pas.
C'est le code initial de sutom qui code les solutions dans un fichier.
Je vais essayer de corriger (ou refaire) */
//# sourceMappingURL=definitionMotsATrouver.js.map