// Seyes est un logiciel libre
// sous licence GNU GPL
/// écrit par Arnaud Champollion


//Éléments DOM
const divMarge=document.getElementById('marge');
const marge=document.getElementById('marge1');
const margeSecondaire= document.getElementById('marge2');
const page=document.getElementById('page');
const confirmationNouveau=document.getElementById('confirmation-nouveau');

const fullscreen=document.getElementById('fullscreen');
const bouton_a_propos=document.getElementById('a_propos');
const bouton_ligatures=document.getElementById('bouton_ligatures');
const bouton_copier=document.getElementById('copier');

const graphismes=document.getElementById('graphismes');

const texte_principal=document.getElementById('texte_principal');
const body = document.body;
const site = document.documentElement;
const texte_apropos = document.getElementById('texte_apropos');
const boutons = document.getElementById('boutons');
const texte_marge = document.getElementById('texte_marge');
const texte_marge_complementaire = document.getElementById('texte_marge_complementaire');

const choix_couleurs = document.getElementById('choix_couleurs');
const zone_menu_police = document.getElementById('zone_menu_police');
const zone_menu_copie_lien = document.getElementById('zone_menu_copie_lien');
const zone_lien = document.getElementById('zone_lien');
const boutons_police = document.getElementById('boutons_police');
const applique_couleur = document.getElementById('applique_couleur');
const voile_page = document.getElementById('voile_page');
const voile_marge = document.getElementById('voile_marge');


bouton_ligatures.style.display='none';

// Autres constantes
const isWindows = /Win/i.test(navigator.userAgent);
console.log("Sommes-nous sur Windows ? "+isWindows)

// Pour les ligatures
let signes=['═','║']
let subsitutions=[
    ['on','═'],
    ['or','║'],
    ['s ','╝ '],
    ['s\\.','╝.'],
    ['s,','╝,'],
    ['s;','╝;'],
    ['s:','╝:'],
    ['s!','╝!'],
    ['s/','╝/'],
    ['s-','╝-'],
    ["s'","╝'"],
    [' p',' ╰'],
    [' j',' ╮'],
    [' s',' ╠'],
    [' i',' ╱']
]

let polices=['Arial','AA Cursive','Open Dyslexic','Belle Allure GS','Belle Allure CE','Belle Allure CM']

// Objet qui contient les propriétés pour chaque police
const typesPolices = {
    "Arial": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'normal',
        coef_marge: 0.28,
        coef_marge_windows: 0.25,
        distance_soulignage: 0.3,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "AA Cursive": {
        facteur_taille_police: 0.75,
        epaisseur_police: 'normal',
        coef_marge: 0.35,
        coef_marge_windows: 0.3125,
        distance_soulignage: 0.3,
        ligatures: [0, 1],
        bouton_ligatures_display: null
    },
    "Open Dyslexic": {
        facteur_taille_police: 0.45,
        epaisseur_police: 'normal',
        coef_marge: 0.35,
        coef_marge_windows: 0.35,
        distance_soulignage: 0.5,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Belle Allure CE": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'bold',
        coef_marge: 0.375,
        coef_marge_windows: 0.334821429,
        distance_soulignage: 0.38,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Belle Allure CM": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'bold',
        coef_marge: 0.375,
        coef_marge_windows: 0.334821429,
        distance_soulignage: 0.38,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Belle Allure GS": {
        facteur_taille_police: 0.6,
        epaisseur_police: 'bold',
        coef_marge: 0.375,
        coef_marge_windows: 0.334821429,
        distance_soulignage: 0.38,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    },
    "Acceseditionscursive": {
        facteur_taille_police: 0.674,
        epaisseur_police: 'normal',
        coef_marge: 0.272,
        coef_marge_windows: 0.242857143,
        distance_soulignage: 0.379,
        ligatures: [1, 0],
        bouton_ligatures_display: 'none'
    }
};

// Variables globales
largeur_carreau=80;
let largeur_carreau_old = largeur_carreau;
facteur_taille_police=0.75
taille_police=60;
epaisseur_police='normal';
pleinecran_on=false;
a_propos_on=false;
redim=false;
ligatures_on=false;
dragged=null;
coef_marge=0.35;
coef_marge_windows=0.3125;
distance_soulignage=0.3;
focus_texte=null;
police_active=polices[0];
couleurSoulignementActive='rouge';
typeCarreaux='seyes';
opaciteLignes=1;
regleActive=false;
let isDrawing = false;
let startX, startY;  // Position de départ du trait
let lineDiv = null;  // La div représentant le trait



// Lecture des paramètres à lancer au départ
// Récupération du réglage
async function lire() {
    // Dans les paramètres de la page OpenBoard
    if (window.widget) { // Si OpenBoard
        // Quand on revient sur le widget, on récupère les paramètres stockés
        if (await window.sankore.async.preference('police')!='') {
            change_police(await window.sankore.async.preference('police'));
            texte_principal.innerHTML = await window.sankore.async.preference('texte_principal');
            texte_marge.innerHTML = await window.sankore.async.preference('texte_marge');
           texte_marge_complementaire.innerHTML = await window.sankore.async.preference('texte_marge_complementaire');
            applique_couleur.style.backgroundColor = await window.sankore.async.preference('applique_couleur');
            choix_couleurs.value = await window.sankore.async.preference('choix_couleurs');
            let typeCarreauxRecupere = await window.sankore.async.preference('type-carreaux');
            if (typeCarreauxRecupere) {typeCarreaux=typeCarreauxRecupere;changeCarreaux(typeCarreaux);};
            let opaciteLignesRecupere = await window.sankore.async.preference('opacite-lignes');
            if (opaciteLignesRecupere) {
                opacite(parseFloat(opaciteLignesRecupere));
            };
            let largeurCarreauRecupere = await window.sankore.async.preference('largeur-carreau');
            if (largeurCarreauRecupere) {
                console.log('zoom récupéré largeur '+largeurCarreauRecupere);
                largeur_carreau = parseInt(largeurCarreauRecupere);
                zoom(0,police_active);
            };
            let largeurMargeRecuperee = await window.sankore.async.preference('largeur-marge');
            if (largeurMargeRecuperee) {
                console.log('marge récupérée '+largeurMargeRecuperee);
                regleMarge(parseFloat(largeurMargeRecuperee))
            };
            let graphismesRecuperes = await window.sankore.async.preference('graphismes');
            if (graphismesRecuperes) {
                console.log('graphismes récupérés '+graphismesRecuperes);
                graphismes.innerHTML=graphismesRecuperes;
            };

        } else {
            change_police('Belle Allure CE');
            // Couleur par défaut
            applique_couleur.style.backgroundColor='#0000FF'; // Pour le bouton principal
            choix_couleurs.value='#0000FF'; // Pour le sélecteur
        }
    }
    // dans la mémoire locale du navigateur
    else {

        if (localStorage.getItem('seyes-police')) {
            console.log("lecture paramètres local storage")
            let graphismesRecuperes = localStorage.getItem('seyes-graphismes');
            if (graphismesRecuperes) {
                console.log('graphismes récupérés '+graphismesRecuperes);
                graphismes.innerHTML=graphismesRecuperes;
            };
            change_police(localStorage.getItem('seyes-police'))
            texte_principal.innerHTML = localStorage.getItem('seyes-texte_principal')
            texte_marge.innerHTML = localStorage.getItem('seyes-texte_marge');
            texte_marge_complementaire.innerHTML = localStorage.getItem('seyes- texte_marge_complementaire');
            applique_couleur.style.backgroundColor = localStorage.getItem('seyes-applique_couleur')
            choix_couleurs.value = localStorage.getItem('seyes-choix_couleurs')
            let typeCarreauxRecupere = localStorage.getItem('type_carreaux');
            if (typeCarreauxRecupere) {typeCarreaux=typeCarreauxRecupere;changeCarreaux(typeCarreaux);}; 
            let opaciteLignesRecupere = localStorage.getItem('opacite-lignes');
            if (opaciteLignesRecupere) {
                opacite(parseFloat(opaciteLignesRecupere));
            };
            let largeurCarreauRecupere = localStorage.getItem('largeur-carreau');
            if (largeurCarreauRecupere) {
                largeur_carreau = parseInt(largeurCarreauRecupere);
                zoom(parseInt,police_active);
            };
            let largeurMargeRecuperee = localStorage.getItem('largeur-marge');
            if (largeurMargeRecuperee) {
                console.log('largeurMargeRecuperee' + largeurMargeRecuperee)
                regleMarge(parseFloat(largeurMargeRecuperee));
            };


        }
        else {            
            change_police('Belle Allure CE');
            // Couleur par défaut
            applique_couleur.style.backgroundColor='#0000FF'; // Pour le bouton principal
            choix_couleurs.value='#0000FF'; // Pour le sélecteur
            raz();
        }
        let largeurCarreauRecupere = localStorage.getItem('largeur-carreau');
        if (largeurCarreauRecupere) {
            zoom(parseInt(largeurCarreauRecupere),police_active);
        };

        checkUrl();
    }
}

function regleMarge(largeur) {
    largeur_marge = largeur;
    console.log('calc(100% - ' + (largeur_marge - 3) + 'px)')
    page.style.width='calc(100% - ' + (largeur_marge - 3) + 'px)';
    console.log(largeur_marge + 'px')
    divMarge.style.width=largeur_marge + 'px';
}

function activeRegle() {
    regleActive=!regleActive;
    page.classList.toggle('trace');
    boutons.classList.toggle('trace');
    document.getElementById('regle').classList.toggle('trace');

    if (regleActive){
        console.log('activation de la règle');
    }
    else {
        page.style.cursor=null;
        console.log('désactivation de la règle')
    }
}

function checkUrl() {
    console.log('Lecture des paramètres de l\'URL');
    
    // Récupération de l'URL et des paramètres
    let url = window.location.search;

    // Remplacer les occurrences de '&amp;' par '&' uniquement dans les paramètres de l'URL
    if (url.includes('&amp;')) {
        url = url.replace(/&amp;/g, '&');
        console.log('Correction des &amp; dans les paramètres de l\'URL : ' + url);
    }


    let urlParams = new URLSearchParams(url);

    // Parcourir et afficher tous les paramètres
    for (let [key, value] of urlParams) {
        console.log(`${key}: ${value}`);
    }

    // Vérification des différents paramètres attendus dans l'URL
    if (urlParams.has('marge')) {
        let texteMargeRecupere = urlParams.get('marge');
        console.log(texteMargeRecupere);
        document.getElementById('texte_marge').innerHTML = texteMargeRecupere;  // Met à jour la marge
    }

    if (urlParams.has('marge_secondaire')) {
        texteMargeRecupere = urlParams.get('marge_secondaire');
        console.log(texteMargeRecupere);
        document.getElementById(' texte_marge_complementaire').innerHTML = texteMargeRecupere;  // Met à jour la marge
    }

    if (urlParams.has('graphismes')) {
        let graphismesRecuperes = urlParams.get('graphismes');
        console.log(graphismesRecuperes);
        graphismes.innerHTML = graphismesRecuperes; 
    }

    if (urlParams.has('police')) {
        let police = urlParams.get('police');
        console.log('police '+urlParams.get('police'))
        change_police(police);
    }

    if (urlParams.has('largeur_carreau')) {
        let largeurCarreau = urlParams.get('largeur_carreau');
        console.log('Largeur carreau : '+largeurCarreau)
        largeur_carreau = parseInt(largeurCarreau);  // Applique la taille de la police
        zoom(0, police_active);
    }


    if (urlParams.has('type_carreaux')) {
        typeCarreaux = urlParams.get('type_carreaux');
        console.log('Appel changement de carreaux')
        changeCarreaux(typeCarreaux);
        console.log('Type carreaux par URL : '+typeCarreaux)
    }

    if (urlParams.has('opacite_lignes')) {
        opacite(parseFloat(urlParams.get('opacite_lignes')));
        console.log('Opacité lignes : '+parseFloat(urlParams.get('opacite_lignes')))

    }

    if (urlParams.has('largeur_marge')) {
        regleMarge(parseFloat(urlParams.get('largeur_marge')));
        console.log('largeur marge: '+parseFloat(urlParams.get('largeur_marge')))

    }

    // Récupération du hash pour le texte principal
    if (window.location.hash) {
        let textePrincipalRecupere = decodeURIComponent(window.location.hash.substring(1));  // Enlève le '#' et décode
        console.log(textePrincipalRecupere);
        document.getElementById('texte_principal').innerHTML = textePrincipalRecupere;  // Met à jour le texte principal
    }

    // Nettoyage de l'URL après traitement des paramètres
    const baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
    
    // Remplace l'URL actuelle avec l'URL de base (sans les paramètres et sans hash) sans recharger la page
    window.history.replaceState({}, document.title, baseUrl);
    
    console.log('URL nettoyée : ' + baseUrl);
}




function majLien() {
    // Base URL (tu peux la modifier selon tes besoins)
    let baseURL = window.location.protocol + "//" + window.location.host + window.location.pathname + '?';
    if (window.widget){baseURL='https://educajou.forge.apps.education.fr/seyes?';}

    // Construction des paramètres de l'URL en encodant les contenus des textes et les variables globales
    const params = new URLSearchParams({
        marge: texte_marge.innerHTML,
        marge_secondaire: texte_marge_complementaire.innerHTML,
        police: police_active,
        largeur_carreau: largeur_carreau,
        type_carreaux: typeCarreaux,
        opacite_lignes: opaciteLignes,
        largeur_marge: largeur_marge,
        graphismes: graphismes.innerHTML
    });

    // Encodage du texte principal dans le hash
    const hash = encodeURIComponent(texte_principal.innerHTML);

    // Construction du lien final avec le hash pour le texte principal
    const lienFinal = baseURL + params.toString() + '#' + hash;

    console.log(lienFinal);
    zone_lien.innerText = lienFinal;
}


// Fonction pour copier le lien et afficher un popup
function copier() {
    // Récupération du contenu de la zone de lien
    const zoneLien = document.getElementById('zone_lien');
    const texteLien = zoneLien.innerText;

    // Copie du texte dans le presse-papiers
    navigator.clipboard.writeText(texteLien)
        .then(() => {
            // Création du popup "Lien copié"
            const popup = document.createElement('div');
            popup.innerHTML = "Lien copié";
            popup.style.position = "absolute";
            popup.style.backgroundColor = "black";
            popup.style.color = "white";
            popup.style.fontSize = "12px";
            popup.style.padding = "5px 10px";
            popup.style.borderRadius = "5px";
            popup.style.zIndex = "1000";
            popup.style.opacity = "0";
            popup.style.transition = "opacity 0.3s";

            // Position du popup près du bouton "copier"
            const bouton = document.getElementById('copier');
            const rect = bouton.getBoundingClientRect();
            popup.style.left = rect.left + window.scrollX + "px";
            popup.style.top = rect.top + window.scrollY - 30 + "px";  // 30px au-dessus du bouton

            // Ajout du popup au document
            document.body.appendChild(popup);

            // Faire apparaître le popup
            setTimeout(() => {
                popup.style.opacity = "1";
            }, 10);

            // Faire disparaître le popup après 2 secondes
            setTimeout(() => {
                popup.style.opacity = "0";
                setTimeout(() => {
                    document.body.removeChild(popup);
                }, 300);  // Retire le popup du DOM après la transition
            }, 2000);
        })
        .catch((err) => {
            console.error('Erreur lors de la copie : ', err);
        });
}


// Calcul de la distance de Levenshtein
function levenshtein(a, b) {
    const matrix = Array.from({ length: b.length + 1 }, (_, i) => [i]);
    for (let i = 1; i <= a.length; i++) matrix[0][i] = i;

    for (let j = 1; j <= b.length; j++) {
        for (let i = 1; i <= a.length; i++) {
            if (a[i - 1] === b[j - 1]) matrix[j][i] = matrix[j - 1][i - 1];
            else matrix[j][i] = Math.min(matrix[j - 1][i - 1] + 1, matrix[j][i - 1] + 1, matrix[j - 1][i] + 1);
        }
    }
    return matrix[b.length][a.length];
}

// Tolérance aux erreurs de frappe pour les jours et mois
const joursSemaine = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];
const moisAnnee = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];

// Fonction pour trouver un jour ou mois avec une tolérance à une faute de frappe
function trouverCorrespondanceToleree(chaine, listeMots, tolerance = 1) {
    for (let mot of listeMots) {
        if (levenshtein(chaine.toLowerCase(), mot.toLowerCase()) <= tolerance) {
            return mot; // Retourne le mot correct
        }
    }
    return null; // Aucun mot trouvé avec tolérance
}

// Correction des fautes de frappe dans le texte
function corrigerFautesTexte(texte) {
    let mots = texte.split(/\s+/);

    for (let i = 0; i < mots.length; i++) {
        let motCorrige = trouverCorrespondanceToleree(mots[i], joursSemaine) || trouverCorrespondanceToleree(mots[i], moisAnnee);
        if (motCorrige) {
            mots[i] = motCorrige; // Remplace le mot mal orthographié par le correct
        }
    }

    return mots.join(' ');
}

function calculeDate(format = 'complet') {
    let options;
    console.log(`Format demandé : ${format}`);

    switch (format) {
        case 'complet':
            options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            break;
        case 'sansJour':
            options = { year: 'numeric', month: 'long', day: 'numeric' };
            break;
        case 'sansAnnee':
            options = { weekday: 'long', month: 'long', day: 'numeric' };
            break;
        case 'court':
            let dateObj = new Date();
            let dateCourt = `${dateObj.getDate().toString().padStart(2, '0')}/${(dateObj.getMonth() + 1).toString().padStart(2, '0')}/${dateObj.getFullYear()}`;
            console.log(`Date au format court : ${dateCourt}`);
            return dateCourt;
        default:
            options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    }

    let dateDuJour = new Date().toLocaleDateString('fr-FR', options);
    dateDuJour = dateDuJour.replace(/\b1\b/, '1<span class="exposant">er</span>');
    console.log(`Date calculée : ${dateDuJour}`);
    let date_formatee = dateDuJour.charAt(0).toUpperCase() + dateDuJour.slice(1);
    console.log("date formatée = "+date_formatee)
    return date_formatee;
}

// Calcul de la distance de Levenshtein
function levenshtein(a, b) {
    console.log(`Calcul de la distance de Levenshtein entre "${a}" et "${b}"`);
    const matrix = Array.from({ length: b.length + 1 }, (_, i) => [i]);
    for (let i = 1; i <= a.length; i++) matrix[0][i] = i;

    for (let j = 1; j <= b.length; j++) {
        for (let i = 1; i <= a.length; i++) {
            if (a[i - 1] === b[j - 1]) matrix[j][i] = matrix[j - 1][i - 1];
            else matrix[j][i] = Math.min(matrix[j - 1][i - 1] + 1, matrix[j][i - 1] + 1, matrix[j - 1][i] + 1);
        }
    }
    const distance = matrix[b.length][a.length];
    console.log(`Distance calculée : ${distance}`);
    return distance;
}


// Fonction pour trouver un jour ou mois avec une tolérance à une faute de frappe
function trouverCorrespondanceToleree(chaine, listeMots, tolerance = 1) {
    console.log(`Recherche de correspondance tolérée pour : "${chaine}" dans la liste [${listeMots.join(', ')}]`);
    for (let mot of listeMots) {
        if (levenshtein(chaine.toLowerCase(), mot.toLowerCase()) <= tolerance) {
            console.log(`Correspondance trouvée : "${mot}"`);
            return mot; // Retourne le mot correct
        }
    }
    console.log(`Aucune correspondance trouvée pour "${chaine}"`);
    return null; // Aucun mot trouvé avec tolérance
}

// Correction des fautes de frappe dans le texte
function corrigerFautesTexte(texte) {
    console.log(`Texte avant correction des fautes : "${texte}"`);
    let mots = texte.split(/\s+/);

    for (let i = 0; i < mots.length; i++) {
        let motCorrige = trouverCorrespondanceToleree(mots[i], joursSemaine) || trouverCorrespondanceToleree(mots[i], moisAnnee);
        if (motCorrige) {
            console.log(`Mot corrigé : "${mots[i]}" remplacé par "${motCorrige}"`);
            mots[i] = motCorrige; // Remplace le mot mal orthographié par le correct
        }
    }

    let texteCorrige = mots.join(' ');
    console.log(`Texte après correction des fautes : "${texteCorrige}"`);
    return texteCorrige;
}

function changeDate() {
    console.log("Début de la fonction changeDate");

    // Sélectionne l'élément contenant le texte principal
    let texte_principal = document.getElementById('texte_principal');
    
    if (!texte_principal) {
        console.error("Erreur : élément 'texte_principal' introuvable");
        return;
    }

    // Recherche de la première date avec styles spécifiques
    let foundDate = texte_principal.querySelector('span.souligne, font');  // Trouve la première occurrence
    console.log("Date trouvée :", foundDate ? foundDate.innerHTML : "Aucune date trouvée");

    if (foundDate) {
        // Corriger les fautes typographiques
        let texteCorrige = corrigerFautesTexte(foundDate.innerHTML); 
        let newDateFormatted = '';

        // Expressions régulières pour différents formats de date
        let regexJourComplet = /\b([L|l]undi|[M|m]ardi|[M|m]ercredi|[J|j]eudi|[V|v]endredi|[S|s]amedi|[D|d]imanche)\s(\d{1,2})\s(\w+)\s(\d{4})\b/;
        let regexSansJour = /\b(\d{1,2})\s(\w+)\s(\d{4})\b/;
        let regexSansAnnee = /\b([L|l]undi|[M|m]ardi|[M|m]ercredi|[J|j]eudi|[V|v]endredi|[S|s]amedi|[D|d]imanche)\s(\d{1,2})\s(\w+)\b/;
        let regexCourt = /\b(\d{1,2})\/(\d{1,2})\/(\d{4})\b/;

        // Vérifie le format actuel et met à jour la date dans le même format
        if (regexJourComplet.test(texteCorrige)) {
            console.log("Format détecté : Jour complet");
            newDateFormatted = calculeDate('complet');
            foundDate.innerHTML = texteCorrige.replace(regexJourComplet, newDateFormatted);
        } else if (regexSansJour.test(texteCorrige)) {
            console.log("Format détecté : Sans jour");
            newDateFormatted = calculeDate('sansJour');
            foundDate.innerHTML = texteCorrige.replace(regexSansJour, newDateFormatted);
        } else if (regexSansAnnee.test(texteCorrige)) {
            console.log("Format détecté : Sans année");
            newDateFormatted = calculeDate('sansAnnee');
            foundDate.innerHTML = texteCorrige.replace(regexSansAnnee, newDateFormatted);
        } else if (regexCourt.test(texteCorrige)) {
            console.log("Format détecté : Court");
            newDateFormatted = calculeDate('court');
            foundDate.innerHTML = texteCorrige.replace(regexCourt, newDateFormatted);
        }
 
    } else {
        // Si aucune date n'est trouvée, on ajoute un nouveau div avec la date formatée
        let nouvelleDate = calculeDate('complet'); // Format complet par défaut
        console.log(`Ajout d'une nouvelle date : ${nouvelleDate}`);
        texte_principal.insertAdjacentHTML('afterbegin', `<div><span class="souligne rouge"><font color="#26a269">${nouvelleDate}</font></span></div>`);
    }

    console.log("Fin de la fonction changeDate");
}


// Calcul de la distance de Levenshtein
function levenshtein(a, b) {
    console.log(`Calcul de la distance de Levenshtein entre "${a}" et "${b}"`);
    const matrix = Array.from({ length: b.length + 1 }, (_, i) => [i]);
    for (let i = 1; i <= a.length; i++) matrix[0][i] = i;

    for (let j = 1; j <= b.length; j++) {
        for (let i = 1; i <= a.length; i++) {
            if (a[i - 1] === b[j - 1]) matrix[j][i] = matrix[j - 1][i - 1];
            else matrix[j][i] = Math.min(matrix[j - 1][i - 1] + 1, matrix[j][i - 1] + 1, matrix[j - 1][i] + 1);
        }
    }
    const distance = matrix[b.length][a.length];
    console.log(`Distance calculée : ${distance}`);
    return distance;
}


// Fonction pour trouver un jour ou mois avec une tolérance à une faute de frappe
function trouverCorrespondanceToleree(chaine, listeMots, tolerance = 1) {
    console.log(`Recherche de correspondance tolérée pour : "${chaine}" dans la liste [${listeMots.join(', ')}]`);
    for (let mot of listeMots) {
        if (levenshtein(chaine.toLowerCase(), mot.toLowerCase()) <= tolerance) {
            console.log(`Correspondance trouvée : "${mot}"`);
            return mot; // Retourne le mot correct
        }
    }
    console.log(`Aucune correspondance trouvée pour "${chaine}"`);
    return null; // Aucun mot trouvé avec tolérance
}

// Correction des fautes de frappe dans le texte
function corrigerFautesTexte(texte) {
    console.log(`Texte avant correction des fautes : "${texte}"`);
    let mots = texte.split(/\s+/);

    for (let i = 0; i < mots.length; i++) {
        let motCorrige = trouverCorrespondanceToleree(mots[i], joursSemaine) || trouverCorrespondanceToleree(mots[i], moisAnnee);
        if (motCorrige) {
            console.log(`Mot corrigé : "${mots[i]}" remplacé par "${motCorrige}"`);
            mots[i] = motCorrige; // Remplace le mot mal orthographié par le correct
        }
    }

    let texteCorrige = mots.join(' ');
    console.log(`Texte après correction des fautes : "${texteCorrige}"`);
    return texteCorrige;
}

function changeDate() {
    console.log("Début de la fonction changeDate");

    // Sélectionne l'élément contenant le texte principal
    let texte_principal = document.getElementById('texte_principal');
    
    if (!texte_principal) {
        console.error("Erreur : élément 'texte_principal' introuvable");
        return;
    }

    // Recherche de la première date avec styles spécifiques
    let foundDate = texte_principal.querySelector('span.souligne, font');  // Trouve la première occurrence
    console.log("Date trouvée :", foundDate ? foundDate.innerHTML : "Aucune date trouvée");

    if (foundDate) {
        // Corriger les fautes typographiques
        let texteCorrige = corrigerFautesTexte(foundDate.innerHTML); 
        let newDateFormatted = '';

        // Expressions régulières pour différents formats de date
        let regexJourComplet = /\b([L|l]undi|[M|m]ardi|[M|m]ercredi|[J|j]eudi|[V|v]endredi|[S|s]amedi|[D|d]imanche)\s(\d{1,2})\s(\w+)\s(\d{4})\b/;
        let regexSansJour = /\b(\d{1,2})\s(\w+)\s(\d{4})\b/;
        let regexSansAnnee = /\b([L|l]undi|[M|m]ardi|[M|m]ercredi|[J|j]eudi|[V|v]endredi|[S|s]amedi|[D|d]imanche)\s(\d{1,2})\s(\w+)\b/;
        let regexCourt = /\b(\d{1,2})\/(\d{1,2})\/(\d{4})\b/;

        // Vérifie le format actuel et met à jour la date dans le même format
        if (regexJourComplet.test(texteCorrige)) {
            console.log("Format détecté : Jour complet");
            newDateFormatted = calculeDate('complet');
            foundDate.innerHTML = texteCorrige.replace(regexJourComplet, newDateFormatted);
        } else if (regexSansJour.test(texteCorrige)) {
            console.log("Format détecté : Sans jour");
            newDateFormatted = calculeDate('sansJour');
            foundDate.innerHTML = texteCorrige.replace(regexSansJour, newDateFormatted);
        } else if (regexSansAnnee.test(texteCorrige)) {
            console.log("Format détecté : Sans année");
            newDateFormatted = calculeDate('sansAnnee');
            foundDate.innerHTML = texteCorrige.replace(regexSansAnnee, newDateFormatted);
        } else if (regexCourt.test(texteCorrige)) {
            console.log("Format détecté : Court");
            newDateFormatted = calculeDate('court');
            foundDate.innerHTML = texteCorrige.replace(regexCourt, newDateFormatted);
        }
    } else {
        // Si aucune date n'est trouvée, on ajoute un nouveau div avec la date formatée
        let nouvelleDate = calculeDate('complet'); // Format complet par défaut
        console.log(`Ajout d'une nouvelle date : ${nouvelleDate}`);
        texte_principal.insertAdjacentHTML('afterbegin', `<div><span class="souligne rouge"><font color="#26a269">${nouvelleDate}</font></span></div>`);
    }

    console.log("Fin de la fonction changeDate");
}



lire();
marge.style.backgroundImage=margeSecondaire.style.backgroundImage="url('images/carreau_marge_"+typeCarreaux+".svg";
page.style.backgroundImage="url('images/carreau_"+typeCarreaux+".svg";
document.getElementById('carreaux').style.backgroundImage=`url(images/carreau_${typeCarreaux}.svg`;
console.log('Appel changement de carreaux')
changeCarreaux(typeCarreaux);


// Placement des outils
boutons.style.bottom='0px';
boutons.style.left=page.offsetLeft + page.offsetWidth/2 - boutons.offsetWidth/2 + 'px';
// Cocher la bonne option dans le menu polices
boutons_police.elements['police'][polices.indexOf(police_active)].checked=true;

// Si OpenBoard
if (window.widget) {
    // Quand on quitte le widget
    window.widget.onleave.connect(() => {
    window.sankore.setPreference('police',police_active);
    window.sankore.setPreference('texte_principal',texte_principal.innerHTML);
    window.sankore.setPreference('texte_marge',texte_marge.innerHTML);
    window.sankore.setPreference('applique_couleur',applique_couleur.style.backgroundColor);
    window.sankore.setPreference('choix_couleurs',choix_couleurs.value);
    window.sankore.setPreference('type-carreaux',typeCarreaux);
    window.sankore.setPreference('opacite-lignes',opaciteLignes);
    window.sankore.setPreference('largeur-carreau',largeur_carreau);
    window.sankore.setPreference('largeur-marge',largeur_marge);
    });
}

function adapteMargeCahierDeTexte(valeur){
    largeur_marge = largeur_carreau*valeur;
    page.style.width='calc(100% - ' + (largeur_marge - 3) + 'px)';
    divMarge.style.width=largeur_marge + 'px';    
    document.documentElement.style.setProperty('--largeur-marge-secondaire', largeur_carreau*2.7 + 'px');
}

function zoom(coef,police){
    console.log('zoom('+coef+','+police+')');
    largeur_carreau_old = largeur_carreau;
    largeur_carreau=largeur_carreau+coef;
    taille_police=largeur_carreau*facteur_taille_police;
    texte_principal.style.fontWeight=epaisseur_police;
    texte_marge.style.fontWeight=epaisseur_police;
    texte_marge_complementaire.style.fontWeight=epaisseur_police;
    if (isWindows){calc_marge=largeur_carreau*coef_marge_windows}
    else {calc_marge=largeur_carreau*coef_marge}
    if (police==='Open Dyslexic'){calc_marge=calc_marge*0.90}
    page.style.backgroundSize=largeur_carreau+'px';
    marge.style.backgroundSize=largeur_carreau+'px';
    margeSecondaire.style.backgroundSize=largeur_carreau+'px';
    texte_principal.style.fontSize=taille_police+'px';
    texte_principal.style.marginLeft=largeur_carreau/5+'px';
    texte_principal.style.marginRight=largeur_carreau/5+'px';
    texte_marge.style.fontSize=texte_marge_complementaire.style.fontSize=taille_police+'px';
    texte_marge.style.marginTop=texte_marge_complementaire.style.marginTop=calc_marge+'px'
    texte_marge.style.marginLeft=texte_marge_complementaire.style.marginLeft=largeur_carreau/5+'px';
    texte_marge.style.marginRight=texte_marge_complementaire.style.marginRight=largeur_carreau/5+'px';
    if (typeCarreaux==='terre'){
        console.log('Carreaux terre');
        texte_marge.style.lineHeight=2*largeur_carreau+'px';
        texte_principal.style.lineHeight=2*largeur_carreau+'px';
        texte_principal.style.marginTop=-(calc_marge/3)+'px';
    } else {
        texte_marge_complementaire.style.lineHeight=texte_marge.style.lineHeight=largeur_carreau+'px';
        texte_principal.style.lineHeight=largeur_carreau+'px';
        texte_principal.style.marginTop=calc_marge+'px';
    }

    document.documentElement.style.setProperty('--largeur-marge-secondaire', largeur_carreau*2.7 + 'px');


    const tousLesSpanCustomFont = document.querySelectorAll('.customfont');
    tousLesSpanCustomFont.forEach(span => {
        let policeCustom = span.style.fontFamily.trim();  // trim() pour éviter les espaces indésirables
        let facteurCustom = typesPolices[policeCustom]?.facteur_taille_police;  // Utilisation de la clé policeCustom dans l'objet typesPolices
    
        if (facteurCustom) {
            span.style.fontSize = span.style.lineHeight = largeur_carreau * facteurCustom + "px";
        } else {
            console.warn(`La police ${policeCustom} n'a pas été trouvée dans l'objet typesPolices.`);
        }
    });
    


    // Adaptation des graphismes

    const tousLesGraphismes = graphismes.querySelectorAll('.trait');
    tousLesGraphismes.forEach(graphisme => {

        graphisme.style.transition='none ';

        let computedStyle = window.getComputedStyle(graphisme);
      
        let positionX = parseInt(parseFloat(computedStyle.getPropertyValue('left')) / largeur_carreau_old);
        let positionY = parseInt(parseFloat(computedStyle.getPropertyValue('top')) / largeur_carreau_old);
        let width = parseInt(parseFloat(computedStyle.getPropertyValue('width')) / largeur_carreau_old);
        let height = parseInt(parseFloat(computedStyle.getPropertyValue('height')) / largeur_carreau_old);

        console.log(width)
    
        // Vérifie que les valeurs ne sont pas NaN
        if (isNaN(positionX)) positionX = 0;
        if (isNaN(positionY)) positionY = 0;
        if (isNaN(width)) width = 0;
        if (isNaN(height)) height = 0;
    
        // Ajustement de 'left' et 'top' en fonction de la proportion unique
        let newPositionX = positionX * largeur_carreau;
        let newPositionY = positionY * largeur_carreau;
    
        // Applique les nouvelles valeurs de 'left' et 'top' à l'élément
        graphisme.style.left = `${newPositionX}px`;
        graphisme.style.top = `${newPositionY}px`;
    
        console.log(`Ancienne position X: ${positionX}, Nouvelle position X: ${newPositionX}`);
        console.log(`Ancienne position Y: ${positionY}, Nouvelle position Y: ${newPositionY}`);
    
        // Si l'élément a la classe 'horizontal', on ajuste la largeur
        if (graphisme.classList.contains('horizontal')) {
            let newWidth = width * largeur_carreau + largeur_carreau*0.02117171;  // Calcule la nouvelle largeur
    
            // Applique la nouvelle largeur à l'élément
            graphisme.style.width = `${newWidth}px`;
    
            console.log(`Ancienne largeur: ${width}, Nouvelle largeur: ${newWidth}`);
        }
    
        // Si l'élément a la classe 'vertical', on ajuste la hauteur
        if (graphisme.classList.contains('vertical')) {
            let newHeight = height * largeur_carreau + largeur_carreau*0.02117171;  // Calcule la nouvelle hauteur
    
            // Applique la nouvelle hauteur à l'élément
            graphisme.style.height = `${newHeight}px`;
    
            console.log(`Ancienne hauteur: ${height}, Nouvelle hauteur: ${newHeight}`);
        }

        graphisme.style.transition='';


    });

    updateTransform();
}

function pleinecran(){
    if (pleinecran_on){
        fullscreen.style.backgroundPosition=null;
        pleinecran_sortir();
    } else {
        fullscreen.style.backgroundPosition='0% 0%';
        pleinecran_ouvrir();
    }
    pleinecran_on=!pleinecran_on;
}

function pleinecran_ouvrir(){   
    // Demande le mode plein écran pour l'élément
    if (site.requestFullscreen) {
        site.requestFullscreen();        
    } else if (site.mozRequestFullScreen) { // Pour Firefox
        site.mozRequestFullScreen();
    } else if (site.webkitRequestFullscreen) { // Pour Chrome, Safari et Opera
        site.webkitRequestFullscreen();
    } else if (site.msRequestFullscreen) { // Pour Internet Explorer et Edge
        site.msRequestFullscreen();
    }
}

function pleinecran_sortir(){
    // Pour sortir du mode plein écran
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { // Pour Firefox
        document.mozCancelFullScreen();        
    } else if (document.webkitExitFullscreen) { // Pour Chrome, Safari et Opera
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { // Pour Internet Explorer et Edge
        document.msExitFullscreen();
    }
    recupere_boutons();
}

function nouveauTexte(){
    confirmationNouveau.style.display='block';
}

function ferme_confirmation(){
    confirmationNouveau.style.display='none';
}

function raz(){
    graphismes.innerHTML='';
    texte_marge.innerHTML=texte_marge_complementaire.innerHTML='';
    texte_principal.innerHTML=calculeDate();
    save_param();
}

function recupere_boutons(){
    setTimeout(function() {
        const positionBoutons=boutons.getBoundingClientRect();
        const largeurFenetre = window.innerWidth;
        const hauteurFenetre = window.innerHeight;
        if (positionBoutons.x>largeurFenetre){boutons.style.left=largeurFenetre-positionBoutons.width+'px';}
        if (positionBoutons.y>hauteurFenetre){boutons.style.top=hauteurFenetre-positionBoutons.height+'px';}
    }, 300);

}




function ligatures(mode,sens_force){
    if (ligatures_on){
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition=null;
            ligatures_on=false;
            texte_principal.spellcheck = true;
            texte_marge.spellcheck = true;
            texte_marge_complementaire.spellcheck = true;
        }
        sens=[1,0];
    } else {
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition='0% 0%';
            ligatures_on=true;
            texte_principal.spellcheck = false;
            texte_marge.spellcheck = false;
            texte_marge_complementaire.spellcheck = false;
        }
        sens=[0,1];
    }
    if (sens_force){sens=sens_force}    
    let texte=texte_principal.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_principal.innerHTML=texte;
    texte=texte_marge.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_marge.innerHTML=texte;
}


function ouvreBoutonsSouligne(event) {
    event.stopPropagation();
    document.getElementById('boutons_soulignement').classList.toggle('hide');
}

function ouvreBoutonsCarreau(event) {
    event.stopPropagation();
    document.getElementById('boutons_carreaux').classList.toggle('hide');
}


function update_souligne() {
    texte_principal.style.textUnderlineOffset=distance_soulignage+'em';
    texte_marge.style.textUnderlineOffset=distance_soulignage+'em';
    texte_marge_complementaire.style.textUnderlineOffset=distance_soulignage+'em';
}

function couleurs(couleur) {
    console.log("couleur demandée "+couleur);
    if (focus_texte){focus_texte.focus();}
    applique_couleur.style.backgroundColor=couleur;
    document.execCommand('foreColor', false, couleur);
        // Récupérer la sélection actuelle
        var selection = window.getSelection();
        if (selection.rangeCount > 0) {
            var range = selection.getRangeAt(0);
        }
}

function ouvreCouleurs(){
    console.log("ouverture de l'input couleurs");
    if (choix_couleurs.showPicker) {
        choix_couleurs.showPicker();
    } else {
        choix_couleurs.click();
    }
}

choix_couleurs.addEventListener('focus', function() {
    console.log('L\'input couleurs est ouvert.');
});


//Opacité
function opacite(valeur){
    opaciteLignes=valeur;
    document.getElementById('opacite').value=valeur;
    voile_page.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
    voile_marge.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
    save_param();
}


// Fonction pour positionner le curseur
function positionCursor(position) {
    const range = document.createRange();
    const select = window.getSelection();
    console.log("function positioncursor "+position)
    range.setStart(texte_principal.firstChild, position);
    range.collapse(true);

    select.removeAllRanges();
    select.addRange(range);

    texte_principal.focus();
}

// Fonction pour ajuster la position de la souris au quadrillage
function snapToGrid(value, gridSize = largeur_carreau) {
    return Math.round(value / gridSize) * gridSize;
}

selected=false;

window.addEventListener('keydown', function(event) {
    // Vérifie si la touche "Suppr" (Delete) est appuyée
    if (event.key === 'Delete') {
        // Si un élément est sélectionné, le supprimer
        if (selected) {
            selected.remove(); // Supprime l'élément du DOM
            selected = false; // Réinitialise la variable
            console.log("Élément supprimé et sélection réinitialisée.");
        }
    }
});

function changeCouleurTrait(couleur) {
    if (selected) {
        selected.style.backgroundColor=couleur;
        selected.classList.remove('select');
        selected=null;
    } else {
        alert("Aucun trait n'est sélectionné.")
    }
}

function playAudio(event) {

    if (event.target.classList.contains('bouton-musique')) {

        var audio = document.getElementById('audio');
        var button = document.getElementById('bouton-audio');
        
        if (audio.paused) {
            audio.play();
            button.classList.add('pause');
        } else {
            audio.pause();
            button.classList.remove('pause');
        }

    }
}


function stopAudio(event) {

    if (event.target.classList.contains('bouton-musique')) {

        var audio = document.getElementById('audio');
        var button = document.getElementById('bouton-audio');
        
        audio.pause();  // Met l'audio en pause
        audio.currentTime = 0;  // Remet à zéro le temps de lecture
        button.classList.remove('pause');  // Change l'icône du bouton à "Play"

    }
}

function clic(event) {

    console.log('clic');

    // Ferme les menus si nécessaire
    if (!event.target.classList.contains('deroule_souligne') && !event.target.classList.contains('deroule_carreaux')) {
        console.log('Fermeture auto')
        document.getElementById('boutons_soulignement').classList.add('hide');
        document.getElementById('boutons_carreaux').classList.add('hide');
    }


    if (event.target.classList.contains('bouton-supprimer-trait') && !regleActive && selected) {
        selected.remove();
        selected=null;
    }

    if (event.target.classList.contains('bouton-couleur-trait') && !regleActive && selected) {
        console.log("Clic sur bouton couleur")
        document.getElementById('inputCouleurTrait').click();
    }

    if (event.target.classList.contains('trait') && !regleActive) {
        if (selected) {
            selected.classList.remove('select');
        }
        event.target.classList.add('select');
        selected=event.target;
    } else if (selected && !event.target.classList.contains('bouton-couleur-trait')){
        selected.classList.remove('select');
        selected=null;
    }




    const borderWidth = 10; // Largeur de la bordure (en pixels)
    const rect = page.getBoundingClientRect(); // Récupère les dimensions et la position de la div "page"
    posX = event?.targetTouches?.[0]?.clientX || event.clientX;
    posY = event?.targetTouches?.[0]?.clientY || event.clientY;
    const mouseX = posX - rect.left; // Position de la souris horizontalement par rapport à la div "page"
    const mouseY = posY - rect.top;

    let draggable_item = false;
    if (event.target.classList.contains('draggable_item')) { draggable_item = true; }

    if (event.target.classList.contains('draggable') || draggable_item) {
        console.log("clic sur draggable");
        event.preventDefault();
        if (draggable_item) { dragged = event.target.parentNode; }
        else { dragged = event.target; }
        diffsourisx = posX - dragged.offsetLeft;
        diffsourisy = posY - dragged.offsetTop;
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        dragged.classList.add('dragged');    
    } else if (mouseX <= borderWidth && mouseX > -borderWidth) { // Vérifie si le clic est dans une zone de 5px de la bordure gauche
        redim = true;
        posXDepartSouris = posX;
        largeur_page = page.offsetWidth;
        largeur_marge = divMarge.offsetWidth;
    } else if (regleActive) {  // Gérer le traçage des lignes si regleActive est vrai
        console.log('gestion du trait')
        const snappedX = snapToGrid(mouseX);
        const snappedY = snapToGrid(mouseY);
        if (Math.abs(mouseX - snappedX) < largeur_carreau/8 && Math.abs(mouseY - snappedY) < largeur_carreau/8) {
            isDrawing = true;
            startX = snappedX;
            startY = snappedY;

            // Créer une div pour représenter le trait
            lineDiv = document.createElement('div');
            lineDiv.classList.add('trait');
            lineDiv.classList.add('horizontal');
            lineDiv.classList.add('draggable');

            lineDiv.style.position = 'absolute';
            lineDiv.style.backgroundColor = 'red';
            lineDiv.style.width = '6px';  // Par défaut, un trait vertical
            lineDiv.style.height = '6px'; // Minimum taille
            lineDiv.style.left = startX + 'px';
            lineDiv.style.top = startY + 'px';

            let boutonSupprimer = document.createElement('button');
            boutonSupprimer.classList.add('bouton-supprimer-trait');
            lineDiv.appendChild(boutonSupprimer);

            let boutonCouleur = document.createElement('button');
            boutonCouleur.classList.add('bouton-couleur-trait');
            lineDiv.appendChild(boutonCouleur);


            graphismes.appendChild(lineDiv);
        }
    }
}

function move(event) {
    posXSouris = event?.targetTouches?.[0]?.clientX || event?.clientX;
    posYSouris = event?.targetTouches?.[0]?.clientY || event?.clientY;
    const rect = page.getBoundingClientRect();
    const mouseX = posXSouris - rect.left;
    const mouseY = posYSouris - rect.top;

    if (redim) {
        diffX = posXSouris - posXDepartSouris;
        if (largeur_marge + diffX >= 60) {
            page.style.width = (largeur_page - diffX - 3) + 'px';
            console.log((largeur_marge + diffX) + 'px');
            divMarge.style.width = (largeur_marge + diffX) + 'px';
        } else {
            page.style.width = 'calc(100% - 63px)';
            divMarge.style.width = '60px';
        }
    }

    if (dragged) {
        dragged.style.right = null;
        dragged.style.bottom = null;
        event.preventDefault();
        const pageX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
        const pageY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
        dragged.style.left = pageX - diffsourisx + "px";
        dragged.style.top = pageY - diffsourisy + "px";
    } else if (isDrawing && lineDiv) {  // Gérer le traçage de la ligne
        const snappedX = snapToGrid(mouseX);
        const snappedY = snapToGrid(mouseY);

        // Calculer la direction du trait (horizontal ou vertical)
        const diffX = snappedX - startX;
        const diffY = snappedY - startY;

        if (Math.abs(diffX) > Math.abs(diffY)) {
            // Traçage horizontal
            let longueur = Math.abs(diffX);
            let nbCarreaux = parseInt(longueur/largeur_carreau);
            lineDiv.style.width = longueur + largeur_carreau*0.02117171 + 'px';
            lineDiv.style.height = '6px';
            lineDiv.style.top = startY + 'px';
            lineDiv.style.left = startX + 'px';
            lineDiv.classList.remove('vertical');
            lineDiv.classList.add('horizontal');

            if (diffX < 0) {
                lineDiv.style.left = snappedX + 'px';  // Ajuster la position si on dessine vers la gauche  
            }
        } else {
            // Traçage vertical
            lineDiv.style.height = Math.abs(diffY) + largeur_carreau*0.02117171 + 'px';
            lineDiv.style.width = '6px';
            lineDiv.classList.remove('horizontal');
            lineDiv.classList.add('vertical');
            if (diffY < 0) {
                lineDiv.style.top = snappedY + 'px';  // Ajuster la position si on dessine vers le haut
            }
        }
    }
}

function release(event) {
    redim = false;
    largeur_page = page.offsetWidth;
    largeur_marge = divMarge.offsetWidth;

    // Fin du déplacement d'élément
    if (dragged) {
        dragged.classList.remove('dragged');

        if (dragged.classList.contains('trait')) {
            // On récupère la position actuelle de l'élément dragged (position absolute)
            let currentLeft = parseInt(dragged.style.left, 10);
            let currentTop = parseInt(dragged.style.top, 10);
            
            // Calculer les nouvelles coordonnées alignées sur la grille
            let newLeft = Math.round(currentLeft / largeur_carreau) * largeur_carreau;
            let newTop = Math.round(currentTop / largeur_carreau) * largeur_carreau;
            
            // Repositionner dragged sur la grille invisible
            dragged.style.left = `${newLeft}px`;
            dragged.style.top = `${newTop}px`;
        }
        

        dragged = null;
    }

    // Fin du traçage de la ligne
    if (isDrawing) {
        isDrawing = false;
        lineDiv = null;
        activeRegle();
    }

    save_param();

    
}

// Fonction pour changer le curseur lors du survol des points d'accroche
function moveCursor(event) {
    const rect = page.getBoundingClientRect();
    const mouseX = (event?.targetTouches?.[0]?.clientX || event.clientX) - rect.left;
    const mouseY = (event?.targetTouches?.[0]?.clientY || event.clientY) - rect.top;

    if (regleActive) {
        const snappedX = snapToGrid(mouseX);
        const snappedY = snapToGrid(mouseY);

        console.log(snappedX)


        if (Math.abs(mouseX - snappedX) < largeur_carreau/8 && Math.abs(mouseY - snappedY) < largeur_carreau/8) {
            page.style.cursor = 'url(images/crayonrouge.png) 0 30, auto';
        } else if (!isDrawing){
            page.style.cursor = 'url(images/crayon.png) 0 30, auto ';
        }
    }
}

document.addEventListener('mousemove', moveCursor);


function a_propos (){
    if (a_propos_on){
        a_propos_on=false;
        texte_principal.contentEditable = 'true';
        texte_principal.innerHTML=sauvegarde;
        bouton_a_propos.style.backgroundPosition=null;

    } else {
        a_propos_on=true;
        texte_principal.contentEditable = 'false';
        sauvegarde=texte_principal.innerHTML;
        texte_principal.innerHTML=texte_apropos.innerHTML;
        if (police_active!='AA Cursive'){
            ligatures('auto',[1,0])
        }
        bouton_a_propos.style.backgroundPosition='0% 0%';
    }
}


function menu_police() {
    if (zone_menu_police.style.display==='block'){ferme_menu_police()}
    else {zone_menu_police.style.display='block';}
}

function ferme_menu_police() {
    zone_menu_police.style.display='none';
}

function menu_copier_lien() {
    majLien();
    if (zone_menu_copie_lien.style.display==='block'){ferme_menu_copie_lien()}
    else {zone_menu_copie_lien.style.display='block';}
}

function ferme_menu_copie_lien() {
    zone_menu_copie_lien.style.display='none';
}




// Fonction pour changer la police
function change_police(police) {
    // Récupérer le texte sélectionné
    let texteSelectionne = window.getSelection().toString();
    console.log('Texte sélectionné : ' + texteSelectionne);

    if (texteSelectionne) {
        changePoliceSelection(police);
    } else {
        texte_principal.style.fontFamily = police;
        texte_marge.style.fontFamily = police;
        texte_marge_complementaire.style.fontFamily = police;
        police_active = police;

        if (!window.widget) {
            localStorage.setItem('seyes-police', police);
        }

        // Si la police existe dans notre objet typesPolices
        if (typesPolices[police]) {
            let config = typesPolices[police];
            coef_marge = config.coef_marge;
            coef_marge_windows = config.coef_marge_windows;
            facteur_taille_police = config.facteur_taille_police;
            epaisseur_police = config.epaisseur_police;
            distance_soulignage = config.distance_soulignage;
            bouton_ligatures.style.display = config.bouton_ligatures_display;
            ligatures('auto', config.ligatures);
        }
    }
    zoom(0, police_active);
    update_souligne();
}


function cree_image(event){
    let input=event.target;
    let nouvelle_image=new Image();

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            nouvelle_image.src = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }

    nouvelle_image.style.position='absolute';
    nouvelle_image.style.top='100px';
    nouvelle_image.style.left='100px';
    nouvelle_image.classList.add('draggable');
    nouvelle_image.style.width='300px';

    body.appendChild(nouvelle_image);
}

function souligne(couleur) {
    if (couleur) {
        couleurSoulignementActive = couleur;
        let boutonClique = document.getElementById('souligne_' + couleur);
        document.getElementById('souligne').style.backgroundImage = boutonClique.style.backgroundImage;
    } else {
        couleur = couleurSoulignementActive;
    }

    console.log('Soulignement ' + couleur);

    let selection = window.getSelection();
    if (selection.rangeCount > 0) {
        let range = selection.getRangeAt(0);

        // Si la sélection traverse plusieurs éléments
        if (!isSingleElementSelected(range)) {
            handleMultiElementSelection(range, couleur);
        } else {
            // Gérer la sélection sur un seul élément
            let startContainer = range.startContainer;
            let endContainer = range.endContainer;

            if (startContainer.parentNode.tagName === 'SPAN' || endContainer.parentNode.tagName === 'SPAN') {
                handlePartialSpanSelection(range, couleur);
            } else {
                let selectedText = range.cloneContents(); // Clone le contenu sélectionné

                let wrapper = document.createElement('div');
                wrapper.appendChild(selectedText);

                wrapper.querySelectorAll('span.souligne').forEach(function(span) {
                    let parent = span.parentNode;
                    while (span.firstChild) parent.insertBefore(span.firstChild, span);
                    parent.removeChild(span);
                });

                range.deleteContents();

                if (couleur === 'vide') {
                    range.insertNode(document.createTextNode(wrapper.textContent));
                } else {
                    let span = document.createElement('span');
                    span.classList.add('souligne', couleur);
                    span.innerHTML = wrapper.innerHTML;
                    range.insertNode(span);
                }
            }
        }
    }
}

// Fonction pour gérer une sélection qui s'étend sur plusieurs éléments
function handleMultiElementSelection(range, couleur) {
    let fragment = range.cloneContents(); // Clone le contenu sélectionné
    let nodes = Array.from(fragment.childNodes); // Récupérer tous les nœuds dans la sélection

    // Fusionner le texte de tous les nœuds
    let mergedText = '';
    nodes.forEach(node => {
        if (node.nodeType === Node.TEXT_NODE) {
            mergedText += node.textContent;
        } else if (node.nodeType === Node.ELEMENT_NODE) {
            mergedText += node.textContent; // Extraire le texte à l'intérieur des <span>
        }
    });

    // Supprimer la sélection originale
    range.deleteContents();

    if (couleur === 'vide') {
        // Si on veut supprimer le soulignement, insérer simplement le texte fusionné
        range.insertNode(document.createTextNode(mergedText));
    } else {
        // Appliquer une nouvelle couleur à l'ensemble du texte fusionné
        let span = document.createElement('span');
        span.classList.add('souligne', couleur);
        span.textContent = mergedText;
        range.insertNode(span);
    }
}

// Fonction pour vérifier si la sélection est dans un seul élément
function isSingleElementSelected(range) {
    let commonAncestor = range.commonAncestorContainer;
    let startContainer = range.startContainer;
    let endContainer = range.endContainer;
    return (startContainer === endContainer || commonAncestor === startContainer.parentNode);
}

// Fonction pour gérer le cas où la sélection est partielle à l'intérieur d'un <span>
function handlePartialSpanSelection(range, couleur) {
    let startContainer = range.startContainer;
    let endContainer = range.endContainer;

    if (startContainer === endContainer && startContainer.nodeType === Node.TEXT_NODE) {
        let parent = startContainer.parentNode;

        if (parent.tagName === 'SPAN' && parent.classList.contains('souligne')) {
            let text = startContainer.nodeValue;
            let before = text.substring(0, range.startOffset);
            let selected = text.substring(range.startOffset, range.endOffset);
            let after = text.substring(range.endOffset);

            range.deleteContents();

            if (before) {
                let spanBefore = document.createElement('span');
                spanBefore.classList.add('souligne', parent.classList[1]);
                spanBefore.textContent = before;
                parent.parentNode.insertBefore(spanBefore, parent);
            }

            if (couleur === 'vide') {
                parent.parentNode.insertBefore(document.createTextNode(selected), parent);
            } else {
                let spanSelected = document.createElement('span');
                spanSelected.classList.add('souligne', couleur);
                spanSelected.textContent = selected;
                parent.parentNode.insertBefore(spanSelected, parent);
            }

            if (after) {
                let spanAfter = document.createElement('span');
                spanAfter.classList.add('souligne', parent.classList[1]);
                spanAfter.textContent = after;
                parent.parentNode.insertBefore(spanAfter, parent);
            }

            parent.remove();
        }
    }
}






function changePoliceSelection(police) {
    console.log("changement de police sur une sélection");
    // Récupérer la sélection actuelle
    let selection = window.getSelection();
    let range = selection.rangeCount > 0 ? selection.getRangeAt(0) : null;
    let texteSelectionne = selection.toString();
    console.log('Texte sélectionné : ' + texteSelectionne);
    console.log('Police demandée : ' + police);
    console.log('Range : ' + range);


    // Vérifier si la police existe dans l'objet typesPolices
    if (texteSelectionne && range && typesPolices[police]) {
        console.log("La police existe");

        let config = typesPolices[police];

        // Calcul de la nouvelle taille de police et des marges en fonction du facteur
        let taille_police_selection = largeur_carreau * config.facteur_taille_police;

        // Appliquer le style de la police
        let style = `
            font-family: ${police}; 
            font-size: ${taille_police_selection}px; 
            font-weight: ${config.epaisseur_police}; 
            line-height: ${taille_police_selection}px; 
        `;

        let startContainer = range.startContainer;
        let endContainer = range.endContainer;

        // Vérifier si la sélection s'étend sur plusieurs éléments
        if (startContainer !== endContainer) {
            // Gestion de la sélection sur plusieurs divs ou éléments
            console.log("Sélection sur plusieurs éléments");

            let fragment = range.cloneContents(); // Copier le contenu de la sélection
            let elements = fragment.querySelectorAll('*');

            elements.forEach(el => {
                // Appliquer le style uniquement au texte si l'élément n'est pas vide
                if (el.textContent.trim() !== '') {
                    let spanStyled = document.createElement('span');
                    spanStyled.className = 'customfont';
                    spanStyled.style.cssText = style;
                    spanStyled.innerHTML = el.innerHTML; // Conserver le contenu original
                    el.innerHTML = '';  // Remplacer par le span stylé
                    el.appendChild(spanStyled);
                }
            });

            // Remplacer la sélection par le contenu modifié
            range.deleteContents();
            range.insertNode(fragment);
        } else {
            // Cas où la sélection est dans un seul élément
            let spanStyled = document.createElement('span');
            spanStyled.className = 'customfont';
            spanStyled.style.cssText = style;
            spanStyled.textContent = texteSelectionne;

            // Supprimer la sélection initiale et insérer le span avec le style appliqué
            range.deleteContents();
            range.insertNode(spanStyled);

            // Réinitialiser la sélection pour correspondre au nouveau <span>
            selection.removeAllRanges();
            let newRange = document.createRange();
            newRange.setStart(spanStyled.firstChild, 0);
            newRange.setEnd(spanStyled.firstChild, spanStyled.firstChild.length);
            selection.addRange(newRange);
        }
    }
}



function date() {
    let options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };
    let dateDuJour = new Date().toLocaleDateString('fr-FR', options); // Obtient la date actuelle au format français (jour/mois/année)
    return dateDuJour;
}

texte_principal.addEventListener("focusout", save_param);
texte_marge.addEventListener("focusout", save_param);
texte_marge_complementaire.addEventListener("focusout", save_param);

// Écouteurs de souris
document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

//Raccourcis clavier


function changeCarreaux(type) {
    console.log('Changement des carreaux '+type)
    typeCarreaux=type;
    marge.style.backgroundImage="url('images/carreau_marge_"+typeCarreaux+".svg";
    margeSecondaire.style.backgroundImage="url('images/carreau_marge_"+typeCarreaux+".svg";

    page.style.backgroundImage="url('images/carreau_"+typeCarreaux+".svg";
    document.getElementById('carreaux').style.backgroundImage=`url(images/carreau_${type}.svg`;
    if (type==='terre'){
        page.style.borderLeft='none';
    } else {
        page.style.borderLeft=null;
    }
    if (type==='cahier_de_textes'){
        margeSecondaire.style.display='block';
        adapteMargeCahierDeTexte(7);
    } else {
        margeSecondaire.style.display=null;
        adapteMargeCahierDeTexte(4);
    }
    zoom(0,police_active);
    save_param();
}


function save_param() {
    if (!window.widget) {
        console.log("save local sotrage");
        localStorage.setItem('seyes-graphismes',graphismes.innerHTML);
        localStorage.setItem('seyes-police',police_active);
        localStorage.setItem('seyes-texte_principal',texte_principal.innerHTML);
        localStorage.setItem('seyes-texte_marge',texte_marge.innerHTML);
        localStorage.setItem('seyes-texte_marge_complementaire',texte_marge_complementaire.innerHTML);
        localStorage.setItem('seyes-applique_couleur', applique_couleur.style.backgroundColor);
        localStorage.setItem('seyes-choix_couleurs', choix_couleurs.value);
        localStorage.setItem('type_carreaux', typeCarreaux);
        localStorage.setItem('opacite-lignes', opaciteLignes);
        if (typeof largeur_marge !== 'undefined' && largeur_marge) {
            localStorage.setItem('largeur-marge', largeur_marge);
        }
    }
}



function ramenerFocus(div) {
    div.focus();
}

texte_principal.addEventListener('blur', function() {
    focus_texte=texte_principal;
});

texte_marge.addEventListener('blur', function() {
    focus_texte=texte_marge;
});

texte_marge_complementaire.addEventListener('blur', function() {
    focus_texte=texte_marge;
});


page.addEventListener('paste', function(e) {    
    // Quand un "coller" est détecté

    e.preventDefault(); // On inhibe le comportement "normal".

    //On récupère le contenu du presse-papiers
    var text = (e.originalEvent || e).clipboardData.getData('text/plain');

    // On supprime le formatage HTML.
    text = text.replace(/<[^>]*>/g, '');
    
    // On insère le texte sans styles.
    document.execCommand('insertText', false, text);
});







function ajouterBackgroundImages() {
    // Sélectionner tous les boutons dans la div boutons_soulignement
    const boutons = document.querySelectorAll('#boutons_soulignement button');

    // Parcourir chaque bouton
    boutons.forEach(bouton => {
        // Récupérer l'ID du bouton
        const boutonId = bouton.id;

        // Construire l'URL de l'image en fonction de l'ID du bouton
        const imageUrl = `images/${boutonId}.svg`;

        // Appliquer le background-image au bouton
        console.log('ajout des images de soulignement');
        console.log(imageUrl);
        bouton.style.backgroundImage = `url('${imageUrl}')`;

    });

        // Sélectionner tous les boutons dans la div boutons_soulignement
        const boutonsCarreau = document.querySelectorAll('#boutons_carreaux button');

        // Parcourir chaque bouton
        boutonsCarreau.forEach(bouton => {
            // Récupérer l'ID du bouton
            const boutonId = bouton.id;
    
            // Construire l'URL de l'image en fonction de l'ID du bouton
            let imageUrl = `images/${boutonId}.svg`;
            console.log('ajout des images de carreaux');
            console.log(imageUrl);    
            // Appliquer le background-image au bouton
            if (imageUrl===`images/carreau_cahier_de_textes.svg`){
                imageUrl='images/cahier_de_textes.svg';
            }
            bouton.style.backgroundImage = `url('${imageUrl}')`;
    
        });
}

// Appeler la fonction pour appliquer les images après le chargement du DOM
window.onload = ajouterBackgroundImages;

  // Fonction pour modifier la position
  function updateTransform() {
    document.documentElement.style.setProperty('--decalage-depart', - largeur_carreau*0.02117171 + 'px');
    document.documentElement.style.setProperty('--decalage-position', (-2 - largeur_carreau*0.02117171) + 'px');
  }

  // Exemple: Déplacer l'élément de 100px à droite et 50px vers le bas
  updateTransform();