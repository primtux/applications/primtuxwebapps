
// Cette fonction fait tomber des feuilles d'automne

// Variables

const ecartement = 50; // Écartement moyen des feuilles
const aleaPosition = 25; // Aléea de position
const largeurMoyenneFeuilles = 50;
const ecartMoyenneLargeurFeuilles = 25;

// Liste de feuilles vides
const imagesFeuilles = [];

// On remplit cette liste.
for (let i = 0; i < 12; i++) {
    let index;
    if (i+1<10){index = '0' +(i+1);}
    else {index = i+1}
    imagesFeuilles.push('images/feuilles/'+ index +'.svg');
}

// Détermination des dimentions de la fenêtre
const largeurFenetre = window.innerWidth;
const hauteurFenetre = window.innerHeight;

// Détermination du nombre de feuilles
const nombreFeuillesX = Math.floor(largeurFenetre / ecartement);
const nombreFeuillesY = Math.floor(hauteurFenetre / ecartement);

// Création des feuilles
for (let i = 0; i < nombreFeuillesY; i++) {
    // Pour chaque rangée
    //let positionY = i*ecartement + ecartement/2;
    let positionY = hauteurFenetre - Math.random()*60;

    for (let i = 0; i < nombreFeuillesX; i++) {
        // Pour chaque rangée
        let positionX = i*ecartement + ecartement/2;
        let largeurFeuille = Math.random() * ecartMoyenneLargeurFeuilles + largeurMoyenneFeuilles;
        let nouvelleFeuille = document.createElement('img');

        nouvelleFeuille.rotation = Math.random() * 360;
        nouvelleFeuille.decalageX = (Math.random() * aleaPosition * 2) - aleaPosition;
        nouvelleFeuille.decalageY = (Math.random() * aleaPosition * 2) - aleaPosition;            

        nouvelleFeuille.src = imagesFeuilles[Math.floor(Math.random() * imagesFeuilles.length)];
        nouvelleFeuille.classList.add('feuille-automne');
        nouvelleFeuille.style.left = positionX + 'px';
        //nouvelleFeuille.style.top = positionY - hauteurFenetre + 'px';
        nouvelleFeuille.style.top = positionY + 'px';

        nouvelleFeuille.style.width = largeurFeuille + 'px';

        nouvelleFeuille.style.transform = 'rotate(' + nouvelleFeuille.rotation + 'deg)' +
        ' translateX(' + nouvelleFeuille.decalageX + 'px)' +
        ' translateY(' + nouvelleFeuille.decalageY + 'px)';
        
        body.appendChild(nouvelleFeuille);

        // Appel de la fonction pour faire tomber la feuille
        //faireTomberFeuille(nouvelleFeuille);
    }
}


// Cette fonction fait descendre une feuille
function faireTomberFeuille(feuille) {
    const vitesse = Math.random() * 2 + 1; // Vitesse de descente aléatoire
    const intervalId = setInterval(function() {
        // Récupère la position actuelle de la feuille
        let positionXActuelle = parseFloat(feuille.style.left);
        let positionYActuelle = parseFloat(feuille.style.top);

        // Si la feuille n'a pas encore atteint le bas de la fenêtre
        if (positionYActuelle < hauteurFenetre - 30) {
            // Descendre la feuille
            feuille.style.top = (positionYActuelle + vitesse) + 'px';
            // Bouger latéralement
            feuille.style.left = (positionXActuelle + (Math.random() * 2) - 1) + 'px';
            // Rotation

            feuille.rotation = feuille.rotation + (Math.random() * 5);
            feuille.style.transform = 'rotate(' + feuille.rotation + 'deg)' +
        ' translateX(' + feuille.decalageX + 'px)' +
        ' translateY(' + feuille.decalageY + 'px)';

        } else {
            // Si la feuille atteint le bas, arrêter l'intervalle
            clearInterval(intervalId);
        }
    }, 10); // 10ms d'intervalle
}

