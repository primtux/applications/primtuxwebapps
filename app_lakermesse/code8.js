gdjs.optionsCode = {};
gdjs.optionsCode.GDscore2Objects1= [];
gdjs.optionsCode.GDscore2Objects2= [];
gdjs.optionsCode.GDscore2Objects3= [];
gdjs.optionsCode.GDscoreObjects1= [];
gdjs.optionsCode.GDscoreObjects2= [];
gdjs.optionsCode.GDscoreObjects3= [];
gdjs.optionsCode.GDfondObjects1= [];
gdjs.optionsCode.GDfondObjects2= [];
gdjs.optionsCode.GDfondObjects3= [];
gdjs.optionsCode.GDbouton_95retourObjects1= [];
gdjs.optionsCode.GDbouton_95retourObjects2= [];
gdjs.optionsCode.GDbouton_95retourObjects3= [];
gdjs.optionsCode.GDtxt_95ecart_95maxiObjects1= [];
gdjs.optionsCode.GDtxt_95ecart_95maxiObjects2= [];
gdjs.optionsCode.GDtxt_95ecart_95maxiObjects3= [];
gdjs.optionsCode.GDtxt_95ecart_95miniObjects1= [];
gdjs.optionsCode.GDtxt_95ecart_95miniObjects2= [];
gdjs.optionsCode.GDtxt_95ecart_95miniObjects3= [];
gdjs.optionsCode.GDtxt_95tirage_95maxiObjects1= [];
gdjs.optionsCode.GDtxt_95tirage_95maxiObjects2= [];
gdjs.optionsCode.GDtxt_95tirage_95maxiObjects3= [];
gdjs.optionsCode.GDtxt_95tirage_95miniObjects1= [];
gdjs.optionsCode.GDtxt_95tirage_95miniObjects2= [];
gdjs.optionsCode.GDtxt_95tirage_95miniObjects3= [];
gdjs.optionsCode.GDtxt_95effacer_95scoreObjects1= [];
gdjs.optionsCode.GDtxt_95effacer_95scoreObjects2= [];
gdjs.optionsCode.GDtxt_95effacer_95scoreObjects3= [];
gdjs.optionsCode.GDspr_95bouton_95plus2Objects1= [];
gdjs.optionsCode.GDspr_95bouton_95plus2Objects2= [];
gdjs.optionsCode.GDspr_95bouton_95plus2Objects3= [];
gdjs.optionsCode.GDspr_95bouton_95plus4Objects1= [];
gdjs.optionsCode.GDspr_95bouton_95plus4Objects2= [];
gdjs.optionsCode.GDspr_95bouton_95plus4Objects3= [];
gdjs.optionsCode.GDspr_95bouton_95plus3Objects1= [];
gdjs.optionsCode.GDspr_95bouton_95plus3Objects2= [];
gdjs.optionsCode.GDspr_95bouton_95plus3Objects3= [];
gdjs.optionsCode.GDspr_95bouton_95plusObjects1= [];
gdjs.optionsCode.GDspr_95bouton_95plusObjects2= [];
gdjs.optionsCode.GDspr_95bouton_95plusObjects3= [];
gdjs.optionsCode.GDspr_95bouton_95moins4Objects1= [];
gdjs.optionsCode.GDspr_95bouton_95moins4Objects2= [];
gdjs.optionsCode.GDspr_95bouton_95moins4Objects3= [];
gdjs.optionsCode.GDspr_95bouton_95moins3Objects1= [];
gdjs.optionsCode.GDspr_95bouton_95moins3Objects2= [];
gdjs.optionsCode.GDspr_95bouton_95moins3Objects3= [];
gdjs.optionsCode.GDspr_95bouton_95moins2Objects1= [];
gdjs.optionsCode.GDspr_95bouton_95moins2Objects2= [];
gdjs.optionsCode.GDspr_95bouton_95moins2Objects3= [];
gdjs.optionsCode.GDspr_95bouton_95moinsObjects1= [];
gdjs.optionsCode.GDspr_95bouton_95moinsObjects2= [];
gdjs.optionsCode.GDspr_95bouton_95moinsObjects3= [];
gdjs.optionsCode.GDtxt_95nombre_95miniObjects1= [];
gdjs.optionsCode.GDtxt_95nombre_95miniObjects2= [];
gdjs.optionsCode.GDtxt_95nombre_95miniObjects3= [];
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects1= [];
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects2= [];
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects3= [];
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects1= [];
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects2= [];
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects3= [];
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects1= [];
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects2= [];
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects3= [];
gdjs.optionsCode.GDtxt_95explications2Objects1= [];
gdjs.optionsCode.GDtxt_95explications2Objects2= [];
gdjs.optionsCode.GDtxt_95explications2Objects3= [];
gdjs.optionsCode.GDtxt_95explicationsObjects1= [];
gdjs.optionsCode.GDtxt_95explicationsObjects2= [];
gdjs.optionsCode.GDtxt_95explicationsObjects3= [];
gdjs.optionsCode.GDmosaique_95rougeObjects1= [];
gdjs.optionsCode.GDmosaique_95rougeObjects2= [];
gdjs.optionsCode.GDmosaique_95rougeObjects3= [];
gdjs.optionsCode.GDmosaique_95verteObjects1= [];
gdjs.optionsCode.GDmosaique_95verteObjects2= [];
gdjs.optionsCode.GDmosaique_95verteObjects3= [];
gdjs.optionsCode.GDmosaique_95jauneObjects1= [];
gdjs.optionsCode.GDmosaique_95jauneObjects2= [];
gdjs.optionsCode.GDmosaique_95jauneObjects3= [];
gdjs.optionsCode.GDniv2Objects1= [];
gdjs.optionsCode.GDniv2Objects2= [];
gdjs.optionsCode.GDniv2Objects3= [];
gdjs.optionsCode.GDniv1Objects1= [];
gdjs.optionsCode.GDniv1Objects2= [];
gdjs.optionsCode.GDniv1Objects3= [];

gdjs.optionsCode.conditionTrue_0 = {val:false};
gdjs.optionsCode.condition0IsTrue_0 = {val:false};
gdjs.optionsCode.condition1IsTrue_0 = {val:false};
gdjs.optionsCode.condition2IsTrue_0 = {val:false};
gdjs.optionsCode.condition3IsTrue_0 = {val:false};
gdjs.optionsCode.condition4IsTrue_0 = {val:false};


gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_95retourObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595moinsObjects1Objects = Hashtable.newFrom({"spr_bouton_moins": gdjs.optionsCode.GDspr_95bouton_95moinsObjects1});gdjs.optionsCode.eventsList0x4fcf3a5c = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")) + 4;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").sub(1);
}}

}


{


{
}

}


}; //End of gdjs.optionsCode.eventsList0x4fcf3a5c
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595plusObjects1Objects = Hashtable.newFrom({"spr_bouton_plus": gdjs.optionsCode.GDspr_95bouton_95plusObjects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595moins2Objects1Objects = Hashtable.newFrom({"spr_bouton_moins2": gdjs.optionsCode.GDspr_95bouton_95moins2Objects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595plus2Objects1Objects = Hashtable.newFrom({"spr_bouton_plus2": gdjs.optionsCode.GDspr_95bouton_95plus2Objects1});gdjs.optionsCode.eventsList0x4fcdb5f4 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")) - 4;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi").add(1);
}}

}


}; //End of gdjs.optionsCode.eventsList0x4fcdb5f4
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595moins4Objects1Objects = Hashtable.newFrom({"spr_bouton_moins4": gdjs.optionsCode.GDspr_95bouton_95moins4Objects1});gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595plus4Objects1Objects = Hashtable.newFrom({"spr_bouton_plus4": gdjs.optionsCode.GDspr_95bouton_95plus4Objects1});gdjs.optionsCode.eventsList0x5b7338 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.optionsCode.GDmosaique_95jauneObjects1.createFrom(runtimeScene.getObjects("mosaique_jaune"));
gdjs.optionsCode.GDmosaique_95rougeObjects1.createFrom(runtimeScene.getObjects("mosaique_rouge"));
gdjs.optionsCode.GDmosaique_95verteObjects1.createFrom(runtimeScene.getObjects("mosaique_verte"));
{for(var i = 0, len = gdjs.optionsCode.GDmosaique_95jauneObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDmosaique_95jauneObjects1[i].setOpacity(180);
}
}{for(var i = 0, len = gdjs.optionsCode.GDmosaique_95verteObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDmosaique_95verteObjects1[i].setOpacity(180);
}
}{for(var i = 0, len = gdjs.optionsCode.GDmosaique_95rougeObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDmosaique_95rougeObjects1[i].setOpacity(180);
}
}}

}


{



}


{

gdjs.optionsCode.GDbouton_95retourObjects1.createFrom(runtimeScene.getObjects("bouton_retour"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


{



}


{

gdjs.optionsCode.GDspr_95bouton_95moinsObjects1.createFrom(runtimeScene.getObjects("spr_bouton_moins"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595moinsObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")) > 5;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi").sub(1);
}
{ //Subevents
gdjs.optionsCode.eventsList0x4fcf3a5c(runtimeScene);} //End of subevents
}

}


{



}


{



}


{

gdjs.optionsCode.GDspr_95bouton_95plusObjects1.createFrom(runtimeScene.getObjects("spr_bouton_plus"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595plusObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")) < 10;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi").add(1);
}}

}


{



}


{

gdjs.optionsCode.GDspr_95bouton_95moins2Objects1.createFrom(runtimeScene.getObjects("spr_bouton_moins2"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595moins2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")) > 1;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").sub(1);
}}

}


{

gdjs.optionsCode.GDspr_95bouton_95plus2Objects1.createFrom(runtimeScene.getObjects("spr_bouton_plus2"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595plus2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")) < 6;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")) - 1;
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").add(1);
}
{ //Subevents
gdjs.optionsCode.eventsList0x4fcdb5f4(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini"));
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini").sub(1);
}}

}


{



}


{



}


{



}


{



}


{

gdjs.optionsCode.GDspr_95bouton_95moins4Objects1.createFrom(runtimeScene.getObjects("spr_bouton_moins4"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595moins4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("ecart")) > 1;
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("ecart").sub(1);
}}

}


{

gdjs.optionsCode.GDspr_95bouton_95plus4Objects1.createFrom(runtimeScene.getObjects("spr_bouton_plus4"));

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDspr_9595bouton_9595plus4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("ecart")) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"));
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("ecart").add(1);
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("ecart")) < gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().get("nb_fromages_completer").getChild("mini"));
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().get("nb_fromages_completer").getChild("mini").sub(1);
}}

}


{


{
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects1.createFrom(runtimeScene.getObjects("txt_ecart_maximum"));
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects1.createFrom(runtimeScene.getObjects("txt_ecart_minimum"));
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects1.createFrom(runtimeScene.getObjects("txt_nombre_maxi"));
gdjs.optionsCode.GDtxt_95nombre_95miniObjects1.createFrom(runtimeScene.getObjects("txt_nombre_mini"));
{for(var i = 0, len = gdjs.optionsCode.GDtxt_95nombre_95maxiObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDtxt_95nombre_95maxiObjects1[i].setBBText(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi")));
}
}{for(var i = 0, len = gdjs.optionsCode.GDtxt_95nombre_95miniObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDtxt_95nombre_95miniObjects1[i].setBBText(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")));
}
}{for(var i = 0, len = gdjs.optionsCode.GDtxt_95ecart_95minimumObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDtxt_95ecart_95minimumObjects1[i].setBBText(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().get("nb_fromages_completer").getChild("mini")));
}
}{for(var i = 0, len = gdjs.optionsCode.GDtxt_95ecart_95maximumObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDtxt_95ecart_95maximumObjects1[i].setBBText(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("ecart")));
}
}}

}


}; //End of gdjs.optionsCode.eventsList0x5b7338


gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDscore2Objects1.length = 0;
gdjs.optionsCode.GDscore2Objects2.length = 0;
gdjs.optionsCode.GDscore2Objects3.length = 0;
gdjs.optionsCode.GDscoreObjects1.length = 0;
gdjs.optionsCode.GDscoreObjects2.length = 0;
gdjs.optionsCode.GDscoreObjects3.length = 0;
gdjs.optionsCode.GDfondObjects1.length = 0;
gdjs.optionsCode.GDfondObjects2.length = 0;
gdjs.optionsCode.GDfondObjects3.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects3.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95maxiObjects1.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95maxiObjects2.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95maxiObjects3.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95miniObjects1.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95miniObjects2.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95miniObjects3.length = 0;
gdjs.optionsCode.GDtxt_95tirage_95maxiObjects1.length = 0;
gdjs.optionsCode.GDtxt_95tirage_95maxiObjects2.length = 0;
gdjs.optionsCode.GDtxt_95tirage_95maxiObjects3.length = 0;
gdjs.optionsCode.GDtxt_95tirage_95miniObjects1.length = 0;
gdjs.optionsCode.GDtxt_95tirage_95miniObjects2.length = 0;
gdjs.optionsCode.GDtxt_95tirage_95miniObjects3.length = 0;
gdjs.optionsCode.GDtxt_95effacer_95scoreObjects1.length = 0;
gdjs.optionsCode.GDtxt_95effacer_95scoreObjects2.length = 0;
gdjs.optionsCode.GDtxt_95effacer_95scoreObjects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus2Objects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus2Objects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus2Objects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus4Objects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus4Objects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus4Objects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus3Objects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus3Objects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plus3Objects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plusObjects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plusObjects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95plusObjects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins4Objects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins4Objects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins4Objects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins3Objects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins3Objects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins3Objects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins2Objects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins2Objects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moins2Objects3.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moinsObjects1.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moinsObjects2.length = 0;
gdjs.optionsCode.GDspr_95bouton_95moinsObjects3.length = 0;
gdjs.optionsCode.GDtxt_95nombre_95miniObjects1.length = 0;
gdjs.optionsCode.GDtxt_95nombre_95miniObjects2.length = 0;
gdjs.optionsCode.GDtxt_95nombre_95miniObjects3.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects1.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects2.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95maximumObjects3.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects1.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects2.length = 0;
gdjs.optionsCode.GDtxt_95ecart_95minimumObjects3.length = 0;
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects1.length = 0;
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects2.length = 0;
gdjs.optionsCode.GDtxt_95nombre_95maxiObjects3.length = 0;
gdjs.optionsCode.GDtxt_95explications2Objects1.length = 0;
gdjs.optionsCode.GDtxt_95explications2Objects2.length = 0;
gdjs.optionsCode.GDtxt_95explications2Objects3.length = 0;
gdjs.optionsCode.GDtxt_95explicationsObjects1.length = 0;
gdjs.optionsCode.GDtxt_95explicationsObjects2.length = 0;
gdjs.optionsCode.GDtxt_95explicationsObjects3.length = 0;
gdjs.optionsCode.GDmosaique_95rougeObjects1.length = 0;
gdjs.optionsCode.GDmosaique_95rougeObjects2.length = 0;
gdjs.optionsCode.GDmosaique_95rougeObjects3.length = 0;
gdjs.optionsCode.GDmosaique_95verteObjects1.length = 0;
gdjs.optionsCode.GDmosaique_95verteObjects2.length = 0;
gdjs.optionsCode.GDmosaique_95verteObjects3.length = 0;
gdjs.optionsCode.GDmosaique_95jauneObjects1.length = 0;
gdjs.optionsCode.GDmosaique_95jauneObjects2.length = 0;
gdjs.optionsCode.GDmosaique_95jauneObjects3.length = 0;
gdjs.optionsCode.GDniv2Objects1.length = 0;
gdjs.optionsCode.GDniv2Objects2.length = 0;
gdjs.optionsCode.GDniv2Objects3.length = 0;
gdjs.optionsCode.GDniv1Objects1.length = 0;
gdjs.optionsCode.GDniv1Objects2.length = 0;
gdjs.optionsCode.GDniv1Objects3.length = 0;

gdjs.optionsCode.eventsList0x5b7338(runtimeScene);
return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
