gdjs.codeCode = {};
gdjs.codeCode.GDscore2Objects1= [];
gdjs.codeCode.GDscore2Objects2= [];
gdjs.codeCode.GDscore2Objects3= [];
gdjs.codeCode.GDscoreObjects1= [];
gdjs.codeCode.GDscoreObjects2= [];
gdjs.codeCode.GDscoreObjects3= [];
gdjs.codeCode.GDfondObjects1= [];
gdjs.codeCode.GDfondObjects2= [];
gdjs.codeCode.GDfondObjects3= [];
gdjs.codeCode.GDbouton_95retourObjects1= [];
gdjs.codeCode.GDbouton_95retourObjects2= [];
gdjs.codeCode.GDbouton_95retourObjects3= [];
gdjs.codeCode.GDtexte_95codeObjects1= [];
gdjs.codeCode.GDtexte_95codeObjects2= [];
gdjs.codeCode.GDtexte_95codeObjects3= [];
gdjs.codeCode.GDchoix_95nombre2Objects1= [];
gdjs.codeCode.GDchoix_95nombre2Objects2= [];
gdjs.codeCode.GDchoix_95nombre2Objects3= [];
gdjs.codeCode.GDchoix_95nombre3Objects1= [];
gdjs.codeCode.GDchoix_95nombre3Objects2= [];
gdjs.codeCode.GDchoix_95nombre3Objects3= [];
gdjs.codeCode.GDchoix_95nombre4Objects1= [];
gdjs.codeCode.GDchoix_95nombre4Objects2= [];
gdjs.codeCode.GDchoix_95nombre4Objects3= [];
gdjs.codeCode.GDchoix_95nombre5Objects1= [];
gdjs.codeCode.GDchoix_95nombre5Objects2= [];
gdjs.codeCode.GDchoix_95nombre5Objects3= [];
gdjs.codeCode.GDchoix_95nombre1Objects1= [];
gdjs.codeCode.GDchoix_95nombre1Objects2= [];
gdjs.codeCode.GDchoix_95nombre1Objects3= [];

gdjs.codeCode.conditionTrue_0 = {val:false};
gdjs.codeCode.condition0IsTrue_0 = {val:false};
gdjs.codeCode.condition1IsTrue_0 = {val:false};
gdjs.codeCode.condition2IsTrue_0 = {val:false};
gdjs.codeCode.condition3IsTrue_0 = {val:false};
gdjs.codeCode.conditionTrue_1 = {val:false};
gdjs.codeCode.condition0IsTrue_1 = {val:false};
gdjs.codeCode.condition1IsTrue_1 = {val:false};
gdjs.codeCode.condition2IsTrue_1 = {val:false};
gdjs.codeCode.condition3IsTrue_1 = {val:false};


gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects = Hashtable.newFrom({"choix_nombre3": gdjs.codeCode.GDchoix_95nombre3Objects1});gdjs.codeCode.eventsList0x19d29014 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) != 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(1338882468);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(1338939924);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).add(1);
}}

}


}; //End of gdjs.codeCode.eventsList0x19d29014
gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects = Hashtable.newFrom({"choix_nombre5": gdjs.codeCode.GDchoix_95nombre5Objects1});gdjs.codeCode.eventsList0x4fce9b0c = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) != 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).add(1);
}}

}


}; //End of gdjs.codeCode.eventsList0x4fce9b0c
gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects = Hashtable.newFrom({"choix_nombre2": gdjs.codeCode.GDchoix_95nombre2Objects1});gdjs.codeCode.eventsList0x4fcdb50c = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) != 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) == 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


}; //End of gdjs.codeCode.eventsList0x4fcdb50c
gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects = Hashtable.newFrom({"choix_nombre1": gdjs.codeCode.GDchoix_95nombre1Objects1, "choix_nombre4": gdjs.codeCode.GDchoix_95nombre4Objects1});gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_95retourObjects1});gdjs.codeCode.eventsList0x5b7338 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.codeCode.GDchoix_95nombre3Objects1.createFrom(runtimeScene.getObjects("choix_nombre3"));

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(433230220);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.codeCode.eventsList0x19d29014(runtimeScene);} //End of subevents
}

}


{

gdjs.codeCode.GDchoix_95nombre5Objects1.createFrom(runtimeScene.getObjects("choix_nombre5"));

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(1338940548);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.codeCode.eventsList0x4fce9b0c(runtimeScene);} //End of subevents
}

}


{

gdjs.codeCode.GDchoix_95nombre2Objects1.createFrom(runtimeScene.getObjects("choix_nombre2"));

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(1338881668);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.codeCode.eventsList0x4fcdb50c(runtimeScene);} //End of subevents
}

}


{

gdjs.codeCode.GDchoix_95nombre1Objects1.createFrom(runtimeScene.getObjects("choix_nombre1"));
gdjs.codeCode.GDchoix_95nombre4Objects1.createFrom(runtimeScene.getObjects("choix_nombre4"));

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(1338974884);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.codeCode.GDbouton_95retourObjects1.createFrom(runtimeScene.getObjects("bouton_retour"));

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


}; //End of gdjs.codeCode.eventsList0x5b7338


gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDscore2Objects1.length = 0;
gdjs.codeCode.GDscore2Objects2.length = 0;
gdjs.codeCode.GDscore2Objects3.length = 0;
gdjs.codeCode.GDscoreObjects1.length = 0;
gdjs.codeCode.GDscoreObjects2.length = 0;
gdjs.codeCode.GDscoreObjects3.length = 0;
gdjs.codeCode.GDfondObjects1.length = 0;
gdjs.codeCode.GDfondObjects2.length = 0;
gdjs.codeCode.GDfondObjects3.length = 0;
gdjs.codeCode.GDbouton_95retourObjects1.length = 0;
gdjs.codeCode.GDbouton_95retourObjects2.length = 0;
gdjs.codeCode.GDbouton_95retourObjects3.length = 0;
gdjs.codeCode.GDtexte_95codeObjects1.length = 0;
gdjs.codeCode.GDtexte_95codeObjects2.length = 0;
gdjs.codeCode.GDtexte_95codeObjects3.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects3.length = 0;

gdjs.codeCode.eventsList0x5b7338(runtimeScene);
return;

}

gdjs['codeCode'] = gdjs.codeCode;
