gdjs.infosCode = {};
gdjs.infosCode.GDcroco_95gourmandObjects1= [];
gdjs.infosCode.GDcroco_95gourmandObjects2= [];
gdjs.infosCode.GDscoreObjects1= [];
gdjs.infosCode.GDscoreObjects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDfond_95acceuilObjects1= [];
gdjs.infosCode.GDfond_95acceuilObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDdrapeau_951Objects1= [];
gdjs.infosCode.GDdrapeau_951Objects2= [];
gdjs.infosCode.GDdrapeau_952Objects1= [];
gdjs.infosCode.GDdrapeau_952Objects2= [];
gdjs.infosCode.GDpointObjects1= [];
gdjs.infosCode.GDpointObjects2= [];
gdjs.infosCode.GDtxt_95infosObjects1= [];
gdjs.infosCode.GDtxt_95infosObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};
gdjs.infosCode.condition3IsTrue_0 = {val:false};
gdjs.infosCode.conditionTrue_1 = {val:false};
gdjs.infosCode.condition0IsTrue_1 = {val:false};
gdjs.infosCode.condition1IsTrue_1 = {val:false};
gdjs.infosCode.condition2IsTrue_1 = {val:false};
gdjs.infosCode.condition3IsTrue_1 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
gdjs.infosCode.condition2IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.infosCode.condition1IsTrue_0.val ) {
{
{gdjs.infosCode.conditionTrue_1 = gdjs.infosCode.condition2IsTrue_0;
gdjs.infosCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14110988);
}
}}
}
if (gdjs.infosCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDcroco_95gourmandObjects1.length = 0;
gdjs.infosCode.GDcroco_95gourmandObjects2.length = 0;
gdjs.infosCode.GDscoreObjects1.length = 0;
gdjs.infosCode.GDscoreObjects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDfond_95acceuilObjects1.length = 0;
gdjs.infosCode.GDfond_95acceuilObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDdrapeau_951Objects1.length = 0;
gdjs.infosCode.GDdrapeau_951Objects2.length = 0;
gdjs.infosCode.GDdrapeau_952Objects1.length = 0;
gdjs.infosCode.GDdrapeau_952Objects2.length = 0;
gdjs.infosCode.GDpointObjects1.length = 0;
gdjs.infosCode.GDpointObjects2.length = 0;
gdjs.infosCode.GDtxt_95infosObjects1.length = 0;
gdjs.infosCode.GDtxt_95infosObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
