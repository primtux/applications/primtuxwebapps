gdjs.codeCode = {};
gdjs.codeCode.GDcroco_95gourmandObjects1= [];
gdjs.codeCode.GDcroco_95gourmandObjects2= [];
gdjs.codeCode.GDcroco_95gourmandObjects3= [];
gdjs.codeCode.GDscoreObjects1= [];
gdjs.codeCode.GDscoreObjects2= [];
gdjs.codeCode.GDscoreObjects3= [];
gdjs.codeCode.GDscore2Objects1= [];
gdjs.codeCode.GDscore2Objects2= [];
gdjs.codeCode.GDscore2Objects3= [];
gdjs.codeCode.GDfond_95acceuilObjects1= [];
gdjs.codeCode.GDfond_95acceuilObjects2= [];
gdjs.codeCode.GDfond_95acceuilObjects3= [];
gdjs.codeCode.GDbouton_95retourObjects1= [];
gdjs.codeCode.GDbouton_95retourObjects2= [];
gdjs.codeCode.GDbouton_95retourObjects3= [];
gdjs.codeCode.GDfond_95blancObjects1= [];
gdjs.codeCode.GDfond_95blancObjects2= [];
gdjs.codeCode.GDfond_95blancObjects3= [];
gdjs.codeCode.GDdrapeau_951Objects1= [];
gdjs.codeCode.GDdrapeau_951Objects2= [];
gdjs.codeCode.GDdrapeau_951Objects3= [];
gdjs.codeCode.GDdrapeau_952Objects1= [];
gdjs.codeCode.GDdrapeau_952Objects2= [];
gdjs.codeCode.GDdrapeau_952Objects3= [];
gdjs.codeCode.GDpointObjects1= [];
gdjs.codeCode.GDpointObjects2= [];
gdjs.codeCode.GDpointObjects3= [];
gdjs.codeCode.GDtexte_95codeObjects1= [];
gdjs.codeCode.GDtexte_95codeObjects2= [];
gdjs.codeCode.GDtexte_95codeObjects3= [];
gdjs.codeCode.GDchoix_95nombre1Objects1= [];
gdjs.codeCode.GDchoix_95nombre1Objects2= [];
gdjs.codeCode.GDchoix_95nombre1Objects3= [];
gdjs.codeCode.GDchoix_95nombre2Objects1= [];
gdjs.codeCode.GDchoix_95nombre2Objects2= [];
gdjs.codeCode.GDchoix_95nombre2Objects3= [];
gdjs.codeCode.GDchoix_95nombre3Objects1= [];
gdjs.codeCode.GDchoix_95nombre3Objects2= [];
gdjs.codeCode.GDchoix_95nombre3Objects3= [];
gdjs.codeCode.GDchoix_95nombre4Objects1= [];
gdjs.codeCode.GDchoix_95nombre4Objects2= [];
gdjs.codeCode.GDchoix_95nombre4Objects3= [];
gdjs.codeCode.GDchoix_95nombre5Objects1= [];
gdjs.codeCode.GDchoix_95nombre5Objects2= [];
gdjs.codeCode.GDchoix_95nombre5Objects3= [];

gdjs.codeCode.conditionTrue_0 = {val:false};
gdjs.codeCode.condition0IsTrue_0 = {val:false};
gdjs.codeCode.condition1IsTrue_0 = {val:false};
gdjs.codeCode.condition2IsTrue_0 = {val:false};
gdjs.codeCode.condition3IsTrue_0 = {val:false};
gdjs.codeCode.conditionTrue_1 = {val:false};
gdjs.codeCode.condition0IsTrue_1 = {val:false};
gdjs.codeCode.condition1IsTrue_1 = {val:false};
gdjs.codeCode.condition2IsTrue_1 = {val:false};
gdjs.codeCode.condition3IsTrue_1 = {val:false};


gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects = Hashtable.newFrom({"choix_nombre3": gdjs.codeCode.GDchoix_95nombre3Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14090100);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition1IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14091252);
}
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects = Hashtable.newFrom({"choix_nombre5": gdjs.codeCode.GDchoix_95nombre5Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects = Hashtable.newFrom({"choix_nombre2": gdjs.codeCode.GDchoix_95nombre2Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


};gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects = Hashtable.newFrom({"choix_nombre1": gdjs.codeCode.GDchoix_95nombre1Objects1, "choix_nombre4": gdjs.codeCode.GDchoix_95nombre4Objects1});
gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_95retourObjects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


gdjs.codeCode.condition0IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.codeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre3"), gdjs.codeCode.GDchoix_95nombre3Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre3Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14088924);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre5"), gdjs.codeCode.GDchoix_95nombre5Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre5Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14092532);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre2"), gdjs.codeCode.GDchoix_95nombre2Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14095532);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}
{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("choix_nombre1"), gdjs.codeCode.GDchoix_95nombre1Objects1);
gdjs.copyArray(runtimeScene.getObjects("choix_nombre4"), gdjs.codeCode.GDchoix_95nombre4Objects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
gdjs.codeCode.condition2IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDchoix_9595nombre1Objects1ObjectsGDgdjs_46codeCode_46GDchoix_9595nombre4Objects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.codeCode.condition1IsTrue_0.val ) {
{
{gdjs.codeCode.conditionTrue_1 = gdjs.codeCode.condition2IsTrue_0;
gdjs.codeCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14098692);
}
}}
}
if (gdjs.codeCode.condition2IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_95retourObjects1);

gdjs.codeCode.condition0IsTrue_0.val = false;
gdjs.codeCode.condition1IsTrue_0.val = false;
{
gdjs.codeCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_46codeCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.codeCode.condition0IsTrue_0.val ) {
{
gdjs.codeCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.codeCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDcroco_95gourmandObjects1.length = 0;
gdjs.codeCode.GDcroco_95gourmandObjects2.length = 0;
gdjs.codeCode.GDcroco_95gourmandObjects3.length = 0;
gdjs.codeCode.GDscoreObjects1.length = 0;
gdjs.codeCode.GDscoreObjects2.length = 0;
gdjs.codeCode.GDscoreObjects3.length = 0;
gdjs.codeCode.GDscore2Objects1.length = 0;
gdjs.codeCode.GDscore2Objects2.length = 0;
gdjs.codeCode.GDscore2Objects3.length = 0;
gdjs.codeCode.GDfond_95acceuilObjects1.length = 0;
gdjs.codeCode.GDfond_95acceuilObjects2.length = 0;
gdjs.codeCode.GDfond_95acceuilObjects3.length = 0;
gdjs.codeCode.GDbouton_95retourObjects1.length = 0;
gdjs.codeCode.GDbouton_95retourObjects2.length = 0;
gdjs.codeCode.GDbouton_95retourObjects3.length = 0;
gdjs.codeCode.GDfond_95blancObjects1.length = 0;
gdjs.codeCode.GDfond_95blancObjects2.length = 0;
gdjs.codeCode.GDfond_95blancObjects3.length = 0;
gdjs.codeCode.GDdrapeau_951Objects1.length = 0;
gdjs.codeCode.GDdrapeau_951Objects2.length = 0;
gdjs.codeCode.GDdrapeau_951Objects3.length = 0;
gdjs.codeCode.GDdrapeau_952Objects1.length = 0;
gdjs.codeCode.GDdrapeau_952Objects2.length = 0;
gdjs.codeCode.GDdrapeau_952Objects3.length = 0;
gdjs.codeCode.GDpointObjects1.length = 0;
gdjs.codeCode.GDpointObjects2.length = 0;
gdjs.codeCode.GDpointObjects3.length = 0;
gdjs.codeCode.GDtexte_95codeObjects1.length = 0;
gdjs.codeCode.GDtexte_95codeObjects2.length = 0;
gdjs.codeCode.GDtexte_95codeObjects3.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre1Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre2Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre3Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre4Objects3.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects1.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects2.length = 0;
gdjs.codeCode.GDchoix_95nombre5Objects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);

return;

}

gdjs['codeCode'] = gdjs.codeCode;
