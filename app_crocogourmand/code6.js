gdjs.optionsCode = {};
gdjs.optionsCode.GDcroco_95gourmandObjects1= [];
gdjs.optionsCode.GDcroco_95gourmandObjects2= [];
gdjs.optionsCode.GDcroco_95gourmandObjects3= [];
gdjs.optionsCode.GDcroco_95gourmandObjects4= [];
gdjs.optionsCode.GDscoreObjects1= [];
gdjs.optionsCode.GDscoreObjects2= [];
gdjs.optionsCode.GDscoreObjects3= [];
gdjs.optionsCode.GDscoreObjects4= [];
gdjs.optionsCode.GDscore2Objects1= [];
gdjs.optionsCode.GDscore2Objects2= [];
gdjs.optionsCode.GDscore2Objects3= [];
gdjs.optionsCode.GDscore2Objects4= [];
gdjs.optionsCode.GDfond_95acceuilObjects1= [];
gdjs.optionsCode.GDfond_95acceuilObjects2= [];
gdjs.optionsCode.GDfond_95acceuilObjects3= [];
gdjs.optionsCode.GDfond_95acceuilObjects4= [];
gdjs.optionsCode.GDbouton_95retourObjects1= [];
gdjs.optionsCode.GDbouton_95retourObjects2= [];
gdjs.optionsCode.GDbouton_95retourObjects3= [];
gdjs.optionsCode.GDbouton_95retourObjects4= [];
gdjs.optionsCode.GDfond_95blancObjects1= [];
gdjs.optionsCode.GDfond_95blancObjects2= [];
gdjs.optionsCode.GDfond_95blancObjects3= [];
gdjs.optionsCode.GDfond_95blancObjects4= [];
gdjs.optionsCode.GDdrapeau_951Objects1= [];
gdjs.optionsCode.GDdrapeau_951Objects2= [];
gdjs.optionsCode.GDdrapeau_951Objects3= [];
gdjs.optionsCode.GDdrapeau_951Objects4= [];
gdjs.optionsCode.GDdrapeau_952Objects1= [];
gdjs.optionsCode.GDdrapeau_952Objects2= [];
gdjs.optionsCode.GDdrapeau_952Objects3= [];
gdjs.optionsCode.GDdrapeau_952Objects4= [];
gdjs.optionsCode.GDpointObjects1= [];
gdjs.optionsCode.GDpointObjects2= [];
gdjs.optionsCode.GDpointObjects3= [];
gdjs.optionsCode.GDpointObjects4= [];
gdjs.optionsCode.GDplus_95grandObjects1= [];
gdjs.optionsCode.GDplus_95grandObjects2= [];
gdjs.optionsCode.GDplus_95grandObjects3= [];
gdjs.optionsCode.GDplus_95grandObjects4= [];
gdjs.optionsCode.GDswitch_95vocabulaireObjects1= [];
gdjs.optionsCode.GDswitch_95vocabulaireObjects2= [];
gdjs.optionsCode.GDswitch_95vocabulaireObjects3= [];
gdjs.optionsCode.GDswitch_95vocabulaireObjects4= [];
gdjs.optionsCode.GDplus_95queObjects1= [];
gdjs.optionsCode.GDplus_95queObjects2= [];
gdjs.optionsCode.GDplus_95queObjects3= [];
gdjs.optionsCode.GDplus_95queObjects4= [];
gdjs.optionsCode.GDinfosObjects1= [];
gdjs.optionsCode.GDinfosObjects2= [];
gdjs.optionsCode.GDinfosObjects3= [];
gdjs.optionsCode.GDinfosObjects4= [];

gdjs.optionsCode.conditionTrue_0 = {val:false};
gdjs.optionsCode.condition0IsTrue_0 = {val:false};
gdjs.optionsCode.condition1IsTrue_0 = {val:false};
gdjs.optionsCode.condition2IsTrue_0 = {val:false};
gdjs.optionsCode.condition3IsTrue_0 = {val:false};
gdjs.optionsCode.conditionTrue_1 = {val:false};
gdjs.optionsCode.condition0IsTrue_1 = {val:false};
gdjs.optionsCode.condition1IsTrue_1 = {val:false};
gdjs.optionsCode.condition2IsTrue_1 = {val:false};
gdjs.optionsCode.condition3IsTrue_1 = {val:false};


gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_95retourObjects1});
gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{


{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(2)));
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.optionsCode.eventsList1 = function(runtimeScene) {

{


gdjs.optionsCode.eventsList0(runtimeScene);
}


{


{
}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDswitch_9595vocabulaireObjects2Objects = Hashtable.newFrom({"switch_vocabulaire": gdjs.optionsCode.GDswitch_95vocabulaireObjects2});
gdjs.optionsCode.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("switch_vocabulaire"), gdjs.optionsCode.GDswitch_95vocabulaireObjects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDswitch_9595vocabulaireObjects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition2IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14119268);
}
}}
}
if (gdjs.optionsCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio").add(1);
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio")) > 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio").setNumber(0);
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio")) == 0;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("plus_grand"), gdjs.optionsCode.GDplus_95grandObjects2);
gdjs.copyArray(runtimeScene.getObjects("plus_que"), gdjs.optionsCode.GDplus_95queObjects2);
gdjs.copyArray(runtimeScene.getObjects("switch_vocabulaire"), gdjs.optionsCode.GDswitch_95vocabulaireObjects2);
{for(var i = 0, len = gdjs.optionsCode.GDplus_95queObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDplus_95queObjects2[i].setOpacity(255);
}
}{for(var i = 0, len = gdjs.optionsCode.GDplus_95grandObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDplus_95grandObjects2[i].setOpacity(100);
}
}{for(var i = 0, len = gdjs.optionsCode.GDswitch_95vocabulaireObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDswitch_95vocabulaireObjects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio")));
}
}}

}


{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio")) == 1;
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("plus_grand"), gdjs.optionsCode.GDplus_95grandObjects1);
gdjs.copyArray(runtimeScene.getObjects("plus_que"), gdjs.optionsCode.GDplus_95queObjects1);
gdjs.copyArray(runtimeScene.getObjects("switch_vocabulaire"), gdjs.optionsCode.GDswitch_95vocabulaireObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDplus_95queObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDplus_95queObjects1[i].setOpacity(100);
}
}{for(var i = 0, len = gdjs.optionsCode.GDplus_95grandObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDplus_95grandObjects1[i].setOpacity(255);
}
}{for(var i = 0, len = gdjs.optionsCode.GDswitch_95vocabulaireObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDswitch_95vocabulaireObjects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("switch_audio")));
}
}}

}


};gdjs.optionsCode.eventsList3 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.optionsCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDfond_95blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.optionsCode.GDbouton_95retourObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.optionsCode.eventsList2(runtimeScene);
}


{


{
}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDcroco_95gourmandObjects1.length = 0;
gdjs.optionsCode.GDcroco_95gourmandObjects2.length = 0;
gdjs.optionsCode.GDcroco_95gourmandObjects3.length = 0;
gdjs.optionsCode.GDcroco_95gourmandObjects4.length = 0;
gdjs.optionsCode.GDscoreObjects1.length = 0;
gdjs.optionsCode.GDscoreObjects2.length = 0;
gdjs.optionsCode.GDscoreObjects3.length = 0;
gdjs.optionsCode.GDscoreObjects4.length = 0;
gdjs.optionsCode.GDscore2Objects1.length = 0;
gdjs.optionsCode.GDscore2Objects2.length = 0;
gdjs.optionsCode.GDscore2Objects3.length = 0;
gdjs.optionsCode.GDscore2Objects4.length = 0;
gdjs.optionsCode.GDfond_95acceuilObjects1.length = 0;
gdjs.optionsCode.GDfond_95acceuilObjects2.length = 0;
gdjs.optionsCode.GDfond_95acceuilObjects3.length = 0;
gdjs.optionsCode.GDfond_95acceuilObjects4.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects3.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects4.length = 0;
gdjs.optionsCode.GDfond_95blancObjects1.length = 0;
gdjs.optionsCode.GDfond_95blancObjects2.length = 0;
gdjs.optionsCode.GDfond_95blancObjects3.length = 0;
gdjs.optionsCode.GDfond_95blancObjects4.length = 0;
gdjs.optionsCode.GDdrapeau_951Objects1.length = 0;
gdjs.optionsCode.GDdrapeau_951Objects2.length = 0;
gdjs.optionsCode.GDdrapeau_951Objects3.length = 0;
gdjs.optionsCode.GDdrapeau_951Objects4.length = 0;
gdjs.optionsCode.GDdrapeau_952Objects1.length = 0;
gdjs.optionsCode.GDdrapeau_952Objects2.length = 0;
gdjs.optionsCode.GDdrapeau_952Objects3.length = 0;
gdjs.optionsCode.GDdrapeau_952Objects4.length = 0;
gdjs.optionsCode.GDpointObjects1.length = 0;
gdjs.optionsCode.GDpointObjects2.length = 0;
gdjs.optionsCode.GDpointObjects3.length = 0;
gdjs.optionsCode.GDpointObjects4.length = 0;
gdjs.optionsCode.GDplus_95grandObjects1.length = 0;
gdjs.optionsCode.GDplus_95grandObjects2.length = 0;
gdjs.optionsCode.GDplus_95grandObjects3.length = 0;
gdjs.optionsCode.GDplus_95grandObjects4.length = 0;
gdjs.optionsCode.GDswitch_95vocabulaireObjects1.length = 0;
gdjs.optionsCode.GDswitch_95vocabulaireObjects2.length = 0;
gdjs.optionsCode.GDswitch_95vocabulaireObjects3.length = 0;
gdjs.optionsCode.GDswitch_95vocabulaireObjects4.length = 0;
gdjs.optionsCode.GDplus_95queObjects1.length = 0;
gdjs.optionsCode.GDplus_95queObjects2.length = 0;
gdjs.optionsCode.GDplus_95queObjects3.length = 0;
gdjs.optionsCode.GDplus_95queObjects4.length = 0;
gdjs.optionsCode.GDinfosObjects1.length = 0;
gdjs.optionsCode.GDinfosObjects2.length = 0;
gdjs.optionsCode.GDinfosObjects3.length = 0;
gdjs.optionsCode.GDinfosObjects4.length = 0;

gdjs.optionsCode.eventsList3(runtimeScene);

return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
