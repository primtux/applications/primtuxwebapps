## [Unreleased : 1.0]

### Added

- police "Luciole" sur tous les applicatifs
- responsive
- interface tactile
- design system sur l'intégralité des softs
- retoucher les favicons
- utiliser des fichiers réutilisables : css/js

## [Unreleased : 0.1]

### Added

Intégration des softs :
    [X] aujardin
    [X] achats
    [X] blocs-logiques
    [X] histoires
    [X] zebulon
    [X] balance-virtuelle
    [X] alacampagne
    [X] association-images
    [X] associations-images2
    [X] avoir-etre
    [X] chiffres-lettres
    [X] croissant-decroissant
    [X] leximots
    [X] lis-ecris
    [X] lis-ecris2
    [X] ordre-alphabetique
    [X] digiscreen

    [X] Jclic-C1-Clavier-Souris
    [X] Jclic-C1-Les-Lettres
    [X] Jclic-C1-Lexique
    [X] Jclic-C1-Tri-Rangement-Intrus-Images-séquentielles
    [X] Jclic-C2-CE1-Orthographe
    [X] Jclic-C2-CE1-Conjugaison
    [X] Jclic-C2-CE1-Grammaire
    [X] Jclic-C2-CE1-Lexique
    [X] Jclic-C2-CE1-Numération
    [X] Jclic-C2-CE1-Sons-complexes
    [X] Jclic-C2-CP-Numération
    [X] Jclic-C2-CP-Sons
    [X] Jclic-C3-Anglais
    [X] Jclic-C3-Calcul
    [X] Jclic-C3-Conjugaison
    [X] Jclic-C3-EMC
    [X] Jclic-C3-Géométrie
    [X] Jclic-C3-Lexique
    [X] Jclic-C3-Questionner_le_monde

    [X] Aleccor-lecture-c3
    [X] Aleccor-conjugaison-c3
    [X] Aleccor-grammaire-c3-la-nature
    [X] Aleccor-grammaire-c3-les-fonctions
    [X] Aleccor-grammaire-c3-types-de-phrases
    [X] Aleccor-lexique-c3-les-contraires
    [X] Aleccor-lexique-c3-suffixes-prefixes
    [X] Aleccor-orthographe-c3-homophones-grammaticaux
    [X] Aleccor-orthographe-c3-homophones-lexicaux
    [X] Aleccor-orthographe-c3
    [X] Aleccor-sciences-c3

    [X] edit-interactive-svg_1.1.4
    [X] poufpoufce1 19.10.0
    [X] poufpoufcp 19.10.0
    [X] poufpoufjeux 19.10.0
    [X] pragmactivites 0.5
