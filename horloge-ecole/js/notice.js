// Get references to the notice modal and close button
const noticeModal = document.getElementById("noticeModal_id");
const btFermerNotice = document.getElementById("fermerNotice_id");

// Function to display the notice modal
function afficherAide() {
  noticeModal.style.display = "flex";
  btFermerNotice.focus();
}
// Function to close the notice modal
function fermerNotice() {
  noticeModal.style.display = "none";
}
// Add event listener to the close button
btFermerNotice.addEventListener("click", fermerNotice);

// Get references to the licence modal and close button
const licenceModal = document.getElementById("licenceModal_id");
const btFermerLicence = document.getElementById("fermerLicence_id");
// Function to display the licence modal
function afficherLicence() {
  licenceModal.style.display = "flex";
  btFermerLicence.focus();
}
// Function to close the notice modal
function fermerLicence() {
  licenceModal.style.display = "none";
}
// Add event listener to the close button
btFermerLicence.addEventListener("click", fermerLicence);


//////////////// Fenêtres pour les réponses //////////////////
const reponseCorrecteModal = document.getElementById("reponseCorrecteModal_id");
const reponseErreurModal = document.getElementById("reponseErreurModal_id");
const btFermerReponseCorrecte = document.getElementById("fermerReponseCorrecte_id");
const btFermerReponseErreur = document.getElementById("fermerReponseErreur_id");

function afficherReponse(etat){
  if (etat===1){
    reponseCorrecteModal.style.display = "flex";
    btFermerReponseCorrecte.focus();
  }
  else {
    reponseErreurModal.style.display = "flex";
    btFermerReponseErreur.focus();
  }
}
function fermerReponseCorrecte() {
  reponseCorrecteModal.style.display = "none";
  lancerHeureAleatoire();
  reponse_boite_heures_id.value="";
  reponse_boite_minutes_id.value="";
  reponse_boite_secondes_id.value="";
}
function fermerReponseErreur() {
  reponseErreurModal.style.display = "none";
}  

btFermerReponseCorrecte.addEventListener("click", fermerReponseCorrecte);
btFermerReponseErreur.addEventListener("click", fermerReponseErreur);

