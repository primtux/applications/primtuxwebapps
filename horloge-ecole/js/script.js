'use strict';
var set_aiguille_horloge_int //nécessaire pour arrêter le mouvement des aiguilles de l'heure interne
var set_horloge_int

// Calcul position aiguilles
function calculPositionAiguillesHeureInterne() {
  let heure_int = document.getElementById("aiguille_heures_id");
  let minute_int = document.getElementById("aiguille_minutes_id");
  let secondes_int = document.getElementById("aiguille_secondes_id");
  let date_now = new Date();
  let hr_int = date_now.getHours();
  let min_int = date_now.getMinutes();
  let sec_int = date_now.getSeconds();
  let calc_hr_int = (hr_int * 30) + (min_int / 2);
  let calc_min_int = (min_int * 6) + (sec_int / 10);
  let calc_sec_int = sec_int * 6;
  heure_int.style.transform = `rotate(${calc_hr_int}deg)`;
  minute_int.style.transform = `rotate(${calc_min_int}deg)`;
  secondes_int.style.transform = `rotate(${calc_sec_int}deg)`;
}

function demarrage() {
  deplacementAiguillesHeureInterne();
  defilementHeureCourante();
}
function deplacementAiguillesHeureInterne() {
  set_aiguille_horloge_int = setInterval(calculPositionAiguillesHeureInterne, 1000);
}

// Déplacement des aiguilles pour heure demandée en mode interactif
function deplacementAiguillesHeureInteractive() {
  let heure = document.getElementById("aiguille_heures_id");
  let minute = document.getElementById("aiguille_minutes_id");
  let secondes = document.getElementById("aiguille_secondes_id");
  let hr = boite_heures_id.value;
  let min = boite_minutes_id.value;
  let sec = boite_secondes_id.value;
  let calc_hr = (hr * 30) + (min / 2);
  let calc_min = (min * 6) + (sec / 10);
  if (cacExoAvecSecondes_id.checked == false) { // Cas où on ne s'occupe pas des secondes
    sec=0
  }
  let calc_sec = sec * 6;
  heure.style.transform = `rotate(${calc_hr}deg)`;
  minute.style.transform = `rotate(${calc_min}deg)`;
  secondes.style.transform = `rotate(${calc_sec}deg)`;
}

//Heure digitale en cours
function heureCourante() {
  let date_now = new Date();
  let hr = date_now.getHours();
  if (hr < 10) { hr = "0" + hr };
  let min = date_now.getMinutes();
  if (min < 10) { min = "0" + min };
  let sec = date_now.getSeconds();
  if (sec < 10) { sec = "0" + sec };
  let heure = hr + ":" + min + ":" + sec;
  let hrBoite = document.getElementById("boite_heures_id");
  hrBoite.value = hr;
  let minBoite = document.getElementById("boite_minutes_id");
  minBoite.value = min;
  boite_secondes_id.value = sec;
}

function defilementHeureCourante() {
  set_horloge_int = setInterval(heureCourante, 1000);
}
defilementHeureCourante();

// Affichage/Masquage des élèments de l'horloge
// Initialisation de l'affichage
function initialiserAffichage(){
  cercleSVG.style.visibility = "visible";
  graduationsSVG.style.visibility = "visible";
  minutesSVG.style.visibility = "hidden";
  minutesMoinsSVG.style.visibility = "hidden";
  minutesMoins.style.visibility = "visible";
  heures_apremSVG.style.visibility = "hidden";
  heures_matinSVG.style.visibility = "hidden";
  heuresRomain.style.visibility = "visible";
  heures_matin_romainSVG.style.visibility = "hidden";
  etQuartSVG.style.visibility = "hidden";
  etDemieSVG.style.visibility = "hidden";
  moinsLeQuartSVG.style.visibility = "hidden";
  aiguille_heures_id.style.visibility = "visible";
  aiguille_minutes_id.style.visibility = "visible";
  aiguille_secondes_id.style.visibility = "visible";
  cacAiguillesManip_id.style.visibility = "hidden";
  btAffichageGlobalAiguilleManip_id.style.visibility = "hidden";
  boite_heures_id.style.visibility = "visible";
  boite_minutes_id.style.visibility = "visible";
  boite_secondes_id.style.visibility = "visible";
  cacBoites_id.style.visibility = "visible";
  cacAiguilles_id.style.visibility = "visible";
  cacAiguillesManip_id.style.visibility = "hidden";
  consoleIncrementation.style.visibility = "hidden";
  btHeureAleatoire_id.style.visibility = "hidden";
  zoneTypeExercice_id.style.visibility = "hidden";
  cacAiguilleManipHeures_id.checked = false;
  cacAiguilleManipMinutes_id.checked = false;
  cacAiguilleManipSecondes_id.checked = false;
  casesAffichage_id.style.display = "none";
  affichagesParticuliers.style.visibility="hidden";
  cacHeuresRomain.checked = false;
  cacMinutesMoins.checked = false;
  reinitialiserAffichageHorloge();
}
initialiserAffichage();

//////////////////////////////////////////////////////////////////////////////////////////////
//                                       AFFICHAGE HORLOGE                                  //
//////////////////////////////////////////////////////////////////////////////////////////////

// Affichage des différentes parties sauf les minutes et des heures avec 2 affichages possibles

function basculeAffichageSVG(id) {
  let imgSVG = document.getElementById(id);
  if (imgSVG.style.visibility == "visible") {
    imgSVG.style.visibility = "hidden";
  }
  else {
    imgSVG.style.visibility = "visible";
  }
}
// Affichage des minutes
function basculeAffichageMinutes() {
  let partieMinutesMoins = document.getElementById("minutesMoins");
  let cacMinutesMoinsCoche = document.getElementById("cacMinutesMoins");
  if (minutesSVG.style.visibility == "visible" || minutesMoinsSVG.style.visibility == "visible") {
    minutesSVG.style.visibility = "hidden";
    minutesMoinsSVG.style.visibility = "hidden";
    partieMinutesMoins.style.display = "none";
    cacMinutesMoinsCoche.checked = false;
  }
  else if (minutesSVG.style.visibility == "hidden") {
    minutesSVG.style.visibility = "visible";
    minutesMoinsSVG.style.visibility = "hidden";
    partieMinutesMoins.style.display = "inline";
    minutesMoins.style.visibility = "visible";
  }
}
function basculeAffichageMinutesMoins() {
  if (minutesSVG.style.visibility == "visible") {
    minutesSVG.style.visibility = "hidden";
    minutesMoinsSVG.style.visibility = "visible";
    minutesMoins.style.visibility = "visible";
  }
  else {
    minutesSVG.style.visibility = "visible";
    minutesMoinsSVG.style.visibility = "hidden";
  }
}

// Affichage des heures
function basculeAffichageHeures() {
  let partieHeuresRomain = document.getElementById("heuresRomain");
  let cacHeuresRomainCoche = document.getElementById("cacHeuresRomain");
  if (heures_matinSVG.style.visibility == "visible" || heures_matin_romainSVG.style.visibility == "visible") {
    heures_matinSVG.style.visibility = "hidden";
    heures_matin_romainSVG.style.visibility = "hidden";
    partieHeuresRomain.style.display = "none";
    cacHeuresRomainCoche.checked = false;
  }
  else if (heures_matinSVG.style.visibility == "hidden") {
    heures_matinSVG.style.visibility = "visible";
    heures_matin_romainSVG.style.visibility = "hidden";
    partieHeuresRomain.style.display = "block";
    heuresRomain.style.visibility = "visible";
  }
}
function basculeAffichageHeuresRomain() {
  if (heures_matinSVG.style.visibility == "visible") {
    heures_matinSVG.style.visibility = "hidden";
    heures_matin_romainSVG.style.visibility = "visible";
    heuresRomain.style.visibility = "visible";
  }
  else {
    heures_matinSVG.style.visibility = "visible";
    heures_matin_romainSVG.style.visibility = "hidden";
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////

function remplaceTexteBouton(boutonId, texte) {
  var bouton = document.getElementById(boutonId);
  if (bouton.innerText) {
    bouton.innerText = texte;
  }
  else {//if (button.innerHTML)
    bouton.innerHTML = texte;
  }
}

function afficherHorlogeInterne() {
  let color1 = 'blue';
  let color2 = "rgba(220, 0, 0, 1)"; //rouge
  btHeureAleatoire_id.style.visibility = "hidden";
  zoneTypeExercice_id.style.visibility = "hidden";
  consoleIncrementation.style.visibility = "hidden";
  formReponse_id.style.visibility = "hidden";
  btVerifierAiguilles_id.style.visibility = "hidden";
  btAffichageGlobalAiguilleManip_id.style.visibility = "hidden";
  cacAiguillesManip_id.style.visibility = "hidden";
  cacAiguilleManipHeures_id.checked = false;
  cacAiguilleManipMinutes_id.checked = false;
  cacAiguilleManipSecondes_id.checked = false;
  reponse_boite_secondes_id.style.visibility = false;
  btSecondesMoins_id.style.visibility = "hidden";
  btSecondesPlus_id.style.visibility = "hidden";
  pas_secondes_id.style.visibility = "hidden";
  aiguille_secondes_id.style.visibility = "visible";
  cacAiguilleSecondes.checked = true;
  cacBoiteSecondes.checked = true;
  boite_secondes_id.style.visibility = "visible";
  reponse_boite_secondes_id.style.visibility = "hidden";

  let bouton = document.getElementById("btAffichageGlobalHeureDigit_id");
  if (bouton.innerText == "Afficher l'heure digitale") {
    consoleHeureDigitale_id.style.visibility = "visible";
    cacBoites_id.style.visibility = "visible";
    horlogeDigitale_id.style.visibility = "visible";
    if (cacBoiteHeures.checked == true) {
      boite_heures_id.style.visibility = "visible";
    };
    if (cacBoiteMinutes.checked == true) {
      boite_minutes_id.style.visibility = "visible";
    };
    if (cacBoiteSecondes.checked == true) {
      boite_secondes_id.style.visibility = "visible";
    };
    remplaceTexteBouton("btAffichageGlobalHeureDigit_id", "Masquer l'heure digitale");
    bouton.style.backgroundColor = color2;
  }
  bouton = document.getElementById("btAffichageGlobalAiguille_id");
  if (bouton.innerText == "Afficher les aiguilles") {
    cacAiguilles_id.style.visibility = "visible";
    if (cacAiguilleHeures.checked == true) {
      aiguille_heures_id.style.visibility = "visible";
    };
    if (cacAiguilleMinutes.checked == true) {
      aiguille_minutes_id.style.visibility = "visible";
    };
    if (cacAiguilleSecondes.checked == true) {
      aiguille_secondes_id.style.visibility = "visible";
    };
    remplaceTexteBouton("btAffichageGlobalAiguille_id", "Masquer les aiguilles");
    bouton.style.backgroundColor = color2;
  }
  remplaceTexteBouton("boutonChangeMode", "Afficher horloge INTERACTIVE");
  boutonChangeMode.style.backgroundColor = color2;
}


function afficherConsoleInteractive() {
  let color1 = 'blue';
  let color2 = "rgba(220, 0, 0, 1)"; //rouge
  cacAiguillesManip_id.style.visibility = "visible";
  cacAiguilleManipHeures_id.checked = true
  cacAiguilleManipMinutes_id.checked = true
  cacAiguilleManipSecondes_id.checked = true
  consoleIncrementation.style.visibility = "visible";
  btHeureAleatoire_id.style.visibility = "visible";
  btAffichageGlobalAiguilleManip_id.style.visibility = "visible";
  zoneTypeExercice_id.style.visibility = "visible";
  formReponse_id.style.visibility = "hidden";
  btVerifierAiguilles_id.style.visibility = "hidden";
  cacAiguillesLiées_id.checked = true;
  cacAiguilleManipHeures_id.checked = true;
  cacAiguilleManipMinutes_id.checked = true;
  cacAiguilleManipSecondes_id.checked = true;
  reponse_boite_secondes_id.style.visibility = "hidden";

  let bouton = document.getElementById("btAffichageGlobalHeureDigit_id");
  if (bouton.innerText == "Afficher l'heure digitale") {
    consoleHeureDigitale_id.style.visibility = "visible";
    cacBoites_id.style.visibility = "visible";
    horlogeDigitale_id.style.visibility = "visible";
    if (cacBoiteHeures.checked == true) {
      boite_heures_id.style.visibility = "visible";
    };
    if (cacBoiteMinutes.checked == true) {
      boite_minutes_id.style.visibility = "visible";
    };
    if (cacBoiteSecondes.checked == true) {
      boite_secondes_id.style.visibility = "visible";
    };
    remplaceTexteBouton("btAffichageGlobalHeureDigit_id", "Masquer l'heure digitale");
    bouton.style.backgroundColor = color2;
  }
  bouton = document.getElementById("btAffichageGlobalAiguille_id");
  if (bouton.innerText == "Afficher les aiguilles") {
    cacAiguilles_id.style.visibility = "visible";
    if (cacAiguilleHeures.checked == true) {
      aiguille_heures_id.style.visibility = "visible";
    };
    if (cacAiguilleMinutes.checked == true) {
      aiguille_minutes_id.style.visibility = "visible";
    };
    if (cacAiguilleSecondes.checked == true) {
      aiguille_secondes_id.style.visibility = "visible";
    };
    remplaceTexteBouton("btAffichageGlobalAiguille_id", "Masquer les aiguilles");
    bouton.style.backgroundColor = color2;
  }
    bouton = document.getElementById("btAffichageGlobalAiguilleManip_id");
    if (bouton.innerText == "Afficher les aiguilles manipulables") {
      remplaceTexteBouton("btAffichageGlobalAiguilleManip_id", "Masquer les aiguilles manipulables");
      bouton.style.backgroundColor = color2;
    }
  remplaceTexteBouton("boutonChangeMode", "Afficher horloge INTERNE");
  boutonChangeMode.style.backgroundColor = color1;
}

function changerTypeHorloge() {
  let boutonChange = document.getElementById("boutonChangeMode");
  let boutonAiguilleManip = document.getElementById("btAffichageGlobalAiguilleManip_id");
  var color1 = 'blue';
  var color2 = "rgba(220, 0, 0, 1)"; //rouge

  if (boutonChange.innerText == "Afficher horloge INTERACTIVE") {
    clearInterval(set_aiguille_horloge_int);
    clearInterval(set_horloge_int);
    afficherConsoleInteractive();
    deplacementAiguillesHeureInteractive();
    // remplaceTexteBouton("boutonChangeMode", "Afficher horloge INTERNE");
    // boutonChange.style.backgroundColor = color1;
  }
  else if (boutonChange.innerText == "Afficher horloge INTERNE") {
    afficherHorlogeInterne();
    set_aiguille_horloge_int = setInterval(calculPositionAiguillesHeureInterne, 1000);
    defilementHeureCourante();
    remplaceTexteBouton("boutonChangeMode", "Afficher horloge INTERACTIVE");
    boutonChange.style.backgroundColor = color2;
    remplaceTexteBouton("btAffichageGlobalAiguilleManip_id", "Masquer les aiguilles manipulables");
    boutonAiguilleManip.style.backgroundColor = color2;
  }
}

function actualiser(id) {
  deplacementAiguillesHeureInteractive();
  formatageInput2Chiffres(id);
}

function formatageInput(id) {
  var input = document.getElementById(id);
  var value = input.value;
  // Remove any non-digit characters
  value = value.replace(/\D/g, "");
  // Ensure the value is limited to two characters
  if (value.length > 2) {
    value = value.substr(0, 2);
  }
  input.value = value;
}
function formatageInput2Chiffres(id) {
  var input = document.getElementById(id);
  var value = input.value;
  // Remove any non-digit characters
  value = value.replace(/\D/g, "");
  // Ensure the value is limited to two characters
  if (value.length > 2) {
    value = value.substr(0, 2);
  }
  // Force a zero display for one-number input
  if (value.length === 1) {
    value = "0" + value;
  }
  input.value = value;
}


function basculeAffichage(id) {
  let partie = document.getElementById(id);
  if (partie.style.visibility == "visible") {
    partie.style.visibility = "hidden";
  }
  else if (partie.style.visibility == "hidden") {
    partie.style.visibility = "visible";
  }
}

function changerAffichageGlobalHeureDigitale(id) {
  var bouton = document.getElementById(id);
  var color1 = "rgb(29, 157, 37)"; // vert
  var color2 = "rgba(220, 0, 0, 1)"; // rouge

  if (bouton.innerText == "Masquer l'heure digitale") {
    boite_heures_id.style.visibility = "hidden";
    boite_minutes_id.style.visibility = "hidden";
    boite_secondes_id.style.visibility = "hidden";
    consoleHeureDigitale_id.style.visibility = "hidden";
    cacBoites_id.style.visibility = "hidden";
    horlogeDigitale_id.style.visibility = "hidden";
    remplaceTexteBouton(id, "Afficher l'heure digitale");
    bouton.style.backgroundColor = color1;
  }
  else if (bouton.innerText == "Afficher l'heure digitale") {
    consoleHeureDigitale_id.style.visibility = "visible";
    cacBoites_id.style.visibility = "visible";
    horlogeDigitale_id.style.visibility = "visible";
    if (cacBoiteHeures.checked == true) {
      boite_heures_id.style.visibility = "visible";
    };
    if (cacBoiteMinutes.checked == true) {
      boite_minutes_id.style.visibility = "visible";
    };
    if (cacBoiteSecondes.checked == true) {
      boite_secondes_id.style.visibility = "visible";
    };
    remplaceTexteBouton(id, "Masquer l'heure digitale");
    bouton.style.backgroundColor = color2;
  }
}

function changerAffichageGlobalAiguilles(id) {
  var bouton = document.getElementById(id);
  // console.log(bouton);
  var color1 = "rgb(29, 157, 37)"; // vert
  var color2 = "rgba(220, 0, 0, 1)"; // rouge

  if (bouton.innerText == "Masquer les aiguilles") {
    aiguille_heures_id.style.visibility = "hidden";
    aiguille_minutes_id.style.visibility = "hidden";
    aiguille_secondes_id.style.visibility = "hidden";
    cacAiguilles_id.style.visibility = "hidden";

    remplaceTexteBouton(id, "Afficher les aiguilles");
    bouton.style.backgroundColor = color1;
  }
  else if (bouton.innerText == "Afficher les aiguilles") {
    cacAiguilles_id.style.visibility = "visible";
    if (cacAiguilleHeures.checked == true) {
      aiguille_heures_id.style.visibility = "visible";
    };
    if (cacAiguilleMinutes.checked == true) {
      aiguille_minutes_id.style.visibility = "visible";
    };
    if (cacAiguilleSecondes.checked == true) {
      aiguille_secondes_id.style.visibility = "visible";
    };
    remplaceTexteBouton(id, "Masquer les aiguilles");
    bouton.style.backgroundColor = color2;
  }
}

function changerAffichageGlobalAiguillesManip(id) {
  var bouton = document.getElementById(id);
  console.log(bouton);
  var color1 = "rgb(29, 157, 37)"; // vert
  var color2 = "rgba(220, 0, 0, 1)"; // rouge

  if (bouton.innerText == "Masquer les aiguilles manipulables") {
    cacAiguillesManip_id.style.visibility = "hidden";
    cacAiguilleManipHeures_id.checked = false;
    cacAiguilleManipMinutes_id.checked = false;
    cacAiguilleManipSecondes_id.checked = false;

    remplaceTexteBouton(id, "Afficher les aiguilles manipulables");
    bouton.style.backgroundColor = color1;
  }
  else if (bouton.innerText == "Afficher les aiguilles manipulables") {
    cacAiguillesManip_id.style.visibility = "visible";
    cacAiguilleManipHeures_id.checked = true;
    cacAiguilleManipMinutes_id.checked = true;
    cacAiguilleManipSecondes_id.checked = true;
    remplaceTexteBouton(id, "Masquer les aiguilles manipulables");
    bouton.style.backgroundColor = color2;
  }
}

function reinitialiserAffichageHorloge() {
  cacCercle.checked = true;
  cacGraduations.checked = true;
  cacHeuresMatin.checked = false;
  cacHeuresRomain.checked = false;
  heuresRomain.style.visibility = "hidden";
  cacHeuresAprem.checked = false;
  cacMinutes.checked = false;
  cacMinutesMoins.checked = false;
  minutesMoins.style.visibility = "hidden";
  cacEtQuart.checked = false;
  cacEtDemie.checked = false;
  cacMoinsLeQuart.checked = false;
  cercleSVG.style.visibility = "visible";
  graduationsSVG.style.visibility = "visible";
  minutesSVG.style.visibility = "hidden";
  minutesMoinsSVG.style.visibility = "hidden";
  heures_apremSVG.style.visibility = "hidden";
  heures_matinSVG.style.visibility = "hidden";
  heures_matin_romainSVG.style.visibility = "hidden";
  etQuartSVG.style.visibility = "hidden";
  etDemieSVG.style.visibility = "hidden";
  moinsLeQuartSVG.style.visibility = "hidden";
}

function incrementationHoraire(id) {
  var declencheur = document.getElementById(id);
  var incrementHeures = parseInt(document.getElementById("pas_heures_id").value);
  var incrementMinutes = parseInt(document.getElementById("pas_minutes_id").value);
  var incrementSecondes = parseInt(document.getElementById("pas_secondes_id").value);
  var boiteHeuresAffichees = document.getElementById("boite_heures_id");
  var heuresAffichees = parseInt(boiteHeuresAffichees.value);
  var boiteMinutesAffichees = document.getElementById("boite_minutes_id");
  var minutesAffichees = parseInt(boiteMinutesAffichees.value);
  var boiteSecondesAffichees = document.getElementById("boite_secondes_id");
  var secondesAffichees = parseInt(boiteSecondesAffichees.value);
  var nouvellesHeures, nouvellesMinutes, nouvellesSecondes;
  if (id == "btHeuresPlus_id") {
    nouvellesHeures = heuresAffichees + incrementHeures;
    if (nouvellesHeures > 23) {
      nouvellesHeures = nouvellesHeures - 24;
    }
    boiteHeuresAffichees.value = nouvellesHeures;
  }
  if (id == "btHeuresMoins_id") {
    nouvellesHeures = heuresAffichees - incrementHeures;
    if (nouvellesHeures < 0) {
      nouvellesHeures = nouvellesHeures + 24;
    }
    boiteHeuresAffichees.value = nouvellesHeures;
  }
  if (id == "btMinutesPlus_id") {
    nouvellesMinutes = minutesAffichees + incrementMinutes;
    if (nouvellesMinutes > 59) {
      nouvellesMinutes = nouvellesMinutes - 60;
      nouvellesHeures = heuresAffichees + 1;
      if (nouvellesHeures > 23) {
        nouvellesHeures = nouvellesHeures - 24;
      }
      boiteHeuresAffichees.value = nouvellesHeures;
    }
    boiteMinutesAffichees.value = nouvellesMinutes;
  }
  if (id == "btMinutesMoins_id") {
    nouvellesMinutes = minutesAffichees - incrementMinutes;
    if (nouvellesMinutes < 0) {
      nouvellesMinutes = nouvellesMinutes + 60;
      nouvellesHeures = heuresAffichees - 1;
      if (nouvellesHeures < 0) {
        nouvellesHeures = nouvellesHeures + 24;
      }
      boiteHeuresAffichees.value = nouvellesHeures;
    }
    boiteMinutesAffichees.value = nouvellesMinutes;
  }
  if (id == "btSecondesPlus_id") {
    nouvellesSecondes = secondesAffichees + incrementSecondes;
    if (nouvellesSecondes > 59) {
      nouvellesSecondes = nouvellesSecondes - 60;
      nouvellesMinutes = minutesAffichees + 1;
      if (nouvellesMinutes > 59) {
        nouvellesMinutes = nouvellesMinutes - 60;
        nouvellesHeures = heuresAffichees + 1;
        if (nouvellesHeures > 23) {
          nouvellesHeures = nouvellesHeures - 24;
        }
        boiteHeuresAffichees.value = nouvellesHeures;
      }
      boiteMinutesAffichees.value = nouvellesMinutes;
    }
    boiteSecondesAffichees.value = nouvellesSecondes;
  }
  if (id == "btSecondesMoins_id") {
    nouvellesSecondes = secondesAffichees - incrementSecondes;
    if (nouvellesSecondes < 0) {
      nouvellesSecondes = nouvellesSecondes + 60;
      nouvellesMinutes = minutesAffichees - 1;
      if (nouvellesMinutes < 0) {
        nouvellesMinutes = nouvellesMinutes + 60;
        nouvellesHeures = heuresAffichees - 1;
        if (nouvellesHeures < 0) {
          nouvellesHeures = nouvellesHeures + 24;
        }
        boiteHeuresAffichees.value = nouvellesHeures;
      }
      boiteMinutesAffichees.value = nouvellesMinutes;
    }
    boiteSecondesAffichees.value = nouvellesSecondes;
  }
  deplacementAiguillesHeureInteractive();
  formatageInput2Chiffres("boite_heures_id");
  formatageInput2Chiffres("boite_minutes_id");
  formatageInput2Chiffres("boite_secondes_id");
}

function lancerHeureAleatoire(id) {
  // Générer un tirage aléatoire entre 0 et 24 ou 0 et 60
  var tirageHeures = Math.floor(Math.random() * 24);
  var tirageMinutes = Math.floor(Math.random() * 60);
  var tirageSecondes = Math.floor(Math.random() * 60);
  let hrBoite = document.getElementById("boite_heures_id");
  hrBoite.value =tirageHeures;
  formatageInput2Chiffres("boite_heures_id");
  let minBoite = document.getElementById("boite_minutes_id");
  minBoite.value =tirageMinutes;
  formatageInput2Chiffres("boite_minutes_id");
  let secBoite = document.getElementById("boite_secondes_id");
  if (cacExoAvecSecondes_id.checked == true){
    secBoite.value =tirageSecondes;
  }
  else {boite_secondes_id.value = 0;}
  formatageInput2Chiffres("boite_secondes_id");
  deplacementAiguillesHeureInteractive();
}

// Exercice Type 1 : placer les aiguilles selon l'heure digitale
function affichageExType1(){
  afficherConsoleInteractive() // Affiche tous les élèments de la console interactive
  let boutonAffichageAiguilles = document.getElementById("btAffichageGlobalAiguille_id");
  if (boutonAffichageAiguilles.innerText == "Masquer les aiguilles") {
    btAffichageGlobalAiguilleManip_id.style.visibility = "visible";
    boite_heures_id.style.visibility = "visible";
    boite_minutes_id.style.visibility = "visible";
    cacBoites_id.style.visibility = "visible";
    cacAiguilles_id.style.visibility = "hidden";
    cacAiguillesManip_id.style.visibility = "visible";
    consoleIncrementation.style.visibility = "visible";
    btHeureAleatoire_id.style.visibility = "visible";
    changerAffichageGlobalAiguilles("btAffichageGlobalAiguille_id");
    btVerifierAiguilles_id.style.visibility = "visible";
    reponse_boite_secondes_id.style.visibility = "hidden";
  }
  if (cacExoAvecSecondes_id.checked == true){
    boite_secondes_id.style.visibility = "visible";
    cacAiguilleManipSecondes_id.checked = true;
  }
  else {
    boite_secondes_id.style.visibility = "hidden";
    cacAiguilleManipSecondes_id.checked = false;
  }
}
// Exercice Type 2 : retrouver l'heure digitale à partir des aiguilles
function affichageExType2(){
  afficherConsoleInteractive() // Affiche tous les élèments de la console interactive
  let boutonAffichageAiguilles = document.getElementById("btAffichageGlobalHeureDigit_id");
  if (boutonAffichageAiguilles.innerText == "Masquer l'heure digitale") {
    boite_heures_id.style.visibility = "hidden";
    boite_minutes_id.style.visibility = "hidden";
    boite_secondes_id.style.visibility = "hidden";
    cacBoites_id.style.visibility = "hidden";
    cacAiguilles_id.style.visibility = "visible";
    consoleIncrementation.style.visibility = "visible";
    btHeureAleatoire_id.style.visibility = "visible";
    changerAffichageGlobalHeureDigitale("btAffichageGlobalHeureDigit_id");
  }
  // Masquer la partie concernant les aiguilles manipulables pour pouvoir saisir une heure
  btAffichageGlobalAiguilleManip_id.style.visibility = "hidden";
  cacAiguillesManip_id.style.visibility = "hidden";
  cacAiguilleManipHeures_id.checked = false;
  cacAiguilleManipMinutes_id.checked = false;
  cacAiguilleManipSecondes_id.checked = false;
  // Affichage du formulaire pour répondre
  formReponse_id.style.visibility = "visible";
  // Affichage de la boite réponse pour les secondes
  if (cacExoAvecSecondes_id.checked == true){
    reponse_boite_secondes_id.style.visibility = "visible";
    boite_secondes_id.style.visibility = "hidden";
  }
  else {
    reponse_boite_secondes_id.style.visibility = "hidden";
    boite_secondes_id.style.visibility = "hidden";
  }
}

function AfficherToutHorlogeInteractive(){
  afficherConsoleInteractive();
  affichageDesSecondes();
}

function actualiserReponse(id) {
  formatageInput2Chiffres(id);
}

/////////////////////////////////////////////////////////////////////////////////
function verifierReponse(){
  indicationErreurHeures_id.style.visibility="hidden";
  indicationErreurMinutes_id.style.visibility="hidden";
  indicationErreurSecondes_id.style.visibility="hidden";

  let heureProposee = Number(reponse_boite_heures_id.value);
  if (heureProposee >= 12) {
    heureProposee=heureProposee-12;
  }
  let minuteProposee = Number(reponse_boite_minutes_id.value);
  let secondeProposee = Number(reponse_boite_secondes_id.value);
  let heureHorloge = Number(boite_heures_id.value);
  if (heureHorloge >= 12) {
    heureHorloge=heureHorloge-12;
  }
  console.log('Hr horloge:',heureHorloge, 'Hr proposée:',heureProposee);

  let minuteHorloge = Number(boite_minutes_id.value);
  let secondeHorloge = Number(boite_secondes_id.value);

  let hrOK=0;
  let minOK=0;
  let secOK=0;
  if (heureProposee===heureHorloge){
    hrOK=1;
    } 
  else {
    indicationErreurHeures_id.style.visibility="visible";
  };
  if (minuteProposee===minuteHorloge) {
    minOK=1;
    }
  else {
    indicationErreurMinutes_id.style.visibility="visible";
  };
  if (secondeProposee===secondeHorloge){
    secOK=1;
    }
  else {
    indicationErreurSecondes_id.style.visibility="visible";
  };
  if (hrOK*minOK*secOK===1){
    afficherReponse(1);
    }
  else {
    afficherReponse(0);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
function verifierAiguilles(){
  indicationErreurHeures_id.style.visibility="hidden";
  indicationErreurMinutes_id.style.visibility="hidden";
  indicationErreurSecondes_id.style.visibility="hidden";
  
  let angleAiguilleManipHeures=hourAngle*180/Math.PI;
  let angleAiguilleManipMinutes=minuteAngle*180/Math.PI;
  let angleAiguilleManipSecondes=secondAngle*180/Math.PI;
  let angleAiguilleHeures=recupererAngleRotation("aiguille_heures_id");
  let angleAiguilleMinutes=recupererAngleRotation("aiguille_minutes_id");
  let angleAiguilleSecondes=recupererAngleRotation("aiguille_secondes_id");
  if (angleAiguilleManipHeures < 0){
    angleAiguilleManipHeures = angleAiguilleManipHeures+360;
  }
  if (angleAiguilleHeures < 0){
    angleAiguilleHeures = angleAiguilleHeures+360;
  }
  if (angleAiguilleHeures >= 360){
    angleAiguilleHeures = angleAiguilleHeures-360;
  }
  if (angleAiguilleManipMinutes < 0){
    angleAiguilleManipMinutes = angleAiguilleManipMinutes+360;
  }
  if (angleAiguilleMinutes < 0){
    angleAiguilleMinutes = angleAiguilleMinutes+360;
  }
  if (angleAiguilleMinutes >= 360){
    angleAiguilleMinutes = angleAiguilleMinutes-360;
  }
  if (angleAiguilleManipSecondes < 0){
    angleAiguilleManipSecondes = angleAiguilleManipSecondes+360;
  }
  if (angleAiguilleSecondes < 0){
    angleAiguilleSecondes = angleAiguilleSecondes+360;
  }
  if (angleAiguilleSecondes >= 360){
    angleAiguilleSecondes = angleAiguilleSecondes-360;
  }
  let tolerance = 2.5; // +-2.5 deg soit 5 deg
  let angleAiguilleHeuresSup = angleAiguilleHeures+2*tolerance;
  let angleAiguilleHeuresInf = angleAiguilleHeures-2*tolerance;
  if (angleAiguilleHeuresInf >= angleAiguilleHeuresSup){ // Cas autour de 0 (aiguille vert le haut)
    angleAiguilleHeuresSup = angleAiguilleHeuresSup + 360;
  };
  let angleAiguilleMinutesSup = angleAiguilleMinutes+tolerance;
  let angleAiguilleMinutesInf = angleAiguilleMinutes-tolerance;
  if (angleAiguilleMinutesInf >= angleAiguilleMinutesSup){ // Cas autour de 0 (aiguille vert le haut)
    angleAiguilleMinutesSup = angleAiguilleMinutesSup + 360;
  };
  let angleAiguilleSecondesSup = angleAiguilleSecondes+tolerance;
  let angleAiguilleSecondesInf = angleAiguilleSecondes-tolerance;
  if (angleAiguilleSecondesInf >= angleAiguilleSecondesSup){ // Cas autour de 0 (aiguille vert le haut)
    angleAiguilleSecondesSup = angleAiguilleSecondesSup + 360;
  };
  console.log('Heures Manip:',angleAiguilleManipHeures,'Minutes Manip:',angleAiguilleManipMinutes,'Secondes Manip:',angleAiguilleManipSecondes);
  console.log('Heures:',angleAiguilleHeures,'Minutes:',angleAiguilleMinutes,'Secondes:',angleAiguilleSecondes);
  console.log(angleAiguilleHeuresInf,angleAiguilleHeuresSup);
  let hrOK = 0;
  let minOK = 0;
  let secOK = 0;
  if ((angleAiguilleManipHeures >= angleAiguilleHeuresInf) && (angleAiguilleManipHeures <= angleAiguilleHeuresSup)){
    console.log('Heures bonnes');
    hrOK=1;
    }
  else {
    indicationErreurHeures_id.style.visibility="visible";
  }
  if ((angleAiguilleManipMinutes >= angleAiguilleMinutesInf) && (angleAiguilleManipMinutes <= angleAiguilleMinutesSup)){
    console.log('Minutes bonnes');
    minOK=1;
    }
  else {
    indicationErreurMinutes_id.style.visibility="visible";
  }
  if ((angleAiguilleManipSecondes >= angleAiguilleSecondesInf) && (angleAiguilleManipSecondes <= angleAiguilleSecondesSup)){
    console.log('Secondes bonnes');
    secOK=1;
    }
  else {
    indicationErreurSecondes_id.style.visibility="visible";
  }
  if (hrOK*minOK*secOK===1){
    afficherReponse(1);
    console.log('Bonne réponse');
  }
  else {
    afficherReponse(0);
  };
}

function recupererAngleRotation(id){
  var element = document.getElementById(id);
  // Récupérez la valeur de la propriété 'transform' des styles en ligne
  var transformValue = element.style.transform;
  // Analysez la valeur de 'transform' pour extraire l'angle de rotation
  if (transformValue && transformValue.includes('rotate')) {
    // Trouver la partie de la chaîne de caractères qui contient l'angle de rotation
    var match = transformValue.match(/rotate\(([^)]+)/);
    if (match && match[1]) {
      // Récupérer l'angle de rotation
      var rotationValue = match[1].trim();
       var angle = parseFloat(rotationValue);
      return angle;
    }
  }
  return 0; // Retourne 0 si aucun angle de rotation n'est trouvé
}

////////////////////////////////////////////////////////////////////////////////////////
function déplacementLiéAiguillesManip() {
  var element = document.getElementById('cacAiguillesLiées_id');
  if (element.checked == true) {
    console.log("coché:","OUI");
    réinitialisationAffichage(); // permet de replacer les aiguilles dans une position liée
    return true;
  }
  else {
    console.log("coché:","NON");
    return false;
  }
}

////////////////////////////////////////////////////////////////////////////////////////
// Permet d'afficher ou masquer les réglages pour afficher les indications sur l'horloge

function toggleRéglageIndications() {  
  if (casesAffichage_id.style.display === "block") {
    casesAffichage_id.style.display = "none";
  } else {
    casesAffichage_id.style.display = "block";
  }
}