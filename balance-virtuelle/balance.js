// tableaux des objets à peser
// Sont requis les champs suivants, dans l'ordre, pour chaque objet
// chemin et nom de l'image, masse en g, largeur de l'image, hauteur de l'image, auto ou,
// si c'est en perspective, indiquer les coordonnées du point de l'image devant se trouver
// au centre du plateau, relativement à son angle supérieur gauche
const Objets1_1 = [
  ["img/boite-mais.png",413,116,132,"auto"],
  ["img/cahier.png",96,239,137,"152,38"],
  ["img/bol-farine.png",817,200,153,"auto"],
  ["img/pommedeterre.png",152,99,66,"auto"],
  ["img/tomate.png",110,90,72,"auto"]
];
const Objets1_2 = [
  ["img/bouteille.png",628,56,200,"33,240"],
  ["img/livre-poche.png",259,186,133,"86,90"],
  ["img/oeuf.png",62,90,71,"auto"],
  ["img/ballon.png",420,239,240,"auto"],
  ["img/casserole.png",814,252,150,"auto"]
];
const Objets2_1 = [
  ["img/vieux-livre.png",1565,280,104,"135,60"],
  ["img/potimarron.png",3931,288,346,"auto"],
  ["img/gigot.png",2172,252,161,"118,108"],
  ["img/poulet.png",1906,252,161,"120,120"],
  ["img/colis1.png",3003,241,165,"120,140"]
];
const Objets2_2 = [
  ["img/chat.png",4945,300,274,"150,220"],
  ["img/filet-PdeT.png",3844,250,248,"125,200"],
  ["img/pain.png",1469,250,177,"125,128"],
  ["img/casque.png",1687,282,300,"141,260"],
  ["img/colis2.png",2238,165,242,"auto"]
];

const nbMasses = 15;

// Paramètres de la balance
const paramBalance = {
  haut0_1: 147,
  bas0_1: 181,
  haut0_2: 141,
  bas0_2: 217,
  diametre1: 214,
  diametre2: 260,
  baseRotation: 2,
  baseDecalageBalance: 6,
  zoneValide1_1: [69,147,283,181],
  zoneValide1_2: [406,147,619,181],
  zoneValide2_1: [40,141,300,217],
  zoneValide2_2: [385,141,645,217]
};

// Paramètres de suivi d'exercices
const paramExercices = {
  objetsPeses: [],
  difficulte: 0,
  ctObjets: 0,
  reponse: "",
  serieCourante: 0,
  seriesFaites: []
}

// Paramètres de pesée
const paramPesee = {
  positionX : 0,
  positionY: 0,
  pointeurX: 0,
  pointeurY: 0,
  bas0: 0,
  haut0: 0,
  chargeGauche: 0,
  chargeDroite: 0,
  zoneValide1: [],
  zoneValide2: [],
  oldPosition: 3
}

const reserveMasses = document.getElementById("reserve");
const balance = document.getElementById("balance");
const massesOrdonnees = document.getElementById("range-masses");
const plateauG = document.getElementById("plateau-gauche");
const plateauD = document.getElementById("plateau-droit");

function createTip(ev) {
  const title = ev.target.title;
  ev.target.title = '';
  ev.target.setAttribute("tooltip", title);
  const tooltipWrap = document.createElement("div"); //créé un bloc div
  tooltipWrap.className = 'tooltip'; //lui ajoute une classe
  tooltipWrap.appendChild(document.createTextNode(title)); //ajoute le noeud texte à la div nouvellement créée.
  const firstChild = document.body.firstChild;//renvoie le 1er élément après body
  firstChild.parentNode.insertBefore(tooltipWrap, firstChild); //ajoute le tooltip avant l'élément
  const padding = 5;
  const linkProps = ev.target.getBoundingClientRect();
  const tooltipProps = tooltipWrap.getBoundingClientRect();
  const topPos = linkProps.top - (tooltipProps.height + padding);
  tooltipWrap.setAttribute('style','top:'+topPos+'px;'+'left:'+linkProps.left+'px;')
}

function cancelTip(ev) {
  if ( ! document.querySelector(".tooltip")) {return;}
  const title = ev.target.getAttribute("tooltip");
  ev.target.title = title;
  ev.target.removeAttribute("tooltip");
  document.querySelector(".tooltip").remove();
}

function getIntValeur(strPixels) {
  const valeur = parseInt(strPixels.substr(0, strPixels.length - 2));
  return valeur;
}

function getPossibilites(mesure) {
  const reg = new RegExp("[.]","g");
  let liste = "-";
  let valeurKg = mesure / 1000;
  liste = liste + mesure + " g-" + mesure + "g-" + valeurKg.toFixed(3) + " kg-" + valeurKg.toFixed(3) + "kg-";
  liste = liste.replace(reg,",");
  return liste;
}

function chargeMassesBase() {
  const idMassesBase = ["masse1-500","masse1-200","masse1-100","masse2-100","masse1-50","masse1-20","masse1-10","masse2-10","masse1-5","masse1-2","masse2-2","masse1-1"];
  const boiteMassesBase = document.getElementById("boite-masses-base");
  const massesBase = boiteMassesBase.children;
  for (var i = 0; i < massesBase.length; i++) {
    let masse = massesBase[i].cloneNode(true);
    masse.id = idMassesBase[i];
    reserveMasses.appendChild(masse);
  }
}

function chargeMasses5kg () {
  const idMasses5kg = ["masse1-2000","masse1-1000","masse2-1000"];
  const boiteMasses5kg = document.getElementById("boite-masses-5kg");
  const masses5kg = boiteMasses5kg.children;
  for (var i = 0; i < masses5kg.length; i++) {
    let masse = masses5kg[i].cloneNode(true);
    masse.id = idMasses5kg[i];
    reserveMasses.appendChild(masse);
  }
}

function parametreBalance(Niveau) {
  if (Niveau === 1) {
    paramPesee.zoneValide1 = paramBalance.zoneValide1_1;
    paramPesee.zoneValide2 = paramBalance.zoneValide1_2;
    paramPesee.haut0 = paramBalance.haut0_1;
    paramPesee.bas0 = paramBalance.bas0_1;
    diametre = paramBalance.diametre1;
  }
  else {
    paramPesee.zoneValide1 = paramBalance.zoneValide2_1;
    paramPesee.zoneValide2 = paramBalance.zoneValide2_2;
    paramPesee.haut0 = paramBalance.haut0_2;
    paramPesee.bas0 = paramBalance.bas0_2;
    diametre = paramBalance.diametre2;
  }
}

function parametreObjets(NoSerie) {
  if (paramExercices.difficulte === "Niveau1") {
    if (NoSerie === 1) {paramExercices.objetsPeses = melange(Objets1_1);}
    else {paramExercices.objetsPeses = melange(Objets1_2);}
  }
  else {
    if (NoSerie === 3) {paramExercices.objetsPeses = melange(Objets2_1);}
    else {paramExercices.objetsPeses = melange(Objets2_2);}
  }
}

function placeObjet() {
  let posX;
  let posY;
  let nouvellePesee = new Image();
  nouvellePesee.src = paramExercices.objetsPeses[paramExercices.ctObjets][0];
  paramExercices.reponse = paramExercices.objetsPeses[paramExercices.ctObjets][1];
  nouvellePesee.id = "objet";
  nouvellePesee.className = "mobile";
  nouvellePesee.draggable = false;
  let NoPlateau = alea(1,2);
  let centreY = paramPesee.bas0 - parseInt((paramPesee.bas0 - paramPesee.haut0) / 2);
  if (NoPlateau === 1) {
    nouvellePesee.name = "plateau1";
    paramPesee.chargeGauche = paramPesee.chargeGauche + paramExercices.objetsPeses[paramExercices.ctObjets][1];
    var centreX = paramPesee.zoneValide1[2] - parseInt(diametre / 2);
  }
  else {
    nouvellePesee.name = "plateau2";
    paramPesee.chargeDroite = paramPesee.chargeDroite + paramExercices.objetsPeses[paramExercices.ctObjets][1];
    var centreX = paramPesee.zoneValide2[2] - parseInt(diametre / 2);
  }
  if (paramExercices.objetsPeses[paramExercices.ctObjets][4] === "auto") {
    posY = centreY + parseInt(paramExercices.objetsPeses[paramExercices.ctObjets][3] / 10) - paramExercices.objetsPeses[paramExercices.ctObjets][3];
    posX = centreX - parseInt(paramExercices.objetsPeses[paramExercices.ctObjets][2] / 2);
    }
    else {
      var tab = paramExercices.objetsPeses[paramExercices.ctObjets][4].split(",");
      posY = centreY - parseInt(tab[1]);
      posX = centreX - parseInt(tab[0]);
    }
  nouvellePesee.style.top = posY + "px";
  nouvellePesee.style.left = posX + "px";
  balance.appendChild(nouvellePesee);
  setTimeout(function () {equilibreBalance()},50);
}

function videReserve() {
  while (reserveMasses.firstChild) {
    reserveMasses.removeChild(reserveMasses.firstChild);
  }
}

function initialiseMasses() {
  chargeMassesBase();
  if (paramExercices.difficulte === "Niveau2") {chargeMasses5kg()}
}

function reinitialise() {
  const objetsBalance = balance.children;
  // Nettoie la balance
  for (let i = 0; i < objetsBalance.length; i++) {
    if ((objetsBalance[i].classList.contains("masse")) || (objetsBalance[i].id === "objet")) {
      balance.removeChild(objetsBalance[i]);
      i -= 1;
    }
  }
  paramPesee.chargeGauche = 0;
  paramPesee.chargeDroite = 0;
  equilibreBalance();
  paramPesee.oldPosition = 3;
  // Nettoie la zone des masses rangées en ordre décroissant
  while (massesOrdonnees.firstChild) {
    massesOrdonnees.removeChild(massesOrdonnees.firstChild);
  }
}

function activeBtSeries() {
  if (paramExercices.seriesFaites.indexOf(1) < 0) {
    document.getElementById("serie1").disabled = false;
  }
  if (paramExercices.seriesFaites.indexOf(2) < 0) {
    document.getElementById("serie2").disabled = false;
  }
  if (paramExercices.seriesFaites.indexOf(3) < 0) {
    document.getElementById("serie3").disabled = false;
  }
  if (paramExercices.seriesFaites.indexOf(4) < 0) {
    document.getElementById("serie4").disabled = false;
  }
}

function desactiveBtSeries() {
  document.getElementById("serie1").disabled = true;
  document.getElementById("serie2").disabled = true;
  document.getElementById("serie3").disabled = true;
  document.getElementById("serie4").disabled = true;
  document.getElementById("apprendre").disabled = true;
}

function defSerie(Numero) {
  reinitialise();
  videReserve();
  if ((Numero === 1) || (Numero === 2)) {
    paramExercices.difficulte = "Niveau1";
    parametreBalance(1);
    plateauG.src = "img/plateau-gauche.png";
    plateauD.src = "img/plateau-droit.png";
    }
  else {
    paramExercices.difficulte = "Niveau2";
    parametreBalance(2);
    plateauG.src = "img/plateau-gauche2.png";
    plateauD.src = "img/plateau-droit2.png";
  }
  document.getElementById("apprendre").disabled = false;
  document.getElementById("infos").innerHTML = "";
  parametreObjets(Numero);
  paramExercices.serieCourante = Numero;
  desactiveBtSeries();
  commencer();
}

function getListeMasses() {
  const objetsBalance = balance.children;
  const tabMasses = [[],[]];
  for (let i = 0; i < objetsBalance.length; i++) {
    if (objetsBalance[i].classList.contains("masse")) {
      tabMasses[0].push(objetsBalance[i]);
      tabMasses[1].push(parseInt(objetsBalance[i].id.substr(7)));
    }
  };
  return tabMasses;
}

function getListeMassesTriee(tableauMasses) {
  let indice;
  let valeur;
  const tabIndices = [];
  const tabOrdonne = [[],[]];
  while (tabIndices.length < tableauMasses[0].length) {
    valeur = 0;
    for (let i = 0; i < tableauMasses[1].length; i++) {
      // if ((tableauMasses[1][i] > valeur) && (! estPresent(i,tabIndices))) {
      if ((tableauMasses[1][i] > valeur) && (tabIndices.indexOf(i) < 0)) {
        valeur = tableauMasses[1][i];
        indice = i;
      }
    }
    tabIndices.push(indice);
    tabOrdonne[0].push(tableauMasses[0][indice]);
    tabOrdonne[1].push(tableauMasses[1][indice]);
  }
  return tabOrdonne;
}

function apprentissage(ev) {
  // ev.preventDefault();
  reinitialise();
  videReserve();
  paramExercices.difficulte = "Niveau2";
  initialiseMasses();
  parametreBalance(2);
  plateauG.src = "img/plateau-gauche2.png";
  plateauD.src = "img/plateau-droit2.png";
  document.getElementById("apprendre").disabled = true;
  document.getElementById("infos").innerHTML = "<p>Tu peux librement placer des masses sur les plateaux, les enlever, les déplacer d'un plateau à l'autre, pour observer ce qu'il se passe avec la balance.<p>Essaie par exemple d'équilibrer les plateaux en plaçant des masses différentes sur chacun d'eux.</p>";
}

function commencer() {
  initialiseMasses();
  placeObjet();
  document.getElementById("verifier").disabled = false;
  document.getElementById("ranger").disabled = true;
}

function rangeMasses() {
  let tabMasses;
  let listeMassesTriee;
  let unite = "";
  tabMasses = getListeMasses();
  listeMassesTriee = getListeMassesTriee(tabMasses);
  for (let i = 0; i < listeMassesTriee[0].length; i++) {
    let newId = document.createElement('div');
    newId.className = "case-masse";
    if (parseInt(listeMassesTriee[1][i]) >= 1000) {
      unite = "kg";
      listeMassesTriee[1][i] = parseInt(listeMassesTriee[1][i]) / 1000;
    }
    else {
      unite = "g";
    }
    let newValeur = document.createTextNode(listeMassesTriee[1][i] + " " + unite);
    newId.appendChild(newValeur);
    newId.appendChild(listeMassesTriee[0][i].cloneNode(true));
    massesOrdonnees.appendChild(newId);
  }
}

function verification(ev) {
  const Messages = document.getElementById("messages");
  ev.preventDefault();
  Messages.innerHTML = "";
  const formulaire = document.getElementById("resultat") ;
  let proposition = formulaire.elements["proposition"].value;
  if ((paramPesee.chargeGauche === 0) || (paramPesee.chargeDroite === 0)) {
    Messages.innerHTML = "<p><b>ERREUR :</b><br />Aucune masse n'a été placée sur la balance !</p>";
    return;
  }
  if (paramPesee.chargeGauche !== paramPesee.chargeDroite) {
    Messages.innerHTML = "<p><b>ERREUR :</b><br />Les plateaux de la balance ne sont pas équilibrés !</p>";
    return;
  }
  ReponsesPossibles = getPossibilites(paramExercices.objetsPeses[paramExercices.ctObjets][1]);
  proposition = "-" + proposition + "-";
  if (ReponsesPossibles.search(proposition) === -1) {
    Messages.innerHTML = "<p><b>ERREUR :</b><br />Les plateaux de la balance sont équilibrés mais la masse totale indiquée n'est pas correcte !</p>";
    return;
  }
  paramExercices.ctObjets += 1;
  proposition = "";
  const champSaisie = document.getElementById("proposition") ;
  champSaisie.value = "";
  reinitialise();
  videReserve();
  // initialiseMasses();
  document.getElementById("verifier").disabled = true;
  if (paramExercices.ctObjets < paramExercices.objetsPeses.length) {
    showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left');
    }
  else {
    if (paramExercices.difficulte === "Niveau1") {paramExercices.seriesFaites.push(paramExercices.serieCourante)}
    else {paramExercices.seriesFaites.push(paramExercices.serieCourante)}
    if ((paramExercices.seriesFaites.indexOf(1) >= 0) && (paramExercices.seriesFaites.indexOf(2) >= 0) && (paramExercices.seriesFaites.indexOf(3) >= 0) && (paramExercices.seriesFaites.indexOf(4) >= 0)) {
      showDialog("Félicitations ! <br/>Tu as réussi à mesurer correctement la masse de tous les objets de cette série.<p>Tu as de plus effectué avec succès toutes les mesures de toutes les séries. <br/>Tu es un vrai champion !</p>",0.5,'img/trophee.png', 128, 128, 'left');
      }
    else {
      showDialog("Félicitations ! <br/>Tu as réussi à mesurer correctement la masse de tous les objets de cette série.",0.5,'img/trophee.png', 128, 128, 'left');
    }
    activeBtSeries();
    paramExercices.ctObjets = 0;
    return;
  }
  commencer();
}

function equilibreBalance() {
  let newPosition;
  let duree;
  const enfants = balance.children;

  function reinitialiseDureeMvtMasses() {
    for (let i = 0; i < enfants.length; i++) {
      if (enfants[i].classList.contains("masse")) {
        enfants[i].style.transitionDuration = "0s";
      }
    }
  }

  let difference = paramPesee.chargeGauche - paramPesee.chargeDroite;
  if (difference === 0) {newPosition = 3;}
  else {
    if (difference > 0) {
      if (difference > 2) {newPosition = 1;}
      else {newPosition = 2;}
    }
    else {
      if (difference < -2 ) {newPosition = 5;}
      else {newPosition = 4;}
    }
  }
  if (newPosition === paramPesee.oldPosition) {return}

  // Détermine le degré de mouvement à effectuer
  let Rotation = (newPosition - 3) * paramBalance.baseRotation;
  let Translation = (newPosition - 3) * paramBalance.baseDecalageBalance;
  let TranslationMasse = (newPosition - paramPesee.oldPosition) * paramBalance.baseDecalageBalance;
  let ValeurTranslation = Math.abs(Translation);
  let ValeurTranslationMasse = Math.abs(TranslationMasse);
  let AttributRotation = "rotate(" + Rotation + "deg)";
  let AttributTranslationPositif = "translate(0px," + ValeurTranslation + "px)";
  let AttributTranslationNegatif = "translate(0px," + (- ValeurTranslation) + "px)";

  //Modifie les zones d'acceptation des masses sur les plateaux
  if (Translation > 0) {
    paramPesee.zoneValide1[1] = paramPesee.haut0 - ValeurTranslation;
    paramPesee.zoneValide1[3] = paramPesee.bas0 - ValeurTranslation;
    paramPesee.zoneValide2[1] = paramPesee.haut0 + ValeurTranslation;
    paramPesee.zoneValide2[3] = paramPesee.bas0 + ValeurTranslation;
  }
  else {
    paramPesee.zoneValide1[1] = paramPesee.haut0 + ValeurTranslation;
    paramPesee.zoneValide1[3] = paramPesee.bas0 + ValeurTranslation;
    paramPesee.zoneValide2[1] = paramPesee.haut0 - ValeurTranslation;
    paramPesee.zoneValide2[3] = paramPesee.bas0 - ValeurTranslation;
  }

  // Fixe la durée de l'effet de translation
  if (Math.abs(paramPesee.chargeGauche - paramPesee.chargeDroite) > 10) {duree = 0.3;}
  else {
    if (Math.abs(paramPesee.chargeGauche - paramPesee.chargeDroite) > 5) {duree = 0.5;}
    else {duree = 1;}
  }
  let attributDuree = duree + "s";

  // Examine les objets du réceptacle pour leur appliquer le mouvement adéquat
  for (let i = 0; i < enfants.length; i++) {
    if (enfants[i].className === "rotation") {
      enfants[i].style.transitionDuration = attributDuree;
      enfants[i].style.transform = AttributRotation;
    }
    if (enfants[i].className === "mobile") {
      enfants[i].style.transitionDuration = attributDuree;
      if ((enfants[i].id === "axe-gauche") || (enfants[i].id === "plateau-gauche") || (enfants[i].name === "plateau1")) {
        if (Translation > 0) {enfants[i].style.transform = AttributTranslationNegatif;}
        else {enfants[i].style.transform = AttributTranslationPositif;}
      }
      if ((enfants[i].id === "axe-droit") || (enfants[i].id === "plateau-droit") || (enfants[i].name === "plateau2")) {
        if (Translation > 0) {enfants[i].style.transform = AttributTranslationPositif;}
        else {enfants[i].style.transform = AttributTranslationNegatif;}        }
    }
    if (enfants[i].classList.contains("masse")) {
      enfants[i].style.transitionDuration = attributDuree;
      let ValeurAttribPositif = getIntValeur(enfants[i].style.top) + ValeurTranslationMasse;
      let ValeurAttribNegatif = getIntValeur(enfants[i].style.top) - ValeurTranslationMasse;
      let AttribPositif = ValeurAttribPositif + "px";
      let AttribNegatif = ValeurAttribNegatif + "px";
      if (enfants[i].name === "plateau1") {
        if (TranslationMasse > 0) {enfants[i].style.top = AttribNegatif;}
        else {enfants[i].style.top = AttribPositif;}
      }
      if (enfants[i].name === "plateau2") {
        if (TranslationMasse > 0) {enfants[i].style.top = AttribPositif;}
        else {enfants[i].style.top = AttribNegatif;}
      }
    }
  }
  // Remet à 0 la durée de l'effet de transition pour les masses
  setTimeout(function () {reinitialiseDureeMvtMasses()},50);
  paramPesee.oldPosition = newPosition;
  if (paramPesee.chargeGauche === paramPesee.chargeDroite) {
    document.getElementById("ranger").disabled = false;
  }
}

function drag(ev) {
    let infosObjet;
    paramPesee.pointeurX = ev.offsetX;
    paramPesee.pointeurY = ev.offsetY;
    infosObjet = ev.target.id;
    ev.dataTransfer.setData("text", infosObjet);
    // Supprime l'infobulle
    if ( ! document.querySelector(".tooltip")) {return;}
    let title = ev.target.getAttribute("tooltip");
    ev.target.title = title;
    ev.target.removeAttribute("tooltip");
    document.querySelector(".tooltip").remove();
  }

function allowDrop(ev) {
  ev.preventDefault();
}

function drop(ev) {
  const Messages = document.getElementById("messages");
  let gauche, droite, bas;
  let positionValide;
  let survol;
  let objetSurvole;
  let idMasse;
  let Masse;
  let positionGauche;
  let positionHaute;
  let positionInitiale;
  let zIndexValeur;

  function getPositionSouris() {
    survol = false;
    Masse.hidden = true;
    objetSurvole = document.getElementById(ev.target.id);
    let rect = ev.target.parentNode.getBoundingClientRect();
    paramPesee.positionX = ev.pageX - rect.left - window.scrollX;
    paramPesee.positionY = ev.pageY - rect.top - window.scrollY;
    if (objetSurvole.classList.contains("masse")) {survol = true;}
    Masse.hidden = false;
  }

  function changePlateau() {
    if (Masse.name === "plateau1") {
      paramPesee.chargeGauche += parseInt(Masse.id.substr(7));
      paramPesee.chargeDroite -= parseInt(Masse.id.substr(7));
      }
      else {
        paramPesee.chargeGauche -= parseInt(Masse.id.substr(7));
        paramPesee.chargeDroite += parseInt(Masse.id.substr(7));
      }
      setTimeout(function () {equilibreBalance()},50);
  }

  function fixePosition(element) {
    positionGauche = parseInt(paramPesee.positionX - paramPesee.pointeurX);
    positionHaute = parseInt(paramPesee.positionY - paramPesee.pointeurY);
    positionValide = false;
    gauche = positionGauche;
    droite = positionGauche + Masse.clientWidth;
    bas = positionHaute + Masse.clientHeight;
    if (( objetSurvole) && (element.id === objetSurvole.id)) {
      Messages.innerHTML = "<p></p><b>Attention :</b><br /><br /> les masses doivent être précisément positionnées sur un espace libre d'un des plateaux.</p>";
      return;
    }
    if ((survol) && (element.id !== objetSurvole.id)) {
      if (element.clientWidth + objetSurvole.clientWidth > diametre) {
        Messages.innerHTML = "<p></p><b>Attention :</b><br /><br /> les masses doivent être précisément positionnées sur un espace libre d'un des plateaux.</p>";
        return;
      }
      let basSurvole = objetSurvole.y + objetSurvole.clientHeight;
      if (bas <= parseInt(basSurvole + element.clientWidth /3)) {
        Messages.innerHTML = "<p></p><b>Attention :</b><br /><br /> les masses doivent être précisément positionnées sur un espace libre d'un des plateaux.</p>";
        return;
      }
    }
    positionValide = ((gauche > paramPesee.zoneValide1[0]) && (bas > paramPesee.zoneValide1[1]) && (droite < paramPesee.zoneValide1[2]) && (bas < paramPesee.zoneValide1[3])) ||
                     ((gauche > paramPesee.zoneValide2[0]) && (bas > paramPesee.zoneValide2[1]) && (droite < paramPesee.zoneValide2[2]) && (bas < paramPesee.zoneValide2[3]));
    if (positionValide) {
      Messages.innerHTML = "";
      element.style.left = positionGauche + "px";
      element.style.top = positionHaute + "px";
      positionInitiale = element.name;
      if (positionGauche > paramPesee.zoneValide2[0]) {element.name = "plateau2";}
        else {element.name = "plateau1";}
      if ((element.name !== positionInitiale) && (positionInitiale !== "")) {changePlateau();}
      if (survol) {
        if (objetSurvole.style.zIndex === "") {
          zIndexValeur = 1;
        }
        else {
          zIndexValeur = parseInt(objetSurvole.style.zIndex) + 1;
        }
        element.style.zIndex = zIndexValeur.toString();
      }
    }
    else {Messages.innerHTML = "<p></p><b>Attention :</b><br /><br /> les masses doivent être précisément positionnées sur un espace libre d'un des plateaux.</p>";}
  }

  ev.preventDefault();
  idMasse = ev.dataTransfer.getData("text");
  Masse = document.getElementById(idMasse);
  // Si la masse n'a pas quitté la réserve, on ne fait rien
  if ((Masse.parentNode.id === "reserve") && (ev.target.id === "reserve")) {return;}
  // Vérifie si la masse est déplacée de la balance vers la réserve
  if ((Masse.parentNode.id === "balance") && (ev.target.id === "reserve")) {
    if (Masse.name === "plateau1") {
      paramPesee.chargeGauche -= parseInt(Masse.id.substr(7));
      }
      else {
        paramPesee.chargeDroite -= parseInt(Masse.id.substr(7));
    }
    setTimeout(function () {equilibreBalance()},50);
    reserveMasses.appendChild(Masse);
    Masse.name = "";
    return;
  }
  getPositionSouris();
  fixePosition(Masse);
  // Vérifie si la masse est correctement placée et vient de la réserve
  if ((Masse.parentNode.id === "reserve") && (positionValide)) {
    balance.appendChild(Masse);
    if (Masse.name === "plateau1") {
      paramPesee.chargeGauche += parseInt(Masse.id.substr(7));
      }
      else {
        paramPesee.chargeDroite += parseInt(Masse.id.substr(7));
    }
    setTimeout(function () {equilibreBalance()},50);
  }
}
