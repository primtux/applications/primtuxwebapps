gdjs.menuCode = {};
gdjs.menuCode.GDTITREObjects1= [];
gdjs.menuCode.GDTITREObjects2= [];
gdjs.menuCode.GDTITREObjects3= [];
gdjs.menuCode.GDstartObjects1= [];
gdjs.menuCode.GDstartObjects2= [];
gdjs.menuCode.GDstartObjects3= [];
gdjs.menuCode.GDoptionsObjects1= [];
gdjs.menuCode.GDoptionsObjects2= [];
gdjs.menuCode.GDoptionsObjects3= [];
gdjs.menuCode.GDtexte_9595formatObjects1= [];
gdjs.menuCode.GDtexte_9595formatObjects2= [];
gdjs.menuCode.GDtexte_9595formatObjects3= [];
gdjs.menuCode.GDsprite_9595optionsObjects1= [];
gdjs.menuCode.GDsprite_9595optionsObjects2= [];
gdjs.menuCode.GDsprite_9595optionsObjects3= [];
gdjs.menuCode.GDsprite_9595demarrerObjects1= [];
gdjs.menuCode.GDsprite_9595demarrerObjects2= [];
gdjs.menuCode.GDsprite_9595demarrerObjects3= [];
gdjs.menuCode.GDimage_9595lamaObjects1= [];
gdjs.menuCode.GDimage_9595lamaObjects2= [];
gdjs.menuCode.GDimage_9595lamaObjects3= [];
gdjs.menuCode.GDtitre_9595appliObjects1= [];
gdjs.menuCode.GDtitre_9595appliObjects2= [];
gdjs.menuCode.GDtitre_9595appliObjects3= [];
gdjs.menuCode.GDVersionObjects1= [];
gdjs.menuCode.GDVersionObjects2= [];
gdjs.menuCode.GDVersionObjects3= [];
gdjs.menuCode.GDsprite_9595decouvrirObjects1= [];
gdjs.menuCode.GDsprite_9595decouvrirObjects2= [];
gdjs.menuCode.GDsprite_9595decouvrirObjects3= [];
gdjs.menuCode.GDtexte_9595decouvrirObjects1= [];
gdjs.menuCode.GDtexte_9595decouvrirObjects2= [];
gdjs.menuCode.GDtexte_9595decouvrirObjects3= [];
gdjs.menuCode.GDtexte_9595inventerObjects1= [];
gdjs.menuCode.GDtexte_9595inventerObjects2= [];
gdjs.menuCode.GDtexte_9595inventerObjects3= [];
gdjs.menuCode.GDtexte_9595avertissementObjects1= [];
gdjs.menuCode.GDtexte_9595avertissementObjects2= [];
gdjs.menuCode.GDtexte_9595avertissementObjects3= [];
gdjs.menuCode.GDsprite_9595logo1Objects1= [];
gdjs.menuCode.GDsprite_9595logo1Objects2= [];
gdjs.menuCode.GDsprite_9595logo1Objects3= [];
gdjs.menuCode.GDsprite_9595horizontalObjects1= [];
gdjs.menuCode.GDsprite_9595horizontalObjects2= [];
gdjs.menuCode.GDsprite_9595horizontalObjects3= [];
gdjs.menuCode.GDressourcesObjects1= [];
gdjs.menuCode.GDressourcesObjects2= [];
gdjs.menuCode.GDressourcesObjects3= [];
gdjs.menuCode.GDsprite_9595retourObjects1= [];
gdjs.menuCode.GDsprite_9595retourObjects2= [];
gdjs.menuCode.GDsprite_9595retourObjects3= [];
gdjs.menuCode.GDfond_9595blancObjects1= [];
gdjs.menuCode.GDfond_9595blancObjects2= [];
gdjs.menuCode.GDfond_9595blancObjects3= [];
gdjs.menuCode.GDcase_9595minusculesObjects1= [];
gdjs.menuCode.GDcase_9595minusculesObjects2= [];
gdjs.menuCode.GDcase_9595minusculesObjects3= [];
gdjs.menuCode.GDcase_9595majusculesObjects1= [];
gdjs.menuCode.GDcase_9595majusculesObjects2= [];
gdjs.menuCode.GDcase_9595majusculesObjects3= [];
gdjs.menuCode.GDtexte_9595majusculesObjects1= [];
gdjs.menuCode.GDtexte_9595majusculesObjects2= [];
gdjs.menuCode.GDtexte_9595majusculesObjects3= [];
gdjs.menuCode.GDtexte_9595minusculesObjects1= [];
gdjs.menuCode.GDtexte_9595minusculesObjects2= [];
gdjs.menuCode.GDtexte_9595minusculesObjects3= [];
gdjs.menuCode.GDtexte_9595choisir_9595motsObjects1= [];
gdjs.menuCode.GDtexte_9595choisir_9595motsObjects2= [];
gdjs.menuCode.GDtexte_9595choisir_9595motsObjects3= [];
gdjs.menuCode.GDsprite_9595baguetteObjects1= [];
gdjs.menuCode.GDsprite_9595baguetteObjects2= [];
gdjs.menuCode.GDsprite_9595baguetteObjects3= [];
gdjs.menuCode.GDcredit_9595imagesObjects1= [];
gdjs.menuCode.GDcredit_9595imagesObjects2= [];
gdjs.menuCode.GDcredit_9595imagesObjects3= [];
gdjs.menuCode.GDtexte_9595auteurObjects1= [];
gdjs.menuCode.GDtexte_9595auteurObjects2= [];
gdjs.menuCode.GDtexte_9595auteurObjects3= [];
gdjs.menuCode.GDsprite_9595serge_9595lamaObjects1= [];
gdjs.menuCode.GDsprite_9595serge_9595lamaObjects2= [];
gdjs.menuCode.GDsprite_9595serge_9595lamaObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{



}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_syllanimo", "cases_animaux", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde_temporaire1"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde_temporaire1")), runtimeScene.getGame().getVariables().getFromIndex(1));
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDstartObjects1Objects = Hashtable.newFrom({"start": gdjs.menuCode.GDstartObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsprite_95959595demarrerObjects1Objects = Hashtable.newFrom({"sprite_demarrer": gdjs.menuCode.GDsprite_9595demarrerObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsprite_95959595decouvrirObjects1Objects = Hashtable.newFrom({"sprite_decouvrir": gdjs.menuCode.GDsprite_9595decouvrirObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDoptionsObjects1Objects = Hashtable.newFrom({"options": gdjs.menuCode.GDoptionsObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsprite_95959595optionsObjects1Objects = Hashtable.newFrom({"sprite_options": gdjs.menuCode.GDsprite_9595optionsObjects1});
gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs.menuCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("start"), gdjs.menuCode.GDstartObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDstartObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_demarrer"), gdjs.menuCode.GDsprite_9595demarrerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsprite_95959595demarrerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_decouvrir"), gdjs.menuCode.GDsprite_9595decouvrirObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsprite_95959595decouvrirObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "decouvre", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("options"), gdjs.menuCode.GDoptionsObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDoptionsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "code");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_options"), gdjs.menuCode.GDsprite_9595optionsObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsprite_95959595optionsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.pushScene(runtimeScene, "code");
}}

}


{



}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDTITREObjects1.length = 0;
gdjs.menuCode.GDTITREObjects2.length = 0;
gdjs.menuCode.GDTITREObjects3.length = 0;
gdjs.menuCode.GDstartObjects1.length = 0;
gdjs.menuCode.GDstartObjects2.length = 0;
gdjs.menuCode.GDstartObjects3.length = 0;
gdjs.menuCode.GDoptionsObjects1.length = 0;
gdjs.menuCode.GDoptionsObjects2.length = 0;
gdjs.menuCode.GDoptionsObjects3.length = 0;
gdjs.menuCode.GDtexte_9595formatObjects1.length = 0;
gdjs.menuCode.GDtexte_9595formatObjects2.length = 0;
gdjs.menuCode.GDtexte_9595formatObjects3.length = 0;
gdjs.menuCode.GDsprite_9595optionsObjects1.length = 0;
gdjs.menuCode.GDsprite_9595optionsObjects2.length = 0;
gdjs.menuCode.GDsprite_9595optionsObjects3.length = 0;
gdjs.menuCode.GDsprite_9595demarrerObjects1.length = 0;
gdjs.menuCode.GDsprite_9595demarrerObjects2.length = 0;
gdjs.menuCode.GDsprite_9595demarrerObjects3.length = 0;
gdjs.menuCode.GDimage_9595lamaObjects1.length = 0;
gdjs.menuCode.GDimage_9595lamaObjects2.length = 0;
gdjs.menuCode.GDimage_9595lamaObjects3.length = 0;
gdjs.menuCode.GDtitre_9595appliObjects1.length = 0;
gdjs.menuCode.GDtitre_9595appliObjects2.length = 0;
gdjs.menuCode.GDtitre_9595appliObjects3.length = 0;
gdjs.menuCode.GDVersionObjects1.length = 0;
gdjs.menuCode.GDVersionObjects2.length = 0;
gdjs.menuCode.GDVersionObjects3.length = 0;
gdjs.menuCode.GDsprite_9595decouvrirObjects1.length = 0;
gdjs.menuCode.GDsprite_9595decouvrirObjects2.length = 0;
gdjs.menuCode.GDsprite_9595decouvrirObjects3.length = 0;
gdjs.menuCode.GDtexte_9595decouvrirObjects1.length = 0;
gdjs.menuCode.GDtexte_9595decouvrirObjects2.length = 0;
gdjs.menuCode.GDtexte_9595decouvrirObjects3.length = 0;
gdjs.menuCode.GDtexte_9595inventerObjects1.length = 0;
gdjs.menuCode.GDtexte_9595inventerObjects2.length = 0;
gdjs.menuCode.GDtexte_9595inventerObjects3.length = 0;
gdjs.menuCode.GDtexte_9595avertissementObjects1.length = 0;
gdjs.menuCode.GDtexte_9595avertissementObjects2.length = 0;
gdjs.menuCode.GDtexte_9595avertissementObjects3.length = 0;
gdjs.menuCode.GDsprite_9595logo1Objects1.length = 0;
gdjs.menuCode.GDsprite_9595logo1Objects2.length = 0;
gdjs.menuCode.GDsprite_9595logo1Objects3.length = 0;
gdjs.menuCode.GDsprite_9595horizontalObjects1.length = 0;
gdjs.menuCode.GDsprite_9595horizontalObjects2.length = 0;
gdjs.menuCode.GDsprite_9595horizontalObjects3.length = 0;
gdjs.menuCode.GDressourcesObjects1.length = 0;
gdjs.menuCode.GDressourcesObjects2.length = 0;
gdjs.menuCode.GDressourcesObjects3.length = 0;
gdjs.menuCode.GDsprite_9595retourObjects1.length = 0;
gdjs.menuCode.GDsprite_9595retourObjects2.length = 0;
gdjs.menuCode.GDsprite_9595retourObjects3.length = 0;
gdjs.menuCode.GDfond_9595blancObjects1.length = 0;
gdjs.menuCode.GDfond_9595blancObjects2.length = 0;
gdjs.menuCode.GDfond_9595blancObjects3.length = 0;
gdjs.menuCode.GDcase_9595minusculesObjects1.length = 0;
gdjs.menuCode.GDcase_9595minusculesObjects2.length = 0;
gdjs.menuCode.GDcase_9595minusculesObjects3.length = 0;
gdjs.menuCode.GDcase_9595majusculesObjects1.length = 0;
gdjs.menuCode.GDcase_9595majusculesObjects2.length = 0;
gdjs.menuCode.GDcase_9595majusculesObjects3.length = 0;
gdjs.menuCode.GDtexte_9595majusculesObjects1.length = 0;
gdjs.menuCode.GDtexte_9595majusculesObjects2.length = 0;
gdjs.menuCode.GDtexte_9595majusculesObjects3.length = 0;
gdjs.menuCode.GDtexte_9595minusculesObjects1.length = 0;
gdjs.menuCode.GDtexte_9595minusculesObjects2.length = 0;
gdjs.menuCode.GDtexte_9595minusculesObjects3.length = 0;
gdjs.menuCode.GDtexte_9595choisir_9595motsObjects1.length = 0;
gdjs.menuCode.GDtexte_9595choisir_9595motsObjects2.length = 0;
gdjs.menuCode.GDtexte_9595choisir_9595motsObjects3.length = 0;
gdjs.menuCode.GDsprite_9595baguetteObjects1.length = 0;
gdjs.menuCode.GDsprite_9595baguetteObjects2.length = 0;
gdjs.menuCode.GDsprite_9595baguetteObjects3.length = 0;
gdjs.menuCode.GDcredit_9595imagesObjects1.length = 0;
gdjs.menuCode.GDcredit_9595imagesObjects2.length = 0;
gdjs.menuCode.GDcredit_9595imagesObjects3.length = 0;
gdjs.menuCode.GDtexte_9595auteurObjects1.length = 0;
gdjs.menuCode.GDtexte_9595auteurObjects2.length = 0;
gdjs.menuCode.GDtexte_9595auteurObjects3.length = 0;
gdjs.menuCode.GDsprite_9595serge_9595lamaObjects1.length = 0;
gdjs.menuCode.GDsprite_9595serge_9595lamaObjects2.length = 0;
gdjs.menuCode.GDsprite_9595serge_9595lamaObjects3.length = 0;

gdjs.menuCode.eventsList1(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
