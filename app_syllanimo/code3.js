gdjs.codeCode = {};
gdjs.codeCode.GDtexte_9595codeObjects1= [];
gdjs.codeCode.GDtexte_9595codeObjects2= [];
gdjs.codeCode.GDtexte_9595codeObjects3= [];
gdjs.codeCode.GDvariableObjects1= [];
gdjs.codeCode.GDvariableObjects2= [];
gdjs.codeCode.GDvariableObjects3= [];
gdjs.codeCode.GDsprite_9595nb9Objects1= [];
gdjs.codeCode.GDsprite_9595nb9Objects2= [];
gdjs.codeCode.GDsprite_9595nb9Objects3= [];
gdjs.codeCode.GDsprite_9595nb8Objects1= [];
gdjs.codeCode.GDsprite_9595nb8Objects2= [];
gdjs.codeCode.GDsprite_9595nb8Objects3= [];
gdjs.codeCode.GDsprite_9595nb7Objects1= [];
gdjs.codeCode.GDsprite_9595nb7Objects2= [];
gdjs.codeCode.GDsprite_9595nb7Objects3= [];
gdjs.codeCode.GDsprite_9595nb6Objects1= [];
gdjs.codeCode.GDsprite_9595nb6Objects2= [];
gdjs.codeCode.GDsprite_9595nb6Objects3= [];
gdjs.codeCode.GDsprite_9595nb5Objects1= [];
gdjs.codeCode.GDsprite_9595nb5Objects2= [];
gdjs.codeCode.GDsprite_9595nb5Objects3= [];
gdjs.codeCode.GDsprite_9595nb4Objects1= [];
gdjs.codeCode.GDsprite_9595nb4Objects2= [];
gdjs.codeCode.GDsprite_9595nb4Objects3= [];
gdjs.codeCode.GDsprite_9595nb3Objects1= [];
gdjs.codeCode.GDsprite_9595nb3Objects2= [];
gdjs.codeCode.GDsprite_9595nb3Objects3= [];
gdjs.codeCode.GDsprite_9595nb2Objects1= [];
gdjs.codeCode.GDsprite_9595nb2Objects2= [];
gdjs.codeCode.GDsprite_9595nb2Objects3= [];
gdjs.codeCode.GDsprite_9595nb1Objects1= [];
gdjs.codeCode.GDsprite_9595nb1Objects2= [];
gdjs.codeCode.GDsprite_9595nb1Objects3= [];
gdjs.codeCode.GDsprite_9595retourObjects1= [];
gdjs.codeCode.GDsprite_9595retourObjects2= [];
gdjs.codeCode.GDsprite_9595retourObjects3= [];
gdjs.codeCode.GDfond_9595blancObjects1= [];
gdjs.codeCode.GDfond_9595blancObjects2= [];
gdjs.codeCode.GDfond_9595blancObjects3= [];
gdjs.codeCode.GDcase_9595minusculesObjects1= [];
gdjs.codeCode.GDcase_9595minusculesObjects2= [];
gdjs.codeCode.GDcase_9595minusculesObjects3= [];
gdjs.codeCode.GDcase_9595majusculesObjects1= [];
gdjs.codeCode.GDcase_9595majusculesObjects2= [];
gdjs.codeCode.GDcase_9595majusculesObjects3= [];
gdjs.codeCode.GDtexte_9595majusculesObjects1= [];
gdjs.codeCode.GDtexte_9595majusculesObjects2= [];
gdjs.codeCode.GDtexte_9595majusculesObjects3= [];
gdjs.codeCode.GDtexte_9595minusculesObjects1= [];
gdjs.codeCode.GDtexte_9595minusculesObjects2= [];
gdjs.codeCode.GDtexte_9595minusculesObjects3= [];
gdjs.codeCode.GDtexte_9595choisir_9595motsObjects1= [];
gdjs.codeCode.GDtexte_9595choisir_9595motsObjects2= [];
gdjs.codeCode.GDtexte_9595choisir_9595motsObjects3= [];
gdjs.codeCode.GDsprite_9595baguetteObjects1= [];
gdjs.codeCode.GDsprite_9595baguetteObjects2= [];
gdjs.codeCode.GDsprite_9595baguetteObjects3= [];
gdjs.codeCode.GDcredit_9595imagesObjects1= [];
gdjs.codeCode.GDcredit_9595imagesObjects2= [];
gdjs.codeCode.GDcredit_9595imagesObjects3= [];
gdjs.codeCode.GDtexte_9595auteurObjects1= [];
gdjs.codeCode.GDtexte_9595auteurObjects2= [];
gdjs.codeCode.GDtexte_9595auteurObjects3= [];
gdjs.codeCode.GDsprite_9595serge_9595lamaObjects1= [];
gdjs.codeCode.GDsprite_9595serge_9595lamaObjects2= [];
gdjs.codeCode.GDsprite_9595serge_9595lamaObjects3= [];


gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb8Objects1Objects = Hashtable.newFrom({"sprite_nb8": gdjs.codeCode.GDsprite_9595nb8Objects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13518636);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects2.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects2[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13520308);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb2Objects1Objects = Hashtable.newFrom({"sprite_nb2": gdjs.codeCode.GDsprite_9595nb2Objects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects2.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects2[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb3Objects1Objects = Hashtable.newFrom({"sprite_nb3": gdjs.codeCode.GDsprite_9595nb3Objects1});
gdjs.codeCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) != 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects2);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects2.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects2[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) == 2;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects1);
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb9Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb7Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb6Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb5Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb4Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb1Objects1Objects = Hashtable.newFrom({"sprite_nb9": gdjs.codeCode.GDsprite_9595nb9Objects1, "sprite_nb7": gdjs.codeCode.GDsprite_9595nb7Objects1, "sprite_nb6": gdjs.codeCode.GDsprite_9595nb6Objects1, "sprite_nb5": gdjs.codeCode.GDsprite_9595nb5Objects1, "sprite_nb4": gdjs.codeCode.GDsprite_9595nb4Objects1, "sprite_nb1": gdjs.codeCode.GDsprite_9595nb1Objects1});
gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595retourObjects1Objects = Hashtable.newFrom({"sprite_retour": gdjs.codeCode.GDsprite_9595retourObjects1});
gdjs.codeCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_nb8"), gdjs.codeCode.GDsprite_9595nb8Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb8Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13517852);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(1).setString("");
}
{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_nb2"), gdjs.codeCode.GDsprite_9595nb2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13522212);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_nb3"), gdjs.codeCode.GDsprite_9595nb3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13525532);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.codeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_nb1"), gdjs.codeCode.GDsprite_9595nb1Objects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_nb4"), gdjs.codeCode.GDsprite_9595nb4Objects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_nb5"), gdjs.codeCode.GDsprite_9595nb5Objects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_nb6"), gdjs.codeCode.GDsprite_9595nb6Objects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_nb7"), gdjs.codeCode.GDsprite_9595nb7Objects1);
gdjs.copyArray(runtimeScene.getObjects("sprite_nb9"), gdjs.codeCode.GDsprite_9595nb9Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595nb9Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb7Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb6Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb5Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb4Objects1ObjectsGDgdjs_9546codeCode_9546GDsprite_95959595nb1Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(13529116);
}
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("variable"), gdjs.codeCode.GDvariableObjects1);
{runtimeScene.getScene().getVariables().getFromIndex(1).setNumber(0);
}{for(var i = 0, len = gdjs.codeCode.GDvariableObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDvariableObjects1[i].setString(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().getFromIndex(1)));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sprite_retour"), gdjs.codeCode.GDsprite_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsprite_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDtexte_9595codeObjects1.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects2.length = 0;
gdjs.codeCode.GDtexte_9595codeObjects3.length = 0;
gdjs.codeCode.GDvariableObjects1.length = 0;
gdjs.codeCode.GDvariableObjects2.length = 0;
gdjs.codeCode.GDvariableObjects3.length = 0;
gdjs.codeCode.GDsprite_9595nb9Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb9Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb9Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb8Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb8Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb8Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb7Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb7Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb7Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb6Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb6Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb6Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb5Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb5Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb5Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb4Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb4Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb4Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb3Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb3Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb3Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb2Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb2Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb2Objects3.length = 0;
gdjs.codeCode.GDsprite_9595nb1Objects1.length = 0;
gdjs.codeCode.GDsprite_9595nb1Objects2.length = 0;
gdjs.codeCode.GDsprite_9595nb1Objects3.length = 0;
gdjs.codeCode.GDsprite_9595retourObjects1.length = 0;
gdjs.codeCode.GDsprite_9595retourObjects2.length = 0;
gdjs.codeCode.GDsprite_9595retourObjects3.length = 0;
gdjs.codeCode.GDfond_9595blancObjects1.length = 0;
gdjs.codeCode.GDfond_9595blancObjects2.length = 0;
gdjs.codeCode.GDfond_9595blancObjects3.length = 0;
gdjs.codeCode.GDcase_9595minusculesObjects1.length = 0;
gdjs.codeCode.GDcase_9595minusculesObjects2.length = 0;
gdjs.codeCode.GDcase_9595minusculesObjects3.length = 0;
gdjs.codeCode.GDcase_9595majusculesObjects1.length = 0;
gdjs.codeCode.GDcase_9595majusculesObjects2.length = 0;
gdjs.codeCode.GDcase_9595majusculesObjects3.length = 0;
gdjs.codeCode.GDtexte_9595majusculesObjects1.length = 0;
gdjs.codeCode.GDtexte_9595majusculesObjects2.length = 0;
gdjs.codeCode.GDtexte_9595majusculesObjects3.length = 0;
gdjs.codeCode.GDtexte_9595minusculesObjects1.length = 0;
gdjs.codeCode.GDtexte_9595minusculesObjects2.length = 0;
gdjs.codeCode.GDtexte_9595minusculesObjects3.length = 0;
gdjs.codeCode.GDtexte_9595choisir_9595motsObjects1.length = 0;
gdjs.codeCode.GDtexte_9595choisir_9595motsObjects2.length = 0;
gdjs.codeCode.GDtexte_9595choisir_9595motsObjects3.length = 0;
gdjs.codeCode.GDsprite_9595baguetteObjects1.length = 0;
gdjs.codeCode.GDsprite_9595baguetteObjects2.length = 0;
gdjs.codeCode.GDsprite_9595baguetteObjects3.length = 0;
gdjs.codeCode.GDcredit_9595imagesObjects1.length = 0;
gdjs.codeCode.GDcredit_9595imagesObjects2.length = 0;
gdjs.codeCode.GDcredit_9595imagesObjects3.length = 0;
gdjs.codeCode.GDtexte_9595auteurObjects1.length = 0;
gdjs.codeCode.GDtexte_9595auteurObjects2.length = 0;
gdjs.codeCode.GDtexte_9595auteurObjects3.length = 0;
gdjs.codeCode.GDsprite_9595serge_9595lamaObjects1.length = 0;
gdjs.codeCode.GDsprite_9595serge_9595lamaObjects2.length = 0;
gdjs.codeCode.GDsprite_9595serge_9595lamaObjects3.length = 0;

gdjs.codeCode.eventsList3(runtimeScene);

return;

}

gdjs['codeCode'] = gdjs.codeCode;
