gdjs.introCode = {};
gdjs.introCode.localVariables = [];
gdjs.introCode.GDfond_9595introObjects1= [];
gdjs.introCode.GDfond_9595introObjects2= [];
gdjs.introCode.GDsp_9595bouton_9595continuerObjects1= [];
gdjs.introCode.GDsp_9595bouton_9595continuerObjects2= [];
gdjs.introCode.GDbouton_9595retourObjects1= [];
gdjs.introCode.GDbouton_9595retourObjects2= [];
gdjs.introCode.GDcoffreObjects1= [];
gdjs.introCode.GDcoffreObjects2= [];
gdjs.introCode.GDsp_9595diamantObjects1= [];
gdjs.introCode.GDsp_9595diamantObjects2= [];
gdjs.introCode.GDGreenDotBarObjects1= [];
gdjs.introCode.GDGreenDotBarObjects2= [];
gdjs.introCode.GDsp_9595cadre0Objects1= [];
gdjs.introCode.GDsp_9595cadre0Objects2= [];
gdjs.introCode.GDpapierObjects1= [];
gdjs.introCode.GDpapierObjects2= [];
gdjs.introCode.GDtext_9595modeleObjects1= [];
gdjs.introCode.GDtext_9595modeleObjects2= [];
gdjs.introCode.GDclavierObjects1= [];
gdjs.introCode.GDclavierObjects2= [];


gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDsp_95959595bouton_95959595continuerObjects1Objects = Hashtable.newFrom({"sp_bouton_continuer": gdjs.introCode.GDsp_9595bouton_9595continuerObjects1});
gdjs.introCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_continuer"), gdjs.introCode.GDsp_9595bouton_9595continuerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDsp_95959595bouton_95959595continuerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDfond_9595introObjects1.length = 0;
gdjs.introCode.GDfond_9595introObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595continuerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595continuerObjects2.length = 0;
gdjs.introCode.GDbouton_9595retourObjects1.length = 0;
gdjs.introCode.GDbouton_9595retourObjects2.length = 0;
gdjs.introCode.GDcoffreObjects1.length = 0;
gdjs.introCode.GDcoffreObjects2.length = 0;
gdjs.introCode.GDsp_9595diamantObjects1.length = 0;
gdjs.introCode.GDsp_9595diamantObjects2.length = 0;
gdjs.introCode.GDGreenDotBarObjects1.length = 0;
gdjs.introCode.GDGreenDotBarObjects2.length = 0;
gdjs.introCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.introCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.introCode.GDpapierObjects1.length = 0;
gdjs.introCode.GDpapierObjects2.length = 0;
gdjs.introCode.GDtext_9595modeleObjects1.length = 0;
gdjs.introCode.GDtext_9595modeleObjects2.length = 0;
gdjs.introCode.GDclavierObjects1.length = 0;
gdjs.introCode.GDclavierObjects2.length = 0;

gdjs.introCode.eventsList0(runtimeScene);
gdjs.introCode.GDfond_9595introObjects1.length = 0;
gdjs.introCode.GDfond_9595introObjects2.length = 0;
gdjs.introCode.GDsp_9595bouton_9595continuerObjects1.length = 0;
gdjs.introCode.GDsp_9595bouton_9595continuerObjects2.length = 0;
gdjs.introCode.GDbouton_9595retourObjects1.length = 0;
gdjs.introCode.GDbouton_9595retourObjects2.length = 0;
gdjs.introCode.GDcoffreObjects1.length = 0;
gdjs.introCode.GDcoffreObjects2.length = 0;
gdjs.introCode.GDsp_9595diamantObjects1.length = 0;
gdjs.introCode.GDsp_9595diamantObjects2.length = 0;
gdjs.introCode.GDGreenDotBarObjects1.length = 0;
gdjs.introCode.GDGreenDotBarObjects2.length = 0;
gdjs.introCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.introCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.introCode.GDpapierObjects1.length = 0;
gdjs.introCode.GDpapierObjects2.length = 0;
gdjs.introCode.GDtext_9595modeleObjects1.length = 0;
gdjs.introCode.GDtext_9595modeleObjects2.length = 0;
gdjs.introCode.GDclavierObjects1.length = 0;
gdjs.introCode.GDclavierObjects2.length = 0;


return;

}

gdjs['introCode'] = gdjs.introCode;
