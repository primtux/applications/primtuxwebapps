gdjs.infosCode = {};
gdjs.infosCode.localVariables = [];
gdjs.infosCode.GDbbtxt_9595info1Objects1= [];
gdjs.infosCode.GDbbtxt_9595info1Objects2= [];
gdjs.infosCode.GDsp_9595info1Objects1= [];
gdjs.infosCode.GDsp_9595info1Objects2= [];
gdjs.infosCode.GDbbtxt_9595info2Objects1= [];
gdjs.infosCode.GDbbtxt_9595info2Objects2= [];
gdjs.infosCode.GDsp_9595info2Objects1= [];
gdjs.infosCode.GDsp_9595info2Objects2= [];
gdjs.infosCode.GDbbtxt_9595info3Objects1= [];
gdjs.infosCode.GDbbtxt_9595info3Objects2= [];
gdjs.infosCode.GDsp_9595info3Objects1= [];
gdjs.infosCode.GDsp_9595info3Objects2= [];
gdjs.infosCode.GDsp_9595info4Objects1= [];
gdjs.infosCode.GDsp_9595info4Objects2= [];
gdjs.infosCode.GDbbtxt_9595info4Objects1= [];
gdjs.infosCode.GDbbtxt_9595info4Objects2= [];
gdjs.infosCode.GDbouton_9595retourObjects1= [];
gdjs.infosCode.GDbouton_9595retourObjects2= [];
gdjs.infosCode.GDcoffreObjects1= [];
gdjs.infosCode.GDcoffreObjects2= [];
gdjs.infosCode.GDsp_9595diamantObjects1= [];
gdjs.infosCode.GDsp_9595diamantObjects2= [];
gdjs.infosCode.GDGreenDotBarObjects1= [];
gdjs.infosCode.GDGreenDotBarObjects2= [];
gdjs.infosCode.GDsp_9595cadre0Objects1= [];
gdjs.infosCode.GDsp_9595cadre0Objects2= [];
gdjs.infosCode.GDpapierObjects1= [];
gdjs.infosCode.GDpapierObjects2= [];
gdjs.infosCode.GDtext_9595modeleObjects1= [];
gdjs.infosCode.GDtext_9595modeleObjects2= [];
gdjs.infosCode.GDclavierObjects1= [];
gdjs.infosCode.GDclavierObjects2= [];


gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_9595retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_9546infosCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDbbtxt_9595info1Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info1Objects2.length = 0;
gdjs.infosCode.GDsp_9595info1Objects1.length = 0;
gdjs.infosCode.GDsp_9595info1Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects2.length = 0;
gdjs.infosCode.GDsp_9595info2Objects1.length = 0;
gdjs.infosCode.GDsp_9595info2Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects2.length = 0;
gdjs.infosCode.GDsp_9595info3Objects1.length = 0;
gdjs.infosCode.GDsp_9595info3Objects2.length = 0;
gdjs.infosCode.GDsp_9595info4Objects1.length = 0;
gdjs.infosCode.GDsp_9595info4Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info4Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info4Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDcoffreObjects1.length = 0;
gdjs.infosCode.GDcoffreObjects2.length = 0;
gdjs.infosCode.GDsp_9595diamantObjects1.length = 0;
gdjs.infosCode.GDsp_9595diamantObjects2.length = 0;
gdjs.infosCode.GDGreenDotBarObjects1.length = 0;
gdjs.infosCode.GDGreenDotBarObjects2.length = 0;
gdjs.infosCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.infosCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.infosCode.GDpapierObjects1.length = 0;
gdjs.infosCode.GDpapierObjects2.length = 0;
gdjs.infosCode.GDtext_9595modeleObjects1.length = 0;
gdjs.infosCode.GDtext_9595modeleObjects2.length = 0;
gdjs.infosCode.GDclavierObjects1.length = 0;
gdjs.infosCode.GDclavierObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
gdjs.infosCode.GDbbtxt_9595info1Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info1Objects2.length = 0;
gdjs.infosCode.GDsp_9595info1Objects1.length = 0;
gdjs.infosCode.GDsp_9595info1Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info2Objects2.length = 0;
gdjs.infosCode.GDsp_9595info2Objects1.length = 0;
gdjs.infosCode.GDsp_9595info2Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info3Objects2.length = 0;
gdjs.infosCode.GDsp_9595info3Objects1.length = 0;
gdjs.infosCode.GDsp_9595info3Objects2.length = 0;
gdjs.infosCode.GDsp_9595info4Objects1.length = 0;
gdjs.infosCode.GDsp_9595info4Objects2.length = 0;
gdjs.infosCode.GDbbtxt_9595info4Objects1.length = 0;
gdjs.infosCode.GDbbtxt_9595info4Objects2.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects1.length = 0;
gdjs.infosCode.GDbouton_9595retourObjects2.length = 0;
gdjs.infosCode.GDcoffreObjects1.length = 0;
gdjs.infosCode.GDcoffreObjects2.length = 0;
gdjs.infosCode.GDsp_9595diamantObjects1.length = 0;
gdjs.infosCode.GDsp_9595diamantObjects2.length = 0;
gdjs.infosCode.GDGreenDotBarObjects1.length = 0;
gdjs.infosCode.GDGreenDotBarObjects2.length = 0;
gdjs.infosCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.infosCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.infosCode.GDpapierObjects1.length = 0;
gdjs.infosCode.GDpapierObjects2.length = 0;
gdjs.infosCode.GDtext_9595modeleObjects1.length = 0;
gdjs.infosCode.GDtext_9595modeleObjects2.length = 0;
gdjs.infosCode.GDclavierObjects1.length = 0;
gdjs.infosCode.GDclavierObjects2.length = 0;


return;

}

gdjs['infosCode'] = gdjs.infosCode;
