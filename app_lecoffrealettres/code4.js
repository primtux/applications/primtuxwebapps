gdjs.codeCode = {};
gdjs.codeCode.localVariables = [];
gdjs.codeCode.GDsp_9595boutonObjects1= [];
gdjs.codeCode.GDsp_9595boutonObjects2= [];
gdjs.codeCode.GDsp_9595boutonObjects3= [];
gdjs.codeCode.GDNewTextObjects1= [];
gdjs.codeCode.GDNewTextObjects2= [];
gdjs.codeCode.GDNewTextObjects3= [];
gdjs.codeCode.GDbouton_9595retourObjects1= [];
gdjs.codeCode.GDbouton_9595retourObjects2= [];
gdjs.codeCode.GDbouton_9595retourObjects3= [];
gdjs.codeCode.GDcoffreObjects1= [];
gdjs.codeCode.GDcoffreObjects2= [];
gdjs.codeCode.GDcoffreObjects3= [];
gdjs.codeCode.GDsp_9595diamantObjects1= [];
gdjs.codeCode.GDsp_9595diamantObjects2= [];
gdjs.codeCode.GDsp_9595diamantObjects3= [];
gdjs.codeCode.GDGreenDotBarObjects1= [];
gdjs.codeCode.GDGreenDotBarObjects2= [];
gdjs.codeCode.GDGreenDotBarObjects3= [];
gdjs.codeCode.GDsp_9595cadre0Objects1= [];
gdjs.codeCode.GDsp_9595cadre0Objects2= [];
gdjs.codeCode.GDsp_9595cadre0Objects3= [];
gdjs.codeCode.GDpapierObjects1= [];
gdjs.codeCode.GDpapierObjects2= [];
gdjs.codeCode.GDpapierObjects3= [];
gdjs.codeCode.GDtext_9595modeleObjects1= [];
gdjs.codeCode.GDtext_9595modeleObjects2= [];
gdjs.codeCode.GDtext_9595modeleObjects3= [];
gdjs.codeCode.GDclavierObjects1= [];
gdjs.codeCode.GDclavierObjects2= [];
gdjs.codeCode.GDclavierObjects3= [];


gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsp_95959595boutonObjects1Objects = Hashtable.newFrom({"sp_bouton": gdjs.codeCode.GDsp_9595boutonObjects1});
gdjs.codeCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.codeCode.GDsp_9595boutonObjects1, gdjs.codeCode.GDsp_9595boutonObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.codeCode.GDsp_9595boutonObjects2.length;i<l;++i) {
    if ( gdjs.codeCode.GDsp_9595boutonObjects2[i].getVariableNumber(gdjs.codeCode.GDsp_9595boutonObjects2[i].getVariables().getFromIndex(0)) != 3 ) {
        isConditionTrue_0 = true;
        gdjs.codeCode.GDsp_9595boutonObjects2[k] = gdjs.codeCode.GDsp_9595boutonObjects2[i];
        ++k;
    }
}
gdjs.codeCode.GDsp_9595boutonObjects2.length = k;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


{

gdjs.copyArray(gdjs.codeCode.GDsp_9595boutonObjects1, gdjs.codeCode.GDsp_9595boutonObjects2);


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.codeCode.GDsp_9595boutonObjects2.length;i<l;++i) {
    if ( gdjs.codeCode.GDsp_9595boutonObjects2[i].getVariableNumber(gdjs.codeCode.GDsp_9595boutonObjects2[i].getVariables().getFromIndex(0)) != 5 ) {
        isConditionTrue_0 = true;
        gdjs.codeCode.GDsp_9595boutonObjects2[k] = gdjs.codeCode.GDsp_9595boutonObjects2[i];
        ++k;
    }
}
gdjs.codeCode.GDsp_9595boutonObjects2.length = k;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


{

/* Reuse gdjs.codeCode.GDsp_9595boutonObjects1 */

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.codeCode.GDsp_9595boutonObjects1.length;i<l;++i) {
    if ( gdjs.codeCode.GDsp_9595boutonObjects1[i].getVariableNumber(gdjs.codeCode.GDsp_9595boutonObjects1[i].getVariables().getFromIndex(0)) != 2 ) {
        isConditionTrue_0 = true;
        gdjs.codeCode.GDsp_9595boutonObjects1[k] = gdjs.codeCode.GDsp_9595boutonObjects1[i];
        ++k;
    }
}
gdjs.codeCode.GDsp_9595boutonObjects1.length = k;
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


};gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.codeCode.GDbouton_9595retourObjects1});
gdjs.codeCode.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("sp_bouton"), gdjs.codeCode.GDsp_9595boutonObjects1);
{for(var i = 0, len = gdjs.codeCode.GDsp_9595boutonObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDsp_9595boutonObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.codeCode.GDsp_9595boutonObjects1.length ;i < len;++i) {
    gdjs.codeCode.GDsp_9595boutonObjects1[i].setAnimationFrame(gdjs.codeCode.GDsp_9595boutonObjects1[i].getVariables().getFromIndex(0).getAsNumber());
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton"), gdjs.codeCode.GDsp_9595boutonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDsp_95959595boutonObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}
{ //Subevents
gdjs.codeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "configuration", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.codeCode.GDbouton_9595retourObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.codeCode.mapOfGDgdjs_9546codeCode_9546GDbouton_95959595retourObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs.codeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.codeCode.GDsp_9595boutonObjects1.length = 0;
gdjs.codeCode.GDsp_9595boutonObjects2.length = 0;
gdjs.codeCode.GDsp_9595boutonObjects3.length = 0;
gdjs.codeCode.GDNewTextObjects1.length = 0;
gdjs.codeCode.GDNewTextObjects2.length = 0;
gdjs.codeCode.GDNewTextObjects3.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects1.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects2.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects3.length = 0;
gdjs.codeCode.GDcoffreObjects1.length = 0;
gdjs.codeCode.GDcoffreObjects2.length = 0;
gdjs.codeCode.GDcoffreObjects3.length = 0;
gdjs.codeCode.GDsp_9595diamantObjects1.length = 0;
gdjs.codeCode.GDsp_9595diamantObjects2.length = 0;
gdjs.codeCode.GDsp_9595diamantObjects3.length = 0;
gdjs.codeCode.GDGreenDotBarObjects1.length = 0;
gdjs.codeCode.GDGreenDotBarObjects2.length = 0;
gdjs.codeCode.GDGreenDotBarObjects3.length = 0;
gdjs.codeCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.codeCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.codeCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.codeCode.GDpapierObjects1.length = 0;
gdjs.codeCode.GDpapierObjects2.length = 0;
gdjs.codeCode.GDpapierObjects3.length = 0;
gdjs.codeCode.GDtext_9595modeleObjects1.length = 0;
gdjs.codeCode.GDtext_9595modeleObjects2.length = 0;
gdjs.codeCode.GDtext_9595modeleObjects3.length = 0;
gdjs.codeCode.GDclavierObjects1.length = 0;
gdjs.codeCode.GDclavierObjects2.length = 0;
gdjs.codeCode.GDclavierObjects3.length = 0;

gdjs.codeCode.eventsList1(runtimeScene);
gdjs.codeCode.GDsp_9595boutonObjects1.length = 0;
gdjs.codeCode.GDsp_9595boutonObjects2.length = 0;
gdjs.codeCode.GDsp_9595boutonObjects3.length = 0;
gdjs.codeCode.GDNewTextObjects1.length = 0;
gdjs.codeCode.GDNewTextObjects2.length = 0;
gdjs.codeCode.GDNewTextObjects3.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects1.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects2.length = 0;
gdjs.codeCode.GDbouton_9595retourObjects3.length = 0;
gdjs.codeCode.GDcoffreObjects1.length = 0;
gdjs.codeCode.GDcoffreObjects2.length = 0;
gdjs.codeCode.GDcoffreObjects3.length = 0;
gdjs.codeCode.GDsp_9595diamantObjects1.length = 0;
gdjs.codeCode.GDsp_9595diamantObjects2.length = 0;
gdjs.codeCode.GDsp_9595diamantObjects3.length = 0;
gdjs.codeCode.GDGreenDotBarObjects1.length = 0;
gdjs.codeCode.GDGreenDotBarObjects2.length = 0;
gdjs.codeCode.GDGreenDotBarObjects3.length = 0;
gdjs.codeCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.codeCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.codeCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.codeCode.GDpapierObjects1.length = 0;
gdjs.codeCode.GDpapierObjects2.length = 0;
gdjs.codeCode.GDpapierObjects3.length = 0;
gdjs.codeCode.GDtext_9595modeleObjects1.length = 0;
gdjs.codeCode.GDtext_9595modeleObjects2.length = 0;
gdjs.codeCode.GDtext_9595modeleObjects3.length = 0;
gdjs.codeCode.GDclavierObjects1.length = 0;
gdjs.codeCode.GDclavierObjects2.length = 0;
gdjs.codeCode.GDclavierObjects3.length = 0;


return;

}

gdjs['codeCode'] = gdjs.codeCode;
