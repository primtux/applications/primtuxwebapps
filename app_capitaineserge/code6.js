gdjs.tirage_95abscisseCode = {};
gdjs.tirage_95abscisseCode.GDbouton_95retourObjects1= [];
gdjs.tirage_95abscisseCode.GDbouton_95retourObjects2= [];
gdjs.tirage_95abscisseCode.GDbouton_95retourObjects3= [];
gdjs.tirage_95abscisseCode.GDscoreObjects1= [];
gdjs.tirage_95abscisseCode.GDscoreObjects2= [];
gdjs.tirage_95abscisseCode.GDscoreObjects3= [];
gdjs.tirage_95abscisseCode.GDileObjects1= [];
gdjs.tirage_95abscisseCode.GDileObjects2= [];
gdjs.tirage_95abscisseCode.GDileObjects3= [];
gdjs.tirage_95abscisseCode.GDile2Objects1= [];
gdjs.tirage_95abscisseCode.GDile2Objects2= [];
gdjs.tirage_95abscisseCode.GDile2Objects3= [];
gdjs.tirage_95abscisseCode.GDile3Objects1= [];
gdjs.tirage_95abscisseCode.GDile3Objects2= [];
gdjs.tirage_95abscisseCode.GDile3Objects3= [];
gdjs.tirage_95abscisseCode.GDile4Objects1= [];
gdjs.tirage_95abscisseCode.GDile4Objects2= [];
gdjs.tirage_95abscisseCode.GDile4Objects3= [];
gdjs.tirage_95abscisseCode.GDile5Objects1= [];
gdjs.tirage_95abscisseCode.GDile5Objects2= [];
gdjs.tirage_95abscisseCode.GDile5Objects3= [];
gdjs.tirage_95abscisseCode.GDile6Objects1= [];
gdjs.tirage_95abscisseCode.GDile6Objects2= [];
gdjs.tirage_95abscisseCode.GDile6Objects3= [];
gdjs.tirage_95abscisseCode.GDfaux_95vraiObjects1= [];
gdjs.tirage_95abscisseCode.GDfaux_95vraiObjects2= [];
gdjs.tirage_95abscisseCode.GDfaux_95vraiObjects3= [];
gdjs.tirage_95abscisseCode.GDfond_95blancObjects1= [];
gdjs.tirage_95abscisseCode.GDfond_95blancObjects2= [];
gdjs.tirage_95abscisseCode.GDfond_95blancObjects3= [];
gdjs.tirage_95abscisseCode.GDfond_95boisObjects1= [];
gdjs.tirage_95abscisseCode.GDfond_95boisObjects2= [];
gdjs.tirage_95abscisseCode.GDfond_95boisObjects3= [];
gdjs.tirage_95abscisseCode.GDvieObjects1= [];
gdjs.tirage_95abscisseCode.GDvieObjects2= [];
gdjs.tirage_95abscisseCode.GDvieObjects3= [];
gdjs.tirage_95abscisseCode.GDdrapeauObjects1= [];
gdjs.tirage_95abscisseCode.GDdrapeauObjects2= [];
gdjs.tirage_95abscisseCode.GDdrapeauObjects3= [];
gdjs.tirage_95abscisseCode.GDserge_95capitaineObjects1= [];
gdjs.tirage_95abscisseCode.GDserge_95capitaineObjects2= [];
gdjs.tirage_95abscisseCode.GDserge_95capitaineObjects3= [];
gdjs.tirage_95abscisseCode.GDaide_95sergeObjects1= [];
gdjs.tirage_95abscisseCode.GDaide_95sergeObjects2= [];
gdjs.tirage_95abscisseCode.GDaide_95sergeObjects3= [];
gdjs.tirage_95abscisseCode.GDvert_95abscisseObjects1= [];
gdjs.tirage_95abscisseCode.GDvert_95abscisseObjects2= [];
gdjs.tirage_95abscisseCode.GDvert_95abscisseObjects3= [];
gdjs.tirage_95abscisseCode.GDjaune_95ordonneeObjects1= [];
gdjs.tirage_95abscisseCode.GDjaune_95ordonneeObjects2= [];
gdjs.tirage_95abscisseCode.GDjaune_95ordonneeObjects3= [];
gdjs.tirage_95abscisseCode.GDpelle_95carteObjects1= [];
gdjs.tirage_95abscisseCode.GDpelle_95carteObjects2= [];
gdjs.tirage_95abscisseCode.GDpelle_95carteObjects3= [];
gdjs.tirage_95abscisseCode.GDbouton_95continuerObjects1= [];
gdjs.tirage_95abscisseCode.GDbouton_95continuerObjects2= [];
gdjs.tirage_95abscisseCode.GDbouton_95continuerObjects3= [];
gdjs.tirage_95abscisseCode.GDbarreObjects1= [];
gdjs.tirage_95abscisseCode.GDbarreObjects2= [];
gdjs.tirage_95abscisseCode.GDbarreObjects3= [];
gdjs.tirage_95abscisseCode.GDfond_95menuObjects1= [];
gdjs.tirage_95abscisseCode.GDfond_95menuObjects2= [];
gdjs.tirage_95abscisseCode.GDfond_95menuObjects3= [];

gdjs.tirage_95abscisseCode.conditionTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition0IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition1IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition2IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition3IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition4IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition5IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.condition6IsTrue_0 = {val:false};
gdjs.tirage_95abscisseCode.conditionTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition0IsTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition1IsTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition2IsTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition3IsTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition4IsTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition5IsTrue_1 = {val:false};
gdjs.tirage_95abscisseCode.condition6IsTrue_1 = {val:false};


gdjs.tirage_95abscisseCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95abscisseCode.conditionTrue_1 = gdjs.tirage_95abscisseCode.condition0IsTrue_0;
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95abscisseCode.eventsList1 = function(runtimeScene) {

{


{
}

}


};gdjs.tirage_95abscisseCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95abscisseCode.conditionTrue_1 = gdjs.tirage_95abscisseCode.condition0IsTrue_0;
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}}
if (gdjs.tirage_95abscisseCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}
{ //Subevents
gdjs.tirage_95abscisseCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.tirage_95abscisseCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(13);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95abscisseCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95abscisseCode.conditionTrue_1 = gdjs.tirage_95abscisseCode.condition0IsTrue_0;
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition1IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}}
}
if (gdjs.tirage_95abscisseCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95abscisseCode.eventsList3(runtimeScene);} //End of subevents
}

}


};gdjs.tirage_95abscisseCode.eventsList5 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95abscisseCode.conditionTrue_1 = gdjs.tirage_95abscisseCode.condition0IsTrue_0;
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition3IsTrue_1.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a4")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition3IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition1IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition2IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a4")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}}
}
}
if (gdjs.tirage_95abscisseCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a5").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95abscisseCode.eventsList6 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95abscisseCode.conditionTrue_1 = gdjs.tirage_95abscisseCode.condition0IsTrue_0;
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition3IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition4IsTrue_1.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a4")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a5")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
if( gdjs.tirage_95abscisseCode.condition4IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition3IsTrue_0.val = false;
gdjs.tirage_95abscisseCode.condition4IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition1IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition2IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a4")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}if ( gdjs.tirage_95abscisseCode.condition3IsTrue_0.val ) {
{
gdjs.tirage_95abscisseCode.condition4IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a5")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0"));
}}
}
}
}
if (gdjs.tirage_95abscisseCode.condition4IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a6").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95abscisseCode.eventsList7 = function(runtimeScene) {

{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95abscisseCode.conditionTrue_1 = gdjs.tirage_95abscisseCode.condition0IsTrue_0;
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition3IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition4IsTrue_1.val = false;
gdjs.tirage_95abscisseCode.condition5IsTrue_1.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage_95abscisseCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage_95abscisseCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage_95abscisseCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage_95abscisseCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage_95abscisseCode.condition4IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95abscisseCode.condition5IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 11;
if( gdjs.tirage_95abscisseCode.condition5IsTrue_1.val ) {
    gdjs.tirage_95abscisseCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0").setNumber(gdjs.randomInRange(1, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("a0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95abscisseCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95abscisseCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95abscisseCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 10;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95abscisseCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 12;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95abscisseCode.eventsList6(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95abscisseCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 13;
}if (gdjs.tirage_95abscisseCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage_ordonnee", false);
}}

}


};

gdjs.tirage_95abscisseCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirage_95abscisseCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDbouton_95retourObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDscoreObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDscoreObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDscoreObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDileObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDileObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDileObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDile2Objects1.length = 0;
gdjs.tirage_95abscisseCode.GDile2Objects2.length = 0;
gdjs.tirage_95abscisseCode.GDile2Objects3.length = 0;
gdjs.tirage_95abscisseCode.GDile3Objects1.length = 0;
gdjs.tirage_95abscisseCode.GDile3Objects2.length = 0;
gdjs.tirage_95abscisseCode.GDile3Objects3.length = 0;
gdjs.tirage_95abscisseCode.GDile4Objects1.length = 0;
gdjs.tirage_95abscisseCode.GDile4Objects2.length = 0;
gdjs.tirage_95abscisseCode.GDile4Objects3.length = 0;
gdjs.tirage_95abscisseCode.GDile5Objects1.length = 0;
gdjs.tirage_95abscisseCode.GDile5Objects2.length = 0;
gdjs.tirage_95abscisseCode.GDile5Objects3.length = 0;
gdjs.tirage_95abscisseCode.GDile6Objects1.length = 0;
gdjs.tirage_95abscisseCode.GDile6Objects2.length = 0;
gdjs.tirage_95abscisseCode.GDile6Objects3.length = 0;
gdjs.tirage_95abscisseCode.GDfaux_95vraiObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDfaux_95vraiObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDfaux_95vraiObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95blancObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95blancObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95blancObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95boisObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95boisObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95boisObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDvieObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDvieObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDvieObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDdrapeauObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDdrapeauObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDdrapeauObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDserge_95capitaineObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDserge_95capitaineObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDserge_95capitaineObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDaide_95sergeObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDaide_95sergeObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDaide_95sergeObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDvert_95abscisseObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDvert_95abscisseObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDvert_95abscisseObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDjaune_95ordonneeObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDjaune_95ordonneeObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDjaune_95ordonneeObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDpelle_95carteObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDpelle_95carteObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDpelle_95carteObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDbouton_95continuerObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDbouton_95continuerObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDbouton_95continuerObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDbarreObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDbarreObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDbarreObjects3.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95menuObjects1.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95menuObjects2.length = 0;
gdjs.tirage_95abscisseCode.GDfond_95menuObjects3.length = 0;

gdjs.tirage_95abscisseCode.eventsList7(runtimeScene);

return;

}

gdjs['tirage_95abscisseCode'] = gdjs.tirage_95abscisseCode;
