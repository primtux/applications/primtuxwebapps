gdjs.tirage_95ordonneeCode = {};
gdjs.tirage_95ordonneeCode.GDbouton_95retourObjects1= [];
gdjs.tirage_95ordonneeCode.GDbouton_95retourObjects2= [];
gdjs.tirage_95ordonneeCode.GDbouton_95retourObjects3= [];
gdjs.tirage_95ordonneeCode.GDscoreObjects1= [];
gdjs.tirage_95ordonneeCode.GDscoreObjects2= [];
gdjs.tirage_95ordonneeCode.GDscoreObjects3= [];
gdjs.tirage_95ordonneeCode.GDileObjects1= [];
gdjs.tirage_95ordonneeCode.GDileObjects2= [];
gdjs.tirage_95ordonneeCode.GDileObjects3= [];
gdjs.tirage_95ordonneeCode.GDile2Objects1= [];
gdjs.tirage_95ordonneeCode.GDile2Objects2= [];
gdjs.tirage_95ordonneeCode.GDile2Objects3= [];
gdjs.tirage_95ordonneeCode.GDile3Objects1= [];
gdjs.tirage_95ordonneeCode.GDile3Objects2= [];
gdjs.tirage_95ordonneeCode.GDile3Objects3= [];
gdjs.tirage_95ordonneeCode.GDile4Objects1= [];
gdjs.tirage_95ordonneeCode.GDile4Objects2= [];
gdjs.tirage_95ordonneeCode.GDile4Objects3= [];
gdjs.tirage_95ordonneeCode.GDile5Objects1= [];
gdjs.tirage_95ordonneeCode.GDile5Objects2= [];
gdjs.tirage_95ordonneeCode.GDile5Objects3= [];
gdjs.tirage_95ordonneeCode.GDile6Objects1= [];
gdjs.tirage_95ordonneeCode.GDile6Objects2= [];
gdjs.tirage_95ordonneeCode.GDile6Objects3= [];
gdjs.tirage_95ordonneeCode.GDfaux_95vraiObjects1= [];
gdjs.tirage_95ordonneeCode.GDfaux_95vraiObjects2= [];
gdjs.tirage_95ordonneeCode.GDfaux_95vraiObjects3= [];
gdjs.tirage_95ordonneeCode.GDfond_95blancObjects1= [];
gdjs.tirage_95ordonneeCode.GDfond_95blancObjects2= [];
gdjs.tirage_95ordonneeCode.GDfond_95blancObjects3= [];
gdjs.tirage_95ordonneeCode.GDfond_95boisObjects1= [];
gdjs.tirage_95ordonneeCode.GDfond_95boisObjects2= [];
gdjs.tirage_95ordonneeCode.GDfond_95boisObjects3= [];
gdjs.tirage_95ordonneeCode.GDvieObjects1= [];
gdjs.tirage_95ordonneeCode.GDvieObjects2= [];
gdjs.tirage_95ordonneeCode.GDvieObjects3= [];
gdjs.tirage_95ordonneeCode.GDdrapeauObjects1= [];
gdjs.tirage_95ordonneeCode.GDdrapeauObjects2= [];
gdjs.tirage_95ordonneeCode.GDdrapeauObjects3= [];
gdjs.tirage_95ordonneeCode.GDserge_95capitaineObjects1= [];
gdjs.tirage_95ordonneeCode.GDserge_95capitaineObjects2= [];
gdjs.tirage_95ordonneeCode.GDserge_95capitaineObjects3= [];
gdjs.tirage_95ordonneeCode.GDaide_95sergeObjects1= [];
gdjs.tirage_95ordonneeCode.GDaide_95sergeObjects2= [];
gdjs.tirage_95ordonneeCode.GDaide_95sergeObjects3= [];
gdjs.tirage_95ordonneeCode.GDvert_95abscisseObjects1= [];
gdjs.tirage_95ordonneeCode.GDvert_95abscisseObjects2= [];
gdjs.tirage_95ordonneeCode.GDvert_95abscisseObjects3= [];
gdjs.tirage_95ordonneeCode.GDjaune_95ordonneeObjects1= [];
gdjs.tirage_95ordonneeCode.GDjaune_95ordonneeObjects2= [];
gdjs.tirage_95ordonneeCode.GDjaune_95ordonneeObjects3= [];
gdjs.tirage_95ordonneeCode.GDpelle_95carteObjects1= [];
gdjs.tirage_95ordonneeCode.GDpelle_95carteObjects2= [];
gdjs.tirage_95ordonneeCode.GDpelle_95carteObjects3= [];
gdjs.tirage_95ordonneeCode.GDbouton_95continuerObjects1= [];
gdjs.tirage_95ordonneeCode.GDbouton_95continuerObjects2= [];
gdjs.tirage_95ordonneeCode.GDbouton_95continuerObjects3= [];
gdjs.tirage_95ordonneeCode.GDbarreObjects1= [];
gdjs.tirage_95ordonneeCode.GDbarreObjects2= [];
gdjs.tirage_95ordonneeCode.GDbarreObjects3= [];
gdjs.tirage_95ordonneeCode.GDfond_95menuObjects1= [];
gdjs.tirage_95ordonneeCode.GDfond_95menuObjects2= [];
gdjs.tirage_95ordonneeCode.GDfond_95menuObjects3= [];

gdjs.tirage_95ordonneeCode.conditionTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition0IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition1IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition2IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition3IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition4IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition5IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.condition6IsTrue_0 = {val:false};
gdjs.tirage_95ordonneeCode.conditionTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition0IsTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition1IsTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition2IsTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition3IsTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition4IsTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition5IsTrue_1 = {val:false};
gdjs.tirage_95ordonneeCode.condition6IsTrue_1 = {val:false};


gdjs.tirage_95ordonneeCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ordonneeCode.conditionTrue_1 = gdjs.tirage_95ordonneeCode.condition0IsTrue_0;
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95ordonneeCode.eventsList1 = function(runtimeScene) {

{


{
}

}


};gdjs.tirage_95ordonneeCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ordonneeCode.conditionTrue_1 = gdjs.tirage_95ordonneeCode.condition0IsTrue_0;
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}}
if (gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}
{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.tirage_95ordonneeCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(13);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95ordonneeCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ordonneeCode.conditionTrue_1 = gdjs.tirage_95ordonneeCode.condition0IsTrue_0;
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}}
}
if (gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList3(runtimeScene);} //End of subevents
}

}


};gdjs.tirage_95ordonneeCode.eventsList5 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ordonneeCode.conditionTrue_1 = gdjs.tirage_95ordonneeCode.condition0IsTrue_0;
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b4")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition3IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b4")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}}
}
}
if (gdjs.tirage_95ordonneeCode.condition3IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b5").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95ordonneeCode.eventsList6 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ordonneeCode.conditionTrue_1 = gdjs.tirage_95ordonneeCode.condition0IsTrue_0;
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition4IsTrue_1.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b4")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b5")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
if( gdjs.tirage_95ordonneeCode.condition4IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition3IsTrue_0.val = false;
gdjs.tirage_95ordonneeCode.condition4IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition1IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition2IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b4")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}if ( gdjs.tirage_95ordonneeCode.condition3IsTrue_0.val ) {
{
gdjs.tirage_95ordonneeCode.condition4IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b5")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0"));
}}
}
}
}
if (gdjs.tirage_95ordonneeCode.condition4IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b6").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.tirage_95ordonneeCode.eventsList7 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 1;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "bateau_entrainement", false);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 2;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "ile_entrainement", false);
}}

}


};gdjs.tirage_95ordonneeCode.eventsList8 = function(runtimeScene) {

{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ordonneeCode.conditionTrue_1 = gdjs.tirage_95ordonneeCode.condition0IsTrue_0;
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition4IsTrue_1.val = false;
gdjs.tirage_95ordonneeCode.condition5IsTrue_1.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage_95ordonneeCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage_95ordonneeCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage_95ordonneeCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage_95ordonneeCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage_95ordonneeCode.condition4IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ordonneeCode.condition5IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 11;
if( gdjs.tirage_95ordonneeCode.condition5IsTrue_1.val ) {
    gdjs.tirage_95ordonneeCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0").setNumber(gdjs.randomInRange(1, gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0))));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("b0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList4(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 10;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList5(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 12;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList6(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 13;
}if (gdjs.tirage_95ordonneeCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ordonneeCode.eventsList7(runtimeScene);} //End of subevents
}

}


};

gdjs.tirage_95ordonneeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirage_95ordonneeCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDbouton_95retourObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDscoreObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDscoreObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDscoreObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDileObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDileObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDileObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDile2Objects1.length = 0;
gdjs.tirage_95ordonneeCode.GDile2Objects2.length = 0;
gdjs.tirage_95ordonneeCode.GDile2Objects3.length = 0;
gdjs.tirage_95ordonneeCode.GDile3Objects1.length = 0;
gdjs.tirage_95ordonneeCode.GDile3Objects2.length = 0;
gdjs.tirage_95ordonneeCode.GDile3Objects3.length = 0;
gdjs.tirage_95ordonneeCode.GDile4Objects1.length = 0;
gdjs.tirage_95ordonneeCode.GDile4Objects2.length = 0;
gdjs.tirage_95ordonneeCode.GDile4Objects3.length = 0;
gdjs.tirage_95ordonneeCode.GDile5Objects1.length = 0;
gdjs.tirage_95ordonneeCode.GDile5Objects2.length = 0;
gdjs.tirage_95ordonneeCode.GDile5Objects3.length = 0;
gdjs.tirage_95ordonneeCode.GDile6Objects1.length = 0;
gdjs.tirage_95ordonneeCode.GDile6Objects2.length = 0;
gdjs.tirage_95ordonneeCode.GDile6Objects3.length = 0;
gdjs.tirage_95ordonneeCode.GDfaux_95vraiObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDfaux_95vraiObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDfaux_95vraiObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95blancObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95blancObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95blancObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95boisObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95boisObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95boisObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDvieObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDvieObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDvieObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDdrapeauObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDdrapeauObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDdrapeauObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDserge_95capitaineObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDserge_95capitaineObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDserge_95capitaineObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDaide_95sergeObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDaide_95sergeObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDaide_95sergeObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDvert_95abscisseObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDvert_95abscisseObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDvert_95abscisseObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDjaune_95ordonneeObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDjaune_95ordonneeObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDjaune_95ordonneeObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDpelle_95carteObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDpelle_95carteObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDpelle_95carteObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDbouton_95continuerObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDbouton_95continuerObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDbouton_95continuerObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDbarreObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDbarreObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDbarreObjects3.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95menuObjects1.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95menuObjects2.length = 0;
gdjs.tirage_95ordonneeCode.GDfond_95menuObjects3.length = 0;

gdjs.tirage_95ordonneeCode.eventsList8(runtimeScene);

return;

}

gdjs['tirage_95ordonneeCode'] = gdjs.tirage_95ordonneeCode;
