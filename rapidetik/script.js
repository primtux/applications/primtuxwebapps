// RapidÉtik est un logiciel libre.
// Licence GNU GPL
// Arnaud Champollion

// Listes
let emplacements=[];

// Éléments DOM
const divPhrase = document.getElementById('phrase');
const accueil = document.getElementById('accueil');
const divEtiquettes = document.getElementById('etiquettes');
const divConsigne = document.getElementById('consigne');
const bouton_phrase = document.getElementById('bouton_phrase');
const bouton_check = document.getElementById('bouton_check');
const bouton_suivant = document.getElementById('exercice_suivant');
const bouton_menu = document.getElementById('bouton_menu');
const bouton_consigne = document.getElementById('bouton_consigne');
const bouton_entendre_consigne = document.getElementById('bouton_entendre_consigne');
const bouton_entendre_phrase = document.getElementById('bouton_entendre_phrase');
const bouton_partager_exercice = document.getElementById('bouton_partager_exercice');
const bouton_terminer_exercice = document.getElementById('bouton_terminer_exercice');
const bouton_imprimer_exercice = document.getElementById('bouton_imprimer_exercice');
const case_chiffrement = document.getElementById('chiffrement');
const qrcodeDiv = document.getElementById('qrcode');
const speaker = document.getElementById('speaker');
const mini_hautparleur = document.getElementById('mini_haut_parleur');
const input_phrase = document.getElementById('input_phrase');
const texte_accueil = document.getElementById('texte_accueil');
const zone_partage = document.getElementById('zone_partage');
const zone_lien = document.getElementById('zone_lien');
const zone_apropos = document.getElementById('apropos');
const menu = document.getElementById('menu');
const menu_choix_langues = document.getElementById('menu_choix_langues');
const bouton_choix_langue = document.getElementById('bouton_choix_langue');
const boutonEtiquettesSeules = document.getElementById('choix-etiquettes-seules');

const body = document.body;

//Réglages de départ
let langue='fr-FR'
let menu_ouvert=false;
let consigne_ouverte=false;
let menu_choix_langues_ouvert=false;
let hauteur=0;
let mode_exercice = 'phrase_en_desordre';
let chiffrement = true;
let mode_multi=false;
let mode_partage='exercice';
let etiquettesSeules = false;
bouton_partager_exercice.disabled=true;
bouton_terminer_exercice.disabled=true;
bouton_imprimer_exercice.disabled=true;

let tablette_smartphone;
tablette_smartphone = "ontouchstart" in document.documentElement;


// Réglages d'après l'URL

// Récupération de l'URL actuelle
let url = new URL(window.location.href);
// Vérification si le paramètre "langue" est présent dans l'URL
if (url.searchParams.has('langue')) {change_langue(url.searchParams.get('langue'),'auto');}

// PRIMTUX

    body.style.background='none';
    body.style.backgroundImage='url(images/primtux.png)';
    input_phrase.style.width='calc(100% - 88px)';
    input_phrase.style.marginLeft='80px';
    bouton_choix_langue.style.top = menu_choix_langues.style.top ='150px';
    mini_hautparleur.style.top ='210px';


if (url.searchParams.get('etiquettes-seules')==="true") {
  etiquettesSeules = true;
}

boutonEtiquettesSeules.checked = etiquettesSeules;

// Focus sur la zone de saisie
input_phrase.focus();

// En cas de problème avec la récupération du fichier MarkDown
const defaultData = "- Ceci est une phrase par défaut";

// Consigne orale
consigne="Place les étiquettes sur les emplacements dans le bon ordre pour former la phrase demandée. Tu peux écouter la phrase en cliquant sur le bouton oreille. Quand tu as terminé clique tout en bas, sur le bouton valider."
son_consigne = new SpeechSynthesisUtterance(consigne);
son_consigne.pitch=1;
son_consigne.rate=0.8;
son_consigne.lang='fr-FR';
consigne_speaker="Déplace une étiquette ici pour écouter le mot."
son_speaker = new SpeechSynthesisUtterance(consigne_speaker);
son_speaker.pitch=1;
son_speaker.rate=0.8;
son_speaker.lang='fr-FR'; 

// Si un exercice est dans l'URL lancer directement la création des éqieuettes
if (test_url()){
  console.log('présence de hash')
  cree_etiquettes('par lien');
} else {
  accueil.style.display='block';
}

// Vérification d'un exercice présent dans l'URL
function test_url() {
  const hash = window.location.hash;
  if (hash) {return true}
  else {return false}
}

// Récupération de la phrase depuis l'URL
function get_exercice(url_phrase) {
  console.log('get exercice')
  const hash = window.location.hash;
  let phrase_a_retourner;
  if (url_phrase){
    let exercice = getMarkdownContent(url_phrase);
    lien_codim=url_phrase;
			return exercice;
  } else if (hash) {
    chaine_a_examiner = decodeURIComponent(hash.substring(1));
    console.log('chaine à examiner '+chaine_a_examiner)
    //Si http après le hash, on va chercher la phrase dans un fichier MD
    if (chaine_a_examiner.startsWith('http')){
      mode_multi=true;      
			let exercice = getMarkdownContent(chaine_a_examiner);
      lien_codim=chaine_a_examiner;
			return exercice;
		}
    // Si pas de http, on prend la phrase directe
    else {
      
      if (chaine_a_examiner.endsWith("/")) {phrase_a_retourner = chaine_a_examiner.slice(0, -1);}
      else {phrase_a_retourner = chaine_a_examiner}

      // Déchiffrement
      // Vérification si le paramètre "langue" est déjà présent dans l'URL
      if (url.searchParams.has('chiffrement')){
        if (url.searchParams.get('chiffrement')==='64') {
          phrase_a_retourner=decodeURIComponent(atob(phrase_a_retourner));
        } else {
        phrase_a_retourner=dechiffrementCesar(phrase_a_retourner,3);
        url.searchParams.delete('chiffrement');
        }
      }
		console.log('phrase à retourner '+phrase_a_retourner);
    // On nettoie l'URL pour que l'élève ne lise pas la phrase dedans ;)
    // Nettoyer l'URL après le symbole dièse (#)
    let nouvelle_url = (url.toString()).split('#')[0];
    // Remplacer l'URL actuelle
    window.history.replaceState({}, document.title, nouvelle_url);
    return phrase_a_retourner;
      }
    
  }
} 

function changeEtiquettesSeules() {
  etiquettesSeules = boutonEtiquettesSeules.checked;
  console.log('etiquette seules = '+etiquettesSeules);
}


function getMarkdownContent(urlMD) {
  console.log('getMarkdownContent '+urlMD)
	// Récupération du markdown externe
	if (urlMD !== "") {
    if (urlMD.endsWith('/')) {
      // Supprime le dernier caractère / si présent
      urlMD = urlMD.slice(0, -1);
    }
		// Gestion des fichiers hébergés sur github
		if (urlMD.startsWith("https://github.com")) {
			urlMD = urlMD.replace(
				"https://github.com",
				"https://raw.githubusercontent.com"
			);
			urlMD = urlMD.replace("/blob/", "/");      
		}
		// Gestion des fichiers hébergés sur codiMD
		if (
			urlMD.startsWith("https://codimd") &&
			urlMD.indexOf("download") === -1
		) {
			urlMD =
				urlMD.replace("?edit", "").replace("?both", "").replace("?view", "").replace("#","") +
				"/download";
		}
		// Récupération du contenu du fichier
		let promise = fetch(urlMD)
			.then((response) => {
				return response.text();
			}).then((texte) => {
				return ligneAleatoire(texte);
				})
			.catch((error) => {
				alert("Il y a une erreur dans l'URL. Merci de la vérifier et de vous assurer que le fichier est bien accessible.")
				console.log(error);
			});
		return promise;
	} else {
		return ligneAleatoire(defaultData);
	}
}

function uneAutre() {
  etiquettes.forEach((element) => {
    element.remove();  
  });
  spanReponse.remove();
  input_phrase.value=autreLigneAleatoire();
  cree_etiquettes();
}



function autreLigneAleatoire() {
  if (phrases.length===0){phrases=lignesValides.slice()}
  const indexAleatoire = Math.floor(Math.random() * phrases.length);
  let phrase_selectionnee = phrases[indexAleatoire];
  phrases.splice(indexAleatoire,1);
  return phrase_selectionnee;
}


function ligneAleatoire(content) {
  console.log('fonction ligne aléatoire '+content)
  // Divise le texte en lignes
  const lignes = content.split('\n');
  // Ne conserver que les lignes qui commencent par "- "
  lignesValides = lignes.filter(ligne => ligne.trim().startsWith("- "));
  // Vérifier s'il y a des lignes valides  
  if (lignesValides.length === 0) {
    alert("Aucune ligne valide trouvée.");
    return "";
  }
  for (let i = 0; i < lignesValides.length; i++) {
    lignesValides[i] = lignesValides[i].substring(2); // Supprime les deux premiers caractères
  }
  phrases = lignesValides.slice();
  // Sélectionne une ligne au hasard
  const indexAleatoire = Math.floor(Math.random() * lignesValides.length);
  // Retourne la ligne sélectionnée
  phrase_sauvegardee = lignesValides[indexAleatoire];
  phrases.splice(indexAleatoire,1)
  return phrase_sauvegardee;
}

function verifie_jocker(phrase) {
  // Compter le nombre d'occurrences du caractère '*'
  const occurrences = (phrase.match(/\*/g) || []).length;
  console.log("verifie jocker "+phrase)
  // Vérifier si le nombre d'occurrences est pair et non nul
  if (occurrences > 0 && occurrences % 2 === 0) {
      mode_exercice = 'phrase_a_trous';
      return true; // Nombre pair non nul d'occurrences de '*'
  } else {
      mode_exercice = 'phrase_en_desordre';
      return false; // Autre cas
  }
}


// Création des étiquettes
function cree_etiquettes(mode){    
    if (menu_ouvert){ouvre_menu()} //ferme le menu s'il est ouvert
    if (menu_choix_langues_ouvert){ouvre_menu_choix_langues()} //ferme le menu s'il est ouvert
    if (mode==="par lien"){phrase= get_exercice();phrase_sauvegardee = phrase} // récupère la phrase en lien
    else { // récupère la phrase entrée
      phrase=document.getElementById('input_phrase').value;
      if (phrase.startsWith('http')){phrase= get_exercice(phrase);mode_multi=true;}
      else {mode_multi=false;}
      phrase_sauvegardee = phrase;      
    }
    console.log('phrase récupérée '+phrase)
    if (phrase){
		Promise.resolve(phrase).then((phrase) => {
			if (!phrase) {
      phrase = ligneAleatoire(defaultData);}
			traiter_phrase(phrase);
			});
	}
        
    else {alert('Veuillez saisir une phrase.');}

}

function traiter_phrase(phrase) {
        etiquettes_a_creer=[];
	      // Fermer les panneaux si nécessaire
        zone_apropos.style.display=null;
        zone_partage.style.display=null;        
        //Supprimer les espaces inutiles
        phrase = phrase.replace(/ +/g, ' ').trim();
        //Découper la phrase en mots selon les espaces
        
        // Expression régulière pour découper la chaîne en mots tout en tenant compte des parenthèses
        const regex_1 = /\(([^)]+?)\)|\S+/g;
        // Récupérer les correspondances de la chaîne en utilisant la regex
        const motsDebut = phrase.match(regex_1);
        // Supprimer les parenthèses des mots qui en contiennent
        const motsFinaux = motsDebut.map(mot => (mot.startsWith("(") && mot.endsWith(")")) ? mot.slice(1, -1) : mot);

        etiquettes_a_creer = mots = motsFinaux;
        
        // Si la phrase contient des * , création d'un exercice de type "phrase à trous".
        if (verifie_jocker(phrase)){
          etiquettes_a_creer=[];
        // On ne garde que les mots encadrés par les *.
          mots = [];
          contenu_entre_etoiles = /\*([^*]+)\*/g;
          while ((match = contenu_entre_etoiles.exec(phrase)) !== null) {
            let liste_propositions = match[1].split('/');
            console.log("liste propositions "+liste_propositions)
            liste_propositions.forEach(function(element){etiquettes_a_creer.push(element)});            
            mots.push(liste_propositions[0]);
          }
        } // Fin de la fonction *

        // Phrase pour la synthèse vocale
        phrase_a_prononcer=phrase;

        let regex = /^[0-9\s]+$/;
        
        // Pour les chaînes de nombres
        if (regex.test(phrase)) {
          // Remise à zéro de la phrase à prononcer
          phrase_a_prononcer="";
          // Parcours des éléments du tableau
          for (var i = 0; i < mots.length; i++) {
            // Ajoute des guillemets autour de chaque élément du tableau
            phrase_a_prononcer += '(' + mots[i] + ')';
            // Ajoute un espace sauf pour le dernier élément
            if (i !== mots.length - 1) {
              phrase_a_prononcer += ' ';
            }
          }
          console.log('phrase nombre '+phrase_a_prononcer)
        }


        // Pour les phrases à trous
        if (mode_exercice==='phrase_a_trous'){
          phrase_a_prononcer=phrase_a_prononcer.replace(contenu_entre_etoiles, '!!');;
        }
        console.log("phrase à prononcer "+phrase_a_prononcer)

        accueil.style.display='none';
        divPhrase.style.display='block';
        divEtiquettes.style.display='block';        
        mini_hautparleur.style.display='none';
        bouton_choix_langue.style.display='none';


        // Pour chacun des mots
        etiquettes_a_creer.forEach(function(mot) { // pour chaque mot de la phrase
            // Création d'une étiquette span pour chaque mot
            let etiquette = document.createElement('span');
            etiquette.textContent = mot;
            // Ajout de classes de styles
            etiquette.classList.add('etiquette');
            etiquette.classList.add('draggable');
            etiquette.classList.add('libre');           
            // Ajout de l'étiquette à la div des étiquettes
            divEtiquettes.appendChild(etiquette);
            place_etiquette(etiquette);
        // Préparation de la synthèse vocale    
        // à virer if (mode==='par lien'){phrase=mots.join(" ");}
        son_phrase = new SpeechSynthesisUtterance(phrase_a_prononcer);
        son_phrase.pitch=1;
        son_phrase.rate=1;
        son_phrase.lang=langue;         
                       
        });
        etiquettes = document.querySelectorAll('.etiquette'); //création du tableau étiquettes
        adapte_taille_police();          


        if (!etiquettesSeules){cree_silhouettes()};
        dimensions_etiquette=etiquettes[0].getBoundingClientRect();
        if(!etiquettesSeules){bouton_check.style.display="block"};
        if (mode_multi && !etiquettesSeules) {bouton_suivant.style.display="block";}
        bouton_partager_exercice.disabled=false;
        bouton_terminer_exercice.disabled=false;
        bouton_imprimer_exercice.disabled=false;
        if (!etiquettesSeules){bouton_entendre_phrase.style.display="block"};
        if(!etiquettesSeules){bouton_consigne.style.display="block"};
        speaker.style.display="block";
        speakerRect=speaker.getBoundingClientRect();
        lien_a_partager=cree_lien();
}

// Position aléatoire des étiquettes
function place_etiquette(etiquette){
    let collision=true; 
    let essais = 0; 
    // Cherche une position pour l'étiquette en évitant les chevauchements (dans la limite de 500 essais)      
    while (collision && essais<500){
        let posX = Math.floor(Math.random() * (divEtiquettes.clientWidth - etiquette.clientWidth)); // Position horizontale aléatoire
        let posY = Math.floor(Math.random() * (divEtiquettes.clientHeight - etiquette.clientHeight)); // Position verticale aléatoire
        etiquette.style.left = posX + 'px';
        etiquette.style.top = posY + 'px';        
        collision=checkCollisions(etiquette);
        essais+=1;
    }
}

let lecture_en_cours;
// Synthèse vocale
function entendre(contenu){
  if (speechSynthesis.speaking) {
    speechSynthesis.cancel();
    if (contenu!=lecture_en_cours) {
      speechSynthesis.speak(contenu);
      lecture_en_cours=contenu;
    }
  } else {
    speechSynthesis.speak(contenu);
    lecture_en_cours=contenu;
  } 
}

// Création des emplacements pour les réponses
function cree_silhouettes(){
  spanReponse = document.createElement('p');
  spanReponse.id = 'reponse';
  divPhrase.appendChild(spanReponse);
  reponse = document.getElementById('reponse');
  if (mode_exercice==="phrase_a_trous"){
    phrase = phrase_sauvegardee;
    console.log("phrase= "+phrase)
    spanReponse.innerHTML+=phrase.replace(contenu_entre_etoiles, '_');  
  }  
  mots.forEach(function(mot) {
      let emplacement = document.createElement('span');
      emplacement.classList.add('emplacement');
      emplacement.style.height=(etiquettelongue.offsetHeight)+'px';
      spanReponse.appendChild(emplacement);
          })
  emplacements=document.querySelectorAll('.emplacement');
  
  if (mode_exercice==='phrase_a_trous'){
    // Obtient le contenu de l'élément spanReponse
    var contenu = spanReponse.innerHTML;

    // Sépare le contenu à chaque caractère "_"
    let  parties = contenu.split('<span');
    parties = parties.shift();
    parties = parties.split('_');
    console.log("parties : "+parties);

    spanReponse.innerHTML='';

    // Boucle à travers chaque partie et déplace l'élément avec la classe "emplacement" après chaque partie
    let index = 0;
    for (let i = 0; i < parties.length; i++) {
        // Ajoute le texte de la partie
        let texte = parties[i];
        console.log("texte "+texte)
        let spanTexte = document.createElement('span');
        spanTexte.classList.add('partie_texte');
        spanTexte.style.height=(etiquettelongue.offsetHeight)-15+'px';
        spanTexte.textContent = texte;
        spanReponse.appendChild(spanTexte);

        // Si ce n'est pas la dernière partie, ajoute l'élément avec la classe "emplacement"
        if (i !== parties.length - 1) {
            spanReponse.appendChild(emplacements[index]);
            index++;
        }
    }
    spanReponse.style.fontSize=taillePolice+'px';
  }
}

// Vérification de la réponse par comparaison avec la phrase de référence
function verification(){
  let liste_reponse=[];
  emplacements.forEach(function(emplacement){
    if (emplacement.etiquette){liste_reponse.push(emplacement.etiquette.innerHTML);}
    else {liste_reponse.push('')}
  });
  console.log(mots);
  console.log(liste_reponse);
  if (!liste_reponse.includes('')){
    let position=0;
    emplacements.forEach(function(emplacement){
      if (emplacement.classList.contains('occupe')){
        if (emplacement.etiquette.innerHTML===mots[position]){
          emplacement.style.backgroundColor='#5dff00';
        } else {
          emplacement.style.backgroundColor='red';
        }
      }
      position+=1;
    }); 
  } else {
    alert("Place d'abord toutes les étiquettes.");
  }   
}

//function read_more() {
//  console.log("read more")
//  texte_accueil_cache.style.display='block';
//}

function adapte_taille_police(){

    // Fonction pour ajuster la taille de police pour toutes les étiquettes
  
    // Parcourt toutes les étiquettes pour trouver la taille de police maximale
    etiquettelongue=trouverEtiquetteLaPlusLongue();
    console.log(etiquettelongue.textContent)
    let largeurEtiquette = etiquettelongue.offsetWidth;
    let texteLongueur = etiquettelongue.textContent.length;
    taillePolice = 1.2*(largeurEtiquette / texteLongueur);

    if (taillePolice>100){taillePolice=100;}

    // Applique la taille de police maximale à toutes les étiquettes
    etiquettes.forEach(function(etiquette) {
    etiquette.style.fontSize = taillePolice + 'px';
  });
}

// Détection du changement de sens de la fenêtre
window.addEventListener('resize', resize_fenetre);

function resize_fenetre() {
  // Recalcul de la police de caractères
  adapte_taille_police();

  // Recalcul de la hauteur des emplacements réponses
  emplacements.forEach(function(emplacement){
    emplacement.style.height=etiquettelongue.offsetHeight+'px';
  });

  //Déplacement des étiquettes libres
  let etiquettes_libres = document.querySelectorAll('.libre');
  let largeurFenetre = window.innerWidth || document.documentElement.clientWidth;
  let hauteurFenetre = window.innerHeight || document.documentElement.clientHeight;
  etiquettes_libres.forEach(function(etiquette){
    let rect_etiquette=etiquette.getBoundingClientRect();
    if(rect_etiquette.right>largeurFenetre){etiquette.style.left=null;etiquette.style.right='0px';}
    if(rect_etiquette.bottom>hauteurFenetre){etiquette.style.top=null;etiquette.style.bottom='0px';}
  });

  //Déplacement des étiquettes placées
  let emplacements_occupes = document.querySelectorAll('.occupe');
  emplacements_occupes.forEach(function (emplacement) {
    let rect_emplacement=emplacement.getBoundingClientRect();
    let rect_etiquette=emplacement.etiquette.getBoundingClientRect();
    emplacement.etiquette.style.left=(rect_emplacement.x+rect_emplacement.width/2-rect_etiquette.width/2)+'px';
    emplacement.etiquette.style.top=(rect_emplacement.y+rect_emplacement.height/2-rect_etiquette.height/2 + 50 + window.scrollY)-divPhrase.offsetHeight+'px';
  });
}


// Fonction pour déterminer quelle étiquette a le contenu le plus long
function trouverEtiquetteLaPlusLongue() {
    var etiquetteLaPlusLongue = etiquettes[0]; // Supposons que la première étiquette ait le contenu le plus long
  
    // Parcourt toutes les étiquettes pour trouver celle avec le contenu le plus long
    etiquettes.forEach(function(etiquette) {
      if (etiquette.textContent.length > etiquetteLaPlusLongue.textContent.length) {
        etiquetteLaPlusLongue = etiquette;
      }
    });
   return etiquetteLaPlusLongue;
  }


function checkCollisions(etiquette) {
if (mots.length<12){  
  var etiquettes = document.getElementsByClassName('etiquette');
  var etiquetteA = etiquette.getBoundingClientRect();
  i=Array.prototype.indexOf.call(etiquettes, etiquette);
  for (var j = 0; j < etiquettes.length; j++) {
  if (i !== j) {
        var etiquetteB = etiquettes[j].getBoundingClientRect();

        // Vérifie la collision en comparant les positions
        if (
        etiquetteA.left < etiquetteB.right &&
        etiquetteA.right > etiquetteB.left &&
        etiquetteA.top < etiquetteB.bottom &&
        etiquetteA.bottom > etiquetteB.top
        ) {
        // Il y a collision entre l'étiquette i et l'étiquette j
        return(true)
        }
    }
  }
} else {return(false)}
}


function terminer_exercice(input){
  ouvre_menu();
  ferme_partage();
  ferme_apropos();
  remplir('');
  zone_partage.style.display=null;
  divEtiquettes.style.display='none';
  divPhrase.style.display='none';
  accueil.style.display='block';
  mini_hautparleur.style.display=null;
  bouton_choix_langue.style.display=null;

  etiquettes.forEach((element) => {
    element.remove();  
  });
  if (!etiquettesSeules){
    spanReponse.remove();
  }
  bouton_check.style.display=null;
  bouton_suivant.style.display=null;
  bouton_consigne.style.display=null;
  bouton_entendre_phrase.style.display=null;
  speaker.style.display=null;
  bouton_partager_exercice.disabled=true;
  bouton_terminer_exercice.disabled=true;
  bouton_imprimer_exercice.disabled=true;
  // à virer ? bouton_modifier_exercice.disabled=true;
  input_phrase.focus(); 
}

function partager_exercice(mode){
  mode_partage=mode;
  case_chiffrement.checked=true;
  chiffrement=true;
  ouvre_menu();
  ferme_apropos()
  zone_partage.style.display='block';
  let lien_a_partager=cree_lien(mode);
  zone_lien.innerHTML=lien_a_partager;
  qrcodeDiv.innerHTML='';
  let qrcode = new QRCode(qrcodeDiv, {
    text: lien_a_partager,
    width: 200,
    height: 200
});
}

function chiffrementCesar(chaine, decalage) {
  let resultat = '';

  for (let i = 0; i < chaine.length; i++) {
    let caractere = chaine[i];
    let code = chaine.charCodeAt(i);

    if (caractere.match(/[a-zA-Z0-9]/)) {
      if (code >= 65 && code <= 90) { // Lettres majuscules
        caractere = String.fromCharCode(((code - 65 + decalage) % 26) + 65);
      } else if (code >= 97 && code <= 122) { // Lettres minuscules
        caractere = String.fromCharCode(((code - 97 + decalage) % 26) + 97);
      } else if (code >= 48 && code <= 57) { // Chiffres
        caractere = String.fromCharCode(((code - 48 + decalage) % 10) + 48);
      }
    }
    resultat += caractere;
  }
  
  return resultat;
}

function dechiffrementCesar(chaine, decalage) {
  return chiffrementCesar(chaine, -decalage);
}

function remplir(contenu){
  input_phrase.value=contenu;
  window.scrollTo(0, 0);
}


function cree_lien(type){  
  let fin_url;
  console.log("cree_lien type="+type)
  if (type==='collection'){
    fin_url=lien_codim;
  } else {
    fin_url=encodeURIComponent(phrase_sauvegardee);
    console.log(fin_url);
  }

  url = new URL(window.location.href);


if (etiquettesSeules){
  url.searchParams.set('etiquettes-seules', 'true');  // Utilisation de set au lieu de append
}

  if (chiffrement){
    // Chiffrement
    //fin_url=encodeURIComponent(chiffrementCesar(phrase, 3));
    console.log("pret à encoder "+phrase_sauvegardee) 
    fin_url=btoa(encodeURIComponent(phrase_sauvegardee));
    // Ajout du paramètre de chiffrement
    url.searchParams.append('chiffrement', '64');

    // Modification de l'URL sans recharger la page
    window.history.pushState({}, '', url.toString());  
  }
  let currentURL=window.location.href;
  let base_url=window.location.href.split('#')[0];
  if (chiffrement){
    url.searchParams.delete('chiffrement');
    window.history.pushState({}, '', url.toString());
  }

  // Recherche de l'index du point d'interrogation (?) et du dièse (#)
  let startIndex = currentURL.indexOf('?');
  let queryString = "";
  // Vérification de la présence du ? et du #
  if (startIndex !== -1) {
  // Récupération de la partie de l'URL à partir du ?
  queryString = currentURL.substring(startIndex);
  }


  if (!base_url.startsWith('http')){base_url='https://educajou.forge.apps.education.fr/rapidetik/'+queryString}  
  let lien_a_partager=base_url+"#"+fin_url+"/";
  return lien_a_partager;
}

function change_lien(){
let reponse = prompt("Quelle est la racine carrée de 81 ?");
  if (reponse === '9'){
    
    chiffrement=case_chiffrement.checked;
    lien_a_partager=cree_lien(mode_partage);
    zone_lien.innerHTML=lien_a_partager;
    qrcodeDiv.innerHTML='';
    let qrcode = new QRCode(qrcodeDiv, {
      text: lien_a_partager,
      width: 200,
      height: 200
    });
  } else {
    case_chiffrement.checked=!case_chiffrement.checked;
  }
}

function a_propos(){
  ouvre_menu();
  ferme_partage()
  zone_apropos.style.display='block';
}

function copie_lien(){
  navigator.clipboard.writeText(lien_a_partager);
}

function ferme_partage(){
  zone_partage.style.display=null;
}

function ferme_apropos(){
  zone_apropos.style.display=null;
}

// Fonction pour ajuster la largeur des étiquettes à celle de la plus large
function ajusterLargeurEtiquettes() {
    // Sélectionne toutes les étiquettes
    var etiquettes = document.querySelectorAll('.etiquette');
    var largeurMax = 0;
  
    // Parcourt toutes les étiquettes pour trouver la plus large
    etiquettes.forEach(function(etiquette) {
      var largeur = etiquette.getBoundingClientRect().width;
      if (largeur > largeurMax) {
        largeurMax = largeur;
      }
    });
  
    // Applique la largeur maximale à toutes les étiquettes
    etiquettes.forEach(function(etiquette) {
      etiquette.style.width = largeurMax + 'px';
    });
  }
  
  // Appelle la fonction pour ajuster la largeur au chargement de la page
  window.onload = function() {
    ajusterLargeurEtiquettes();
  };




let dragged;
let diffsourisx;
let diffsourisy;

// Clic de souris
function clic(event) {
  if (menu_ouvert && !menu.contains(event.target) && event.target!=bouton_menu){ouvre_menu();}
  if (menu_choix_langues_ouvert && !menu_choix_langues.contains(event.target) && event.target!=bouton_choix_langue){ouvre_menu_choix_langues();}
  if (consigne_ouverte && !divConsigne.contains(event.target) && event.target!=bouton_consigne){ouvre_consigne();}

  emplacement_cible=null;
  if (event.target.classList.contains('draggable')) {
    event.preventDefault();

    // Empêcher le défilement au niveau de la fenêtre
    if(!etiquettesSeules){disableScroll()};
    dragged = event.target;    
    //léger décalage pour l'effet 3D
    dragged.style.left=dragged.offsetLeft-3+'px';
    dragged.style.top=dragged.offsetTop-3+'px';
    dragged.style.bottom=null;

    // Il faut passer par targetTouches pour les écrans tactiles
    const clientX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
    const clientY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
    diffsourisx = clientX - divEtiquettes.offsetLeft - dragged.offsetLeft;
    diffsourisy = clientY - divEtiquettes.offsetTop - dragged.offsetTop;
    dragged.style.zIndex = hauteur + 1;
    posX_objet = dragged.offsetLeft;
    posY_objet = dragged.offsetTop;
    hauteur++
    dragged.classList.add('dragged');
    emplacements.forEach(function(emplacement){
      if (emplacement.etiquette===dragged){
        emplacement.etiquette=null;
        emplacement.classList.remove('occupe');
        dragged.classList.add('libre');
      }
    });
  
  }

}

var bouge = false;

// Déplacement de la souris
function move(event) {
  /* Si une image est en cours de déplacement */
  if (dragged) {
    event.preventDefault();
    /* Déplace l'image en fonction de la position de la souris */
    bouge = true;  

    // Il faut passer par targetTouches pour les écrans tactiles
    const pageX = event?.targetTouches?.[0]?.pageX || event?.pageX || 0;
    const pageY = event?.targetTouches?.[0]?.pageY || event?.pageY || 0;
    dragged.style.left = pageX - diffsourisx - divEtiquettes.offsetLeft + "px";
    dragged.style.top = pageY - diffsourisy - divEtiquettes.offsetTop - window.scrollY + "px";
    draggedRect = dragged.getBoundingClientRect();
    emplacement_cible=null;
    emplacements.forEach(function(emplacement){
      let emplacementRect=emplacement.getBoundingClientRect();
      if (
        draggedRect.top > emplacementRect.top - emplacementRect.height/2 &&
        draggedRect.bottom < emplacementRect.bottom + emplacementRect.height/2 &&
        draggedRect.left > emplacementRect.left - emplacementRect.width/5 &&
        draggedRect.right < emplacementRect.right + emplacementRect.width/5
      ) {
        emplacement.style.backgroundColor='rgb(255,255,255)';
        emplacement_cible=emplacement;
      } else {
        emplacement.style.backgroundColor=null;
        emplacement.cible=true;
      }
     
    });
  }

}

// Relâchement de la souris
function release(event) {
  /* Si une image est en cours de déplacement */
  if (dragged && bouge == true) {
    // Réactiver le défilement
    if(!etiquettesSeules){enableScroll()};

      if (emplacement_cible){
        emplacement_cible.etiquette=dragged;
        emplacement_cible.classList.add('occupe');
        dragged.classList.remove('libre');
        let rect_emplacement=emplacement_cible.getBoundingClientRect();
        let rect_etiquette=dragged.getBoundingClientRect();
        dragged.style.left=(rect_emplacement.x+rect_emplacement.width/2-rect_etiquette.width/2)+'px';
        dragged.style.top=(rect_emplacement.y+rect_emplacement.height/2-rect_etiquette.height/2 + 50 + window.scrollY)-divPhrase.offsetHeight+'px';
      } else {
        //léger décalage pour l'effet 3D
        dragged.style.left=dragged.offsetLeft+3+'px';
        dragged.style.top=dragged.offsetTop+3+'px';
       
        if (
        draggedRect.top < speakerRect.top &&
        draggedRect.right > speakerRect.left &&
        draggedRect.left < speakerRect.right
        ) {
          mot_a_prononcer = new SpeechSynthesisUtterance(dragged.innerHTML);
          mot_a_prononcer.pitch=1;
          mot_a_prononcer.rate=0.8;
          mot_a_prononcer.lang=langue; 
          entendre(mot_a_prononcer);
        }
      } 
   
  dragged.classList.remove('dragged');
  dragged = null;

}

}

// Désactiver le défilement pendant le glisser-déposer
function disableScroll() {
  reponse.style.overflow = 'hidden';
}

// Activer à nouveau le défilement après le glisser-déposer
function enableScroll() {
  reponse.overflow = 'auto';
}

function imprimer(){
   
    // Création d'une nouvelle fenêtre
  var page_impression = window.open("", "_blank");
  // Contenu HTML de la nouvelle page
  var content = `
      <html>
      <head>
          <title>RapidÉtik</title>
          <link href='imprimer.css?version=COMMITHASH' rel='stylesheet' type="text/css">
      </head>
      <body>        
        <div id="en-tete_impression" class="non-printable">
            <button id="printButton">🖨️ Imprimer</button>
            <form>
              <label for="nombre">Exemplaires</label>
              <input type="number" id="nombre" name="nombre" value=4 min="1" max="99">
            </form>
            <label for="showQR">QR-codes</label>
            <input type="checkbox" id="showQR">

          <p>
            <input type="text" id="input_consigne" value='Découpe les étiquettes ✂️'></input>
          </p>
        </div>
        <div id='conteneur_impression'></div>
      </body>
      </html>
  `;
  // Fin du contenu de la nouvelle page

  // Écriture du contenu dans la nouvelle fenêtre
  page_impression.document.write(content);

  // Intégration HTML > DOM
  const conteneur_impression=page_impression.document.getElementById('conteneur_impression');
  const checkbox = page_impression.document.getElementById('showQR');
  const input_consigne = page_impression.document.getElementById('input_consigne');

  // Réglage initial des contrôles
  checkbox.checked=false;
  

  
  // Nombre d'exemplaires
  let nombre_exemplaires = 4;

  

  function reglages(){
     // Boucle de création des blocs d'étiquettess

    // Listes de zones modifiables
    liste_zones_qr_code=[];
    liste_consigne_etiquettes_a_decouper=[];

    for (let i = 0; i < nombre_exemplaires; i++) {

      // Création da la consigne
      let etiquettes_a_decouper = page_impression.document.createElement('div');
      conteneur_impression.appendChild(etiquettes_a_decouper);
      etiquettes_a_decouper.classList.add('etiquettes_a_decouper');
      let consigne_etiquettes_a_decouper = page_impression.document.createElement('p');
      liste_consigne_etiquettes_a_decouper.push(consigne_etiquettes_a_decouper);
      consigne_etiquettes_a_decouper.classList.add('consigne_etiquettes_a_decouper');
      consigne_etiquettes_a_decouper.innerHTML=input_consigne.value;
      etiquettes_a_decouper.appendChild(consigne_etiquettes_a_decouper);
      
      // Création des étiquettes
      etiquettes_a_creer.sort(() => Math.random() - 0.5).forEach(function(mot){
        let etiquette_a_decouper = page_impression.document.createElement('span');
        etiquette_a_decouper.classList.add('etiquette_a_decouper')
        etiquette_a_decouper.innerHTML=mot;
        etiquettes_a_decouper.appendChild(etiquette_a_decouper);    
      });
      
      // Création des QR-codes
      let zone_qrcode_etiquettes = page_impression.document.createElement('div');
      liste_zones_qr_code.push(zone_qrcode_etiquettes);
      zone_qrcode_etiquettes.classList.add('zone_qrcode_etiquettes');
      if (checkbox.checked) {
        zone_qrcode_etiquettes.classList.remove('non-printable');zone_qrcode_etiquettes.classList.remove('cache');      
      } else {
        zone_qrcode_etiquettes.classList.add('non-printable');zone_qrcode_etiquettes.classList.add('cache');      
      }
      etiquettes_a_decouper.appendChild(zone_qrcode_etiquettes);

      // QR-code correction (affiche la phrase)
      let carre_correction = page_impression.document.createElement('div');
      carre_correction.innerHTML='✅ Corrigé<br>';

      carre_correction.classList.add('carre_qr_code');
      zone_qrcode_etiquettes.appendChild(carre_correction);

      let qrcode_correction = new QRCode(carre_correction, {
        text: phrase
      });
   
      // QR-code d'accès à l'exercice en ligne
      let carre_lien = page_impression.document.createElement('div');
      carre_lien.innerHTML='🌐 Exercice en ligne<br>';

      carre_lien.classList.add('carre_qr_code');
      zone_qrcode_etiquettes.appendChild(carre_lien);

      let qrcode_exercice_en_ligne = new QRCode(carre_lien, {
        text: lien_a_partager
      });

      

    }
    
    // Actions des contrôles de formulaire //

    // Bouton imprimer
    page_impression.document.getElementById('printButton').addEventListener('click', function() {
      page_impression.print();
    });

    // Case à cocher "QR-codes"
    checkbox.addEventListener('change', function() {
      if (this.checked) {
        liste_zones_qr_code.forEach(function(element){element.classList.remove('non-printable');element.classList.remove('cache')});      
      } else {
        liste_zones_qr_code.forEach(function(element){element.classList.add('non-printable');element.classList.add('cache')});      
      }
    });
  }

  // Nombre d'exemplaires (relance la boucle)
  page_impression.document.getElementById('nombre').addEventListener('change', function() {
    conteneur_impression.innerHTML='';
    nombre_exemplaires=page_impression.document.getElementById('nombre').value;
    reglages();
  });

  // Mise à jour de la consigne
  input_consigne.addEventListener('keyup', function() {
    liste_consigne_etiquettes_a_decouper.forEach(function(element){
      element.innerHTML=input_consigne.value;
    });
  });

  reglages(); // Lancement initial de la boucle de création
  
} //fin de la fonction imprimer()



function ouvre_consigne(){
  if (consigne_ouverte){consigne_ouverte=false;divConsigne.style.display=null;}
  else {consigne_ouverte=true;divConsigne.style.display='block';}
} 

function ouvre_menu(){
  if (menu_ouvert){menu_ouvert=false;menu.style.display=null;}
  else {
    menu_ouvert=true;
    menu.style.display='block';
    if (mode_multi){document.getElementById('bouton_partager_collection').disabled=false;}
    else {document.getElementById('bouton_partager_collection').disabled=true;}
  }
}

function ouvre_menu_choix_langues(){
  if (menu_choix_langues_ouvert){menu_choix_langues_ouvert=false;menu_choix_langues.style.display=null;menu_choix_langues.style.maxHeight='0px'}
  else {menu_choix_langues_ouvert=true;menu_choix_langues.style.maxHeight='500px';}
}


function mail_lien(){
  let subject = "Exercice sur RapidÉtik";
  let link = encodeURIComponent(cree_lien()); // Encodage du lien
  let body = "Bonjour,%0D%0A%0D%0APour accéder à L'exercice, cliquez sur le lien ci-dessous:%0D%0A%0D%0A" + link;
  let mailtoLink = "mailto:" + "?subject=" + subject + "&body=" + body;
  window.location.href = mailtoLink;
}

function sms_lien(){
  let link = encodeURIComponent(cree_lien()); // Encodage du lien
  let body = "Bonjour,%0D%0A%0D%0APour accéder à L'exercice, cliquez sur le lien ci-dessous:%0D%0A%0D%0A" + link;
  let smsLink = "sms:" + "?body=" + body;
  window.location.href = smsLink;
}





function change_langue(choix,mode){
  console.log('change langue '+choix)
  langue=choix;
  bouton_choix_langue.style.backgroundImage='url(images/'+choix.substring(0, 2)+'.svg)';
  if (typeof phrase_a_prononcer !== 'undefined'){
  son_phrase = new SpeechSynthesisUtterance(phrase_a_prononcer);
  son_phrase.pitch=1;
  son_phrase.rate=1;
  son_phrase.lang=langue;
  }

  if (mode!='auto'){
    // Fermeture du menu des langues
    ouvre_menu_choix_langues();    
  }

  // Récupération de l'URL actuelle
  let url = new URL(window.location.href);
  // Vérification si le paramètre "langue" est déjà présent dans l'URL
  if (url.searchParams.has('langue')) {
    // Mise à jour de la valeur du paramètre "langue" (par exemple, "en" pour l'anglais)
    url.searchParams.set('langue', langue);
  } else {
    // Ajout du paramètre "langue" avec une valeur (par exemple, "fr" pour le français)
    url.searchParams.append('langue', langue);
  }
  // Modification de l'URL sans recharger la page
  window.history.pushState({}, '', url.toString());
}

document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);

document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);


// Fonction pour détecter l'appui sur la touche Entrée
function keyPressed(event) {
    // Vérifie si la touche appuyée est la touche Entrée (code 13)
    if (event.keyCode === 13) {
        cree_etiquettes(); // Appelle la fonction cree_etiquettes()
    }
}

// Écouteur d'événement pour capturer les pressions de touches
document.addEventListener('keypress', keyPressed);

