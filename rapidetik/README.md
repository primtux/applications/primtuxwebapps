# RapidÉtik

## Présentation

RapidÉtik est un outil permettant de créer rapidement un exercice de type "phrase en désordre".

## Démonstration

Une instance en fonctionnement est utilisable à l'adresse suivante :

https://educajou.forge.apps.education.fr/rapidetik/
