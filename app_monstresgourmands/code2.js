gdjs.tirage_95ancienCode = {};
gdjs.tirage_95ancienCode.GDscore_951Objects1= [];
gdjs.tirage_95ancienCode.GDscore_951Objects2= [];
gdjs.tirage_95ancienCode.GDscore_951Objects3= [];
gdjs.tirage_95ancienCode.GDscore_952Objects1= [];
gdjs.tirage_95ancienCode.GDscore_952Objects2= [];
gdjs.tirage_95ancienCode.GDscore_952Objects3= [];
gdjs.tirage_95ancienCode.GDscore_953Objects1= [];
gdjs.tirage_95ancienCode.GDscore_953Objects2= [];
gdjs.tirage_95ancienCode.GDscore_953Objects3= [];
gdjs.tirage_95ancienCode.GDmonstreObjects1= [];
gdjs.tirage_95ancienCode.GDmonstreObjects2= [];
gdjs.tirage_95ancienCode.GDmonstreObjects3= [];
gdjs.tirage_95ancienCode.GDpanierObjects1= [];
gdjs.tirage_95ancienCode.GDpanierObjects2= [];
gdjs.tirage_95ancienCode.GDpanierObjects3= [];
gdjs.tirage_95ancienCode.GDbouton_95recommencerObjects1= [];
gdjs.tirage_95ancienCode.GDbouton_95recommencerObjects2= [];
gdjs.tirage_95ancienCode.GDbouton_95recommencerObjects3= [];
gdjs.tirage_95ancienCode.GDmonstre3Objects1= [];
gdjs.tirage_95ancienCode.GDmonstre3Objects2= [];
gdjs.tirage_95ancienCode.GDmonstre3Objects3= [];
gdjs.tirage_95ancienCode.GDmonstre2Objects1= [];
gdjs.tirage_95ancienCode.GDmonstre2Objects2= [];
gdjs.tirage_95ancienCode.GDmonstre2Objects3= [];
gdjs.tirage_95ancienCode.GDfondObjects1= [];
gdjs.tirage_95ancienCode.GDfondObjects2= [];
gdjs.tirage_95ancienCode.GDfondObjects3= [];
gdjs.tirage_95ancienCode.GDbouton_95suiteObjects1= [];
gdjs.tirage_95ancienCode.GDbouton_95suiteObjects2= [];
gdjs.tirage_95ancienCode.GDbouton_95suiteObjects3= [];
gdjs.tirage_95ancienCode.GDserge_95aideObjects1= [];
gdjs.tirage_95ancienCode.GDserge_95aideObjects2= [];
gdjs.tirage_95ancienCode.GDserge_95aideObjects3= [];
gdjs.tirage_95ancienCode.GDbouton_95retourObjects1= [];
gdjs.tirage_95ancienCode.GDbouton_95retourObjects2= [];
gdjs.tirage_95ancienCode.GDbouton_95retourObjects3= [];

gdjs.tirage_95ancienCode.conditionTrue_0 = {val:false};
gdjs.tirage_95ancienCode.condition0IsTrue_0 = {val:false};
gdjs.tirage_95ancienCode.condition1IsTrue_0 = {val:false};
gdjs.tirage_95ancienCode.condition2IsTrue_0 = {val:false};
gdjs.tirage_95ancienCode.condition3IsTrue_0 = {val:false};
gdjs.tirage_95ancienCode.condition4IsTrue_0 = {val:false};
gdjs.tirage_95ancienCode.condition5IsTrue_0 = {val:false};
gdjs.tirage_95ancienCode.conditionTrue_1 = {val:false};
gdjs.tirage_95ancienCode.condition0IsTrue_1 = {val:false};
gdjs.tirage_95ancienCode.condition1IsTrue_1 = {val:false};
gdjs.tirage_95ancienCode.condition2IsTrue_1 = {val:false};
gdjs.tirage_95ancienCode.condition3IsTrue_1 = {val:false};
gdjs.tirage_95ancienCode.condition4IsTrue_1 = {val:false};
gdjs.tirage_95ancienCode.condition5IsTrue_1 = {val:false};


gdjs.tirage_95ancienCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirage_95ancienCode.eventsList1 = function(runtimeScene) {

{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirage_95ancienCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirage_95ancienCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("5").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("4")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0"));
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getScene().getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirage_95ancienCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_niv1", false);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 2;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_niv2", false);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 3;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_niv3", false);
}}

}


};gdjs.tirage_95ancienCode.eventsList5 = function(runtimeScene) {

{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.tirage_95ancienCode.GDfondObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(0);
}{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDfondObjects1[i].setOpacity(150);
}
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ancienCode.condition1IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 3;
}if ( gdjs.tirage_95ancienCode.condition0IsTrue_0.val ) {
{
{gdjs.tirage_95ancienCode.conditionTrue_1 = gdjs.tirage_95ancienCode.condition1IsTrue_0;
gdjs.tirage_95ancienCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(22770060);
}
}}
if (gdjs.tirage_95ancienCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.tirage_95ancienCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.tirage_95ancienCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.tirage_95ancienCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstre2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstreObjects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/monstre3_faim.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstre3Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ancienCode.condition1IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 2;
}if ( gdjs.tirage_95ancienCode.condition0IsTrue_0.val ) {
{
{gdjs.tirage_95ancienCode.conditionTrue_1 = gdjs.tirage_95ancienCode.condition1IsTrue_0;
gdjs.tirage_95ancienCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(19317420);
}
}}
if (gdjs.tirage_95ancienCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.tirage_95ancienCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.tirage_95ancienCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.tirage_95ancienCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstreObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstre3Objects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/monstre2_j_ai_faim.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstre2Objects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ancienCode.condition1IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if ( gdjs.tirage_95ancienCode.condition0IsTrue_0.val ) {
{
{gdjs.tirage_95ancienCode.conditionTrue_1 = gdjs.tirage_95ancienCode.condition1IsTrue_0;
gdjs.tirage_95ancienCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(19318908);
}
}}
if (gdjs.tirage_95ancienCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("monstre"), gdjs.tirage_95ancienCode.GDmonstreObjects1);
gdjs.copyArray(runtimeScene.getObjects("monstre2"), gdjs.tirage_95ancienCode.GDmonstre2Objects1);
gdjs.copyArray(runtimeScene.getObjects("monstre3"), gdjs.tirage_95ancienCode.GDmonstre3Objects1);
{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstre2Objects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstre2Objects1[i].hide();
}
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstre3Objects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstre3Objects1[i].hide();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/j_ai_faim.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.tirage_95ancienCode.GDmonstreObjects1.length ;i < len;++i) {
    gdjs.tirage_95ancienCode.GDmonstreObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
{gdjs.tirage_95ancienCode.conditionTrue_1 = gdjs.tirage_95ancienCode.condition0IsTrue_0;
gdjs.tirage_95ancienCode.condition0IsTrue_1.val = false;
gdjs.tirage_95ancienCode.condition1IsTrue_1.val = false;
gdjs.tirage_95ancienCode.condition2IsTrue_1.val = false;
gdjs.tirage_95ancienCode.condition3IsTrue_1.val = false;
gdjs.tirage_95ancienCode.condition4IsTrue_1.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if( gdjs.tirage_95ancienCode.condition0IsTrue_1.val ) {
    gdjs.tirage_95ancienCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ancienCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 3;
if( gdjs.tirage_95ancienCode.condition1IsTrue_1.val ) {
    gdjs.tirage_95ancienCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ancienCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 5;
if( gdjs.tirage_95ancienCode.condition2IsTrue_1.val ) {
    gdjs.tirage_95ancienCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ancienCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 7;
if( gdjs.tirage_95ancienCode.condition3IsTrue_1.val ) {
    gdjs.tirage_95ancienCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirage_95ancienCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 9;
if( gdjs.tirage_95ancienCode.condition4IsTrue_1.val ) {
    gdjs.tirage_95ancienCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0").setNumber(gdjs.randomInRange(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini")), gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi"))));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("0")));
}{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ancienCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ancienCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ancienCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 10;
}if (gdjs.tirage_95ancienCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ancienCode.eventsList3(runtimeScene);} //End of subevents
}

}


{


gdjs.tirage_95ancienCode.condition0IsTrue_0.val = false;
gdjs.tirage_95ancienCode.condition1IsTrue_0.val = false;
{
gdjs.tirage_95ancienCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 11;
}if ( gdjs.tirage_95ancienCode.condition0IsTrue_0.val ) {
{
gdjs.tirage_95ancienCode.condition1IsTrue_0.val = gdjs.evtTools.sound.isMusicOnChannelStopped(runtimeScene, 1);
}}
if (gdjs.tirage_95ancienCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.tirage_95ancienCode.eventsList4(runtimeScene);} //End of subevents
}

}


};

gdjs.tirage_95ancienCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirage_95ancienCode.GDscore_951Objects1.length = 0;
gdjs.tirage_95ancienCode.GDscore_951Objects2.length = 0;
gdjs.tirage_95ancienCode.GDscore_951Objects3.length = 0;
gdjs.tirage_95ancienCode.GDscore_952Objects1.length = 0;
gdjs.tirage_95ancienCode.GDscore_952Objects2.length = 0;
gdjs.tirage_95ancienCode.GDscore_952Objects3.length = 0;
gdjs.tirage_95ancienCode.GDscore_953Objects1.length = 0;
gdjs.tirage_95ancienCode.GDscore_953Objects2.length = 0;
gdjs.tirage_95ancienCode.GDscore_953Objects3.length = 0;
gdjs.tirage_95ancienCode.GDmonstreObjects1.length = 0;
gdjs.tirage_95ancienCode.GDmonstreObjects2.length = 0;
gdjs.tirage_95ancienCode.GDmonstreObjects3.length = 0;
gdjs.tirage_95ancienCode.GDpanierObjects1.length = 0;
gdjs.tirage_95ancienCode.GDpanierObjects2.length = 0;
gdjs.tirage_95ancienCode.GDpanierObjects3.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.tirage_95ancienCode.GDmonstre3Objects1.length = 0;
gdjs.tirage_95ancienCode.GDmonstre3Objects2.length = 0;
gdjs.tirage_95ancienCode.GDmonstre3Objects3.length = 0;
gdjs.tirage_95ancienCode.GDmonstre2Objects1.length = 0;
gdjs.tirage_95ancienCode.GDmonstre2Objects2.length = 0;
gdjs.tirage_95ancienCode.GDmonstre2Objects3.length = 0;
gdjs.tirage_95ancienCode.GDfondObjects1.length = 0;
gdjs.tirage_95ancienCode.GDfondObjects2.length = 0;
gdjs.tirage_95ancienCode.GDfondObjects3.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95suiteObjects1.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95suiteObjects2.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95suiteObjects3.length = 0;
gdjs.tirage_95ancienCode.GDserge_95aideObjects1.length = 0;
gdjs.tirage_95ancienCode.GDserge_95aideObjects2.length = 0;
gdjs.tirage_95ancienCode.GDserge_95aideObjects3.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirage_95ancienCode.GDbouton_95retourObjects3.length = 0;

gdjs.tirage_95ancienCode.eventsList5(runtimeScene);

return;

}

gdjs['tirage_95ancienCode'] = gdjs.tirage_95ancienCode;
