gdjs.infosCode = {};
gdjs.infosCode.GDscore_951Objects1= [];
gdjs.infosCode.GDscore_951Objects2= [];
gdjs.infosCode.GDscore_952Objects1= [];
gdjs.infosCode.GDscore_952Objects2= [];
gdjs.infosCode.GDscore_953Objects1= [];
gdjs.infosCode.GDscore_953Objects2= [];
gdjs.infosCode.GDmonstreObjects1= [];
gdjs.infosCode.GDmonstreObjects2= [];
gdjs.infosCode.GDpanierObjects1= [];
gdjs.infosCode.GDpanierObjects2= [];
gdjs.infosCode.GDbouton_95recommencerObjects1= [];
gdjs.infosCode.GDbouton_95recommencerObjects2= [];
gdjs.infosCode.GDmonstre3Objects1= [];
gdjs.infosCode.GDmonstre3Objects2= [];
gdjs.infosCode.GDmonstre2Objects1= [];
gdjs.infosCode.GDmonstre2Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDbouton_95suiteObjects1= [];
gdjs.infosCode.GDbouton_95suiteObjects2= [];
gdjs.infosCode.GDserge_95aideObjects1= [];
gdjs.infosCode.GDserge_95aideObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDNewTiledSpriteObjects1= [];
gdjs.infosCode.GDNewTiledSpriteObjects2= [];
gdjs.infosCode.GDcapture_95Objects1= [];
gdjs.infosCode.GDcapture_95Objects2= [];
gdjs.infosCode.GDcapture_95niv1Objects1= [];
gdjs.infosCode.GDcapture_95niv1Objects2= [];
gdjs.infosCode.GDcapture_95niv2Objects1= [];
gdjs.infosCode.GDcapture_95niv2Objects2= [];
gdjs.infosCode.GDcapture_95niv3Objects1= [];
gdjs.infosCode.GDcapture_95niv3Objects2= [];
gdjs.infosCode.GDNewSpriteObjects1= [];
gdjs.infosCode.GDNewSpriteObjects2= [];
gdjs.infosCode.GDinfoObjects1= [];
gdjs.infosCode.GDinfoObjects2= [];
gdjs.infosCode.GDinfo_95niv1Objects1= [];
gdjs.infosCode.GDinfo_95niv1Objects2= [];
gdjs.infosCode.GDinfo_95niv2Objects1= [];
gdjs.infosCode.GDinfo_95niv2Objects2= [];
gdjs.infosCode.GDinfo_95niv3Objects1= [];
gdjs.infosCode.GDinfo_95niv3Objects2= [];
gdjs.infosCode.GDscore_95demoObjects1= [];
gdjs.infosCode.GDscore_95demoObjects2= [];
gdjs.infosCode.GDbouton_95optionsObjects1= [];
gdjs.infosCode.GDbouton_95optionsObjects2= [];
gdjs.infosCode.GDinfo_95scoreObjects1= [];
gdjs.infosCode.GDinfo_95scoreObjects2= [];
gdjs.infosCode.GDcapture_95representationsObjects1= [];
gdjs.infosCode.GDcapture_95representationsObjects2= [];
gdjs.infosCode.GDNewSprite2Objects1= [];
gdjs.infosCode.GDNewSprite2Objects2= [];
gdjs.infosCode.GDNewSprite3Objects1= [];
gdjs.infosCode.GDNewSprite3Objects2= [];
gdjs.infosCode.GDinfo_95sergeObjects1= [];
gdjs.infosCode.GDinfo_95sergeObjects2= [];
gdjs.infosCode.GDinfo_95reglagesObjects1= [];
gdjs.infosCode.GDinfo_95reglagesObjects2= [];
gdjs.infosCode.GDcapture_95reglagesObjects1= [];
gdjs.infosCode.GDcapture_95reglagesObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};
gdjs.infosCode.condition3IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
gdjs.infosCode.condition2IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.infosCode.condition1IsTrue_0.val ) {
{
gdjs.infosCode.condition2IsTrue_0.val = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") > 0.5;
}}
}
if (gdjs.infosCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDscore_951Objects1.length = 0;
gdjs.infosCode.GDscore_951Objects2.length = 0;
gdjs.infosCode.GDscore_952Objects1.length = 0;
gdjs.infosCode.GDscore_952Objects2.length = 0;
gdjs.infosCode.GDscore_953Objects1.length = 0;
gdjs.infosCode.GDscore_953Objects2.length = 0;
gdjs.infosCode.GDmonstreObjects1.length = 0;
gdjs.infosCode.GDmonstreObjects2.length = 0;
gdjs.infosCode.GDpanierObjects1.length = 0;
gdjs.infosCode.GDpanierObjects2.length = 0;
gdjs.infosCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.infosCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.infosCode.GDmonstre3Objects1.length = 0;
gdjs.infosCode.GDmonstre3Objects2.length = 0;
gdjs.infosCode.GDmonstre2Objects1.length = 0;
gdjs.infosCode.GDmonstre2Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDbouton_95suiteObjects1.length = 0;
gdjs.infosCode.GDbouton_95suiteObjects2.length = 0;
gdjs.infosCode.GDserge_95aideObjects1.length = 0;
gdjs.infosCode.GDserge_95aideObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDNewTiledSpriteObjects1.length = 0;
gdjs.infosCode.GDNewTiledSpriteObjects2.length = 0;
gdjs.infosCode.GDcapture_95Objects1.length = 0;
gdjs.infosCode.GDcapture_95Objects2.length = 0;
gdjs.infosCode.GDcapture_95niv1Objects1.length = 0;
gdjs.infosCode.GDcapture_95niv1Objects2.length = 0;
gdjs.infosCode.GDcapture_95niv2Objects1.length = 0;
gdjs.infosCode.GDcapture_95niv2Objects2.length = 0;
gdjs.infosCode.GDcapture_95niv3Objects1.length = 0;
gdjs.infosCode.GDcapture_95niv3Objects2.length = 0;
gdjs.infosCode.GDNewSpriteObjects1.length = 0;
gdjs.infosCode.GDNewSpriteObjects2.length = 0;
gdjs.infosCode.GDinfoObjects1.length = 0;
gdjs.infosCode.GDinfoObjects2.length = 0;
gdjs.infosCode.GDinfo_95niv1Objects1.length = 0;
gdjs.infosCode.GDinfo_95niv1Objects2.length = 0;
gdjs.infosCode.GDinfo_95niv2Objects1.length = 0;
gdjs.infosCode.GDinfo_95niv2Objects2.length = 0;
gdjs.infosCode.GDinfo_95niv3Objects1.length = 0;
gdjs.infosCode.GDinfo_95niv3Objects2.length = 0;
gdjs.infosCode.GDscore_95demoObjects1.length = 0;
gdjs.infosCode.GDscore_95demoObjects2.length = 0;
gdjs.infosCode.GDbouton_95optionsObjects1.length = 0;
gdjs.infosCode.GDbouton_95optionsObjects2.length = 0;
gdjs.infosCode.GDinfo_95scoreObjects1.length = 0;
gdjs.infosCode.GDinfo_95scoreObjects2.length = 0;
gdjs.infosCode.GDcapture_95representationsObjects1.length = 0;
gdjs.infosCode.GDcapture_95representationsObjects2.length = 0;
gdjs.infosCode.GDNewSprite2Objects1.length = 0;
gdjs.infosCode.GDNewSprite2Objects2.length = 0;
gdjs.infosCode.GDNewSprite3Objects1.length = 0;
gdjs.infosCode.GDNewSprite3Objects2.length = 0;
gdjs.infosCode.GDinfo_95sergeObjects1.length = 0;
gdjs.infosCode.GDinfo_95sergeObjects2.length = 0;
gdjs.infosCode.GDinfo_95reglagesObjects1.length = 0;
gdjs.infosCode.GDinfo_95reglagesObjects2.length = 0;
gdjs.infosCode.GDcapture_95reglagesObjects1.length = 0;
gdjs.infosCode.GDcapture_95reglagesObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
