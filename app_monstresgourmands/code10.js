gdjs.optionsCode = {};
gdjs.optionsCode.GDscore_951Objects1= [];
gdjs.optionsCode.GDscore_951Objects2= [];
gdjs.optionsCode.GDscore_951Objects3= [];
gdjs.optionsCode.GDscore_952Objects1= [];
gdjs.optionsCode.GDscore_952Objects2= [];
gdjs.optionsCode.GDscore_952Objects3= [];
gdjs.optionsCode.GDscore_953Objects1= [];
gdjs.optionsCode.GDscore_953Objects2= [];
gdjs.optionsCode.GDscore_953Objects3= [];
gdjs.optionsCode.GDmonstreObjects1= [];
gdjs.optionsCode.GDmonstreObjects2= [];
gdjs.optionsCode.GDmonstreObjects3= [];
gdjs.optionsCode.GDpanierObjects1= [];
gdjs.optionsCode.GDpanierObjects2= [];
gdjs.optionsCode.GDpanierObjects3= [];
gdjs.optionsCode.GDbouton_95recommencerObjects1= [];
gdjs.optionsCode.GDbouton_95recommencerObjects2= [];
gdjs.optionsCode.GDbouton_95recommencerObjects3= [];
gdjs.optionsCode.GDmonstre3Objects1= [];
gdjs.optionsCode.GDmonstre3Objects2= [];
gdjs.optionsCode.GDmonstre3Objects3= [];
gdjs.optionsCode.GDmonstre2Objects1= [];
gdjs.optionsCode.GDmonstre2Objects2= [];
gdjs.optionsCode.GDmonstre2Objects3= [];
gdjs.optionsCode.GDfondObjects1= [];
gdjs.optionsCode.GDfondObjects2= [];
gdjs.optionsCode.GDfondObjects3= [];
gdjs.optionsCode.GDbouton_95suiteObjects1= [];
gdjs.optionsCode.GDbouton_95suiteObjects2= [];
gdjs.optionsCode.GDbouton_95suiteObjects3= [];
gdjs.optionsCode.GDserge_95aideObjects1= [];
gdjs.optionsCode.GDserge_95aideObjects2= [];
gdjs.optionsCode.GDserge_95aideObjects3= [];
gdjs.optionsCode.GDbouton_95retourObjects1= [];
gdjs.optionsCode.GDbouton_95retourObjects2= [];
gdjs.optionsCode.GDbouton_95retourObjects3= [];
gdjs.optionsCode.GDinfoObjects1= [];
gdjs.optionsCode.GDinfoObjects2= [];
gdjs.optionsCode.GDinfoObjects3= [];
gdjs.optionsCode.GDintervalle_95Objects1= [];
gdjs.optionsCode.GDintervalle_95Objects2= [];
gdjs.optionsCode.GDintervalle_95Objects3= [];
gdjs.optionsCode.GDnb_95minObjects1= [];
gdjs.optionsCode.GDnb_95minObjects2= [];
gdjs.optionsCode.GDnb_95minObjects3= [];
gdjs.optionsCode.GDnb_95maxObjects1= [];
gdjs.optionsCode.GDnb_95maxObjects2= [];
gdjs.optionsCode.GDnb_95maxObjects3= [];
gdjs.optionsCode.GDbouton_95plus_95Objects1= [];
gdjs.optionsCode.GDbouton_95plus_95Objects2= [];
gdjs.optionsCode.GDbouton_95plus_95Objects3= [];
gdjs.optionsCode.GDbouton_95plus_952Objects1= [];
gdjs.optionsCode.GDbouton_95plus_952Objects2= [];
gdjs.optionsCode.GDbouton_95plus_952Objects3= [];
gdjs.optionsCode.GDbouton_95moins_95Objects1= [];
gdjs.optionsCode.GDbouton_95moins_95Objects2= [];
gdjs.optionsCode.GDbouton_95moins_95Objects3= [];
gdjs.optionsCode.GDbouton_95moins_952Objects1= [];
gdjs.optionsCode.GDbouton_95moins_952Objects2= [];
gdjs.optionsCode.GDbouton_95moins_952Objects3= [];

gdjs.optionsCode.conditionTrue_0 = {val:false};
gdjs.optionsCode.condition0IsTrue_0 = {val:false};
gdjs.optionsCode.condition1IsTrue_0 = {val:false};
gdjs.optionsCode.condition2IsTrue_0 = {val:false};
gdjs.optionsCode.condition3IsTrue_0 = {val:false};
gdjs.optionsCode.condition4IsTrue_0 = {val:false};
gdjs.optionsCode.condition5IsTrue_0 = {val:false};
gdjs.optionsCode.conditionTrue_1 = {val:false};
gdjs.optionsCode.condition0IsTrue_1 = {val:false};
gdjs.optionsCode.condition1IsTrue_1 = {val:false};
gdjs.optionsCode.condition2IsTrue_1 = {val:false};
gdjs.optionsCode.condition3IsTrue_1 = {val:false};
gdjs.optionsCode.condition4IsTrue_1 = {val:false};
gdjs.optionsCode.condition5IsTrue_1 = {val:false};


gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_9595Objects2Objects = Hashtable.newFrom({"bouton_plus_": gdjs.optionsCode.GDbouton_95plus_95Objects2});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moins_9595Objects2Objects = Hashtable.newFrom({"bouton_moins_": gdjs.optionsCode.GDbouton_95moins_95Objects2});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_95952Objects2Objects = Hashtable.newFrom({"bouton_plus_2": gdjs.optionsCode.GDbouton_95plus_952Objects2});
gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moins_95952Objects1Objects = Hashtable.newFrom({"bouton_moins_2": gdjs.optionsCode.GDbouton_95moins_952Objects1});
gdjs.optionsCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus_"), gdjs.optionsCode.GDbouton_95plus_95Objects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
gdjs.optionsCode.condition4IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_9595Objects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini")) < 6;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini")) < gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi")) - 4;
}if ( gdjs.optionsCode.condition3IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition4IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14303068);
}
}}
}
}
}
if (gdjs.optionsCode.condition4IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_95minObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95minObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95minObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins_"), gdjs.optionsCode.GDbouton_95moins_95Objects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moins_9595Objects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini")) > 1;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition3IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14278436);
}
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_95minObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini").sub(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95minObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95minObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus_2"), gdjs.optionsCode.GDbouton_95plus_952Objects2);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595plus_95952Objects2Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi")) < 10;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition3IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14254756);
}
}}
}
}
if (gdjs.optionsCode.condition3IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_95maxObjects2);
{runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi").add(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxObjects2.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxObjects2[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins_2"), gdjs.optionsCode.GDbouton_95moins_952Objects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
gdjs.optionsCode.condition2IsTrue_0.val = false;
gdjs.optionsCode.condition3IsTrue_0.val = false;
gdjs.optionsCode.condition4IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595moins_95952Objects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.optionsCode.condition1IsTrue_0.val ) {
{
gdjs.optionsCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi")) > 5;
}if ( gdjs.optionsCode.condition2IsTrue_0.val ) {
{
gdjs.optionsCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini")) + 4;
}if ( gdjs.optionsCode.condition3IsTrue_0.val ) {
{
{gdjs.optionsCode.conditionTrue_1 = gdjs.optionsCode.condition4IsTrue_0;
gdjs.optionsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(14235028);
}
}}
}
}
}
if (gdjs.optionsCode.condition4IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_95maxObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi").sub(1);
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi"))));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}}

}


};gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.optionsCode.GDbouton_95retourObjects1});
gdjs.optionsCode.eventsList1 = function(runtimeScene) {

{


{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_monstresgourmands");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_monstresgourmands", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(6)));
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "bulle_1.mp3", 1, false, 100, 1);
}}

}


{


{
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};gdjs.optionsCode.eventsList2 = function(runtimeScene) {

{


gdjs.optionsCode.eventsList1(runtimeScene);
}


};gdjs.optionsCode.eventsList3 = function(runtimeScene) {

{


gdjs.optionsCode.condition0IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.optionsCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("nb_max"), gdjs.optionsCode.GDnb_95maxObjects1);
gdjs.copyArray(runtimeScene.getObjects("nb_min"), gdjs.optionsCode.GDnb_95minObjects1);
{for(var i = 0, len = gdjs.optionsCode.GDnb_95minObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95minObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("mini"))));
}
}{for(var i = 0, len = gdjs.optionsCode.GDnb_95maxObjects1.length ;i < len;++i) {
    gdjs.optionsCode.GDnb_95maxObjects1[i].setString(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6).getChild("nb").getChild("maxi"))));
}
}}

}


{


gdjs.optionsCode.eventsList0(runtimeScene);
}


{


{
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.optionsCode.GDbouton_95retourObjects1);

gdjs.optionsCode.condition0IsTrue_0.val = false;
gdjs.optionsCode.condition1IsTrue_0.val = false;
{
gdjs.optionsCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.optionsCode.mapOfGDgdjs_46optionsCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.optionsCode.condition0IsTrue_0.val ) {
{
gdjs.optionsCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.optionsCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.optionsCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.optionsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.optionsCode.GDscore_951Objects1.length = 0;
gdjs.optionsCode.GDscore_951Objects2.length = 0;
gdjs.optionsCode.GDscore_951Objects3.length = 0;
gdjs.optionsCode.GDscore_952Objects1.length = 0;
gdjs.optionsCode.GDscore_952Objects2.length = 0;
gdjs.optionsCode.GDscore_952Objects3.length = 0;
gdjs.optionsCode.GDscore_953Objects1.length = 0;
gdjs.optionsCode.GDscore_953Objects2.length = 0;
gdjs.optionsCode.GDscore_953Objects3.length = 0;
gdjs.optionsCode.GDmonstreObjects1.length = 0;
gdjs.optionsCode.GDmonstreObjects2.length = 0;
gdjs.optionsCode.GDmonstreObjects3.length = 0;
gdjs.optionsCode.GDpanierObjects1.length = 0;
gdjs.optionsCode.GDpanierObjects2.length = 0;
gdjs.optionsCode.GDpanierObjects3.length = 0;
gdjs.optionsCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.optionsCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.optionsCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.optionsCode.GDmonstre3Objects1.length = 0;
gdjs.optionsCode.GDmonstre3Objects2.length = 0;
gdjs.optionsCode.GDmonstre3Objects3.length = 0;
gdjs.optionsCode.GDmonstre2Objects1.length = 0;
gdjs.optionsCode.GDmonstre2Objects2.length = 0;
gdjs.optionsCode.GDmonstre2Objects3.length = 0;
gdjs.optionsCode.GDfondObjects1.length = 0;
gdjs.optionsCode.GDfondObjects2.length = 0;
gdjs.optionsCode.GDfondObjects3.length = 0;
gdjs.optionsCode.GDbouton_95suiteObjects1.length = 0;
gdjs.optionsCode.GDbouton_95suiteObjects2.length = 0;
gdjs.optionsCode.GDbouton_95suiteObjects3.length = 0;
gdjs.optionsCode.GDserge_95aideObjects1.length = 0;
gdjs.optionsCode.GDserge_95aideObjects2.length = 0;
gdjs.optionsCode.GDserge_95aideObjects3.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects1.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects2.length = 0;
gdjs.optionsCode.GDbouton_95retourObjects3.length = 0;
gdjs.optionsCode.GDinfoObjects1.length = 0;
gdjs.optionsCode.GDinfoObjects2.length = 0;
gdjs.optionsCode.GDinfoObjects3.length = 0;
gdjs.optionsCode.GDintervalle_95Objects1.length = 0;
gdjs.optionsCode.GDintervalle_95Objects2.length = 0;
gdjs.optionsCode.GDintervalle_95Objects3.length = 0;
gdjs.optionsCode.GDnb_95minObjects1.length = 0;
gdjs.optionsCode.GDnb_95minObjects2.length = 0;
gdjs.optionsCode.GDnb_95minObjects3.length = 0;
gdjs.optionsCode.GDnb_95maxObjects1.length = 0;
gdjs.optionsCode.GDnb_95maxObjects2.length = 0;
gdjs.optionsCode.GDnb_95maxObjects3.length = 0;
gdjs.optionsCode.GDbouton_95plus_95Objects1.length = 0;
gdjs.optionsCode.GDbouton_95plus_95Objects2.length = 0;
gdjs.optionsCode.GDbouton_95plus_95Objects3.length = 0;
gdjs.optionsCode.GDbouton_95plus_952Objects1.length = 0;
gdjs.optionsCode.GDbouton_95plus_952Objects2.length = 0;
gdjs.optionsCode.GDbouton_95plus_952Objects3.length = 0;
gdjs.optionsCode.GDbouton_95moins_95Objects1.length = 0;
gdjs.optionsCode.GDbouton_95moins_95Objects2.length = 0;
gdjs.optionsCode.GDbouton_95moins_95Objects3.length = 0;
gdjs.optionsCode.GDbouton_95moins_952Objects1.length = 0;
gdjs.optionsCode.GDbouton_95moins_952Objects2.length = 0;
gdjs.optionsCode.GDbouton_95moins_952Objects3.length = 0;

gdjs.optionsCode.eventsList3(runtimeScene);

return;

}

gdjs['optionsCode'] = gdjs.optionsCode;
