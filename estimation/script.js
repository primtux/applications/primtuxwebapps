const milieu=document.getElementById('milieu');
const droite=document.getElementById('droite');
const debut=document.getElementById('debut');
const fin=document.getElementById('fin');
const mobile=document.getElementById('mobile');
const etiquetteMobile=document.getElementById('etiquette-mobile');
const erreur=document.getElementById('erreur');
const premiereGraduation=document.getElementById('premiere-graduation');
const nombre=document.getElementById('nombre');
const suivant=document.getElementById('suivant');
const ok=document.getElementById('ok');
const boutonGraduations=document.getElementById('boutton-graduations');
const menu=document.getElementById('menu');
const stats=document.getElementById('stats');
const boutonMenu=document.getElementById('bouton-menu');
const boutonStats=document.getElementById('bouton-stats');
const resultats=document.getElementById('resultats');
const choixA=document.getElementById('choix-a');
const choixB=document.getElementById('choix-b');
const choixVerrA=document.getElementById('choix-verr-a');
const choixVerrB=document.getElementById('choix-verr-b');
const a1=document.getElementById('a1');
const a2=document.getElementById('a2');
const b1=document.getElementById('b1');
const b2=document.getElementById('b2');
const body=document.body;


//Paramètres URL

let url = window.location.search;
let urlParams = new URLSearchParams(url);

{
    boutonMenu.style.top='90px';
    menu.style.top='80px';
    body.style.backgroundImage='url(images/primtux.svg)';
  }



// Variables globales
dragged=null;
juste=false;
aidePremiereGraduation=false;
aideGraduations=false;
aideBlocUnite=false;
lock=false;
nbMobile=null;
menuOn=false;
petitEcran=false;
zoneSaisie=nombre;

// Nombres min / max
a=0;
b=10;
lockA=true;
lockB=true;

// Réglages des boutons;
choixA.value=a;
choixB.value=b;
a2.checked=true;
b2.checked=true;

// Autres réglages
nombre.style.fontSize='64px';



// Début des fonctions

function maj(entree){    
    if (entree===choixA){
        a=parseInt(entree.value);
        if (a==b-1){
            b=b+1;
            choixB.value=b;
        }
        verifieValeurA();
    } else if (entree===choixB){
        b=parseInt(entree.value);
        if (b==a+1){            
            a=a-1;
            choixA.value=a;
        }
        verifieValeurB();
    }   
    exerciceSuivant();

}

function changelockA(){
    lockA=!lockA;
    exerciceSuivant();

}

function changelockB(){
    lockB=!lockB;
    exerciceSuivant();

}

function active(bouton){
    bouton.classList.toggle('actif');
    bouton.blur();
}

function tirerBornesAuSort(a,b){
    // Vérification que a < b
    if (a >= b) {
        throw new Error("a doit être inférieur à b");
    }

    // Génération de deux nombres aléatoires entre a et b
    let nbDebut = Math.floor(Math.random() * (b-a-2)) + a ;
    let nbFin = Math.floor(Math.random() * (b-nbDebut-1)) + nbDebut +2 ;
    return [nbDebut, nbFin];
}

function tirerNombreauSort(a,b){
    // Vérification que a < b
    if (a >= b) {
        throw new Error("a doit être inférieur à b");
    }

    let nouveauNombreMobile=nbMobile;

    if (b===a+2){
        nouveauNombreMobile = a + 1;
        console.log("nouveau nombre mobile="+nouveauNombreMobile);
    } else {
        while (nbMobile===nouveauNombreMobile){
            nouveauNombreMobile = Math.floor(Math.random() * (b-a-1)) + a + 1 ;
            console.log("nouveau nombre mobile="+nouveauNombreMobile);
        }
    }

    return nouveauNombreMobile;


}

function majEtiquettes(){
    debut.innerHTML=nbDebut;
    fin.innerHTML=nbFin;
}

function afficheNombreMobile(){    
    nbMobile=tirerNombreauSort(nbDebut,nbFin);
    let ecartGlobal = nbFin - nbDebut;
    let ecartNbMobile = nbMobile - nbDebut;
    let positionXmobile = (ecartNbMobile / ecartGlobal) * 100;
    mobile.style.display = 'flex';
    mobile.style.left = etiquetteMobile.style.left = positionXmobile + '%';

}


function entreNombre(entree){
    if (!lock){
        if (entree==='⌫'){
            if (zoneSaisie.innerText.length===1){
                zoneSaisie.innerText='?';
                zoneSaisie.style.color='gray';
                nombre.style.fontSize='64px';
            } else {
                zoneSaisie.innerText = zoneSaisie.innerText.substring(0, zoneSaisie.innerText.length - 1);
            }
        } else {
            if (zoneSaisie.innerText==='?'){zoneSaisie.innerText='';zoneSaisie.style.color=null;nombre.style.fontSize=null;}
            zoneSaisie.innerText+=entree;
        }
    }
}

function verifieReponse(){    
    if (juste){exerciceSuivant()}
    else if (zoneSaisie.innerText==='?'){ // pas de réponse
        zoneSaisie.style.backgroundColor='yellow';
        lock=true;
        setTimeout(function() {
            zoneSaisie.style.backgroundColor = null;
            lock=false;
        }, 1000);
    }
    else { // juste
        let reponse = parseInt(zoneSaisie.innerText);
        if (reponse===nbMobile){
            zoneSaisie.style.backgroundColor='rgb(0,255,0)';
            suivant.classList.remove('hide');
            ok.classList.add('hide');
            erreur.classList.add('hide');
            juste=true;
            etiquetteMobile.innerHTML=reponse;
            etiquetteMobile.style.backgroundColor='rgb(0,255,0)';
            
        } else { // faux
            zoneSaisie.style.backgroundColor='rgb(255,0,0)';
            lock=true;
            afficheFaux(reponse);
            setTimeout(function() {
                nombre.style.fontSize='64px';
                zoneSaisie.innerText='?';
                zoneSaisie.style.color='gray';
                zoneSaisie.style.backgroundColor = null;
                juste=false;
                lock=false;
            }, 1000);
        }
        resultat(reponse);
    }
}

function afficheFaux(reponse){
    if (nbDebut<=reponse && reponse<=nbFin){
        erreur.classList.remove('hide');
        erreur.style.left=ecartement * (reponse - nbDebut) + '%';
        erreur.innerText = reponse;        
    }
}

function resultat(reponse){
    let ligne=document.createElement('p');
    resultats.appendChild(ligne);

    //Nombres
    let etiquette=document.createElement('span');
    etiquette.classList.add('etiquette-stat');
    etiquette.innerText=nbDebut;
    ligne.appendChild(etiquette);
    etiquette=document.createElement('span');
    etiquette.classList.add('etiquette-stat');
    etiquette.innerText=nbMobile;
    if (juste) {etiquette.style.backgroundColor='rgb(0, 255, 0)'}
    else {etiquette.style.backgroundColor='yellow';}
    ligne.appendChild(etiquette);
    if (!juste){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat');
        etiquette.style.backgroundColor='red';
        etiquette.innerText=reponse;
        ligne.appendChild(etiquette); 
    }
    etiquette=document.createElement('span');
    etiquette.classList.add('etiquette-stat');
    etiquette.innerText=nbFin;
    ligne.appendChild(etiquette);
    //Aides
    if (aidePremiereGraduation){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat','aide');
        etiquette.style.backgroundImage='url(images/graduation.svg)';
        ligne.appendChild(etiquette);
    } 
    if (aideGraduations){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat','aide');
        etiquette.style.backgroundImage='url(images/graduations.svg)';
        ligne.appendChild(etiquette);
    } 
    if (aideBlocUnite){
        etiquette=document.createElement('span');
        etiquette.classList.add('etiquette-stat','aide');
        etiquette.style.backgroundImage='url(images/bloc.svg)';
        ligne.appendChild(etiquette);
    } 

}

function effaceStats(){
    resultats.innerHTML='';
}



function exerciceSuivant(){
    console.log("ExerciceSuivant");
    const boutonsActifs = document.querySelectorAll('.actif');
    boutonsActifs.forEach(bouton=>{
        bouton.classList.remove('actif');
    });
    etiquetteMobile.innerText='?';
    nombre.innerText='?';
    zoneSaisie.style.color='gray';
    zoneSaisie.style.backgroundColor = null;
    bornes=tirerBornesAuSort(a,b);
    if (lockA){nbDebut=a}
    else{nbDebut=bornes[0];}
    if (lockB){nbFin=b}
    else{nbFin=bornes[1];}
    majEtiquettes(nbDebut,nbFin);
    afficheNombreMobile();
    creeGraduations();
    adapteBlocs();
    juste=false;
    suivant.classList.add('hide');
    ok.classList.remove('hide');
    nombre.style.fontSize='64px';
    aidePremiereGraduation=false;
    aideGraduations=false;
    aideBlocUnite=false;
    erreur.classList.add('hide');
    etiquetteMobile.style.backgroundColor=null;

}

function adapteBlocs(){
    const blocs = droite.querySelectorAll('.bloc-unite');
    blocs.forEach(bloc=>{
        bloc.style.width=ecartement+'%';
        bloc.classList.add('hide');
    });
}

function creeGraduations(){
    const graduations = droite.querySelectorAll('.toutes');
    graduations.forEach(graduation=>{
        graduation.remove();
    });


    let nombreDeGraduationsACreer = nbFin - nbDebut - 1;
    ecartement = 100 / (nbFin - nbDebut);

    premiereGraduation.style.left= ecartement + '%';
    premiereGraduation.classList.add('hide');


    for (let i = 0; i < nombreDeGraduationsACreer; i++) {
        let nouvelleGraduation = document.createElement('div');
        nouvelleGraduation.classList.add('graduation','toutes','hide');
        nouvelleGraduation.style.left= (i+1) * ecartement + '%';
        droite.appendChild(nouvelleGraduation);
    }
}

function visibilite(classe,nombre){
    const objets = document.querySelectorAll('.'+classe);
    objets.forEach(objet=>{
        objet.classList.toggle('hide');
    });
}

function estEnfantDe(element,parent) {
    // Parcours les parents de l'élément donné
    while (element.parentNode) {
        element = element.parentNode;
        // Si un des parents est l'élément menu, retourne vrai
        if (element === parent) {
            return true;
        }
    }
    // Si aucun parent n'est l'élément menu, retourne faux
    return false;
}

function clic(event){
    let cible = event.target;

    if (cible!=menu && !estEnfantDe(cible,menu) && cible !=boutonMenu){visibiliteMenu('ferme');}
    if (cible!=menu && !estEnfantDe(cible,stats) && cible !=boutonStats){visibiliteStats('ferme');}

    
    if (cible.classList.contains('draggable')) {
        posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        posY = event?.targetTouches?.[0]?.clientY || event.clientY;
        dragged = cible;
        event.preventDefault();
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        diffsourisx = (posX - posX_objet);
        diffsourisy = (posY - posY_objet);
        dragged.classList.add('dragged');
    }
}

function move(event) {
    event.preventDefault();
    if (dragged) { 
        posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        posY = event?.targetTouches?.[0]?.clientY || event.clientY;
        dragged.style.left = posX - diffsourisx + "px";
        dragged.style.top = posY - diffsourisy + "px";
    }
}

function release(event) {
    if (dragged) {
        dragged.classList.remove('dragged');
        dragged=null;
    }

}

function info(){
    let texte_info='Estimation est une application libre sous licence GNU/GPL.\nPar Arnaud Champollion\nPolice : Écriture A Rom de https://eduscol.education.fr\nsous licence CC BY ND'
    alert(texte_info);
}

function visibiliteMenu(mode){
    if (mode==='ouvre' && menu.style.left==='-415px'){
        menu.style.left='0px';
        menuOn=true;
    }
    else {
        menu.style.left='-415px';
        ok.focus();
        menuOn=false;
    }
}

function verifieValeurA(){
    if (a>b-2){
        b=a+2;
        choixB.value=b;
    }
}

function verifieValeurB(){
    if (a>b-2){
        a=b-2;
        choixA.value=a;
    }
}

function visibiliteStats(mode){
    if (mode==='ouvre' && stats.style.right==='-415px'){stats.style.right='0px';}
    else {stats.style.right='-415px';ok.focus();}
}

// Écouteurs de souris
document.addEventListener("touchstart", clic);
//document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

document.addEventListener('touchmove', function(event) {
    event.preventDefault();
    move;
}, { passive: false });

// Écouteurs de clavier
document.addEventListener('keydown', function(event) {
    if (!menuOn){
        if (!isNaN(event.key)){
            entreNombre(event.key);
        }
        else if (event.key === "-"){
            entreNombre(event.key);
        } else if (event.key === "Backspace"){
            entreNombre('⌫');
        } else  if (event.key === "Enter") {
            verifieReponse();
        }
    }    
  });

  // Vérifie la hauteur de l'écran lorsque la page est chargée
window.onload = function() {
    checkScreenHeight();
  };
  
  // Vérifie la hauteur de l'écran lorsque la fenêtre est redimensionnée
  window.onresize = function() {
    checkScreenHeight();
  };
  
  
  // Fonction pour vérifier la hauteur de l'écran

  function checkScreenHeight() {
    var screenHeight = window.innerHeight;
    if (screenHeight < 420) {
        if (!petitEcran){
            zoneSaisie=etiquetteMobile;
            etiquetteMobile.innerText=nombre.innerText;
            etiquetteMobile.style.backgroundColor=window.getComputedStyle(nombre).getPropertyValue("background-color");
        }
        petitEcran=true;

      // Ajoutez ici le code que vous souhaitez exécuter si la hauteur de l'écran est inférieure à 420 pixels
    } else {        
        zoneSaisie=nombre;
        if (petitEcran){
            nombre.innerText=etiquetteMobile.innerText;
            etiquetteMobile.innerText='?';
            console.log(window.getComputedStyle(etiquetteMobile).getPropertyValue("background-color"))
            nombre.style.backgroundColor=window.getComputedStyle(etiquetteMobile).getPropertyValue("background-color");
        }
        petitEcran=false;
    }
  }

// Lancement du premier exercice
exerciceSuivant();
