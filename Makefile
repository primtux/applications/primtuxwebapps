.ONESHELL:

.PHONY: unittest ruff flake8 requirements init gen check build.debian run.server

SHELL := /bin/bash

unittest:
	source ./bin/activate
	python3 -m unittest

ruff:
	source ./bin/activate
	ruff check .

flake8:
	source ./bin/activate
	flake8 --filename="./gen"
	make ruff

requirements:
	source ./bin/activate
	pip install -r requirements.txt

gen:
	source ./bin/activate
	python ./gen

check:
	source ./bin/activate
	python ./check

init:
	python3 -m venv .
	make requirements
	make gen

test:
	source ./bin/activate
	make unittest
	make flake8

build.debian:
	make gen
	debuild --no-lintian -us -uc

run.server:
	make gen
	python -m http.server 1234
