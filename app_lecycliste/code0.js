gdjs.menuCode = {};
gdjs.menuCode.GDbouton_95recommencerObjects1= [];
gdjs.menuCode.GDbouton_95recommencerObjects2= [];
gdjs.menuCode.GDbouton_95validerObjects1= [];
gdjs.menuCode.GDbouton_95validerObjects2= [];
gdjs.menuCode.GDbouton_95retourObjects1= [];
gdjs.menuCode.GDbouton_95retourObjects2= [];
gdjs.menuCode.GDcyclisteObjects1= [];
gdjs.menuCode.GDcyclisteObjects2= [];
gdjs.menuCode.GDajout_95uniteObjects1= [];
gdjs.menuCode.GDajout_95uniteObjects2= [];
gdjs.menuCode.GDajout_95dizaineObjects1= [];
gdjs.menuCode.GDajout_95dizaineObjects2= [];
gdjs.menuCode.GDretrait_95dizaineObjects1= [];
gdjs.menuCode.GDretrait_95dizaineObjects2= [];
gdjs.menuCode.GDretrait_95uniteObjects1= [];
gdjs.menuCode.GDretrait_95uniteObjects2= [];
gdjs.menuCode.GDfond_95paysage1Objects1= [];
gdjs.menuCode.GDfond_95paysage1Objects2= [];
gdjs.menuCode.GDscoreObjects1= [];
gdjs.menuCode.GDscoreObjects2= [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDjeu1Objects1= [];
gdjs.menuCode.GDjeu1Objects2= [];
gdjs.menuCode.GDbouton_95optionsObjects1= [];
gdjs.menuCode.GDbouton_95optionsObjects2= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDcreditObjects1= [];
gdjs.menuCode.GDcreditObjects2= [];

gdjs.menuCode.conditionTrue_0 = {val:false};
gdjs.menuCode.condition0IsTrue_0 = {val:false};
gdjs.menuCode.condition1IsTrue_0 = {val:false};
gdjs.menuCode.condition2IsTrue_0 = {val:false};
gdjs.menuCode.condition3IsTrue_0 = {val:false};
gdjs.menuCode.conditionTrue_1 = {val:false};
gdjs.menuCode.condition0IsTrue_1 = {val:false};
gdjs.menuCode.condition1IsTrue_1 = {val:false};
gdjs.menuCode.condition2IsTrue_1 = {val:false};
gdjs.menuCode.condition3IsTrue_1 = {val:false};


gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595optionsObjects1Objects = Hashtable.newFrom({"bouton_options": gdjs.menuCode.GDbouton_95optionsObjects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu1Objects1Objects = Hashtable.newFrom({"jeu1": gdjs.menuCode.GDjeu1Objects1});gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.menuCode.GDscoreObjects1.createFrom(runtimeScene.getObjects("score"));
{gdjs.evtTools.input.showCursor(runtimeScene);
}{for(var i = 0, len = gdjs.menuCode.GDscoreObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscoreObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}}

}


{

gdjs.menuCode.GDbouton_95optionsObjects1.createFrom(runtimeScene.getObjects("bouton_options"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDbouton_9595optionsObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "options", false);
}}

}


{

gdjs.menuCode.GDjeu1Objects1.createFrom(runtimeScene.getObjects("jeu1"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDjeu1Objects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.menuCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.menuCode.GDsergeObjects1.createFrom(runtimeScene.getObjects("serge"));

gdjs.menuCode.condition0IsTrue_0.val = false;
gdjs.menuCode.condition1IsTrue_0.val = false;
gdjs.menuCode.condition2IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_46menuCode_46GDsergeObjects1Objects, runtimeScene, true, false);
}if ( gdjs.menuCode.condition0IsTrue_0.val ) {
{
gdjs.menuCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.menuCode.condition1IsTrue_0.val ) {
{
{gdjs.menuCode.conditionTrue_1 = gdjs.menuCode.condition2IsTrue_0;
gdjs.menuCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9890244);
}
}}
}
if (gdjs.menuCode.condition2IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.menuCode.condition0IsTrue_0.val = false;
{
gdjs.menuCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
}if (gdjs.menuCode.condition0IsTrue_0.val) {
gdjs.menuCode.GDscoreObjects1.createFrom(runtimeScene.getObjects("score"));
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscoreObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDscoreObjects1[i].setAnimation(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.menuCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.menuCode.GDbouton_95validerObjects1.length = 0;
gdjs.menuCode.GDbouton_95validerObjects2.length = 0;
gdjs.menuCode.GDbouton_95retourObjects1.length = 0;
gdjs.menuCode.GDbouton_95retourObjects2.length = 0;
gdjs.menuCode.GDcyclisteObjects1.length = 0;
gdjs.menuCode.GDcyclisteObjects2.length = 0;
gdjs.menuCode.GDajout_95uniteObjects1.length = 0;
gdjs.menuCode.GDajout_95uniteObjects2.length = 0;
gdjs.menuCode.GDajout_95dizaineObjects1.length = 0;
gdjs.menuCode.GDajout_95dizaineObjects2.length = 0;
gdjs.menuCode.GDretrait_95dizaineObjects1.length = 0;
gdjs.menuCode.GDretrait_95dizaineObjects2.length = 0;
gdjs.menuCode.GDretrait_95uniteObjects1.length = 0;
gdjs.menuCode.GDretrait_95uniteObjects2.length = 0;
gdjs.menuCode.GDfond_95paysage1Objects1.length = 0;
gdjs.menuCode.GDfond_95paysage1Objects2.length = 0;
gdjs.menuCode.GDscoreObjects1.length = 0;
gdjs.menuCode.GDscoreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDjeu1Objects1.length = 0;
gdjs.menuCode.GDjeu1Objects2.length = 0;
gdjs.menuCode.GDbouton_95optionsObjects1.length = 0;
gdjs.menuCode.GDbouton_95optionsObjects2.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDcreditObjects1.length = 0;
gdjs.menuCode.GDcreditObjects2.length = 0;

gdjs.menuCode.eventsList0(runtimeScene);
return;

}

gdjs['menuCode'] = gdjs.menuCode;
