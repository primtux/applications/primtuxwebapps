gdjs.tirageCode = {};
gdjs.tirageCode.GDbouton_95recommencerObjects1= [];
gdjs.tirageCode.GDbouton_95recommencerObjects2= [];
gdjs.tirageCode.GDbouton_95recommencerObjects3= [];
gdjs.tirageCode.GDbouton_95recommencerObjects4= [];
gdjs.tirageCode.GDbouton_95validerObjects1= [];
gdjs.tirageCode.GDbouton_95validerObjects2= [];
gdjs.tirageCode.GDbouton_95validerObjects3= [];
gdjs.tirageCode.GDbouton_95validerObjects4= [];
gdjs.tirageCode.GDbouton_95retourObjects1= [];
gdjs.tirageCode.GDbouton_95retourObjects2= [];
gdjs.tirageCode.GDbouton_95retourObjects3= [];
gdjs.tirageCode.GDbouton_95retourObjects4= [];
gdjs.tirageCode.GDcyclisteObjects1= [];
gdjs.tirageCode.GDcyclisteObjects2= [];
gdjs.tirageCode.GDcyclisteObjects3= [];
gdjs.tirageCode.GDcyclisteObjects4= [];
gdjs.tirageCode.GDajout_95uniteObjects1= [];
gdjs.tirageCode.GDajout_95uniteObjects2= [];
gdjs.tirageCode.GDajout_95uniteObjects3= [];
gdjs.tirageCode.GDajout_95uniteObjects4= [];
gdjs.tirageCode.GDajout_95dizaineObjects1= [];
gdjs.tirageCode.GDajout_95dizaineObjects2= [];
gdjs.tirageCode.GDajout_95dizaineObjects3= [];
gdjs.tirageCode.GDajout_95dizaineObjects4= [];
gdjs.tirageCode.GDretrait_95dizaineObjects1= [];
gdjs.tirageCode.GDretrait_95dizaineObjects2= [];
gdjs.tirageCode.GDretrait_95dizaineObjects3= [];
gdjs.tirageCode.GDretrait_95dizaineObjects4= [];
gdjs.tirageCode.GDretrait_95uniteObjects1= [];
gdjs.tirageCode.GDretrait_95uniteObjects2= [];
gdjs.tirageCode.GDretrait_95uniteObjects3= [];
gdjs.tirageCode.GDretrait_95uniteObjects4= [];
gdjs.tirageCode.GDfond_95paysage1Objects1= [];
gdjs.tirageCode.GDfond_95paysage1Objects2= [];
gdjs.tirageCode.GDfond_95paysage1Objects3= [];
gdjs.tirageCode.GDfond_95paysage1Objects4= [];
gdjs.tirageCode.GDscoreObjects1= [];
gdjs.tirageCode.GDscoreObjects2= [];
gdjs.tirageCode.GDscoreObjects3= [];
gdjs.tirageCode.GDscoreObjects4= [];

gdjs.tirageCode.conditionTrue_0 = {val:false};
gdjs.tirageCode.condition0IsTrue_0 = {val:false};
gdjs.tirageCode.condition1IsTrue_0 = {val:false};
gdjs.tirageCode.condition2IsTrue_0 = {val:false};
gdjs.tirageCode.condition3IsTrue_0 = {val:false};
gdjs.tirageCode.condition4IsTrue_0 = {val:false};
gdjs.tirageCode.condition5IsTrue_0 = {val:false};
gdjs.tirageCode.condition6IsTrue_0 = {val:false};
gdjs.tirageCode.condition7IsTrue_0 = {val:false};
gdjs.tirageCode.condition8IsTrue_0 = {val:false};
gdjs.tirageCode.condition9IsTrue_0 = {val:false};
gdjs.tirageCode.condition10IsTrue_0 = {val:false};
gdjs.tirageCode.conditionTrue_1 = {val:false};
gdjs.tirageCode.condition0IsTrue_1 = {val:false};
gdjs.tirageCode.condition1IsTrue_1 = {val:false};
gdjs.tirageCode.condition2IsTrue_1 = {val:false};
gdjs.tirageCode.condition3IsTrue_1 = {val:false};
gdjs.tirageCode.condition4IsTrue_1 = {val:false};
gdjs.tirageCode.condition5IsTrue_1 = {val:false};
gdjs.tirageCode.condition6IsTrue_1 = {val:false};
gdjs.tirageCode.condition7IsTrue_1 = {val:false};
gdjs.tirageCode.condition8IsTrue_1 = {val:false};
gdjs.tirageCode.condition9IsTrue_1 = {val:false};
gdjs.tirageCode.condition10IsTrue_1 = {val:false};


gdjs.tirageCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("valeur")) == 1;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4")) == 1;
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(2);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("valeur")) == 2;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("10")) == 1;
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(2);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("valeur")) == 3;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("6")) == 1;
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(2);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("valeur")) == 4;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("8")) == 1;
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(2);
}}

}


};gdjs.tirageCode.eventsList1 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("boucle_tirage")) == 1;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("valeur").setNumber(gdjs.randomInRange(1, 4));
}
{ //Subevents
gdjs.tirageCode.eventsList0(runtimeScene);} //End of subevents
}

}


};gdjs.tirageCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition1IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
gdjs.tirageCode.condition3IsTrue_1.val = false;
gdjs.tirageCode.condition4IsTrue_1.val = false;
gdjs.tirageCode.condition5IsTrue_1.val = false;
gdjs.tirageCode.condition6IsTrue_1.val = false;
gdjs.tirageCode.condition7IsTrue_1.val = false;
gdjs.tirageCode.condition8IsTrue_1.val = false;
gdjs.tirageCode.condition9IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 0;
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 10;
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 20;
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition3IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 30;
if( gdjs.tirageCode.condition3IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition4IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 40;
if( gdjs.tirageCode.condition4IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition5IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 50;
if( gdjs.tirageCode.condition5IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition6IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 60;
if( gdjs.tirageCode.condition6IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition7IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 70;
if( gdjs.tirageCode.condition7IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition8IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 80;
if( gdjs.tirageCode.condition8IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition9IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur")) == 90;
if( gdjs.tirageCode.condition9IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(3);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 0;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(3);
}}

}


};gdjs.tirageCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("boucle_tirage")) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("valeur").setNumber(gdjs.randomInRange(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("mini")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("maxi"))));
}
{ //Subevents
gdjs.tirageCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("boucle_tirage")) == 3;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", false);
}}

}


};gdjs.tirageCode.mapOfGDgdjs_46tirageCode_46GDbouton_9595recommencerObjects1Objects = Hashtable.newFrom({"bouton_recommencer": gdjs.tirageCode.GDbouton_95recommencerObjects1});gdjs.tirageCode.mapOfGDgdjs_46tirageCode_46GDbouton_9595validerObjects1Objects = Hashtable.newFrom({"bouton_valider": gdjs.tirageCode.GDbouton_95validerObjects1});gdjs.tirageCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("boucle_tirage").setNumber(1);
}}

}


{


gdjs.tirageCode.eventsList1(runtimeScene);
}


{


gdjs.tirageCode.eventsList3(runtimeScene);
}


{

gdjs.tirageCode.GDbouton_95recommencerObjects1.createFrom(runtimeScene.getObjects("bouton_recommencer"));

gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.tirageCode.mapOfGDgdjs_46tirageCode_46GDbouton_9595recommencerObjects1Objects, runtimeScene, true, false);
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "tirage", false);
}}

}


{

gdjs.tirageCode.GDbouton_95validerObjects1.createFrom(runtimeScene.getObjects("bouton_valider"));

gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.tirageCode.mapOfGDgdjs_46tirageCode_46GDbouton_9595validerObjects1Objects, runtimeScene, true, false);
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.tirageCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", false);
}}

}


};

gdjs.tirageCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirageCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.tirageCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.tirageCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.tirageCode.GDbouton_95recommencerObjects4.length = 0;
gdjs.tirageCode.GDbouton_95validerObjects1.length = 0;
gdjs.tirageCode.GDbouton_95validerObjects2.length = 0;
gdjs.tirageCode.GDbouton_95validerObjects3.length = 0;
gdjs.tirageCode.GDbouton_95validerObjects4.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects3.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects4.length = 0;
gdjs.tirageCode.GDcyclisteObjects1.length = 0;
gdjs.tirageCode.GDcyclisteObjects2.length = 0;
gdjs.tirageCode.GDcyclisteObjects3.length = 0;
gdjs.tirageCode.GDcyclisteObjects4.length = 0;
gdjs.tirageCode.GDajout_95uniteObjects1.length = 0;
gdjs.tirageCode.GDajout_95uniteObjects2.length = 0;
gdjs.tirageCode.GDajout_95uniteObjects3.length = 0;
gdjs.tirageCode.GDajout_95uniteObjects4.length = 0;
gdjs.tirageCode.GDajout_95dizaineObjects1.length = 0;
gdjs.tirageCode.GDajout_95dizaineObjects2.length = 0;
gdjs.tirageCode.GDajout_95dizaineObjects3.length = 0;
gdjs.tirageCode.GDajout_95dizaineObjects4.length = 0;
gdjs.tirageCode.GDretrait_95dizaineObjects1.length = 0;
gdjs.tirageCode.GDretrait_95dizaineObjects2.length = 0;
gdjs.tirageCode.GDretrait_95dizaineObjects3.length = 0;
gdjs.tirageCode.GDretrait_95dizaineObjects4.length = 0;
gdjs.tirageCode.GDretrait_95uniteObjects1.length = 0;
gdjs.tirageCode.GDretrait_95uniteObjects2.length = 0;
gdjs.tirageCode.GDretrait_95uniteObjects3.length = 0;
gdjs.tirageCode.GDretrait_95uniteObjects4.length = 0;
gdjs.tirageCode.GDfond_95paysage1Objects1.length = 0;
gdjs.tirageCode.GDfond_95paysage1Objects2.length = 0;
gdjs.tirageCode.GDfond_95paysage1Objects3.length = 0;
gdjs.tirageCode.GDfond_95paysage1Objects4.length = 0;
gdjs.tirageCode.GDscoreObjects1.length = 0;
gdjs.tirageCode.GDscoreObjects2.length = 0;
gdjs.tirageCode.GDscoreObjects3.length = 0;
gdjs.tirageCode.GDscoreObjects4.length = 0;

gdjs.tirageCode.eventsList4(runtimeScene);
return;

}

gdjs['tirageCode'] = gdjs.tirageCode;
