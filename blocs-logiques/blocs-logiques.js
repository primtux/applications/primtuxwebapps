"use strict"

var elementsDessinChoisi = [];
var reduction = 4;
const largeurCanvas = 800;
const hauteurCanvas = 600;
// const tailleImgCouleur = 25;
var decX;
var decY;
const tolerancePosition = 20;
var dessinLibre = false;
var selectionFaite = false;

const options = {
  affichageCouleur: "image",
  selectionCouleur: "fixee",
}

const canvas = document.getElementById("graphCanvas");
const context = canvas.getContext("2d");
const btEffaceCanvas = document.getElementById('efface-canvas');
const btsDessin = document.getElementsByClassName("btcommande zoneActions_zoneGauche_choixDessins_bouton");
const  ecranBlanc = document.getElementById("ecran-blanc");
const aide = document.getElementById("aide");

function alea(min,max) {
  var nb = min + (max-min+1) * Math.random();
  return Math.floor(nb);
}

function cloneDessin(dessin) {
  elementsDessinChoisi = [];
  dessin.forEach(function(element){
    let copie = JSON.parse(JSON.stringify(element));
    elementsDessinChoisi.push(copie);
  });
}

function lanceAide(){
  ecranBlanc.classList.replace("invisible","visible");
  aide.classList.replace("invisible","visible");
  document.getElementById("btok").focus();
}

function quitteAide(ev) {
  ev.preventDefault();
  aide.classList.replace("visible","invisible");
  ecranBlanc.classList.replace("visible","invisible");
}

function deselectionneBoutons() {
  let i;
  for (i=0; i<btsDessin.length; i++) {
    btsDessin[i].classList.remove("zoneActions_zoneGauche_choixDessins_bouton--selected");
  }
}

function affichageCouleurs(modeAffichage) {
  options.affichageCouleur = modeAffichage;
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
}

function choixCouleurs(modeCouleur) {
  options.selectionCouleur = modeCouleur;
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
}

function getPosCouleur(element) {
  let position = {x: 0, y: 0};
  position.x = element.posX + (tailles[element.forme].largeur - 25) / 2;
  position.y = element.posY + (tailles[element.forme].hauteur - 25) / 2;
  if ((element.forme === "ptri") || (element.forme === "gtri")) {
    position.y = element.posY + tailles[element.forme].hauteur / 2;
  }
  return(position);
}

function effaceCanvas() {
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
}

function activeDessinLibre() {
  deselectionneBoutons();
  let btSelected = document.getElementById("dessin-libre");
  btSelected.classList.add("zoneActions_zoneGauche_choixDessins_bouton--selected");
  dessinLibre = true;
  context.globalCompositeOperation = 'multiply';
  btEffaceCanvas.disabled = false;
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
}

function dessineForme(elementsDessin) {
  let posCouleur = {x: 0, y: 0};
  context.font = '16px arial';
  for (let element of elementsDessin) {
      context.drawImage(element.img, element.posX, element.posY);
      posCouleur = getPosCouleur(element);
      if (options.affichageCouleur === "image") {
        context.drawImage(element.imgCouleur, posCouleur.x, posCouleur.y);
      }
      else {
        context.fillText(element.couleur, posCouleur.x - 5, posCouleur.y + 12);
      }
  }
}

/* function chargeElementsDessin(objet) {
  let couleur = "";
  if (!(objet in dessins)) {
      return;
  }
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
  let ctImg = 0;
  let ctCouleurs = 0;
  const nbImages = dessins[objet].length;
  cloneDessin(dessins[objet]);
  for (let dessin of elementsDessinChoisi) {
      dessin.img = new Image();
      dessin.img.src = dessin.url;
      dessin.imgCouleur = new Image();
      if (options.selectionCouleur === "aleatoire") {
        couleur = couleurs[alea(0,3)];
        dessin.couleur = couleur;
      }
      dessin.imgCouleur.src = "img/" + dessin.couleur + ".png";
      dessin.img.onload = function() {
          ctImg += 1;
          if ((ctImg === nbImages) && (ctCouleurs === nbImages)) {
              dessineForme(elementsDessinChoisi);
          }
      }
      dessin.imgCouleur.onload = function() {
          ctCouleurs += 1;
          if ((ctImg === nbImages) && (ctCouleurs === nbImages)) {
              dessineForme(elementsDessinChoisi);
          }
      }
  }
} */

function chargeElementsDessin() {
  let couleur = "";
  let ctImg = 0;
  let ctCouleurs = 0;
  const nbImages = elementsDessinChoisi.length;
  for (let dessin of elementsDessinChoisi) {
    dessin.img = new Image();
    dessin.img.src = dessin.url;
    dessin.imgCouleur = new Image();
    if (options.selectionCouleur === "aleatoire") {
      couleur = couleurs[alea(0,3)];
      dessin.couleur = couleur;
    }
    dessin.imgCouleur.src = "img/" + dessin.couleur + ".png";
    dessin.img.onload = function() {
        ctImg += 1;
        if ((ctImg === nbImages) && (ctCouleurs === nbImages)) {
            dessineForme(elementsDessinChoisi);
        }
    }
    dessin.imgCouleur.onload = function() {
        ctCouleurs += 1;
        if ((ctImg === nbImages) && (ctCouleurs === nbImages)) {
            dessineForme(elementsDessinChoisi);
        }
    }
  }
}

function chargeSelection(dessin) {
  if (!(dessin in dessins)) {
      return;
  }
  deselectionneBoutons();
  let btSelected = document.getElementById(dessin);
  btSelected.classList.add("zoneActions_zoneGauche_choixDessins_bouton--selected");
  dessinLibre = false;
  selectionFaite = true;
  context.globalCompositeOperation = 'source-over';
  btEffaceCanvas.disabled = true;
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
  cloneDessin(dessins[dessin]);
  chargeElementsDessin();
}

function creeFigure() {
  let i;
/*   let x = 0;
  let y = 0; */
  let tailleX, tailleY;
  let nbElements = alea(1,5);
  let dessin = [];
  deselectionneBoutons();
  let btSelected = document.getElementById("blocs-hasard");
  btSelected.classList.add("zoneActions_zoneGauche_choixDessins_bouton--selected");
  context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
  dessinLibre = false;
  selectionFaite = true;
  context.globalCompositeOperation = 'source-over';
  btEffaceCanvas.disabled = true;
  for (i=0; i<nbElements; i++) {
    let element = new Object;
    element.forme = elements[alea(0,9)];
    element.url = "img/" + element.forme + ".png";
    tailleX = tailles[element.forme].largeur;
    tailleY = tailles[element.forme].hauteur;
    element.posX = alea(0, largeurCanvas - tailleX -1);
    element.posY = alea(0, hauteurCanvas - tailleY -1);
    element.couleur = couleurs[alea(0,3)];
    dessin.push(element);
  }
  cloneDessin(dessin);
  chargeElementsDessin();
}

function elementPresent(proposition) {
  let indice = 0;
  let coordonnees = [-1,-1];
  for (let element of elementsDessinChoisi) {
    if ((proposition.forme === element.forme) && (proposition.couleur === element.couleur) && (proposition.posX >= element.posX - tolerancePosition) && (proposition.posX <= element.posX + tolerancePosition) && (proposition.posY >= element.posY - tolerancePosition) && (proposition.posY <= element.posY + tolerancePosition)) {
      coordonnees[0] = element.posX;
      coordonnees[1] = element.posY;
      elementsDessinChoisi.splice(indice,1);
      return coordonnees;
    }
    indice += 1;
  }
  return coordonnees;
}

function drag(ev) {
  let dde=(navigator.vendor) ? document.body : document.documentElement;
  let obj = ev.currentTarget;
  let img = new Image();
  img.src = ev.currentTarget.src;
  decX = (ev.clientX + dde.scrollLeft - obj.offsetLeft) * reduction;
  decY = (ev.clientY + dde.scrollTop - obj.offsetTop) * reduction;
  ev.dataTransfer.setDragImage(img, decX, decY);
  ev.dataTransfer.setData("Text", obj.id);
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drop(ev) {
  let dde=(navigator.vendor) ? document.body : document.documentElement;
  let dcx, dcy;
  let posElement = [];
  let figure = [];
  let proposition = {forme:"", couleur: "", posX: -1, posY: -1};
  ev.preventDefault();

  let img = new Image();
  let data = ev.dataTransfer.getData("Text");
  img.src = "img/" + data + ".png";
  dcx = ev.clientX + dde.scrollLeft - decX - canvas.offsetLeft - 1;
  dcy = ev.clientY + dde.scrollTop - decY - canvas.offsetTop - 1;
  figure = data.split("-");
  proposition.forme = figure[0];
  proposition.couleur = figure[1];
  proposition.posX = dcx;
  proposition.posY = dcy;
  if (dessinLibre) {
    context.drawImage(img,proposition.posX,proposition.posY);
    return;
  }
  posElement = elementPresent(proposition);
  if (posElement[0] >=0) {
    context.drawImage(img,posElement[0],posElement[1]);
  }
  if ((elementsDessinChoisi.length === 0) && (selectionFaite)) {
    showDialog('Bravo !',0.5,'img/happy-tux.png', 89, 91, 'left');
    selectionFaite = false;
    deselectionneBoutons();
  }
}

/* function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen;

    if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement) {
      requestFullScreen.call(docEl);
    }
    else {
      cancelFullScreen.call(doc);
    }
  }
 */
  /* window.onload = function() {
    toggleFullScreen()
  }
 */