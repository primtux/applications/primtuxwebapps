gdjs.infosCode = {};
gdjs.infosCode.GDscoreObjects1= [];
gdjs.infosCode.GDscoreObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDfond_95cadreObjects1= [];
gdjs.infosCode.GDfond_95cadreObjects2= [];
gdjs.infosCode.GDNewObjectObjects1= [];
gdjs.infosCode.GDNewObjectObjects2= [];
gdjs.infosCode.GDtexte_95info4Objects1= [];
gdjs.infosCode.GDtexte_95info4Objects2= [];
gdjs.infosCode.GDtexte_95info3Objects1= [];
gdjs.infosCode.GDtexte_95info3Objects2= [];
gdjs.infosCode.GDtexte_95info2Objects1= [];
gdjs.infosCode.GDtexte_95info2Objects2= [];
gdjs.infosCode.GDtexte_95infoObjects1= [];
gdjs.infosCode.GDtexte_95infoObjects2= [];
gdjs.infosCode.GDdemoObjects1= [];
gdjs.infosCode.GDdemoObjects2= [];
gdjs.infosCode.GDaide_95serge_953Objects1= [];
gdjs.infosCode.GDaide_95serge_953Objects2= [];
gdjs.infosCode.GDaide_95serge_952Objects1= [];
gdjs.infosCode.GDaide_95serge_952Objects2= [];
gdjs.infosCode.GDaide_95serge_951Objects1= [];
gdjs.infosCode.GDaide_95serge_951Objects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};
gdjs.infosCode.condition3IsTrue_0 = {val:false};
gdjs.infosCode.conditionTrue_1 = {val:false};
gdjs.infosCode.condition0IsTrue_1 = {val:false};
gdjs.infosCode.condition1IsTrue_1 = {val:false};
gdjs.infosCode.condition2IsTrue_1 = {val:false};
gdjs.infosCode.condition3IsTrue_1 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_cadre"), gdjs.infosCode.GDfond_95cadreObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95cadreObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95cadreObjects1[i].setOpacity(200);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
gdjs.infosCode.condition2IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.infosCode.condition1IsTrue_0.val ) {
{
{gdjs.infosCode.conditionTrue_1 = gdjs.infosCode.condition2IsTrue_0;
gdjs.infosCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(12653228);
}
}}
}
if (gdjs.infosCode.condition2IsTrue_0.val) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", true);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDscoreObjects1.length = 0;
gdjs.infosCode.GDscoreObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDfond_95cadreObjects1.length = 0;
gdjs.infosCode.GDfond_95cadreObjects2.length = 0;
gdjs.infosCode.GDNewObjectObjects1.length = 0;
gdjs.infosCode.GDNewObjectObjects2.length = 0;
gdjs.infosCode.GDtexte_95info4Objects1.length = 0;
gdjs.infosCode.GDtexte_95info4Objects2.length = 0;
gdjs.infosCode.GDtexte_95info3Objects1.length = 0;
gdjs.infosCode.GDtexte_95info3Objects2.length = 0;
gdjs.infosCode.GDtexte_95info2Objects1.length = 0;
gdjs.infosCode.GDtexte_95info2Objects2.length = 0;
gdjs.infosCode.GDtexte_95infoObjects1.length = 0;
gdjs.infosCode.GDtexte_95infoObjects2.length = 0;
gdjs.infosCode.GDdemoObjects1.length = 0;
gdjs.infosCode.GDdemoObjects2.length = 0;
gdjs.infosCode.GDaide_95serge_953Objects1.length = 0;
gdjs.infosCode.GDaide_95serge_953Objects2.length = 0;
gdjs.infosCode.GDaide_95serge_952Objects1.length = 0;
gdjs.infosCode.GDaide_95serge_952Objects2.length = 0;
gdjs.infosCode.GDaide_95serge_951Objects1.length = 0;
gdjs.infosCode.GDaide_95serge_951Objects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
return;

}

gdjs['infosCode'] = gdjs.infosCode;
