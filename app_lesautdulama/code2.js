gdjs.tirageCode = {};
gdjs.tirageCode.GDscoreObjects1= [];
gdjs.tirageCode.GDscoreObjects2= [];
gdjs.tirageCode.GDscoreObjects3= [];
gdjs.tirageCode.GDbouton_95retourObjects1= [];
gdjs.tirageCode.GDbouton_95retourObjects2= [];
gdjs.tirageCode.GDbouton_95retourObjects3= [];

gdjs.tirageCode.conditionTrue_0 = {val:false};
gdjs.tirageCode.condition0IsTrue_0 = {val:false};
gdjs.tirageCode.condition1IsTrue_0 = {val:false};
gdjs.tirageCode.condition2IsTrue_0 = {val:false};
gdjs.tirageCode.condition3IsTrue_0 = {val:false};
gdjs.tirageCode.condition4IsTrue_0 = {val:false};
gdjs.tirageCode.condition5IsTrue_0 = {val:false};
gdjs.tirageCode.condition6IsTrue_0 = {val:false};
gdjs.tirageCode.condition7IsTrue_0 = {val:false};
gdjs.tirageCode.conditionTrue_1 = {val:false};
gdjs.tirageCode.condition0IsTrue_1 = {val:false};
gdjs.tirageCode.condition1IsTrue_1 = {val:false};
gdjs.tirageCode.condition2IsTrue_1 = {val:false};
gdjs.tirageCode.condition3IsTrue_1 = {val:false};
gdjs.tirageCode.condition4IsTrue_1 = {val:false};
gdjs.tirageCode.condition5IsTrue_1 = {val:false};
gdjs.tirageCode.condition6IsTrue_1 = {val:false};
gdjs.tirageCode.condition7IsTrue_1 = {val:false};


gdjs.tirageCode.eventsList0 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) <= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"));
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("1").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")) + gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"));
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList1 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) >= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
}}
}
if (gdjs.tirageCode.condition2IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("2").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")) - gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) < gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList2 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
gdjs.tirageCode.condition3IsTrue_0.val = false;
gdjs.tirageCode.condition4IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) <= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"));
}if ( gdjs.tirageCode.condition2IsTrue_0.val ) {
{
gdjs.tirageCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
}if ( gdjs.tirageCode.condition3IsTrue_0.val ) {
{
gdjs.tirageCode.condition4IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
}}
}
}
}
if (gdjs.tirageCode.condition4IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("3").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")) + gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
gdjs.tirageCode.condition3IsTrue_1.val = false;
gdjs.tirageCode.condition4IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
if( gdjs.tirageCode.condition3IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
if( gdjs.tirageCode.condition4IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList3 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
gdjs.tirageCode.condition1IsTrue_0.val = false;
gdjs.tirageCode.condition2IsTrue_0.val = false;
gdjs.tirageCode.condition3IsTrue_0.val = false;
gdjs.tirageCode.condition4IsTrue_0.val = false;
gdjs.tirageCode.condition5IsTrue_0.val = false;
gdjs.tirageCode.condition6IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
}if ( gdjs.tirageCode.condition0IsTrue_0.val ) {
{
gdjs.tirageCode.condition1IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
}if ( gdjs.tirageCode.condition1IsTrue_0.val ) {
{
gdjs.tirageCode.condition2IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
}if ( gdjs.tirageCode.condition2IsTrue_0.val ) {
{
gdjs.tirageCode.condition3IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) > gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini"));
}if ( gdjs.tirageCode.condition3IsTrue_0.val ) {
{
gdjs.tirageCode.condition4IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
}if ( gdjs.tirageCode.condition4IsTrue_0.val ) {
{
gdjs.tirageCode.condition5IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
}if ( gdjs.tirageCode.condition5IsTrue_0.val ) {
{
gdjs.tirageCode.condition6IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("3")) != gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
}}
}
}
}
}
}
if (gdjs.tirageCode.condition6IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("4").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")) - gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
gdjs.tirageCode.condition2IsTrue_1.val = false;
gdjs.tirageCode.condition3IsTrue_1.val = false;
gdjs.tirageCode.condition4IsTrue_1.val = false;
gdjs.tirageCode.condition5IsTrue_1.val = false;
gdjs.tirageCode.condition6IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition2IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0"));
if( gdjs.tirageCode.condition2IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition3IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0")) <= gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini"));
if( gdjs.tirageCode.condition3IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition4IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
if( gdjs.tirageCode.condition4IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition5IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
if( gdjs.tirageCode.condition5IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition6IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("3")) == gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0"));
if( gdjs.tirageCode.condition6IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().getFromIndex(0).sub(1);
}}

}


};gdjs.tirageCode.eventsList4 = function(runtimeScene) {

{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nombre").setNumber(0);
}{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 5;
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0").setNumber(gdjs.randomInRange(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")), gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"))));
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0").setNumber(gdjs.randomInRange(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("mini")), gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("maxi"))));
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")) + gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
{gdjs.tirageCode.conditionTrue_1 = gdjs.tirageCode.condition0IsTrue_0;
gdjs.tirageCode.condition0IsTrue_1.val = false;
gdjs.tirageCode.condition1IsTrue_1.val = false;
{
gdjs.tirageCode.condition0IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 3;
if( gdjs.tirageCode.condition0IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
gdjs.tirageCode.condition1IsTrue_1.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 7;
if( gdjs.tirageCode.condition1IsTrue_1.val ) {
    gdjs.tirageCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0").setNumber(gdjs.randomInRange(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("mini")), gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("maxi"))));
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0").setNumber(gdjs.randomInRange(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("mini")), gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("maxi"))));
}{runtimeScene.getGame().getVariables().getFromIndex(2).getChild("0").setNumber(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("0")) - gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("0")));
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 2;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 4;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 6;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList2(runtimeScene);} //End of subevents
}

}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 8;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.tirageCode.eventsList3(runtimeScene);} //End of subevents
}

}


{



}


{


gdjs.tirageCode.condition0IsTrue_0.val = false;
{
gdjs.tirageCode.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 9;
}if (gdjs.tirageCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


{


{
}

}


};

gdjs.tirageCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tirageCode.GDscoreObjects1.length = 0;
gdjs.tirageCode.GDscoreObjects2.length = 0;
gdjs.tirageCode.GDscoreObjects3.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects1.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects2.length = 0;
gdjs.tirageCode.GDbouton_95retourObjects3.length = 0;

gdjs.tirageCode.eventsList4(runtimeScene);
return;

}

gdjs['tirageCode'] = gdjs.tirageCode;
