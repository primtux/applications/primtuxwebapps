gdjs.tableau_9525Code = {};
gdjs.tableau_9525Code.GDvideoObjects1= [];
gdjs.tableau_9525Code.GDvideoObjects2= [];
gdjs.tableau_9525Code.GDvideoObjects3= [];
gdjs.tableau_9525Code.GDcase_9595depotObjects1= [];
gdjs.tableau_9525Code.GDcase_9595depotObjects2= [];
gdjs.tableau_9525Code.GDcase_9595depotObjects3= [];
gdjs.tableau_9525Code.GDjeton_9595sergeObjects1= [];
gdjs.tableau_9525Code.GDjeton_9595sergeObjects2= [];
gdjs.tableau_9525Code.GDjeton_9595sergeObjects3= [];
gdjs.tableau_9525Code.GDjeton_9595suzieObjects1= [];
gdjs.tableau_9525Code.GDjeton_9595suzieObjects2= [];
gdjs.tableau_9525Code.GDjeton_9595suzieObjects3= [];
gdjs.tableau_9525Code.GDjeton_9595oiseauObjects1= [];
gdjs.tableau_9525Code.GDjeton_9595oiseauObjects2= [];
gdjs.tableau_9525Code.GDjeton_9595oiseauObjects3= [];
gdjs.tableau_9525Code.GDjeton_9595emmaObjects1= [];
gdjs.tableau_9525Code.GDjeton_9595emmaObjects2= [];
gdjs.tableau_9525Code.GDjeton_9595emmaObjects3= [];
gdjs.tableau_9525Code.GDjeton_9595alexObjects1= [];
gdjs.tableau_9525Code.GDjeton_9595alexObjects2= [];
gdjs.tableau_9525Code.GDjeton_9595alexObjects3= [];
gdjs.tableau_9525Code.GDbouton_9595validerObjects1= [];
gdjs.tableau_9525Code.GDbouton_9595validerObjects2= [];
gdjs.tableau_9525Code.GDbouton_9595validerObjects3= [];
gdjs.tableau_9525Code.GDcase_9595griseObjects1= [];
gdjs.tableau_9525Code.GDcase_9595griseObjects2= [];
gdjs.tableau_9525Code.GDcase_9595griseObjects3= [];
gdjs.tableau_9525Code.GDbouton_9595retourObjects1= [];
gdjs.tableau_9525Code.GDbouton_9595retourObjects2= [];
gdjs.tableau_9525Code.GDbouton_9595retourObjects3= [];
gdjs.tableau_9525Code.GDligneObjects1= [];
gdjs.tableau_9525Code.GDligneObjects2= [];
gdjs.tableau_9525Code.GDligneObjects3= [];
gdjs.tableau_9525Code.GDbouton_9595sergeObjects1= [];
gdjs.tableau_9525Code.GDbouton_9595sergeObjects2= [];
gdjs.tableau_9525Code.GDbouton_9595sergeObjects3= [];
gdjs.tableau_9525Code.GDfondObjects1= [];
gdjs.tableau_9525Code.GDfondObjects2= [];
gdjs.tableau_9525Code.GDfondObjects3= [];
gdjs.tableau_9525Code.GDbouton_9595zoomObjects1= [];
gdjs.tableau_9525Code.GDbouton_9595zoomObjects2= [];
gdjs.tableau_9525Code.GDbouton_9595zoomObjects3= [];
gdjs.tableau_9525Code.GDfond_9595zoomObjects1= [];
gdjs.tableau_9525Code.GDfond_9595zoomObjects2= [];
gdjs.tableau_9525Code.GDfond_9595zoomObjects3= [];
gdjs.tableau_9525Code.GDCrossObjects1= [];
gdjs.tableau_9525Code.GDCrossObjects2= [];
gdjs.tableau_9525Code.GDCrossObjects3= [];
gdjs.tableau_9525Code.GDfleche_9595hautObjects1= [];
gdjs.tableau_9525Code.GDfleche_9595hautObjects2= [];
gdjs.tableau_9525Code.GDfleche_9595hautObjects3= [];
gdjs.tableau_9525Code.GDfleche_9595basObjects1= [];
gdjs.tableau_9525Code.GDfleche_9595basObjects2= [];
gdjs.tableau_9525Code.GDfleche_9595basObjects3= [];
gdjs.tableau_9525Code.GDvraiObjects1= [];
gdjs.tableau_9525Code.GDvraiObjects2= [];
gdjs.tableau_9525Code.GDvraiObjects3= [];
gdjs.tableau_9525Code.GDbouton_9595consigneObjects1= [];
gdjs.tableau_9525Code.GDbouton_9595consigneObjects2= [];
gdjs.tableau_9525Code.GDbouton_9595consigneObjects3= [];
gdjs.tableau_9525Code.GDfleche_9595consigneObjects1= [];
gdjs.tableau_9525Code.GDfleche_9595consigneObjects2= [];
gdjs.tableau_9525Code.GDfleche_9595consigneObjects3= [];


gdjs.tableau_9525Code.mapOfGDgdjs_9546tableau_95959525Code_9546GDbouton_95959595retourObjects2Objects = Hashtable.newFrom({"bouton_retour": gdjs.tableau_9525Code.GDbouton_9595retourObjects2});
gdjs.tableau_9525Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.createObjectsFromExternalLayout(runtimeScene, "ui", 0, 0, 0);
}}

}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.tableau_9525Code.GDbouton_9595retourObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.tableau_9525Code.mapOfGDgdjs_9546tableau_95959525Code_9546GDbouton_95959595retourObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("bouton_consigne"), gdjs.tableau_9525Code.GDbouton_9595consigneObjects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.tableau_9525Code.GDbouton_9595retourObjects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_valider"), gdjs.tableau_9525Code.GDbouton_9595validerObjects1);
gdjs.copyArray(runtimeScene.getObjects("bouton_zoom"), gdjs.tableau_9525Code.GDbouton_9595zoomObjects1);
gdjs.copyArray(runtimeScene.getObjects("fleche_consigne"), gdjs.tableau_9525Code.GDfleche_9595consigneObjects1);
gdjs.copyArray(runtimeScene.getObjects("video"), gdjs.tableau_9525Code.GDvideoObjects1);
{for(var i = 0, len = gdjs.tableau_9525Code.GDbouton_9595zoomObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDbouton_9595zoomObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.tableau_9525Code.GDbouton_9595validerObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDbouton_9595validerObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.tableau_9525Code.GDvideoObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDvideoObjects1[i].play();
}
}{for(var i = 0, len = gdjs.tableau_9525Code.GDbouton_9595retourObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDbouton_9595retourObjects1[i].getBehavior("Animation").pauseAnimation();
}
}{for(var i = 0, len = gdjs.tableau_9525Code.GDfleche_9595consigneObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDfleche_9595consigneObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.tableau_9525Code.GDbouton_9595consigneObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDbouton_9595consigneObjects1[i].hide();
}
}}

}


};gdjs.tableau_9525Code.eventsList1 = function(runtimeScene) {

{


gdjs.tableau_9525Code.eventsList0(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("video"), gdjs.tableau_9525Code.GDvideoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.tableau_9525Code.GDvideoObjects1.length;i<l;++i) {
    if ( gdjs.tableau_9525Code.GDvideoObjects1[i].isEnded() ) {
        isConditionTrue_0 = true;
        gdjs.tableau_9525Code.GDvideoObjects1[k] = gdjs.tableau_9525Code.GDvideoObjects1[i];
        ++k;
    }
}
gdjs.tableau_9525Code.GDvideoObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(19294092);
}
}
if (isConditionTrue_0) {
/* Reuse gdjs.tableau_9525Code.GDvideoObjects1 */
{for(var i = 0, len = gdjs.tableau_9525Code.GDvideoObjects1.length ;i < len;++i) {
    gdjs.tableau_9525Code.GDvideoObjects1[i].pause();
}
}}

}


};

gdjs.tableau_9525Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.tableau_9525Code.GDvideoObjects1.length = 0;
gdjs.tableau_9525Code.GDvideoObjects2.length = 0;
gdjs.tableau_9525Code.GDvideoObjects3.length = 0;
gdjs.tableau_9525Code.GDcase_9595depotObjects1.length = 0;
gdjs.tableau_9525Code.GDcase_9595depotObjects2.length = 0;
gdjs.tableau_9525Code.GDcase_9595depotObjects3.length = 0;
gdjs.tableau_9525Code.GDjeton_9595sergeObjects1.length = 0;
gdjs.tableau_9525Code.GDjeton_9595sergeObjects2.length = 0;
gdjs.tableau_9525Code.GDjeton_9595sergeObjects3.length = 0;
gdjs.tableau_9525Code.GDjeton_9595suzieObjects1.length = 0;
gdjs.tableau_9525Code.GDjeton_9595suzieObjects2.length = 0;
gdjs.tableau_9525Code.GDjeton_9595suzieObjects3.length = 0;
gdjs.tableau_9525Code.GDjeton_9595oiseauObjects1.length = 0;
gdjs.tableau_9525Code.GDjeton_9595oiseauObjects2.length = 0;
gdjs.tableau_9525Code.GDjeton_9595oiseauObjects3.length = 0;
gdjs.tableau_9525Code.GDjeton_9595emmaObjects1.length = 0;
gdjs.tableau_9525Code.GDjeton_9595emmaObjects2.length = 0;
gdjs.tableau_9525Code.GDjeton_9595emmaObjects3.length = 0;
gdjs.tableau_9525Code.GDjeton_9595alexObjects1.length = 0;
gdjs.tableau_9525Code.GDjeton_9595alexObjects2.length = 0;
gdjs.tableau_9525Code.GDjeton_9595alexObjects3.length = 0;
gdjs.tableau_9525Code.GDbouton_9595validerObjects1.length = 0;
gdjs.tableau_9525Code.GDbouton_9595validerObjects2.length = 0;
gdjs.tableau_9525Code.GDbouton_9595validerObjects3.length = 0;
gdjs.tableau_9525Code.GDcase_9595griseObjects1.length = 0;
gdjs.tableau_9525Code.GDcase_9595griseObjects2.length = 0;
gdjs.tableau_9525Code.GDcase_9595griseObjects3.length = 0;
gdjs.tableau_9525Code.GDbouton_9595retourObjects1.length = 0;
gdjs.tableau_9525Code.GDbouton_9595retourObjects2.length = 0;
gdjs.tableau_9525Code.GDbouton_9595retourObjects3.length = 0;
gdjs.tableau_9525Code.GDligneObjects1.length = 0;
gdjs.tableau_9525Code.GDligneObjects2.length = 0;
gdjs.tableau_9525Code.GDligneObjects3.length = 0;
gdjs.tableau_9525Code.GDbouton_9595sergeObjects1.length = 0;
gdjs.tableau_9525Code.GDbouton_9595sergeObjects2.length = 0;
gdjs.tableau_9525Code.GDbouton_9595sergeObjects3.length = 0;
gdjs.tableau_9525Code.GDfondObjects1.length = 0;
gdjs.tableau_9525Code.GDfondObjects2.length = 0;
gdjs.tableau_9525Code.GDfondObjects3.length = 0;
gdjs.tableau_9525Code.GDbouton_9595zoomObjects1.length = 0;
gdjs.tableau_9525Code.GDbouton_9595zoomObjects2.length = 0;
gdjs.tableau_9525Code.GDbouton_9595zoomObjects3.length = 0;
gdjs.tableau_9525Code.GDfond_9595zoomObjects1.length = 0;
gdjs.tableau_9525Code.GDfond_9595zoomObjects2.length = 0;
gdjs.tableau_9525Code.GDfond_9595zoomObjects3.length = 0;
gdjs.tableau_9525Code.GDCrossObjects1.length = 0;
gdjs.tableau_9525Code.GDCrossObjects2.length = 0;
gdjs.tableau_9525Code.GDCrossObjects3.length = 0;
gdjs.tableau_9525Code.GDfleche_9595hautObjects1.length = 0;
gdjs.tableau_9525Code.GDfleche_9595hautObjects2.length = 0;
gdjs.tableau_9525Code.GDfleche_9595hautObjects3.length = 0;
gdjs.tableau_9525Code.GDfleche_9595basObjects1.length = 0;
gdjs.tableau_9525Code.GDfleche_9595basObjects2.length = 0;
gdjs.tableau_9525Code.GDfleche_9595basObjects3.length = 0;
gdjs.tableau_9525Code.GDvraiObjects1.length = 0;
gdjs.tableau_9525Code.GDvraiObjects2.length = 0;
gdjs.tableau_9525Code.GDvraiObjects3.length = 0;
gdjs.tableau_9525Code.GDbouton_9595consigneObjects1.length = 0;
gdjs.tableau_9525Code.GDbouton_9595consigneObjects2.length = 0;
gdjs.tableau_9525Code.GDbouton_9595consigneObjects3.length = 0;
gdjs.tableau_9525Code.GDfleche_9595consigneObjects1.length = 0;
gdjs.tableau_9525Code.GDfleche_9595consigneObjects2.length = 0;
gdjs.tableau_9525Code.GDfleche_9595consigneObjects3.length = 0;

gdjs.tableau_9525Code.eventsList1(runtimeScene);

return;

}

gdjs['tableau_9525Code'] = gdjs.tableau_9525Code;
