gdjs.introCode = {};
gdjs.introCode.GDfond_9595introObjects1= [];
gdjs.introCode.GDfond_9595introObjects2= [];
gdjs.introCode.GDbouton_9595valider2Objects1= [];
gdjs.introCode.GDbouton_9595valider2Objects2= [];
gdjs.introCode.GDchargementObjects1= [];
gdjs.introCode.GDchargementObjects2= [];
gdjs.introCode.GDcase_9595depotObjects1= [];
gdjs.introCode.GDcase_9595depotObjects2= [];
gdjs.introCode.GDjeton_9595sergeObjects1= [];
gdjs.introCode.GDjeton_9595sergeObjects2= [];
gdjs.introCode.GDjeton_9595suzieObjects1= [];
gdjs.introCode.GDjeton_9595suzieObjects2= [];
gdjs.introCode.GDjeton_9595oiseauObjects1= [];
gdjs.introCode.GDjeton_9595oiseauObjects2= [];
gdjs.introCode.GDjeton_9595emmaObjects1= [];
gdjs.introCode.GDjeton_9595emmaObjects2= [];
gdjs.introCode.GDjeton_9595alexObjects1= [];
gdjs.introCode.GDjeton_9595alexObjects2= [];
gdjs.introCode.GDbouton_9595validerObjects1= [];
gdjs.introCode.GDbouton_9595validerObjects2= [];
gdjs.introCode.GDcase_9595griseObjects1= [];
gdjs.introCode.GDcase_9595griseObjects2= [];
gdjs.introCode.GDbouton_9595retourObjects1= [];
gdjs.introCode.GDbouton_9595retourObjects2= [];
gdjs.introCode.GDligneObjects1= [];
gdjs.introCode.GDligneObjects2= [];
gdjs.introCode.GDbouton_9595sergeObjects1= [];
gdjs.introCode.GDbouton_9595sergeObjects2= [];
gdjs.introCode.GDfondObjects1= [];
gdjs.introCode.GDfondObjects2= [];
gdjs.introCode.GDbouton_9595zoomObjects1= [];
gdjs.introCode.GDbouton_9595zoomObjects2= [];
gdjs.introCode.GDfond_9595zoomObjects1= [];
gdjs.introCode.GDfond_9595zoomObjects2= [];
gdjs.introCode.GDCrossObjects1= [];
gdjs.introCode.GDCrossObjects2= [];
gdjs.introCode.GDfleche_9595hautObjects1= [];
gdjs.introCode.GDfleche_9595hautObjects2= [];
gdjs.introCode.GDfleche_9595basObjects1= [];
gdjs.introCode.GDfleche_9595basObjects2= [];
gdjs.introCode.GDvraiObjects1= [];
gdjs.introCode.GDvraiObjects2= [];
gdjs.introCode.GDbouton_9595consigneObjects1= [];
gdjs.introCode.GDbouton_9595consigneObjects2= [];
gdjs.introCode.GDfleche_9595consigneObjects1= [];
gdjs.introCode.GDfleche_9595consigneObjects2= [];


gdjs.introCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/applaudissements.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/casquette.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/bonnet.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cache-oreille.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/collier.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/echarpe.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cheminee.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/chemise.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/maison.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/manteau.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/pull.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/renne.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/robe.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/sapin.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/traineau.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/gris.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/bleu.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rouge.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rose.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/vert.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/position_sur.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/position_a-cote.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/position_dans.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/position_devant.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/position_derriere.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/position_sous.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/maison_rouge.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/maison_verte.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/maison_bleue.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/table.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/bonhomme_neige.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/chalet.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/arbre.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/skis.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/reverbere.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/banc.mp3");
}}

}


};gdjs.introCode.eventsList1 = function(runtimeScene) {

{


gdjs.introCode.eventsList0(runtimeScene);
}


};gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595valider2Objects1Objects = Hashtable.newFrom({"bouton_valider2": gdjs.introCode.GDbouton_9595valider2Objects1});
gdjs.introCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider2"), gdjs.introCode.GDbouton_9595valider2Objects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "temps");
}{for(var i = 0, len = gdjs.introCode.GDbouton_9595valider2Objects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595valider2Objects1[i].hide();
}
}
{ //Subevents
gdjs.introCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "temps") >= 5;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(17771612);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_valider2"), gdjs.introCode.GDbouton_9595valider2Objects1);
gdjs.copyArray(runtimeScene.getObjects("chargement"), gdjs.introCode.GDchargementObjects1);
{for(var i = 0, len = gdjs.introCode.GDbouton_9595valider2Objects1.length ;i < len;++i) {
    gdjs.introCode.GDbouton_9595valider2Objects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.introCode.GDchargementObjects1.length ;i < len;++i) {
    gdjs.introCode.GDchargementObjects1[i].hide();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_valider2"), gdjs.introCode.GDbouton_9595valider2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.introCode.mapOfGDgdjs_9546introCode_9546GDbouton_95959595valider2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.introCode.GDbouton_9595valider2Objects1.length;i<l;++i) {
    if ( gdjs.introCode.GDbouton_9595valider2Objects1[i].isVisible() ) {
        isConditionTrue_0 = true;
        gdjs.introCode.GDbouton_9595valider2Objects1[k] = gdjs.introCode.GDbouton_9595valider2Objects1[i];
        ++k;
    }
}
gdjs.introCode.GDbouton_9595valider2Objects1.length = k;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.introCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.introCode.GDfond_9595introObjects1.length = 0;
gdjs.introCode.GDfond_9595introObjects2.length = 0;
gdjs.introCode.GDbouton_9595valider2Objects1.length = 0;
gdjs.introCode.GDbouton_9595valider2Objects2.length = 0;
gdjs.introCode.GDchargementObjects1.length = 0;
gdjs.introCode.GDchargementObjects2.length = 0;
gdjs.introCode.GDcase_9595depotObjects1.length = 0;
gdjs.introCode.GDcase_9595depotObjects2.length = 0;
gdjs.introCode.GDjeton_9595sergeObjects1.length = 0;
gdjs.introCode.GDjeton_9595sergeObjects2.length = 0;
gdjs.introCode.GDjeton_9595suzieObjects1.length = 0;
gdjs.introCode.GDjeton_9595suzieObjects2.length = 0;
gdjs.introCode.GDjeton_9595oiseauObjects1.length = 0;
gdjs.introCode.GDjeton_9595oiseauObjects2.length = 0;
gdjs.introCode.GDjeton_9595emmaObjects1.length = 0;
gdjs.introCode.GDjeton_9595emmaObjects2.length = 0;
gdjs.introCode.GDjeton_9595alexObjects1.length = 0;
gdjs.introCode.GDjeton_9595alexObjects2.length = 0;
gdjs.introCode.GDbouton_9595validerObjects1.length = 0;
gdjs.introCode.GDbouton_9595validerObjects2.length = 0;
gdjs.introCode.GDcase_9595griseObjects1.length = 0;
gdjs.introCode.GDcase_9595griseObjects2.length = 0;
gdjs.introCode.GDbouton_9595retourObjects1.length = 0;
gdjs.introCode.GDbouton_9595retourObjects2.length = 0;
gdjs.introCode.GDligneObjects1.length = 0;
gdjs.introCode.GDligneObjects2.length = 0;
gdjs.introCode.GDbouton_9595sergeObjects1.length = 0;
gdjs.introCode.GDbouton_9595sergeObjects2.length = 0;
gdjs.introCode.GDfondObjects1.length = 0;
gdjs.introCode.GDfondObjects2.length = 0;
gdjs.introCode.GDbouton_9595zoomObjects1.length = 0;
gdjs.introCode.GDbouton_9595zoomObjects2.length = 0;
gdjs.introCode.GDfond_9595zoomObjects1.length = 0;
gdjs.introCode.GDfond_9595zoomObjects2.length = 0;
gdjs.introCode.GDCrossObjects1.length = 0;
gdjs.introCode.GDCrossObjects2.length = 0;
gdjs.introCode.GDfleche_9595hautObjects1.length = 0;
gdjs.introCode.GDfleche_9595hautObjects2.length = 0;
gdjs.introCode.GDfleche_9595basObjects1.length = 0;
gdjs.introCode.GDfleche_9595basObjects2.length = 0;
gdjs.introCode.GDvraiObjects1.length = 0;
gdjs.introCode.GDvraiObjects2.length = 0;
gdjs.introCode.GDbouton_9595consigneObjects1.length = 0;
gdjs.introCode.GDbouton_9595consigneObjects2.length = 0;
gdjs.introCode.GDfleche_9595consigneObjects1.length = 0;
gdjs.introCode.GDfleche_9595consigneObjects2.length = 0;

gdjs.introCode.eventsList2(runtimeScene);

return;

}

gdjs['introCode'] = gdjs.introCode;
