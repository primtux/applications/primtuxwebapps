gdjs.chargementCode = {};
gdjs.chargementCode.GDsergeObjects1= [];
gdjs.chargementCode.GDsergeObjects2= [];
gdjs.chargementCode.GDsergeObjects3= [];
gdjs.chargementCode.GDvaseObjects1= [];
gdjs.chargementCode.GDvaseObjects2= [];
gdjs.chargementCode.GDvaseObjects3= [];
gdjs.chargementCode.GDfleur_95modeleObjects1= [];
gdjs.chargementCode.GDfleur_95modeleObjects2= [];
gdjs.chargementCode.GDfleur_95modeleObjects3= [];
gdjs.chargementCode.GDfleur_95modele5Objects1= [];
gdjs.chargementCode.GDfleur_95modele5Objects2= [];
gdjs.chargementCode.GDfleur_95modele5Objects3= [];
gdjs.chargementCode.GDfleur_95modele6Objects1= [];
gdjs.chargementCode.GDfleur_95modele6Objects2= [];
gdjs.chargementCode.GDfleur_95modele6Objects3= [];
gdjs.chargementCode.GDfleur_95modele7Objects1= [];
gdjs.chargementCode.GDfleur_95modele7Objects2= [];
gdjs.chargementCode.GDfleur_95modele7Objects3= [];
gdjs.chargementCode.GDfleur_95modele8Objects1= [];
gdjs.chargementCode.GDfleur_95modele8Objects2= [];
gdjs.chargementCode.GDfleur_95modele8Objects3= [];
gdjs.chargementCode.GDfleur_95modele9Objects1= [];
gdjs.chargementCode.GDfleur_95modele9Objects2= [];
gdjs.chargementCode.GDfleur_95modele9Objects3= [];
gdjs.chargementCode.GDfleur_95modele10Objects1= [];
gdjs.chargementCode.GDfleur_95modele10Objects2= [];
gdjs.chargementCode.GDfleur_95modele10Objects3= [];
gdjs.chargementCode.GDfleur_95modele4Objects1= [];
gdjs.chargementCode.GDfleur_95modele4Objects2= [];
gdjs.chargementCode.GDfleur_95modele4Objects3= [];
gdjs.chargementCode.GDfleur_95modele3Objects1= [];
gdjs.chargementCode.GDfleur_95modele3Objects2= [];
gdjs.chargementCode.GDfleur_95modele3Objects3= [];
gdjs.chargementCode.GDfleur_95modele2Objects1= [];
gdjs.chargementCode.GDfleur_95modele2Objects2= [];
gdjs.chargementCode.GDfleur_95modele2Objects3= [];
gdjs.chargementCode.GDbouton_95suivantObjects1= [];
gdjs.chargementCode.GDbouton_95suivantObjects2= [];
gdjs.chargementCode.GDbouton_95suivantObjects3= [];
gdjs.chargementCode.GDbouton_95recommencerObjects1= [];
gdjs.chargementCode.GDbouton_95recommencerObjects2= [];
gdjs.chargementCode.GDbouton_95recommencerObjects3= [];
gdjs.chargementCode.GDbouton_95retourObjects1= [];
gdjs.chargementCode.GDbouton_95retourObjects2= [];
gdjs.chargementCode.GDbouton_95retourObjects3= [];
gdjs.chargementCode.GDscore1Objects1= [];
gdjs.chargementCode.GDscore1Objects2= [];
gdjs.chargementCode.GDscore1Objects3= [];
gdjs.chargementCode.GDscore2Objects1= [];
gdjs.chargementCode.GDscore2Objects2= [];
gdjs.chargementCode.GDscore2Objects3= [];
gdjs.chargementCode.GDserge_95zoomObjects1= [];
gdjs.chargementCode.GDserge_95zoomObjects2= [];
gdjs.chargementCode.GDserge_95zoomObjects3= [];
gdjs.chargementCode.GDbulleObjects1= [];
gdjs.chargementCode.GDbulleObjects2= [];
gdjs.chargementCode.GDbulleObjects3= [];
gdjs.chargementCode.GDswitch_95nbObjects1= [];
gdjs.chargementCode.GDswitch_95nbObjects2= [];
gdjs.chargementCode.GDswitch_95nbObjects3= [];
gdjs.chargementCode.GDswitch_95couleurObjects1= [];
gdjs.chargementCode.GDswitch_95couleurObjects2= [];
gdjs.chargementCode.GDswitch_95couleurObjects3= [];
gdjs.chargementCode.GDfond_952Objects1= [];
gdjs.chargementCode.GDfond_952Objects2= [];
gdjs.chargementCode.GDfond_952Objects3= [];
gdjs.chargementCode.GDnb_95maxiObjects1= [];
gdjs.chargementCode.GDnb_95maxiObjects2= [];
gdjs.chargementCode.GDnb_95maxiObjects3= [];
gdjs.chargementCode.GDfleur_95couleurObjects1= [];
gdjs.chargementCode.GDfleur_95couleurObjects2= [];
gdjs.chargementCode.GDfleur_95couleurObjects3= [];
gdjs.chargementCode.GDfleur_95monochromeObjects1= [];
gdjs.chargementCode.GDfleur_95monochromeObjects2= [];
gdjs.chargementCode.GDfleur_95monochromeObjects3= [];
gdjs.chargementCode.GDserge_95logoObjects1= [];
gdjs.chargementCode.GDserge_95logoObjects2= [];
gdjs.chargementCode.GDserge_95logoObjects3= [];
gdjs.chargementCode.GDnb_95fleurs_95demandeObjects1= [];
gdjs.chargementCode.GDnb_95fleurs_95demandeObjects2= [];
gdjs.chargementCode.GDnb_95fleurs_95demandeObjects3= [];
gdjs.chargementCode.GDtxt_95eObjects1= [];
gdjs.chargementCode.GDtxt_95eObjects2= [];
gdjs.chargementCode.GDtxt_95eObjects3= [];
gdjs.chargementCode.GDchargementObjects1= [];
gdjs.chargementCode.GDchargementObjects2= [];
gdjs.chargementCode.GDchargementObjects3= [];
gdjs.chargementCode.GDinfoObjects1= [];
gdjs.chargementCode.GDinfoObjects2= [];
gdjs.chargementCode.GDinfoObjects3= [];

gdjs.chargementCode.conditionTrue_0 = {val:false};
gdjs.chargementCode.condition0IsTrue_0 = {val:false};
gdjs.chargementCode.condition1IsTrue_0 = {val:false};
gdjs.chargementCode.condition2IsTrue_0 = {val:false};
gdjs.chargementCode.condition3IsTrue_0 = {val:false};


gdjs.chargementCode.eventsList0 = function(runtimeScene) {

{


{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_1.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_2.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_3.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_4.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_5.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_6.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_7.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_8.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_9.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/xylo_10.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/applaudissements.mp3");
}}

}


{


{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cueille .mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/4e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/5e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/6e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/7e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/8e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/9e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/10e.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/faux.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/vrai.mp3");
}}

}


{


{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere_blanche.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere_bleue.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere_grise.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere_noire.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/1ere_rouge.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e_blanche.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e_bleue.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e_grise.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e_noire.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/2e_rouge.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e_blanche.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e_bleue.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e_grise.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e_noire.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/3e_rouge.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/4e_grise.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/4e_rouge.mp3");
}}

}


};gdjs.chargementCode.mapOfGDgdjs_46chargementCode_46GDbouton_9595suivantObjects1Objects = Hashtable.newFrom({"bouton_suivant": gdjs.chargementCode.GDbouton_95suivantObjects1});
gdjs.chargementCode.eventsList1 = function(runtimeScene) {

{


gdjs.chargementCode.condition0IsTrue_0.val = false;
{
gdjs.chargementCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.chargementCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("bouton_suivant"), gdjs.chargementCode.GDbouton_95suivantObjects1);
gdjs.copyArray(runtimeScene.getObjects("fond_2"), gdjs.chargementCode.GDfond_952Objects1);
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{for(var i = 0, len = gdjs.chargementCode.GDbouton_95suivantObjects1.length ;i < len;++i) {
    gdjs.chargementCode.GDbouton_95suivantObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.chargementCode.GDfond_952Objects1.length ;i < len;++i) {
    gdjs.chargementCode.GDfond_952Objects1[i].setOpacity(150);
}
}
{ //Subevents
gdjs.chargementCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.chargementCode.condition0IsTrue_0.val = false;
{
gdjs.chargementCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 7, "chrono");
}if (gdjs.chargementCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("bouton_suivant"), gdjs.chargementCode.GDbouton_95suivantObjects1);
gdjs.copyArray(runtimeScene.getObjects("chargement"), gdjs.chargementCode.GDchargementObjects1);
{for(var i = 0, len = gdjs.chargementCode.GDbouton_95suivantObjects1.length ;i < len;++i) {
    gdjs.chargementCode.GDbouton_95suivantObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.chargementCode.GDchargementObjects1.length ;i < len;++i) {
    gdjs.chargementCode.GDchargementObjects1[i].setString("Ressources chargées");
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_suivant"), gdjs.chargementCode.GDbouton_95suivantObjects1);

gdjs.chargementCode.condition0IsTrue_0.val = false;
gdjs.chargementCode.condition1IsTrue_0.val = false;
gdjs.chargementCode.condition2IsTrue_0.val = false;
{
gdjs.chargementCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.chargementCode.mapOfGDgdjs_46chargementCode_46GDbouton_9595suivantObjects1Objects, runtimeScene, true, false);
}if ( gdjs.chargementCode.condition0IsTrue_0.val ) {
{
gdjs.chargementCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.chargementCode.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.chargementCode.GDbouton_95suivantObjects1.length;i<l;++i) {
    if ( gdjs.chargementCode.GDbouton_95suivantObjects1[i].isVisible() ) {
        gdjs.chargementCode.condition2IsTrue_0.val = true;
        gdjs.chargementCode.GDbouton_95suivantObjects1[k] = gdjs.chargementCode.GDbouton_95suivantObjects1[i];
        ++k;
    }
}
gdjs.chargementCode.GDbouton_95suivantObjects1.length = k;}}
}
if (gdjs.chargementCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge_zoom"), gdjs.chargementCode.GDserge_95zoomObjects1);

gdjs.chargementCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.chargementCode.GDserge_95zoomObjects1.length;i<l;++i) {
    if ( gdjs.chargementCode.GDserge_95zoomObjects1[i].getY() > 288 ) {
        gdjs.chargementCode.condition0IsTrue_0.val = true;
        gdjs.chargementCode.GDserge_95zoomObjects1[k] = gdjs.chargementCode.GDserge_95zoomObjects1[i];
        ++k;
    }
}
gdjs.chargementCode.GDserge_95zoomObjects1.length = k;}if (gdjs.chargementCode.condition0IsTrue_0.val) {
/* Reuse gdjs.chargementCode.GDserge_95zoomObjects1 */
{for(var i = 0, len = gdjs.chargementCode.GDserge_95zoomObjects1.length ;i < len;++i) {
    gdjs.chargementCode.GDserge_95zoomObjects1[i].setY(gdjs.chargementCode.GDserge_95zoomObjects1[i].getY() - (1));
}
}}

}


};

gdjs.chargementCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.chargementCode.GDsergeObjects1.length = 0;
gdjs.chargementCode.GDsergeObjects2.length = 0;
gdjs.chargementCode.GDsergeObjects3.length = 0;
gdjs.chargementCode.GDvaseObjects1.length = 0;
gdjs.chargementCode.GDvaseObjects2.length = 0;
gdjs.chargementCode.GDvaseObjects3.length = 0;
gdjs.chargementCode.GDfleur_95modeleObjects1.length = 0;
gdjs.chargementCode.GDfleur_95modeleObjects2.length = 0;
gdjs.chargementCode.GDfleur_95modeleObjects3.length = 0;
gdjs.chargementCode.GDfleur_95modele5Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele5Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele5Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele6Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele6Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele6Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele7Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele7Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele7Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele8Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele8Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele8Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele9Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele9Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele9Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele10Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele10Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele10Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele4Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele4Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele4Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele3Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele3Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele3Objects3.length = 0;
gdjs.chargementCode.GDfleur_95modele2Objects1.length = 0;
gdjs.chargementCode.GDfleur_95modele2Objects2.length = 0;
gdjs.chargementCode.GDfleur_95modele2Objects3.length = 0;
gdjs.chargementCode.GDbouton_95suivantObjects1.length = 0;
gdjs.chargementCode.GDbouton_95suivantObjects2.length = 0;
gdjs.chargementCode.GDbouton_95suivantObjects3.length = 0;
gdjs.chargementCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.chargementCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.chargementCode.GDbouton_95recommencerObjects3.length = 0;
gdjs.chargementCode.GDbouton_95retourObjects1.length = 0;
gdjs.chargementCode.GDbouton_95retourObjects2.length = 0;
gdjs.chargementCode.GDbouton_95retourObjects3.length = 0;
gdjs.chargementCode.GDscore1Objects1.length = 0;
gdjs.chargementCode.GDscore1Objects2.length = 0;
gdjs.chargementCode.GDscore1Objects3.length = 0;
gdjs.chargementCode.GDscore2Objects1.length = 0;
gdjs.chargementCode.GDscore2Objects2.length = 0;
gdjs.chargementCode.GDscore2Objects3.length = 0;
gdjs.chargementCode.GDserge_95zoomObjects1.length = 0;
gdjs.chargementCode.GDserge_95zoomObjects2.length = 0;
gdjs.chargementCode.GDserge_95zoomObjects3.length = 0;
gdjs.chargementCode.GDbulleObjects1.length = 0;
gdjs.chargementCode.GDbulleObjects2.length = 0;
gdjs.chargementCode.GDbulleObjects3.length = 0;
gdjs.chargementCode.GDswitch_95nbObjects1.length = 0;
gdjs.chargementCode.GDswitch_95nbObjects2.length = 0;
gdjs.chargementCode.GDswitch_95nbObjects3.length = 0;
gdjs.chargementCode.GDswitch_95couleurObjects1.length = 0;
gdjs.chargementCode.GDswitch_95couleurObjects2.length = 0;
gdjs.chargementCode.GDswitch_95couleurObjects3.length = 0;
gdjs.chargementCode.GDfond_952Objects1.length = 0;
gdjs.chargementCode.GDfond_952Objects2.length = 0;
gdjs.chargementCode.GDfond_952Objects3.length = 0;
gdjs.chargementCode.GDnb_95maxiObjects1.length = 0;
gdjs.chargementCode.GDnb_95maxiObjects2.length = 0;
gdjs.chargementCode.GDnb_95maxiObjects3.length = 0;
gdjs.chargementCode.GDfleur_95couleurObjects1.length = 0;
gdjs.chargementCode.GDfleur_95couleurObjects2.length = 0;
gdjs.chargementCode.GDfleur_95couleurObjects3.length = 0;
gdjs.chargementCode.GDfleur_95monochromeObjects1.length = 0;
gdjs.chargementCode.GDfleur_95monochromeObjects2.length = 0;
gdjs.chargementCode.GDfleur_95monochromeObjects3.length = 0;
gdjs.chargementCode.GDserge_95logoObjects1.length = 0;
gdjs.chargementCode.GDserge_95logoObjects2.length = 0;
gdjs.chargementCode.GDserge_95logoObjects3.length = 0;
gdjs.chargementCode.GDnb_95fleurs_95demandeObjects1.length = 0;
gdjs.chargementCode.GDnb_95fleurs_95demandeObjects2.length = 0;
gdjs.chargementCode.GDnb_95fleurs_95demandeObjects3.length = 0;
gdjs.chargementCode.GDtxt_95eObjects1.length = 0;
gdjs.chargementCode.GDtxt_95eObjects2.length = 0;
gdjs.chargementCode.GDtxt_95eObjects3.length = 0;
gdjs.chargementCode.GDchargementObjects1.length = 0;
gdjs.chargementCode.GDchargementObjects2.length = 0;
gdjs.chargementCode.GDchargementObjects3.length = 0;
gdjs.chargementCode.GDinfoObjects1.length = 0;
gdjs.chargementCode.GDinfoObjects2.length = 0;
gdjs.chargementCode.GDinfoObjects3.length = 0;

gdjs.chargementCode.eventsList1(runtimeScene);

return;

}

gdjs['chargementCode'] = gdjs.chargementCode;
