gdjs.infosCode = {};
gdjs.infosCode.GDsergeObjects1= [];
gdjs.infosCode.GDsergeObjects2= [];
gdjs.infosCode.GDvaseObjects1= [];
gdjs.infosCode.GDvaseObjects2= [];
gdjs.infosCode.GDfleur_95modeleObjects1= [];
gdjs.infosCode.GDfleur_95modeleObjects2= [];
gdjs.infosCode.GDfleur_95modele5Objects1= [];
gdjs.infosCode.GDfleur_95modele5Objects2= [];
gdjs.infosCode.GDfleur_95modele6Objects1= [];
gdjs.infosCode.GDfleur_95modele6Objects2= [];
gdjs.infosCode.GDfleur_95modele7Objects1= [];
gdjs.infosCode.GDfleur_95modele7Objects2= [];
gdjs.infosCode.GDfleur_95modele8Objects1= [];
gdjs.infosCode.GDfleur_95modele8Objects2= [];
gdjs.infosCode.GDfleur_95modele9Objects1= [];
gdjs.infosCode.GDfleur_95modele9Objects2= [];
gdjs.infosCode.GDfleur_95modele10Objects1= [];
gdjs.infosCode.GDfleur_95modele10Objects2= [];
gdjs.infosCode.GDfleur_95modele4Objects1= [];
gdjs.infosCode.GDfleur_95modele4Objects2= [];
gdjs.infosCode.GDfleur_95modele3Objects1= [];
gdjs.infosCode.GDfleur_95modele3Objects2= [];
gdjs.infosCode.GDfleur_95modele2Objects1= [];
gdjs.infosCode.GDfleur_95modele2Objects2= [];
gdjs.infosCode.GDbouton_95suivantObjects1= [];
gdjs.infosCode.GDbouton_95suivantObjects2= [];
gdjs.infosCode.GDbouton_95recommencerObjects1= [];
gdjs.infosCode.GDbouton_95recommencerObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDscore1Objects1= [];
gdjs.infosCode.GDscore1Objects2= [];
gdjs.infosCode.GDscore2Objects1= [];
gdjs.infosCode.GDscore2Objects2= [];
gdjs.infosCode.GDserge_95zoomObjects1= [];
gdjs.infosCode.GDserge_95zoomObjects2= [];
gdjs.infosCode.GDbulleObjects1= [];
gdjs.infosCode.GDbulleObjects2= [];
gdjs.infosCode.GDswitch_95nbObjects1= [];
gdjs.infosCode.GDswitch_95nbObjects2= [];
gdjs.infosCode.GDswitch_95couleurObjects1= [];
gdjs.infosCode.GDswitch_95couleurObjects2= [];
gdjs.infosCode.GDfond_952Objects1= [];
gdjs.infosCode.GDfond_952Objects2= [];
gdjs.infosCode.GDnb_95maxiObjects1= [];
gdjs.infosCode.GDnb_95maxiObjects2= [];
gdjs.infosCode.GDfleur_95couleurObjects1= [];
gdjs.infosCode.GDfleur_95couleurObjects2= [];
gdjs.infosCode.GDfleur_95monochromeObjects1= [];
gdjs.infosCode.GDfleur_95monochromeObjects2= [];
gdjs.infosCode.GDserge_95logoObjects1= [];
gdjs.infosCode.GDserge_95logoObjects2= [];
gdjs.infosCode.GDnb_95fleurs_95demandeObjects1= [];
gdjs.infosCode.GDnb_95fleurs_95demandeObjects2= [];
gdjs.infosCode.GDtxt_95eObjects1= [];
gdjs.infosCode.GDtxt_95eObjects2= [];
gdjs.infosCode.GDinfoObjects1= [];
gdjs.infosCode.GDinfoObjects2= [];
gdjs.infosCode.GDNewObjectObjects1= [];
gdjs.infosCode.GDNewObjectObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];
gdjs.infosCode.GDNewObject2Objects1= [];
gdjs.infosCode.GDNewObject2Objects2= [];
gdjs.infosCode.GDjeu2Objects1= [];
gdjs.infosCode.GDjeu2Objects2= [];
gdjs.infosCode.GDinfos2Objects1= [];
gdjs.infosCode.GDinfos2Objects2= [];
gdjs.infosCode.GDinfos_95jeu2Objects1= [];
gdjs.infosCode.GDinfos_95jeu2Objects2= [];
gdjs.infosCode.GDinfos_95optionsObjects1= [];
gdjs.infosCode.GDinfos_95optionsObjects2= [];
gdjs.infosCode.GDinfos_95jeu1Objects1= [];
gdjs.infosCode.GDinfos_95jeu1Objects2= [];
gdjs.infosCode.GDjeu1Objects1= [];
gdjs.infosCode.GDjeu1Objects2= [];
gdjs.infosCode.GDNewObject3Objects1= [];
gdjs.infosCode.GDNewObject3Objects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDinfos_95contactObjects1= [];
gdjs.infosCode.GDinfos_95contactObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond_2"), gdjs.infosCode.GDfond_952Objects1);
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(100);
}
}{for(var i = 0, len = gdjs.infosCode.GDfond_952Objects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_952Objects1[i].setOpacity(150);
}
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDsergeObjects1.length = 0;
gdjs.infosCode.GDsergeObjects2.length = 0;
gdjs.infosCode.GDvaseObjects1.length = 0;
gdjs.infosCode.GDvaseObjects2.length = 0;
gdjs.infosCode.GDfleur_95modeleObjects1.length = 0;
gdjs.infosCode.GDfleur_95modeleObjects2.length = 0;
gdjs.infosCode.GDfleur_95modele5Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele5Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele6Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele6Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele7Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele7Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele8Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele8Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele9Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele9Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele10Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele10Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele4Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele4Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele3Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele3Objects2.length = 0;
gdjs.infosCode.GDfleur_95modele2Objects1.length = 0;
gdjs.infosCode.GDfleur_95modele2Objects2.length = 0;
gdjs.infosCode.GDbouton_95suivantObjects1.length = 0;
gdjs.infosCode.GDbouton_95suivantObjects2.length = 0;
gdjs.infosCode.GDbouton_95recommencerObjects1.length = 0;
gdjs.infosCode.GDbouton_95recommencerObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDscore1Objects1.length = 0;
gdjs.infosCode.GDscore1Objects2.length = 0;
gdjs.infosCode.GDscore2Objects1.length = 0;
gdjs.infosCode.GDscore2Objects2.length = 0;
gdjs.infosCode.GDserge_95zoomObjects1.length = 0;
gdjs.infosCode.GDserge_95zoomObjects2.length = 0;
gdjs.infosCode.GDbulleObjects1.length = 0;
gdjs.infosCode.GDbulleObjects2.length = 0;
gdjs.infosCode.GDswitch_95nbObjects1.length = 0;
gdjs.infosCode.GDswitch_95nbObjects2.length = 0;
gdjs.infosCode.GDswitch_95couleurObjects1.length = 0;
gdjs.infosCode.GDswitch_95couleurObjects2.length = 0;
gdjs.infosCode.GDfond_952Objects1.length = 0;
gdjs.infosCode.GDfond_952Objects2.length = 0;
gdjs.infosCode.GDnb_95maxiObjects1.length = 0;
gdjs.infosCode.GDnb_95maxiObjects2.length = 0;
gdjs.infosCode.GDfleur_95couleurObjects1.length = 0;
gdjs.infosCode.GDfleur_95couleurObjects2.length = 0;
gdjs.infosCode.GDfleur_95monochromeObjects1.length = 0;
gdjs.infosCode.GDfleur_95monochromeObjects2.length = 0;
gdjs.infosCode.GDserge_95logoObjects1.length = 0;
gdjs.infosCode.GDserge_95logoObjects2.length = 0;
gdjs.infosCode.GDnb_95fleurs_95demandeObjects1.length = 0;
gdjs.infosCode.GDnb_95fleurs_95demandeObjects2.length = 0;
gdjs.infosCode.GDtxt_95eObjects1.length = 0;
gdjs.infosCode.GDtxt_95eObjects2.length = 0;
gdjs.infosCode.GDinfoObjects1.length = 0;
gdjs.infosCode.GDinfoObjects2.length = 0;
gdjs.infosCode.GDNewObjectObjects1.length = 0;
gdjs.infosCode.GDNewObjectObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;
gdjs.infosCode.GDNewObject2Objects1.length = 0;
gdjs.infosCode.GDNewObject2Objects2.length = 0;
gdjs.infosCode.GDjeu2Objects1.length = 0;
gdjs.infosCode.GDjeu2Objects2.length = 0;
gdjs.infosCode.GDinfos2Objects1.length = 0;
gdjs.infosCode.GDinfos2Objects2.length = 0;
gdjs.infosCode.GDinfos_95jeu2Objects1.length = 0;
gdjs.infosCode.GDinfos_95jeu2Objects2.length = 0;
gdjs.infosCode.GDinfos_95optionsObjects1.length = 0;
gdjs.infosCode.GDinfos_95optionsObjects2.length = 0;
gdjs.infosCode.GDinfos_95jeu1Objects1.length = 0;
gdjs.infosCode.GDinfos_95jeu1Objects2.length = 0;
gdjs.infosCode.GDjeu1Objects1.length = 0;
gdjs.infosCode.GDjeu1Objects2.length = 0;
gdjs.infosCode.GDNewObject3Objects1.length = 0;
gdjs.infosCode.GDNewObject3Objects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDinfos_95contactObjects1.length = 0;
gdjs.infosCode.GDinfos_95contactObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);

return;

}

gdjs['infosCode'] = gdjs.infosCode;
