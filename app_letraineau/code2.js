gdjs.menuCode = {};
gdjs.menuCode.GDfondObjects1= [];
gdjs.menuCode.GDfondObjects2= [];
gdjs.menuCode.GDfondObjects3= [];
gdjs.menuCode.GDtexte_95951Objects1= [];
gdjs.menuCode.GDtexte_95951Objects2= [];
gdjs.menuCode.GDtexte_95951Objects3= [];
gdjs.menuCode.GDnb_95952Objects1= [];
gdjs.menuCode.GDnb_95952Objects2= [];
gdjs.menuCode.GDnb_95952Objects3= [];
gdjs.menuCode.GDbouton_9595plusObjects1= [];
gdjs.menuCode.GDbouton_9595plusObjects2= [];
gdjs.menuCode.GDbouton_9595plusObjects3= [];
gdjs.menuCode.GDbouton_9595moinsObjects1= [];
gdjs.menuCode.GDbouton_9595moinsObjects2= [];
gdjs.menuCode.GDbouton_9595moinsObjects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDimagesObjects1= [];
gdjs.menuCode.GDimagesObjects2= [];
gdjs.menuCode.GDimagesObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDpersonnageObjects1= [];
gdjs.menuCode.GDpersonnageObjects2= [];
gdjs.menuCode.GDpersonnageObjects3= [];
gdjs.menuCode.GDbouton_9595commencerObjects1= [];
gdjs.menuCode.GDbouton_9595commencerObjects2= [];
gdjs.menuCode.GDbouton_9595commencerObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_letraineau", "reglages");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_letraineau", "reglages", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(0));
}}

}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("nb_2"), gdjs.menuCode.GDnb_95952Objects1);
{for(var i = 0, len = gdjs.menuCode.GDnb_95952Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDnb_95952Objects1[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max"))));
}
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595plusObjects1Objects = Hashtable.newFrom({"bouton_plus": gdjs.menuCode.GDbouton_9595plusObjects1});
gdjs.menuCode.eventsList2 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_letraineau");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_letraineau", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(0)));
}}

}


};gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


gdjs.menuCode.eventsList2(runtimeScene);
}


};gdjs.menuCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max")) < 30;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_2"), gdjs.menuCode.GDnb_95952Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max").add(1);
}{for(var i = 0, len = gdjs.menuCode.GDnb_95952Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDnb_95952Objects1[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max"))));
}
}
{ //Subevents
gdjs.menuCode.eventsList3(runtimeScene);} //End of subevents
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595moinsObjects1Objects = Hashtable.newFrom({"bouton_moins": gdjs.menuCode.GDbouton_9595moinsObjects1});
gdjs.menuCode.eventsList5 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_letraineau");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_letraineau", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(0)));
}}

}


};gdjs.menuCode.eventsList6 = function(runtimeScene) {

{


gdjs.menuCode.eventsList5(runtimeScene);
}


};gdjs.menuCode.eventsList7 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max")) > 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("nb_2"), gdjs.menuCode.GDnb_95952Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max").sub(1);
}{for(var i = 0, len = gdjs.menuCode.GDnb_95952Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDnb_95952Objects1[i].getBehavior("Text").setText(gdjs.evtTools.common.toString(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0).getChild("nb_max"))));
}
}
{ //Subevents
gdjs.menuCode.eventsList6(runtimeScene);} //End of subevents
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595commencerObjects1Objects = Hashtable.newFrom({"bouton_commencer": gdjs.menuCode.GDbouton_9595commencerObjects1});
gdjs.menuCode.eventsList8 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.menuCode.GDfondObjects1);
{for(var i = 0, len = gdjs.menuCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDfondObjects1[i].getBehavior("Opacity").setOpacity(200);
}
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_plus"), gdjs.menuCode.GDbouton_9595plusObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595plusObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9809924);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.menuCode.eventsList4(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_moins"), gdjs.menuCode.GDbouton_9595moinsObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595moinsObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9812316);
}
}
}
if (isConditionTrue_0) {

{ //Subevents
gdjs.menuCode.eventsList7(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_commencer"), gdjs.menuCode.GDbouton_9595commencerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595commencerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDfondObjects1.length = 0;
gdjs.menuCode.GDfondObjects2.length = 0;
gdjs.menuCode.GDfondObjects3.length = 0;
gdjs.menuCode.GDtexte_95951Objects1.length = 0;
gdjs.menuCode.GDtexte_95951Objects2.length = 0;
gdjs.menuCode.GDtexte_95951Objects3.length = 0;
gdjs.menuCode.GDnb_95952Objects1.length = 0;
gdjs.menuCode.GDnb_95952Objects2.length = 0;
gdjs.menuCode.GDnb_95952Objects3.length = 0;
gdjs.menuCode.GDbouton_9595plusObjects1.length = 0;
gdjs.menuCode.GDbouton_9595plusObjects2.length = 0;
gdjs.menuCode.GDbouton_9595plusObjects3.length = 0;
gdjs.menuCode.GDbouton_9595moinsObjects1.length = 0;
gdjs.menuCode.GDbouton_9595moinsObjects2.length = 0;
gdjs.menuCode.GDbouton_9595moinsObjects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDimagesObjects1.length = 0;
gdjs.menuCode.GDimagesObjects2.length = 0;
gdjs.menuCode.GDimagesObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDpersonnageObjects1.length = 0;
gdjs.menuCode.GDpersonnageObjects2.length = 0;
gdjs.menuCode.GDpersonnageObjects3.length = 0;
gdjs.menuCode.GDbouton_9595commencerObjects1.length = 0;
gdjs.menuCode.GDbouton_9595commencerObjects2.length = 0;
gdjs.menuCode.GDbouton_9595commencerObjects3.length = 0;

gdjs.menuCode.eventsList8(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
