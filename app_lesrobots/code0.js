gdjs.menuCode = {};
gdjs.menuCode.GDjeu0Objects1_1final = [];

gdjs.menuCode.GDjeu1Objects1_1final = [];

gdjs.menuCode.GDjeu2Objects1_1final = [];

gdjs.menuCode.GDrobot1Objects1_1final = [];

gdjs.menuCode.GDrobot2Objects1_1final = [];

gdjs.menuCode.GDrobot3Objects1_1final = [];

gdjs.menuCode.GDjeu1Objects1= [];
gdjs.menuCode.GDjeu1Objects2= [];
gdjs.menuCode.GDjeu1Objects3= [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDtitreObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDjeu2Objects1= [];
gdjs.menuCode.GDjeu2Objects2= [];
gdjs.menuCode.GDjeu2Objects3= [];
gdjs.menuCode.GDjeu0Objects1= [];
gdjs.menuCode.GDjeu0Objects2= [];
gdjs.menuCode.GDjeu0Objects3= [];
gdjs.menuCode.GDrobot1Objects1= [];
gdjs.menuCode.GDrobot1Objects2= [];
gdjs.menuCode.GDrobot1Objects3= [];
gdjs.menuCode.GDrobot3Objects1= [];
gdjs.menuCode.GDrobot3Objects2= [];
gdjs.menuCode.GDrobot3Objects3= [];
gdjs.menuCode.GDmonochromeObjects1= [];
gdjs.menuCode.GDmonochromeObjects2= [];
gdjs.menuCode.GDmonochromeObjects3= [];
gdjs.menuCode.GDcouleurObjects1= [];
gdjs.menuCode.GDcouleurObjects2= [];
gdjs.menuCode.GDcouleurObjects3= [];
gdjs.menuCode.GDrobot2Objects1= [];
gdjs.menuCode.GDrobot2Objects2= [];
gdjs.menuCode.GDrobot2Objects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDcreditsObjects1= [];
gdjs.menuCode.GDcreditsObjects2= [];
gdjs.menuCode.GDcreditsObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDoutilsObjects1= [];
gdjs.menuCode.GDoutilsObjects2= [];
gdjs.menuCode.GDoutilsObjects3= [];
gdjs.menuCode.GDloupeObjects1= [];
gdjs.menuCode.GDloupeObjects2= [];
gdjs.menuCode.GDloupeObjects3= [];
gdjs.menuCode.GDcercle1Objects1= [];
gdjs.menuCode.GDcercle1Objects2= [];
gdjs.menuCode.GDcercle1Objects3= [];
gdjs.menuCode.GDcercle2Objects1= [];
gdjs.menuCode.GDcercle2Objects2= [];
gdjs.menuCode.GDcercle2Objects3= [];
gdjs.menuCode.GDcercle3Objects1= [];
gdjs.menuCode.GDcercle3Objects2= [];
gdjs.menuCode.GDcercle3Objects3= [];
gdjs.menuCode.GDsoustitreObjects1= [];
gdjs.menuCode.GDsoustitreObjects2= [];
gdjs.menuCode.GDsoustitreObjects3= [];
gdjs.menuCode.GDcompetencesObjects1= [];
gdjs.menuCode.GDcompetencesObjects2= [];
gdjs.menuCode.GDcompetencesObjects3= [];
gdjs.menuCode.GDbouton_9595infosObjects1= [];
gdjs.menuCode.GDbouton_9595infosObjects2= [];
gdjs.menuCode.GDbouton_9595infosObjects3= [];
gdjs.menuCode.GDscore_95952Objects1= [];
gdjs.menuCode.GDscore_95952Objects2= [];
gdjs.menuCode.GDscore_95952Objects3= [];
gdjs.menuCode.GDscore_9595Objects1= [];
gdjs.menuCode.GDscore_9595Objects2= [];
gdjs.menuCode.GDscore_9595Objects3= [];
gdjs.menuCode.GDbouton_9595suivantObjects1= [];
gdjs.menuCode.GDbouton_9595suivantObjects2= [];
gdjs.menuCode.GDbouton_9595suivantObjects3= [];
gdjs.menuCode.GDbouton_9595recommencerObjects1= [];
gdjs.menuCode.GDbouton_9595recommencerObjects2= [];
gdjs.menuCode.GDbouton_9595recommencerObjects3= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects3= [];
gdjs.menuCode.GDbouton_9595switchObjects1= [];
gdjs.menuCode.GDbouton_9595switchObjects2= [];
gdjs.menuCode.GDbouton_9595switchObjects3= [];
gdjs.menuCode.GDfaux_9595vraiObjects1= [];
gdjs.menuCode.GDfaux_9595vraiObjects2= [];
gdjs.menuCode.GDfaux_9595vraiObjects3= [];
gdjs.menuCode.GDlamaObjects1= [];
gdjs.menuCode.GDlamaObjects2= [];
gdjs.menuCode.GDlamaObjects3= [];
gdjs.menuCode.GDdrapeau1Objects1= [];
gdjs.menuCode.GDdrapeau1Objects2= [];
gdjs.menuCode.GDdrapeau1Objects3= [];
gdjs.menuCode.GDdrapeau2Objects1= [];
gdjs.menuCode.GDdrapeau2Objects2= [];
gdjs.menuCode.GDdrapeau2Objects3= [];
gdjs.menuCode.GDpointObjects1= [];
gdjs.menuCode.GDpointObjects2= [];
gdjs.menuCode.GDpointObjects3= [];
gdjs.menuCode.GDfond_9595chambreObjects1= [];
gdjs.menuCode.GDfond_9595chambreObjects2= [];
gdjs.menuCode.GDfond_9595chambreObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_lesrobots", "score");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_lesrobots", "score", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde_temporaire"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde_temporaire")), runtimeScene.getGame().getVariables().getFromIndex(3));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("score_"), gdjs.menuCode.GDscore_9595Objects2);
gdjs.copyArray(runtimeScene.getObjects("score_2"), gdjs.menuCode.GDscore_95952Objects2);
{for(var i = 0, len = gdjs.menuCode.GDscore_9595Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_9595Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_95952Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDscore_95952Objects2[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")));
}
}}

}


{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/quel_robot.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/et.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/carre_blanc.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/carre_bleu.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/carre_gris.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/carre_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/carre_noir.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/carre_vert.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/triangle_blanc.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/triangle_bleu.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/triangle_gris.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/triangle_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/triangle_noir.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/triangle_vert.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rectangle_blanc.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rectangle_bleu.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rectangle_gris.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rectangle_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rectangle_noir.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/rectangle_vert.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cercle_blanc.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cercle_bleu.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cercle_gris.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cercle_jaune.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cercle_noir.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/cercle_vert.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/bravo_1.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/choix_couleurs.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne_commande.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/consigne_fabrication2.mp3");
}{gdjs.evtTools.sound.preloadMusic(runtimeScene, "audio/applaudissements.mp3");
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu1Objects2Objects = Hashtable.newFrom({"jeu1": gdjs.menuCode.GDjeu1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDrobot3Objects2Objects = Hashtable.newFrom({"robot3": gdjs.menuCode.GDrobot3Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu0Objects2Objects = Hashtable.newFrom({"jeu0": gdjs.menuCode.GDjeu0Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDrobot1Objects2Objects = Hashtable.newFrom({"robot1": gdjs.menuCode.GDrobot1Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu2Objects2Objects = Hashtable.newFrom({"jeu2": gdjs.menuCode.GDjeu2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDrobot2Objects2Objects = Hashtable.newFrom({"robot2": gdjs.menuCode.GDrobot2Objects2});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595switchObjects2Objects = Hashtable.newFrom({"bouton_switch": gdjs.menuCode.GDbouton_9595switchObjects2});
gdjs.menuCode.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("bouton_switch"), gdjs.menuCode.GDbouton_9595switchObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595switchObjects2Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(10190188);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).add(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(10191044);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_switch"), gdjs.menuCode.GDbouton_9595switchObjects2);
gdjs.copyArray(runtimeScene.getObjects("cercle1"), gdjs.menuCode.GDcercle1Objects2);
gdjs.copyArray(runtimeScene.getObjects("cercle2"), gdjs.menuCode.GDcercle2Objects2);
gdjs.copyArray(runtimeScene.getObjects("cercle3"), gdjs.menuCode.GDcercle3Objects2);
gdjs.copyArray(runtimeScene.getObjects("couleur"), gdjs.menuCode.GDcouleurObjects2);
gdjs.copyArray(runtimeScene.getObjects("monochrome"), gdjs.menuCode.GDmonochromeObjects2);
gdjs.copyArray(runtimeScene.getObjects("robot1"), gdjs.menuCode.GDrobot1Objects2);
gdjs.copyArray(runtimeScene.getObjects("robot2"), gdjs.menuCode.GDrobot2Objects2);
gdjs.copyArray(runtimeScene.getObjects("robot3"), gdjs.menuCode.GDrobot3Objects2);
{for(var i = 0, len = gdjs.menuCode.GDbouton_9595switchObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDbouton_9595switchObjects2[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.menuCode.GDrobot1Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDrobot1Objects2[i].setAnimationFrame(0);
}
for(var i = 0, len = gdjs.menuCode.GDrobot3Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDrobot3Objects2[i].setAnimationFrame(0);
}
for(var i = 0, len = gdjs.menuCode.GDrobot2Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDrobot2Objects2[i].setAnimationFrame(0);
}
for(var i = 0, len = gdjs.menuCode.GDcercle1Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDcercle1Objects2[i].setAnimationFrame(0);
}
for(var i = 0, len = gdjs.menuCode.GDcercle2Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDcercle2Objects2[i].setAnimationFrame(0);
}
for(var i = 0, len = gdjs.menuCode.GDcercle3Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDcercle3Objects2[i].setAnimationFrame(0);
}
}{for(var i = 0, len = gdjs.menuCode.GDcouleurObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDcouleurObjects2[i].setOpacity(255);
}
}{for(var i = 0, len = gdjs.menuCode.GDmonochromeObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDmonochromeObjects2[i].setOpacity(100);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(10192876);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("bouton_switch"), gdjs.menuCode.GDbouton_9595switchObjects2);
gdjs.copyArray(runtimeScene.getObjects("cercle1"), gdjs.menuCode.GDcercle1Objects2);
gdjs.copyArray(runtimeScene.getObjects("cercle2"), gdjs.menuCode.GDcercle2Objects2);
gdjs.copyArray(runtimeScene.getObjects("cercle3"), gdjs.menuCode.GDcercle3Objects2);
gdjs.copyArray(runtimeScene.getObjects("couleur"), gdjs.menuCode.GDcouleurObjects2);
gdjs.copyArray(runtimeScene.getObjects("monochrome"), gdjs.menuCode.GDmonochromeObjects2);
gdjs.copyArray(runtimeScene.getObjects("robot1"), gdjs.menuCode.GDrobot1Objects2);
gdjs.copyArray(runtimeScene.getObjects("robot2"), gdjs.menuCode.GDrobot2Objects2);
gdjs.copyArray(runtimeScene.getObjects("robot3"), gdjs.menuCode.GDrobot3Objects2);
{for(var i = 0, len = gdjs.menuCode.GDbouton_9595switchObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDbouton_9595switchObjects2[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.menuCode.GDrobot1Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDrobot1Objects2[i].setAnimationFrame(1);
}
for(var i = 0, len = gdjs.menuCode.GDrobot3Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDrobot3Objects2[i].setAnimationFrame(1);
}
for(var i = 0, len = gdjs.menuCode.GDrobot2Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDrobot2Objects2[i].setAnimationFrame(1);
}
for(var i = 0, len = gdjs.menuCode.GDcercle1Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDcercle1Objects2[i].setAnimationFrame(1);
}
for(var i = 0, len = gdjs.menuCode.GDcercle2Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDcercle2Objects2[i].setAnimationFrame(1);
}
for(var i = 0, len = gdjs.menuCode.GDcercle3Objects2.length ;i < len;++i) {
    gdjs.menuCode.GDcercle3Objects2[i].setAnimationFrame(1);
}
}{for(var i = 0, len = gdjs.menuCode.GDcouleurObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDcouleurObjects2[i].setOpacity(100);
}
}{for(var i = 0, len = gdjs.menuCode.GDmonochromeObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDmonochromeObjects2[i].setOpacity(255);
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) > 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(10194700);
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects = Hashtable.newFrom({"serge": gdjs.menuCode.GDsergeObjects1});
gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lesrobots");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lesrobots", "score", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(3)));
}}

}


{


let isConditionTrue_0 = false;
{
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects = Hashtable.newFrom({"bouton_infos": gdjs.menuCode.GDbouton_9595infosObjects1});
gdjs.menuCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("cercle1"), gdjs.menuCode.GDcercle1Objects1);
gdjs.copyArray(runtimeScene.getObjects("cercle2"), gdjs.menuCode.GDcercle2Objects1);
gdjs.copyArray(runtimeScene.getObjects("cercle3"), gdjs.menuCode.GDcercle3Objects1);
gdjs.copyArray(runtimeScene.getObjects("fond_chambre"), gdjs.menuCode.GDfond_9595chambreObjects1);
gdjs.copyArray(runtimeScene.getObjects("robot1"), gdjs.menuCode.GDrobot1Objects1);
gdjs.copyArray(runtimeScene.getObjects("robot2"), gdjs.menuCode.GDrobot2Objects1);
gdjs.copyArray(runtimeScene.getObjects("robot3"), gdjs.menuCode.GDrobot3Objects1);
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{for(var i = 0, len = gdjs.menuCode.GDfond_9595chambreObjects1.length ;i < len;++i) {
    gdjs.menuCode.GDfond_9595chambreObjects1[i].setOpacity(150);
}
}{for(var i = 0, len = gdjs.menuCode.GDcercle1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDcercle1Objects1[i].setOpacity(150);
}
for(var i = 0, len = gdjs.menuCode.GDcercle2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDcercle2Objects1[i].setOpacity(150);
}
for(var i = 0, len = gdjs.menuCode.GDcercle3Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDcercle3Objects1[i].setOpacity(150);
}
}{for(var i = 0, len = gdjs.menuCode.GDrobot1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDrobot1Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDrobot3Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDrobot3Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDrobot2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDrobot2Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDcercle1Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDcercle1Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDcercle2Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDcercle2Objects1[i].pauseAnimation();
}
for(var i = 0, len = gdjs.menuCode.GDcercle3Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDcercle3Objects1[i].pauseAnimation();
}
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.menuCode.GDjeu1Objects1.length = 0;

gdjs.menuCode.GDrobot3Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.menuCode.GDjeu1Objects1_1final.length = 0;
gdjs.menuCode.GDrobot3Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu1"), gdjs.menuCode.GDjeu1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDjeu1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDjeu1Objects1_1final.indexOf(gdjs.menuCode.GDjeu1Objects2[j]) === -1 )
            gdjs.menuCode.GDjeu1Objects1_1final.push(gdjs.menuCode.GDjeu1Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("robot3"), gdjs.menuCode.GDrobot3Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDrobot3Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDrobot3Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDrobot3Objects1_1final.indexOf(gdjs.menuCode.GDrobot3Objects2[j]) === -1 )
            gdjs.menuCode.GDrobot3Objects1_1final.push(gdjs.menuCode.GDrobot3Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu1Objects1_1final, gdjs.menuCode.GDjeu1Objects1);
gdjs.copyArray(gdjs.menuCode.GDrobot3Objects1_1final, gdjs.menuCode.GDrobot3Objects1);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "serge_jeu1", false);
}{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("positionX").setNumber(48);
}}

}


{

gdjs.menuCode.GDjeu0Objects1.length = 0;

gdjs.menuCode.GDrobot1Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.menuCode.GDjeu0Objects1_1final.length = 0;
gdjs.menuCode.GDrobot1Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu0"), gdjs.menuCode.GDjeu0Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu0Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDjeu0Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDjeu0Objects1_1final.indexOf(gdjs.menuCode.GDjeu0Objects2[j]) === -1 )
            gdjs.menuCode.GDjeu0Objects1_1final.push(gdjs.menuCode.GDjeu0Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("robot1"), gdjs.menuCode.GDrobot1Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDrobot1Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDrobot1Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDrobot1Objects1_1final.indexOf(gdjs.menuCode.GDrobot1Objects2[j]) === -1 )
            gdjs.menuCode.GDrobot1Objects1_1final.push(gdjs.menuCode.GDrobot1Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu0Objects1_1final, gdjs.menuCode.GDjeu0Objects1);
gdjs.copyArray(gdjs.menuCode.GDrobot1Objects1_1final, gdjs.menuCode.GDrobot1Objects1);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_construction", false);
}}

}


{

gdjs.menuCode.GDjeu2Objects1.length = 0;

gdjs.menuCode.GDrobot2Objects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs.menuCode.GDjeu2Objects1_1final.length = 0;
gdjs.menuCode.GDrobot2Objects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("jeu2"), gdjs.menuCode.GDjeu2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDjeu2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDjeu2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDjeu2Objects1_1final.indexOf(gdjs.menuCode.GDjeu2Objects2[j]) === -1 )
            gdjs.menuCode.GDjeu2Objects1_1final.push(gdjs.menuCode.GDjeu2Objects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("robot2"), gdjs.menuCode.GDrobot2Objects2);
isConditionTrue_1 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDrobot2Objects2Objects, runtimeScene, true, false);
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs.menuCode.GDrobot2Objects2.length; j < jLen ; ++j) {
        if ( gdjs.menuCode.GDrobot2Objects1_1final.indexOf(gdjs.menuCode.GDrobot2Objects2[j]) === -1 )
            gdjs.menuCode.GDrobot2Objects1_1final.push(gdjs.menuCode.GDrobot2Objects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.menuCode.GDjeu2Objects1_1final, gdjs.menuCode.GDjeu2Objects1);
gdjs.copyArray(gdjs.menuCode.GDrobot2Objects1_1final, gdjs.menuCode.GDrobot2Objects1);
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "serge_commande", false);
}}

}


{


gdjs.menuCode.eventsList2(runtimeScene);
}


{

gdjs.copyArray(runtimeScene.getObjects("serge"), gdjs.menuCode.GDsergeObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsergeObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(10196004);
}
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().getFromIndex(0).add(1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) >= 5;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("score_"), gdjs.menuCode.GDscore_9595Objects1);
gdjs.copyArray(runtimeScene.getObjects("score_2"), gdjs.menuCode.GDscore_95952Objects1);
{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2").setNumber(0);
}{for(var i = 0, len = gdjs.menuCode.GDscore_9595Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_9595Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("1")));
}
}{for(var i = 0, len = gdjs.menuCode.GDscore_95952Objects1.length ;i < len;++i) {
    gdjs.menuCode.GDscore_95952Objects1[i].setAnimation(gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3).getChild("2")));
}
}
{ //Subevents
gdjs.menuCode.eventsList3(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_infos"), gdjs.menuCode.GDbouton_9595infosObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDbouton_95959595infosObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDjeu1Objects1.length = 0;
gdjs.menuCode.GDjeu1Objects2.length = 0;
gdjs.menuCode.GDjeu1Objects3.length = 0;
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDjeu2Objects1.length = 0;
gdjs.menuCode.GDjeu2Objects2.length = 0;
gdjs.menuCode.GDjeu2Objects3.length = 0;
gdjs.menuCode.GDjeu0Objects1.length = 0;
gdjs.menuCode.GDjeu0Objects2.length = 0;
gdjs.menuCode.GDjeu0Objects3.length = 0;
gdjs.menuCode.GDrobot1Objects1.length = 0;
gdjs.menuCode.GDrobot1Objects2.length = 0;
gdjs.menuCode.GDrobot1Objects3.length = 0;
gdjs.menuCode.GDrobot3Objects1.length = 0;
gdjs.menuCode.GDrobot3Objects2.length = 0;
gdjs.menuCode.GDrobot3Objects3.length = 0;
gdjs.menuCode.GDmonochromeObjects1.length = 0;
gdjs.menuCode.GDmonochromeObjects2.length = 0;
gdjs.menuCode.GDmonochromeObjects3.length = 0;
gdjs.menuCode.GDcouleurObjects1.length = 0;
gdjs.menuCode.GDcouleurObjects2.length = 0;
gdjs.menuCode.GDcouleurObjects3.length = 0;
gdjs.menuCode.GDrobot2Objects1.length = 0;
gdjs.menuCode.GDrobot2Objects2.length = 0;
gdjs.menuCode.GDrobot2Objects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDcreditsObjects1.length = 0;
gdjs.menuCode.GDcreditsObjects2.length = 0;
gdjs.menuCode.GDcreditsObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDoutilsObjects1.length = 0;
gdjs.menuCode.GDoutilsObjects2.length = 0;
gdjs.menuCode.GDoutilsObjects3.length = 0;
gdjs.menuCode.GDloupeObjects1.length = 0;
gdjs.menuCode.GDloupeObjects2.length = 0;
gdjs.menuCode.GDloupeObjects3.length = 0;
gdjs.menuCode.GDcercle1Objects1.length = 0;
gdjs.menuCode.GDcercle1Objects2.length = 0;
gdjs.menuCode.GDcercle1Objects3.length = 0;
gdjs.menuCode.GDcercle2Objects1.length = 0;
gdjs.menuCode.GDcercle2Objects2.length = 0;
gdjs.menuCode.GDcercle2Objects3.length = 0;
gdjs.menuCode.GDcercle3Objects1.length = 0;
gdjs.menuCode.GDcercle3Objects2.length = 0;
gdjs.menuCode.GDcercle3Objects3.length = 0;
gdjs.menuCode.GDsoustitreObjects1.length = 0;
gdjs.menuCode.GDsoustitreObjects2.length = 0;
gdjs.menuCode.GDsoustitreObjects3.length = 0;
gdjs.menuCode.GDcompetencesObjects1.length = 0;
gdjs.menuCode.GDcompetencesObjects2.length = 0;
gdjs.menuCode.GDcompetencesObjects3.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects1.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects2.length = 0;
gdjs.menuCode.GDbouton_9595infosObjects3.length = 0;
gdjs.menuCode.GDscore_95952Objects1.length = 0;
gdjs.menuCode.GDscore_95952Objects2.length = 0;
gdjs.menuCode.GDscore_95952Objects3.length = 0;
gdjs.menuCode.GDscore_9595Objects1.length = 0;
gdjs.menuCode.GDscore_9595Objects2.length = 0;
gdjs.menuCode.GDscore_9595Objects3.length = 0;
gdjs.menuCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.menuCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.menuCode.GDbouton_9595suivantObjects3.length = 0;
gdjs.menuCode.GDbouton_9595recommencerObjects1.length = 0;
gdjs.menuCode.GDbouton_9595recommencerObjects2.length = 0;
gdjs.menuCode.GDbouton_9595recommencerObjects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.menuCode.GDbouton_9595switchObjects1.length = 0;
gdjs.menuCode.GDbouton_9595switchObjects2.length = 0;
gdjs.menuCode.GDbouton_9595switchObjects3.length = 0;
gdjs.menuCode.GDfaux_9595vraiObjects1.length = 0;
gdjs.menuCode.GDfaux_9595vraiObjects2.length = 0;
gdjs.menuCode.GDfaux_9595vraiObjects3.length = 0;
gdjs.menuCode.GDlamaObjects1.length = 0;
gdjs.menuCode.GDlamaObjects2.length = 0;
gdjs.menuCode.GDlamaObjects3.length = 0;
gdjs.menuCode.GDdrapeau1Objects1.length = 0;
gdjs.menuCode.GDdrapeau1Objects2.length = 0;
gdjs.menuCode.GDdrapeau1Objects3.length = 0;
gdjs.menuCode.GDdrapeau2Objects1.length = 0;
gdjs.menuCode.GDdrapeau2Objects2.length = 0;
gdjs.menuCode.GDdrapeau2Objects3.length = 0;
gdjs.menuCode.GDpointObjects1.length = 0;
gdjs.menuCode.GDpointObjects2.length = 0;
gdjs.menuCode.GDpointObjects3.length = 0;
gdjs.menuCode.GDfond_9595chambreObjects1.length = 0;
gdjs.menuCode.GDfond_9595chambreObjects2.length = 0;
gdjs.menuCode.GDfond_9595chambreObjects3.length = 0;

gdjs.menuCode.eventsList4(runtimeScene);

return;

}

gdjs['menuCode'] = gdjs.menuCode;
