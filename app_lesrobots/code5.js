gdjs.serge_95jeu1Code = {};
gdjs.serge_95jeu1Code.GDsergeObjects1= [];
gdjs.serge_95jeu1Code.GDsergeObjects2= [];
gdjs.serge_95jeu1Code.GDbulleObjects1= [];
gdjs.serge_95jeu1Code.GDbulleObjects2= [];
gdjs.serge_95jeu1Code.GDoutilsObjects1= [];
gdjs.serge_95jeu1Code.GDoutilsObjects2= [];
gdjs.serge_95jeu1Code.GDfond_9595chambre3Objects1= [];
gdjs.serge_95jeu1Code.GDfond_9595chambre3Objects2= [];
gdjs.serge_95jeu1Code.GDloupeObjects1= [];
gdjs.serge_95jeu1Code.GDloupeObjects2= [];
gdjs.serge_95jeu1Code.GDrobotObjects1= [];
gdjs.serge_95jeu1Code.GDrobotObjects2= [];
gdjs.serge_95jeu1Code.GDscore_95952Objects1= [];
gdjs.serge_95jeu1Code.GDscore_95952Objects2= [];
gdjs.serge_95jeu1Code.GDscore_9595Objects1= [];
gdjs.serge_95jeu1Code.GDscore_9595Objects2= [];
gdjs.serge_95jeu1Code.GDbouton_9595suivantObjects1= [];
gdjs.serge_95jeu1Code.GDbouton_9595suivantObjects2= [];
gdjs.serge_95jeu1Code.GDbouton_9595recommencerObjects1= [];
gdjs.serge_95jeu1Code.GDbouton_9595recommencerObjects2= [];
gdjs.serge_95jeu1Code.GDbouton_9595retourObjects1= [];
gdjs.serge_95jeu1Code.GDbouton_9595retourObjects2= [];
gdjs.serge_95jeu1Code.GDbouton_9595switchObjects1= [];
gdjs.serge_95jeu1Code.GDbouton_9595switchObjects2= [];
gdjs.serge_95jeu1Code.GDfaux_9595vraiObjects1= [];
gdjs.serge_95jeu1Code.GDfaux_9595vraiObjects2= [];
gdjs.serge_95jeu1Code.GDlamaObjects1= [];
gdjs.serge_95jeu1Code.GDlamaObjects2= [];
gdjs.serge_95jeu1Code.GDdrapeau1Objects1= [];
gdjs.serge_95jeu1Code.GDdrapeau1Objects2= [];
gdjs.serge_95jeu1Code.GDdrapeau2Objects1= [];
gdjs.serge_95jeu1Code.GDdrapeau2Objects2= [];
gdjs.serge_95jeu1Code.GDpointObjects1= [];
gdjs.serge_95jeu1Code.GDpointObjects2= [];
gdjs.serge_95jeu1Code.GDfond_9595chambreObjects1= [];
gdjs.serge_95jeu1Code.GDfond_9595chambreObjects2= [];


gdjs.serge_95jeu1Code.mapOfGDgdjs_9546serge_959595jeu1Code_9546GDbouton_95959595suivantObjects1Objects = Hashtable.newFrom({"bouton_suivant": gdjs.serge_95jeu1Code.GDbouton_9595suivantObjects1});
gdjs.serge_95jeu1Code.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("robot"), gdjs.serge_95jeu1Code.GDrobotObjects1);
{for(var i = 0, len = gdjs.serge_95jeu1Code.GDrobotObjects1.length ;i < len;++i) {
    gdjs.serge_95jeu1Code.GDrobotObjects1[i].pauseAnimation();
}
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/consigne_commande.mp3", 1, false, 100, 1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("robot"), gdjs.serge_95jeu1Code.GDrobotObjects1);
{for(var i = 0, len = gdjs.serge_95jeu1Code.GDrobotObjects1.length ;i < len;++i) {
    gdjs.serge_95jeu1Code.GDrobotObjects1[i].playAnimation();
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_suivant"), gdjs.serge_95jeu1Code.GDbouton_9595suivantObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.serge_95jeu1Code.mapOfGDgdjs_9546serge_959595jeu1Code_9546GDbouton_95959595suivantObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.stopMusicOnChannel(runtimeScene, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu1", false);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("a0").setNumber(0);
}}

}


};

gdjs.serge_95jeu1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.serge_95jeu1Code.GDsergeObjects1.length = 0;
gdjs.serge_95jeu1Code.GDsergeObjects2.length = 0;
gdjs.serge_95jeu1Code.GDbulleObjects1.length = 0;
gdjs.serge_95jeu1Code.GDbulleObjects2.length = 0;
gdjs.serge_95jeu1Code.GDoutilsObjects1.length = 0;
gdjs.serge_95jeu1Code.GDoutilsObjects2.length = 0;
gdjs.serge_95jeu1Code.GDfond_9595chambre3Objects1.length = 0;
gdjs.serge_95jeu1Code.GDfond_9595chambre3Objects2.length = 0;
gdjs.serge_95jeu1Code.GDloupeObjects1.length = 0;
gdjs.serge_95jeu1Code.GDloupeObjects2.length = 0;
gdjs.serge_95jeu1Code.GDrobotObjects1.length = 0;
gdjs.serge_95jeu1Code.GDrobotObjects2.length = 0;
gdjs.serge_95jeu1Code.GDscore_95952Objects1.length = 0;
gdjs.serge_95jeu1Code.GDscore_95952Objects2.length = 0;
gdjs.serge_95jeu1Code.GDscore_9595Objects1.length = 0;
gdjs.serge_95jeu1Code.GDscore_9595Objects2.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595suivantObjects1.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595suivantObjects2.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595recommencerObjects1.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595recommencerObjects2.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595retourObjects1.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595retourObjects2.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595switchObjects1.length = 0;
gdjs.serge_95jeu1Code.GDbouton_9595switchObjects2.length = 0;
gdjs.serge_95jeu1Code.GDfaux_9595vraiObjects1.length = 0;
gdjs.serge_95jeu1Code.GDfaux_9595vraiObjects2.length = 0;
gdjs.serge_95jeu1Code.GDlamaObjects1.length = 0;
gdjs.serge_95jeu1Code.GDlamaObjects2.length = 0;
gdjs.serge_95jeu1Code.GDdrapeau1Objects1.length = 0;
gdjs.serge_95jeu1Code.GDdrapeau1Objects2.length = 0;
gdjs.serge_95jeu1Code.GDdrapeau2Objects1.length = 0;
gdjs.serge_95jeu1Code.GDdrapeau2Objects2.length = 0;
gdjs.serge_95jeu1Code.GDpointObjects1.length = 0;
gdjs.serge_95jeu1Code.GDpointObjects2.length = 0;
gdjs.serge_95jeu1Code.GDfond_9595chambreObjects1.length = 0;
gdjs.serge_95jeu1Code.GDfond_9595chambreObjects2.length = 0;

gdjs.serge_95jeu1Code.eventsList0(runtimeScene);

return;

}

gdjs['serge_95jeu1Code'] = gdjs.serge_95jeu1Code;
