gdjs.serge_95commandeCode = {};
gdjs.serge_95commandeCode.GDsergeObjects1= [];
gdjs.serge_95commandeCode.GDsergeObjects2= [];
gdjs.serge_95commandeCode.GDbulleObjects1= [];
gdjs.serge_95commandeCode.GDbulleObjects2= [];
gdjs.serge_95commandeCode.GDoutilsObjects1= [];
gdjs.serge_95commandeCode.GDoutilsObjects2= [];
gdjs.serge_95commandeCode.GDfond_9595chambre3Objects1= [];
gdjs.serge_95commandeCode.GDfond_9595chambre3Objects2= [];
gdjs.serge_95commandeCode.GDscore_95952Objects1= [];
gdjs.serge_95commandeCode.GDscore_95952Objects2= [];
gdjs.serge_95commandeCode.GDscore_9595Objects1= [];
gdjs.serge_95commandeCode.GDscore_9595Objects2= [];
gdjs.serge_95commandeCode.GDbouton_9595suivantObjects1= [];
gdjs.serge_95commandeCode.GDbouton_9595suivantObjects2= [];
gdjs.serge_95commandeCode.GDbouton_9595recommencerObjects1= [];
gdjs.serge_95commandeCode.GDbouton_9595recommencerObjects2= [];
gdjs.serge_95commandeCode.GDbouton_9595retourObjects1= [];
gdjs.serge_95commandeCode.GDbouton_9595retourObjects2= [];
gdjs.serge_95commandeCode.GDbouton_9595switchObjects1= [];
gdjs.serge_95commandeCode.GDbouton_9595switchObjects2= [];
gdjs.serge_95commandeCode.GDfaux_9595vraiObjects1= [];
gdjs.serge_95commandeCode.GDfaux_9595vraiObjects2= [];
gdjs.serge_95commandeCode.GDlamaObjects1= [];
gdjs.serge_95commandeCode.GDlamaObjects2= [];
gdjs.serge_95commandeCode.GDdrapeau1Objects1= [];
gdjs.serge_95commandeCode.GDdrapeau1Objects2= [];
gdjs.serge_95commandeCode.GDdrapeau2Objects1= [];
gdjs.serge_95commandeCode.GDdrapeau2Objects2= [];
gdjs.serge_95commandeCode.GDpointObjects1= [];
gdjs.serge_95commandeCode.GDpointObjects2= [];
gdjs.serge_95commandeCode.GDfond_9595chambreObjects1= [];
gdjs.serge_95commandeCode.GDfond_9595chambreObjects2= [];


gdjs.serge_95commandeCode.mapOfGDgdjs_9546serge_959595commandeCode_9546GDbouton_95959595suivantObjects1Objects = Hashtable.newFrom({"bouton_suivant": gdjs.serge_95commandeCode.GDbouton_9595suivantObjects1});
gdjs.serge_95commandeCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/consigne_fabrication2.mp3", 1, false, 100, 1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_suivant"), gdjs.serge_95commandeCode.GDbouton_9595suivantObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.serge_95commandeCode.mapOfGDgdjs_9546serge_959595commandeCode_9546GDbouton_95959595suivantObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.stopMusicOnChannel(runtimeScene, 1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu_commande", false);
}{runtimeScene.getGame().getVariables().getFromIndex(3).getChild("a0").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(4).getChild("positionX").setNumber(32);
}}

}


};

gdjs.serge_95commandeCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.serge_95commandeCode.GDsergeObjects1.length = 0;
gdjs.serge_95commandeCode.GDsergeObjects2.length = 0;
gdjs.serge_95commandeCode.GDbulleObjects1.length = 0;
gdjs.serge_95commandeCode.GDbulleObjects2.length = 0;
gdjs.serge_95commandeCode.GDoutilsObjects1.length = 0;
gdjs.serge_95commandeCode.GDoutilsObjects2.length = 0;
gdjs.serge_95commandeCode.GDfond_9595chambre3Objects1.length = 0;
gdjs.serge_95commandeCode.GDfond_9595chambre3Objects2.length = 0;
gdjs.serge_95commandeCode.GDscore_95952Objects1.length = 0;
gdjs.serge_95commandeCode.GDscore_95952Objects2.length = 0;
gdjs.serge_95commandeCode.GDscore_9595Objects1.length = 0;
gdjs.serge_95commandeCode.GDscore_9595Objects2.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595suivantObjects1.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595suivantObjects2.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595recommencerObjects1.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595recommencerObjects2.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595retourObjects1.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595retourObjects2.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595switchObjects1.length = 0;
gdjs.serge_95commandeCode.GDbouton_9595switchObjects2.length = 0;
gdjs.serge_95commandeCode.GDfaux_9595vraiObjects1.length = 0;
gdjs.serge_95commandeCode.GDfaux_9595vraiObjects2.length = 0;
gdjs.serge_95commandeCode.GDlamaObjects1.length = 0;
gdjs.serge_95commandeCode.GDlamaObjects2.length = 0;
gdjs.serge_95commandeCode.GDdrapeau1Objects1.length = 0;
gdjs.serge_95commandeCode.GDdrapeau1Objects2.length = 0;
gdjs.serge_95commandeCode.GDdrapeau2Objects1.length = 0;
gdjs.serge_95commandeCode.GDdrapeau2Objects2.length = 0;
gdjs.serge_95commandeCode.GDpointObjects1.length = 0;
gdjs.serge_95commandeCode.GDpointObjects2.length = 0;
gdjs.serge_95commandeCode.GDfond_9595chambreObjects1.length = 0;
gdjs.serge_95commandeCode.GDfond_9595chambreObjects2.length = 0;

gdjs.serge_95commandeCode.eventsList0(runtimeScene);

return;

}

gdjs['serge_95commandeCode'] = gdjs.serge_95commandeCode;
