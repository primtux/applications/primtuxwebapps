gdjs.infosCode = {};
gdjs.infosCode.GDcube_95Objects1= [];
gdjs.infosCode.GDcube_95Objects2= [];
gdjs.infosCode.GDcube_952Objects1= [];
gdjs.infosCode.GDcube_952Objects2= [];
gdjs.infosCode.GDcube_953Objects1= [];
gdjs.infosCode.GDcube_953Objects2= [];
gdjs.infosCode.GDcube_954Objects1= [];
gdjs.infosCode.GDcube_954Objects2= [];
gdjs.infosCode.GDcube_955Objects1= [];
gdjs.infosCode.GDcube_955Objects2= [];
gdjs.infosCode.GDcube_956Objects1= [];
gdjs.infosCode.GDcube_956Objects2= [];
gdjs.infosCode.GDcube_957Objects1= [];
gdjs.infosCode.GDcube_957Objects2= [];
gdjs.infosCode.GDcube_958Objects1= [];
gdjs.infosCode.GDcube_958Objects2= [];
gdjs.infosCode.GDcube_959Objects1= [];
gdjs.infosCode.GDcube_959Objects2= [];
gdjs.infosCode.GDcube_9510Objects1= [];
gdjs.infosCode.GDcube_9510Objects2= [];
gdjs.infosCode.GDbase_953Objects1= [];
gdjs.infosCode.GDbase_953Objects2= [];
gdjs.infosCode.GDbase_954Objects1= [];
gdjs.infosCode.GDbase_954Objects2= [];
gdjs.infosCode.GDbase_955Objects1= [];
gdjs.infosCode.GDbase_955Objects2= [];
gdjs.infosCode.GDbase_956Objects1= [];
gdjs.infosCode.GDbase_956Objects2= [];
gdjs.infosCode.GDbase_957Objects1= [];
gdjs.infosCode.GDbase_957Objects2= [];
gdjs.infosCode.GDbase_958Objects1= [];
gdjs.infosCode.GDbase_958Objects2= [];
gdjs.infosCode.GDbase_959Objects1= [];
gdjs.infosCode.GDbase_959Objects2= [];
gdjs.infosCode.GDbase_9510Objects1= [];
gdjs.infosCode.GDbase_9510Objects2= [];
gdjs.infosCode.GDfondObjects1= [];
gdjs.infosCode.GDfondObjects2= [];
gdjs.infosCode.GDbouton_95retourObjects1= [];
gdjs.infosCode.GDbouton_95retourObjects2= [];
gdjs.infosCode.GDfond_95blancObjects1= [];
gdjs.infosCode.GDfond_95blancObjects2= [];
gdjs.infosCode.GDinfosObjects1= [];
gdjs.infosCode.GDinfosObjects2= [];

gdjs.infosCode.conditionTrue_0 = {val:false};
gdjs.infosCode.condition0IsTrue_0 = {val:false};
gdjs.infosCode.condition1IsTrue_0 = {val:false};
gdjs.infosCode.condition2IsTrue_0 = {val:false};


gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects = Hashtable.newFrom({"bouton_retour": gdjs.infosCode.GDbouton_95retourObjects1});
gdjs.infosCode.eventsList0 = function(runtimeScene) {

{


gdjs.infosCode.condition0IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.infosCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("fond"), gdjs.infosCode.GDfondObjects1);
gdjs.copyArray(runtimeScene.getObjects("fond_blanc"), gdjs.infosCode.GDfond_95blancObjects1);
{for(var i = 0, len = gdjs.infosCode.GDfondObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfondObjects1[i].setOpacity(150);
}
}{for(var i = 0, len = gdjs.infosCode.GDfond_95blancObjects1.length ;i < len;++i) {
    gdjs.infosCode.GDfond_95blancObjects1[i].setOpacity(150);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("bouton_retour"), gdjs.infosCode.GDbouton_95retourObjects1);

gdjs.infosCode.condition0IsTrue_0.val = false;
gdjs.infosCode.condition1IsTrue_0.val = false;
{
gdjs.infosCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infosCode.mapOfGDgdjs_46infosCode_46GDbouton_9595retourObjects1Objects, runtimeScene, true, false);
}if ( gdjs.infosCode.condition0IsTrue_0.val ) {
{
gdjs.infosCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.infosCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "menu", false);
}}

}


};

gdjs.infosCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infosCode.GDcube_95Objects1.length = 0;
gdjs.infosCode.GDcube_95Objects2.length = 0;
gdjs.infosCode.GDcube_952Objects1.length = 0;
gdjs.infosCode.GDcube_952Objects2.length = 0;
gdjs.infosCode.GDcube_953Objects1.length = 0;
gdjs.infosCode.GDcube_953Objects2.length = 0;
gdjs.infosCode.GDcube_954Objects1.length = 0;
gdjs.infosCode.GDcube_954Objects2.length = 0;
gdjs.infosCode.GDcube_955Objects1.length = 0;
gdjs.infosCode.GDcube_955Objects2.length = 0;
gdjs.infosCode.GDcube_956Objects1.length = 0;
gdjs.infosCode.GDcube_956Objects2.length = 0;
gdjs.infosCode.GDcube_957Objects1.length = 0;
gdjs.infosCode.GDcube_957Objects2.length = 0;
gdjs.infosCode.GDcube_958Objects1.length = 0;
gdjs.infosCode.GDcube_958Objects2.length = 0;
gdjs.infosCode.GDcube_959Objects1.length = 0;
gdjs.infosCode.GDcube_959Objects2.length = 0;
gdjs.infosCode.GDcube_9510Objects1.length = 0;
gdjs.infosCode.GDcube_9510Objects2.length = 0;
gdjs.infosCode.GDbase_953Objects1.length = 0;
gdjs.infosCode.GDbase_953Objects2.length = 0;
gdjs.infosCode.GDbase_954Objects1.length = 0;
gdjs.infosCode.GDbase_954Objects2.length = 0;
gdjs.infosCode.GDbase_955Objects1.length = 0;
gdjs.infosCode.GDbase_955Objects2.length = 0;
gdjs.infosCode.GDbase_956Objects1.length = 0;
gdjs.infosCode.GDbase_956Objects2.length = 0;
gdjs.infosCode.GDbase_957Objects1.length = 0;
gdjs.infosCode.GDbase_957Objects2.length = 0;
gdjs.infosCode.GDbase_958Objects1.length = 0;
gdjs.infosCode.GDbase_958Objects2.length = 0;
gdjs.infosCode.GDbase_959Objects1.length = 0;
gdjs.infosCode.GDbase_959Objects2.length = 0;
gdjs.infosCode.GDbase_9510Objects1.length = 0;
gdjs.infosCode.GDbase_9510Objects2.length = 0;
gdjs.infosCode.GDfondObjects1.length = 0;
gdjs.infosCode.GDfondObjects2.length = 0;
gdjs.infosCode.GDbouton_95retourObjects1.length = 0;
gdjs.infosCode.GDbouton_95retourObjects2.length = 0;
gdjs.infosCode.GDfond_95blancObjects1.length = 0;
gdjs.infosCode.GDfond_95blancObjects2.length = 0;
gdjs.infosCode.GDinfosObjects1.length = 0;
gdjs.infosCode.GDinfosObjects2.length = 0;

gdjs.infosCode.eventsList0(runtimeScene);
return;

}

gdjs['infosCode'] = gdjs.infosCode;
